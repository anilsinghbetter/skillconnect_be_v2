var express = require('express');
var router = express.Router();
var path = require('path');
var arrIgnorePages = ['/', '/favicon.ico', '/pageNotFound', '/assets/css/bootstrap.min.css.map'];
const fs = require('fs');
let fetch = require('node-fetch');
var request = require('request');
var config = require('../config');
var request = require('request');

router.get('/admin', function (req, res, next) {
  res.render('pages/user/login', { title: "Skillconnect Admin" });
});
router.get('/trainingpartners_sample_csv', function (req, res) {
  res.download(__dirname + '/sample_csv/TrainingPartners_Sample.csv', 'TrainingPartners_Sample.csv');
});
router.get('/industrypartners_sample_csv', function (req, res) {
  res.download(__dirname + '/sample_csv/IndustryPartners_Sample.csv', 'IndustryPartners_Sample.csv');
});
router.get('/sectorjobroles_sample_csv', function (req, res) {
  res.download(__dirname + '/sample_csv/SectorJobrole_Sample.csv', 'SectorJobrole_Sample.csv');
});
router.get('/candidate_sample_csv', function (req, res) {
  res.download(__dirname + '/sample_csv/Candidate_Sample.csv', 'Candidate_Sample.csv');
});
router.get('/future_hiring_request_sample_csv', function (req, res) {
  res.download(__dirname+ '/sample_csv/Future_hiring_request_sample.csv', 'Future_hiring_request_sample.csv');
});

router.get('/stateCity_sample_csv', function (req, res) {
  res.download(__dirname+ '/sample_csv/Sample_State_City.csv', 'Sample_State_City.csv');
});
router.get('/sectorJobrole/:id', function (req, res) {
  res.download(__dirname+ '/SectorJobrole/' + req.params.id+'_sample_sector_jobrole.csv', req.params.id+'_sample_sector_jobrole.csv');
});
router.get('/sectorJobrole_emp/:id', function (req, res) {
  res.download(__dirname+ '/SectorJobroleEMP/' + req.params.id+'_sample_sector_jobrole.csv', req.params.id+'_sample_sector_jobrole.csv');
});
router.get('/error/:id/:metaid', function (req, res) {
  if(fs.existsSync(__dirname+ '/bulk_error_files/error_'+req.params.id+'_'+req.params.metaid+'.csv')){
    // console.log("File Exists");  
    res.download(__dirname+ '/bulk_error_files/error_'+req.params.id+'_'+req.params.metaid+'.csv', 'error_'+req.params.id+'_'+req.params.metaid+'.csv');
  }else{
    // console.log("File Does Not Exists");
    res.redirect(config.baseURLWeb+'Bulk-Upload-Status');
  }
});
router.get('/', function (req, res, next) {
  res.render('pages/user/home', { title: "Skillconnect" });
});
router.get('/futurerequesterror/:id/:metaid', function (req, res) {
  if(fs.existsSync(__dirname+ '/bulk_error_files/futurehiringrequest_error_'+req.params.id+'_'+req.params.metaid+'.csv')){
    // console.log("File Exists");  
    res.download(__dirname+ '/bulk_error_files/futurehiringrequest_error_'+req.params.id+'_'+req.params.metaid+'.csv', 'futurehiringrequest_error_'+req.params.id+'_'+req.params.metaid+'.csv');
  }else{
    //console.log("File Does Not Exists");
    res.redirect(config.baseURLWeb+'Employer-Bulk-Upload-Status');
  }
});

router.get('/downloadErrorFile/:fileName', function (req, res) {
  if(fs.existsSync(__dirname+ '/Error_files_admin/' + req.params.fileName + '.csv')){
    // console.log("File Exists");  
    res.download(__dirname+ '/Error_files_admin/' + req.params.fileName + '.csv');
  }else{
    //console.log("File Does Not Exists");
    res.redirect('/bulkuploadstatus');
  }
});

router.get('/demands', function (req, res, next) {
  //console.log("Demand Page");
  var objModules = getModuleName(req);
//baseURLBackend
  var url = 'http://13.127.61.150:3001/api/v1/futurehiringrequest/get-statewise-futurehiringrequest';
  return new Promise(function (resolve, reject) {
    request.post(url, { json: true, body: {} }, function (err, result, body) {
      if (!err && result.statusCode === 200) {
        resolve(result);
        // console.log("RES", result.body.data);
        res.render('pages/demands/demands', {
          title: "Demand of Hiring request",
          /* canAdd: 1,
          canEdit: 1,
          canDelete: 1,        
         */
          objModules: objModules,
          url: "/demands",
          name: (req.session.userInfo.name !== undefined && req.session.userInfo.name != '') ? req.session.userInfo.name : '',
          email: (req.session.userInfo.email !== undefined && req.session.userInfo.email != '') ? req.session.userInfo.email : '',
          data: result.body.data
        });
        // res.render('pages/demands/demands', { title: "Demand of Hiring request",data: result.body.data });
      }
      else {
        reject(err);
      }
    });
  });
});

router.get('/driverDemandsByZip', function (req, res, next) {
  //console.log("Demand Page");
  var objModules = getModuleName(req);
//baseURLBackend
  var url = 'http://13.127.61.150:3001/api/v1/futurehiringrequest/get-zipwise-driverFHR';
  return new Promise(function (resolve, reject) {
    request.post(url, { json: true, body: {} }, function (err, result, body) {
      if (!err && result.statusCode === 200) {
        resolve(result);
        // console.log("RES", result.body.data);
        res.render('pages/driverDemandsByZip/driverDemandsByZip', {
          title: "Demand of Driver By Zip Code",
          /* canAdd: 1,
          canEdit: 1,
          canDelete: 1,        
         */
          objModules: objModules,
          url: "/driverDemandsByZip",
          name: (req.session.userInfo.name !== undefined && req.session.userInfo.name != '') ? req.session.userInfo.name : '',
          email: (req.session.userInfo.email !== undefined && req.session.userInfo.email != '') ? req.session.userInfo.email : '',
          data: result.body.data
        });
        // res.render('pages/demands/demands', { title: "Demand of Hiring request",data: result.body.data });
      }
      else {
        reject(err);
      }
    });
  });
});

router.get('/candidatesByZip', function (req, res, next) {
  //console.log("Demand Page");
  var objModules = getModuleName(req);
//baseURLBackend
  var url = 'http://13.127.61.150:3001/api/v1/candidate/get-zipwise-candidate';
  return new Promise(function (resolve, reject) {
    request.post(url, { json: true, body: {} }, function (err, result, body) {
      if (!err && result.statusCode === 200) {
        resolve(result);
        // console.log("RES", result.body.data);
        res.render('pages/candidatesByZip/candidatesByZip', {
          title: "Number Of candidates by Zip Code",
          /* canAdd: 1,
          canEdit: 1,
          canDelete: 1,        
         */
          objModules: objModules,
          url: "/candidatesByZip",
          name: (req.session.userInfo.name !== undefined && req.session.userInfo.name != '') ? req.session.userInfo.name : '',
          email: (req.session.userInfo.email !== undefined && req.session.userInfo.email != '') ? req.session.userInfo.email : '',
          data: result.body.data
        });
        // res.render('pages/demands/demands', { title: "Demand of Hiring request",data: result.body.data });
      }
      else {
        reject(err);
      }
    });
  });
});

router.get('/driversByZip', function (req, res, next) {
  //console.log("Demand Page");
  var objModules = getModuleName(req);
//baseURLBackend
  var url = 'http://13.127.61.150:3001/api/v1/candidate/get-zipwise-drivers';
  return new Promise(function (resolve, reject) {
    request.post(url, { json: true, body: {} }, function (err, result, body) {
      if (!err && result.statusCode === 200) {
        resolve(result);
        // console.log("RES", result.body.data);
        res.render('pages/driversByZip/driversByZip', {
          title: "Supply Of Drivers By Zip Code",
          /* canAdd: 1,
          canEdit: 1,
          canDelete: 1,        
         */
          objModules: objModules,
          url: "/demandsByZip",
          name: (req.session.userInfo.name !== undefined && req.session.userInfo.name != '') ? req.session.userInfo.name : '',
          email: (req.session.userInfo.email !== undefined && req.session.userInfo.email != '') ? req.session.userInfo.email : '',
          data: result.body.data
        });
        // res.render('pages/demands/demands', { title: "Demand of Hiring request",data: result.body.data });
      }
      else {
        reject(err);
      }
    });
  });
});

router.get('*', function (req, res, next) {

  if (arrIgnorePages.indexOf(req.url) != -1) {
    res.render('pages/user/home', { title: "Skillconnect" });
  } else {
    if (req.session.userInfo == undefined) {
      res.render('pages/others/pageNotFound1', {
        message: err.message,
        error: err,
        title: '404: Page not found'
      });
    } else {
      //next();
      commonRender(req, res, req.url);
    }
  }
});

module.exports = router;
function commonRender(req, res, moduleName) {
  moduleName = (moduleName.indexOf('/') == 0 ? moduleName.substr(1, moduleName.length) : moduleName);
  var objRight = checkUserRights(req, moduleName);
  var objModules = getModuleName(req);
  if (objRight.length > 0 && objRight[0].canView == 1) {
    //get module name
    res.render('pages/' + moduleName + '/' + moduleName, {
      title: moduleName.charAt(0).toUpperCase() + moduleName.slice(1),
      canAdd: objRight[0].canAdd,
      canEdit: objRight[0].canEdit,
      canDelete: objRight[0].canDelete,
      objModules: objModules,
      url: moduleName,
      name: (req.session.userInfo.name !== undefined && req.session.userInfo.name != '') ? req.session.userInfo.name : '',
      email: (req.session.userInfo.email !== undefined && req.session.userInfo.email != '') ? req.session.userInfo.email : ''
    });
  } else {
    res.render('pages/others/pageNotFound', {
      title: "404: Page not found",
      objModules: objModules
    });
  }
}
function checkUserRights(req, moduleName) {
  var userRights = req.session.userInfo.userRights;
  var objRight = userRights.filter(function (obj) {
    return obj.moduleName.toLowerCase() == moduleName.toLowerCase();
  })
  return objRight;
}
function getModuleName(req) {
  var userRights = req.session.userInfo.userRights;
  var objModules = { moduleName: [], displayName: [] };
  for (var i = 0; i < userRights.length; i++) {
    if (userRights[i].canView == 1 && userRights[i].adminCreated == 1) {
      objModules.moduleName.push(userRights[i].moduleName);
      objModules.displayName.push(userRights[i].displayName);
    }
  }
  return objModules;
}
