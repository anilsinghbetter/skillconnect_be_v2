<!-- Jquery js -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- Popper js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

<!-- Bootstrap js -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

<!-- Raty JS -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/raty/2.8.0/jquery.raty.min.js"></script>

<script>

// Adding Hints
$('#target').raty({
		score: 3,
		hintList: ['a', '', null, 'd', '5'],
		// Changes the path where your icons are located.
		// Set it only if you want the same path for all icons.
		// Don't mind about the last slash of the path, if you don't put it, it will be setted for you.
		path: '../../assets/images',
		starHalf: 'star-half.png',
		starOff:  'star-off.png',
		starOn:   'star-on.png',
		starType: 'img'
	});
</script>