<!-- Jquery js -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- Popper js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>

<!-- Bootstrap js -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

<!-- Raty JS -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/raty/2.8.0/jquery.raty.min.js"></script>

<script>

// Rating with Different Icons
$('#icon_range').raty({
	path: '../../assets/images',
	starType : 'img',
	iconRange: [
		{ range: 1, on: '1.png', off: '0.png' },
		{ range: 2, on: '2.png', off: '0.png' },
		{ range: 3, on: '3.png', off: '0.png' },
		{ range: 4, on: '4.png', off: '0.png' },
		{ range: 5, on: '5.png', off: '0.png' }
	]
});
</script>