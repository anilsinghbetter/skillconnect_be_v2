	// Path Settings 
	$.fn.raty.defaults.starType = 'i';
	$.fn.raty.defaults.starHalf = 'material-icons starHalf';
	$.fn.raty.defaults.starOff  =  'material-icons starOff';
	$.fn.raty.defaults.starOn  =  'material-icons starOn';
	$.fn.raty.defaults.cancelOff  =  'material-icons cancelOff';
	$.fn.raty.defaults.cancelOn  =  'material-icons cancelOn';
	
	// Default
	$('.rating').raty(
	{
		path: '../../assets/images',
		starHalf: 'star-half.svg',
		starOff:  'star-off.svg',
		starOn:   'star-on.svg',
		starType:   'img'
	});
	
	// Material Icon
	$('.pmd-material-rating').raty();

	$('#fixed').raty({
		readOnly: true,
		score: 2.5,
	});

	// Inline rating value
	$('#inline').raty({
		score: function() {
			return $(this).attr('data-score');
		}
	});

	// With a custom score name and a number of stars:
	$('#custom').raty({
		scoreName: 'entity.score',
		number: 10,
		score: 8,
	});

	// With a custom hint and icons:
	$('#target').raty({
		score: 3,
		hintList: ['a', '', null, 'd', '5'],
		// Changes the path where your icons are located.
		// Set it only if you want the same path for all icons.
		// Don't mind about the last slash of the path, if you don't put it, it will be setted for you.
		path: '../../assets/images',
		starHalf: 'star-half.png',
		starOff:  'star-off.png',
		starOn:   'star-on.png',
		starType: 'img'
	});

	// Click
	$('#click').raty({
		onClick: function(score) {
			alert('score: ' + score);
		}
	});

	// Cancel
	$('#cancel').raty({
		// It will reset the ratings
		cancel: true,
		// Cancel hint text
		cancelHint : 'This is cancel hint!'
	});

	// Cancel Right
	$('#cancel_right').raty({
		// It will reset the ratings
		cancel: true,
		// You can change the place of cancel button, by default it is left
		cancelPlace : 'right'
	});

	// Icon Range
	$('#icon_range').raty({
		path: '../../assets/images/',
		starType : 'img',
		iconRange: [
			{ range: 1, on: '1.png', off: '0.png' },
			{ range: 2, on: '2.png', off: '0.png' },
			{ range: 3, on: '3.png', off: '0.png' },
			{ range: 4, on: '4.png', off: '0.png' },
			{ range: 5, on: '5.png', off: '0.png' }
		]
	});
	// Icon Range1
	$('#icon_range1').raty({
		path: '../../assets/images/',
		// You can use an interval of the same icon jumping some number.
		// The range attribute must be in an ascending order.
		// If the value on or off is omitted then the attribute starOn and starOff will be used.
		starOff   : '0.png',
		starType : 'img',
		iconRange : [
			{ range : 1, on: '1.png' },
			{ range : 3, on: '3.png' },
			{ range : 5, on: '5.png' }
		]
		// Now we have all off icons as 0.png, icons 1 and 2 as 1.png, icon 3 as 3.png and icons 4 and 5 as 5.png.
	});

	// Space
	$('#space').raty({
		score: 1,
		space: false
	});

	// Single
	$('#single').raty({
		single: true
	});

	// Select Half Star
	$('#half').raty({
		half: true
	});