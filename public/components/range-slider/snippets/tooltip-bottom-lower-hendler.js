<!-- Jquery js -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- Slider js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/wnumb/1.1.0/wNumb.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/8.5.1/nouislider.min.js"></script>
<script>
	// multiple range slider with lower tooltip at bottom
	var pmdSliderRangeTooltip = document.getElementById('pmd-slider-range-tooltip-bottom-lower');
	noUiSlider.create(pmdSliderRangeTooltip, {
		start: [10, 50],
		connect: true,
		tooltips: [ wNumb({ decimals: 0 }), wNumb({ decimals: 0 }) ],
		range: {
			'min': 0,
			'max': 100
		}
	});
</script>
