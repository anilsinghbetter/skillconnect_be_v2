<!-- Jquery js -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- Propeller JS --> 
<script type="text/javascript" src="../../../dist/js/propeller.min.js"></script>

<!-- Slider js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/wnumb/1.1.0/wNumb.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/8.5.1/nouislider.min.js"></script>
<script>
	// single hendelr input value
	var pmdSliderValueInput = document.getElementById('pmd-slider-value-input');
	
	noUiSlider.create(pmdSliderValueInput, {
		start: [ 18 ], // Handle start position
		connect: 'lower', // Display a colored bar between the handles
		tooltips: [ wNumb({ decimals: 0 }) ],
		format: wNumb({
			decimals: 0,
		}),
		range: { // Slider can select '0' to '100'
			'min': 0,
			'max': 100
		}
	});

	var valueInput = document.getElementById('value-input');
	
	// When the slider value changes, update the input and span
	pmdSliderValueInput.noUiSlider.on('update', function( values, handle ) {
			valueInput.value  = values[handle];
	});
	
	// When the input changes, set the slider value
	valueInput.addEventListener('change', function(){
		pmdSliderValueInput.noUiSlider.set([this.value]);
	});
	
</script>