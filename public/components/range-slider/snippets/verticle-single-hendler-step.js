<!-- Jquery js -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- Slider js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/wnumb/1.1.0/wNumb.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/8.5.1/nouislider.min.js"></script>
<script>
	
	// single range slider with step
	var pmdVerticleSliderStep = document.getElementById('pmd-ver-slider-step');
	noUiSlider.create(pmdVerticleSliderStep, {
		start: [ 30 ],
		connect: 'lower',
		direction: 'rtl', // Put '0' at the bottom of the slider
		tooltips: [wNumb({ decimals: 0 }) ],
		orientation: 'vertical', // Orient the slider vertically
		range: {
			'min': [  0 ],
			'max': [ 100 ]
		},
		step: 10,
		pips: { // Show a scale with the slider
			mode: 'steps',
			density: 10
		}

	});
	
</script>