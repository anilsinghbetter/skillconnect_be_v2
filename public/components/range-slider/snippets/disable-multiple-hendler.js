<!-- Jquery js -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- Propeller JS --> 
<script type="text/javascript" src="../../../dist/js/propeller.min.js"></script>

<!-- Slider js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/wnumb/1.1.0/wNumb.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/8.5.1/nouislider.min.js"></script>

<script>
	// enable $ desable range slider
	var disableMultiple = document.getElementById('disable-multiple'),
		multipleCheckboxLeft = document.getElementById('multiple-checkbox-left'),
		multipleCheckboxRight = document.getElementById('multiple-checkbox-right'),
		origins = disableMultiple.getElementsByClassName('noUi-origin');
	
	function toggle ( element ){
	
		// If the checkbox is checked, disabled the slider.
		// Otherwise, re-enable it.
		if ( this.checked ) {
			element.setAttribute('disabled', true);
		} else {
			element.removeAttribute('disabled');
		}
	}
	
	noUiSlider.create(disableMultiple, {
		start: [15, 90],
		tooltips: [ wNumb({ decimals: 0 }), wNumb({ decimals: 0 }) ],
		connect: true,
		range: {
			min: 0,
			max: 100
		}
	});
	
	multipleCheckboxLeft.addEventListener('click', function(){
		toggle.call(this, origins[0]);
	});
	
	multipleCheckboxRight.addEventListener('click', function(){
		toggle.call(this, origins[1]);
	}); 

</script>