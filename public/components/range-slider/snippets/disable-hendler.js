<!-- Jquery js -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- Propeller JS --> 
<script type="text/javascript" src="../../../dist/js/propeller.min.js"></script>

<!-- Slider js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/wnumb/1.1.0/wNumb.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/8.5.1/nouislider.min.js"></script>

<script>
	// enable $ desable range slider
	var disableSingle = document.getElementById('disable-single'),
		singleCheckbox = document.getElementById('single-checkbox');
	function toggle ( element ){
		// If the checkbox is checked, disabled the slider.
		// Otherwise, re-enable it.
		if ( this.checked ) {
			element.setAttribute('disabled', true);
		} else {
			element.removeAttribute('disabled');
		}
	}
	noUiSlider.create(disableSingle, {
		start: 47,
		tooltips: [wNumb({ decimals: 0 }) ],
		connect: 'lower',
		range: {
			min: 0,
			max: 100
		}
	});
	singleCheckbox.addEventListener('click', function(){
		toggle.call(this, disableSingle);
	});
</script>