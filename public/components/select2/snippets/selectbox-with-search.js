<!-- Jquery js -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- Propeller JS -->
<script type="text/javascript" src="dist/js/propeller.min.js"></script>

<!-- Select2 js-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.full.js"></script>

<!-- Propeller Select2 -->
<script type="text/javascript">
	$(document).ready(function() {

		<!-- Selectbox with search -->
		$(".select-with-search").select2({
			theme: "bootstrap"
		});
		
	});
</script>
<script type="text/javascript" src="js/pmd-select2.js"></script>