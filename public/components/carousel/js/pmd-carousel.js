/*slider carousel js*/
 $('#slider-carousel').owlCarousel({
	autoplay:true,
	loop:true,
	margin:0,
	items:1,
	nav:true
});

/*basic propeller card js*/
$('#card-carousel').owlCarousel({
	loop:true,
	items:4,
	nav:true,
	dots:true,
	responsive:{
	0:{
		items:1
	},
	600:{
		items:3
	},
	1000:{
		items:4
	}
}
});   

/*responsive js*/
$('#responsive-carousel').owlCarousel({
loop:true,
responsiveClass:true,
responsive:{
	0:{
		items:1,
		nav:false
	},
	600:{
		items:3,
		dots:false,
		nav:true
	},
	1000:{
		items:4,
		nav:true,
		loop:false
	}
}
});   

/*center with loop js*/
$('#center-loop-carousel').owlCarousel({
center:true,
items:2,
loop:true,
dots:false,
responsive:{
	0:{
		items:1
	},
	600:{
		items:4
	},
}
});

/*center without loop js*/
$('#center-nonloop-carousel').owlCarousel({
center:true,
items:2,
loop:false,
dots:false,
responsive:{
	0:{
		items:1
	},
	600:{
		items:4
	},
}
});  

/*merge js*/
$('#merge-carousel').owlCarousel({
	loop:true,
	items:4,
	merge:true,
	dots:false,
	responsive:{
	0:{
		items:1,
	},
	600:{
		mergeFit:true
	},
	1000:{
		mergeFit:false
	}
}
});

/* url hashing navigation js*/
$('#urlhash-carousel').owlCarousel({
	loop:false,
	items:4,
	center:true,
	URLhashListener:true,
	autoplayHoverPause:true,
	startPosition: 'URLHash',
	nav:false,
	dots:false,
	responsive:{
	0:{
		items:1
	},
	600:{
		items:3
	},
	1000:{
		items:4
	}
}
}); 
	
/* Stage padding js*/
$('#padding-carousel').owlCarousel({
	loop:true,
	items:4,
	stagePadding:40,
	nav:false,
	dots:false,
	responsive:{
	0:{
		items:1
	},
	600:{
		items:3
	},
	1000:{
		items:4
	}
}
});

/* autoplay js*/
var owl = $('#autoplay-carousel');
owl.owlCarousel({
items:4,
dots:false,
loop:true,
autoplay:true,
autoplayTimeout:1000,
autoplayHoverPause:true
});
$('.play').on('click',function(){
	owl.trigger('play.owl.autoplay',[1000])
})
$('.stop').on('click',function(){
	owl.trigger('stop.owl.autoplay')
});

/* animate js*/
$('#animate-carousel').owlCarousel({
animateOut: 'slideOutDown',
animateIn: 'flipInX',
items:1,
smartSpeed:450
});

/* auto height js*/
$('#auto-height-carousel').owlCarousel({
items:1,
autoHeight:true,
autoplay:true,
autoplayTimeout:3000,
loop:true
});

$('#video-carousel').owlCarousel({
	items:1,
	merge:true,
	loop:true,
	margin:10,
	video:true,
	lazyLoad:true,
	center:true,
	responsive:{
		480:{
			items:2
		},
		600:{
			items:3
		}
	}
});