<!-- Jquery js -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- Owl Carousel js -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>

<script>
/* auto height js*/
$('#auto-height-carousel').owlCarousel({
items:1,
autoHeight:true,
autoplay:true,
autoplayTimeout:3000,
loop:true,
dots:true,
nav:false,
});
</script>