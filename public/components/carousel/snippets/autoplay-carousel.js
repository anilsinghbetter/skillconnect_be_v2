<!-- Jquery js -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- Owl Carousel js -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>

<script>
/* autoplay js*/
var owl = $('#autoplay-carousel');
owl.owlCarousel({
	items:3,
	dots:false,
	loop:true,
	autoplay:true,
	autoplayTimeout:2000,
	autoplayHoverPause:true,
	responsive:{
		0:{
			items:1
		},
		600:{
			items:3
		},
		1000:{
			items:4
		}
	}
});
$('.play').on('click',function(){
	owl.trigger('play.owl.autoplay',[1000])
})
$('.stop').on('click',function(){
	owl.trigger('stop.owl.autoplay')
});
</script>