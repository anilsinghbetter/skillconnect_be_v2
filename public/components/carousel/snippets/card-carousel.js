<!-- Jquery js -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- Owl Carousel js -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>

<script>
/*basic propeller card js*/
$('#card-carousel').owlCarousel({
	loop:true,
	items:3,
	responsive:{
	0:{
		items:1,
		nav:false,
		dots:true
	},
	600:{
		items:3,
		nav:false,
		dots:true
	},
	1000:{
		items:3,
		nav:true,
		dots:true
	}
}
});   
</script>