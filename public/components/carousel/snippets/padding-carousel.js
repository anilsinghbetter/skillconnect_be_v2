<!-- Jquery js -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- Owl Carousel js -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>

<script>
/* Stage padding js*/
$('#padding-carousel').owlCarousel({
	loop:true,
	dots:false,
	responsive:{
	0:{
		items:1,
		stagePadding:30,
	},
	600:{
		items:2,
		stagePadding:70,
	},
	1000:{
		items:4,
		stagePadding:70,
	}
}
});
</script>