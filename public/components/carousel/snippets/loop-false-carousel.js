<!-- Jquery js -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- Owl Carousel js -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>

<script>
// Carousel with First Item Center and Loop False
$('#center-nonloop-carousel').owlCarousel({
center:true,
items:2,
loop:false,
dots:false,
responsive:{
	0:{
		items:1
	},
	600:{
		items:3
	},
	1000:{
		items:4
	},
}
});  
</script>