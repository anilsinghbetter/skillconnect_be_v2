<!-- Jquery js -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- Owl Carousel js -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>

<script>
/* Video Carousel */
$('#video-carousel').owlCarousel({
	items:2,
	loop:true,
	margin:10,
	video:true,
	lazyLoad:true,
	responsive:{
		0:{
			items:1
		},
		600:{
			items:2
		}
	}
});
</script>