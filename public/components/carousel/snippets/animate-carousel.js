<!-- Jquery js -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- Owl Carousel js -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>

<script>
$('#animate-carousel').owlCarousel({
animateOut: 'slideOutDown',
animateIn: 'flipInX',
items:1,
smartSpeed:450
});
</script>