<!-- Jquery js -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- Owl Carousel js -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>

<script>
/* url hashing navigation js*/
$('#urlhash-carousel').owlCarousel({
	dots:false,
	loop:true,
	items:4,
	center:true,
	URLhashListener:true,
	autoplayHoverPause:true,
	startPosition: 'URLHash',
	responsive:{
	0:{
		items:1
	},
	600:{
		items:3
	},
	1000:{
		items:4
	}
}
}); 
</script>