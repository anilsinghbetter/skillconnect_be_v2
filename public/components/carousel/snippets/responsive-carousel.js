<!-- Jquery js -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- Owl Carousel js -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>

<script>
// Responsive Carousel
$('#responsive-carousel').owlCarousel({
loop:true,
responsiveClass:true,
responsive:{
	0:{
		items:1,
		nav:false
	},
	600:{
		items:3,
		nav:false,
		dots:true
	},
	1000:{
		items:3,
		nav:true,
		dots:true
	}
}
});
</script>