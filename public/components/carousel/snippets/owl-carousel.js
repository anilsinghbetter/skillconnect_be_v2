<!-- Jquery js -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<!-- Owl Carousel js -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>

<script>
// Basic Owl Carousel
 $('#slider-carousel').owlCarousel({
	autoplay:true,
	loop:true,
	margin:0,
	items:1,
	nav:true,
	responsive:{
		0:{
			nav:false,
			dots:true
		},
		600:{
		nav:true,
		dots:true
		},
		1000:{
			nav:true,
			dots:true
		}
	}
});
</script>