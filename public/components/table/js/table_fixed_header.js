// table fixed header 
$.fn.fixedHeader = function (options) {
	var config = { topOffset: 50 };
	if (options) {
		$.extend(config, options);
	}
	return this.each(function () {
		var $this = $(this);
		var $head = $this.find('thead.header');
		var isFixed = 0;
		var headTop = $head.length && $head.offset().top - config.topOffset;
		
		// Table horizontal scroll effect 
		function processScroll() {
			if (!$this.is(':visible')) {
			  return;
			}
			var i;
			var scrollTop = $this.scrollTop();
			var t = $head.length && $head.offset().top - config.topOffset;
			
			if (!isFixed && headTop !== t) {
			  headTop = t;
			}
			if (scrollTop >= headTop && !isFixed) {
			  isFixed = 1;
			} else if (scrollTop <= headTop && isFixed) {
			  isFixed = 0;
			}
			
			isFixed || !isFixed ? $('thead.header-copy', $this).offset({
			  left: $head.offset().left
			}).removeClass('') : $('thead.header-copy', $this).addClass('');
		}

		// Window on scroll event for horizontal scroll
		$this.on('scroll',function(){
			processScroll();
		});
		
		// hack sad times - holdover until rewrite for 2.1
		$head.on('click', function () {
			if (!isFixed) {
				setTimeout(function () {
					$this.scrollTop($this.scrollTop() - 47);
				}, 10);
			}
		});
			
		// append a duplicate header 
		if(!$this.find('.header-copy.header-fixed').length){
			$head.clone().removeClass('header').addClass('header-copy header-fixed').appendTo($this.children("table"));
		}
			  
		// get header width and th width
		$this.find('thead.header > tr > th').each(function(i){
			var headWidth = $(this).innerWidth();
			$this.find('thead.header-copy th:eq(' + i + ')').css("min-width",headWidth+"px");
		});
		 
		// CSS for sticky Header 
		var currentTab = $this.attr("class");
		processScroll();
	});
};
		
$(window).on("load resize",function(){
 	$('.scrollable-table').fixedHeader();
});

$(window).on("scroll",function(){
	var currentScroll = $(this).scrollTop();
	var headerHeight = $(".navbar").outerHeight();
	var tableHeaderTop = $(".table-fixed-head").offset().top;
	var targetArea = tableHeaderTop - headerHeight ;
	var tableHeight = $(".table-fixed-head").height();
	var tableRowHeight = $(".table-fixed-head tr:last-child").outerHeight();
	var ScrollHeight = (tableHeight - tableRowHeight);

	if (currentScroll > targetArea) {
		var newTopPosition = currentScroll - targetArea;
		$(".header-copy").css("top",newTopPosition+"px");
		$(".header-copy").css("box-shadow","0px 5px 32px rgba(0,0,0,.08)");
	} else {
		$(".header-copy").css("top","0");
		$(".header-copy").css("box-shadow","none");
	}

	// Hide fixed header on last 2-3 rows
	// if (currentScroll > ScrollHeight) {
	// 	$(".header-copy").css("opacity","0");
	// } else {
	// 	$(".header-copy").css("opacity","1");
	// }
});

setTimeout(function(){
	$('.scrollable-table').fixedHeader();
}, 2000);