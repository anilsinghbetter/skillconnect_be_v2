// Table with Expand Collapse
$(document).ready(function () {
	$(".child-table-expand").click(function(){
		$(".direct-child-table").slideToggle(300);
		$(this).toggleClass( "child-table-collapse" );
	});
});