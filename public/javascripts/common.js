/**
 * CBT:this is used to display Title in Grid ,
 * when active/deactive user request it used to send table id
 * @type {Object}
 */
objModules = {};

objModules.adminUser = {
    name: "adminUser",
    displayName: "AdminUser",
    tableId: "1"
};
objModules.trainingPartner = {
    name: "trainingPartner",
    displayName: "TrainingPartner",
    tableId: "2"
};
objModules.industryPartner = {
    name: "industryPartner",
    displayName: "IndustryPartner",
    tableId: "3"
};
objModules.role = {
    name: "role",
    displayName: "Role",
    tableId: "4"
};
objModules.module = {
    name: "module",
    displayName: "",
    tableId: "5"
};
objModules.trainingCenter = {
    name: "trainingCenter",
    displayName: "TrainingCenter",
    tableId: "6"
};
objModules.candidate = {
    name: "candidate",
    displayName: "Candidate",
    tableId: "7"
};
//Sector/Job Role Mapping
objModules.sectorJobRole = {
    name: "sectorJobRole",
    displayName: "SectorJobRole",
    tableId: "8"
};
objModules.sector = {
    name: "sector",
    displayName: "Sector",
    tableId: "9"
};
objModules.jobRole = {
    name: "jobRole",
    displayName: "JobRole",
    tableId: "10"
};
//Future Hiring Request table
objModules.hiringrequest = {
    name: "hiringrequest",
    displayName: "Future Hiring request",
    tableId: "11"
};
objModules.contact = {
    name: "contact",
    displayName: "Contact",
    tableId: "12"
};

common = {
    getDefaultImage: "images/noimage.jpg",
    /**
     * CBT:logout function when user click on logout
     * @return {[type]} [description]
     */
    logout: function () {
        var headers = {
            Authorization: $.cookie(Constants.User.authToken)
        };
        var data = {};
        Api.post(Constants.Api.signout, headers, data, function (error, res) {
            if (res != undefined && res.status == true) {
                $('#gridcandidate,#gridHiringRequest,#gridindustryPartner,#gridtrainingPartner').DataTable().state.clear();
                $.removeCookie(Constants.User.authToken);
                window.location.replace('/admin');
            } else if (error.status == false) {
                $("#btnError").attr("data-message", error.error.message);
                $('#btnError').click();
            }
        });
    },
    getSector: function (callBack) {
        var headers = {
            Authorization: $.cookie(Constants.User.authToken)
        };
        Api.post(Constants.Api.getSector + '/web', headers, '', function (error, res) {
            if (res != undefined && res.status == true) {
                callBack({
                    status: true,
                    data: res.data
                });
            } else if (res != undefined && res.status == false) {
                common.showMessage(res.error.message, true);
            } else if (error != undefined && error.status == false) {
                common.showMessage(error.error.message, true);
            }
        });
    },
    getJobrole: function (sectorid, callBack) {
        var headers = {
            Authorization: $.cookie(Constants.User.authToken)
        };
        Api.post(Constants.Api.getSectorJobrole + '/' + sectorid, headers, '', function (error, res) {
            if (res != undefined && res.status == true) {
                callBack({
                    status: true,
                    data: res.data
                });

            } else if (res != undefined && res.status == false) {
                common.showMessage(res.error.message, true);
            } else if (error != undefined && error.status == false) {
                common.showMessage(error.error.message, true);
            }
        });
    },
    getScheme: function (callBack) {
        var headers = {
            Authorization: $.cookie(Constants.User.authToken)
        };
        Api.post(Constants.Api.getSchemeMaster, headers, '', function (error, res) {
            if (res != undefined && res.status == true) {
                callBack({
                    status: true,
                    data: res.data
                });
            } else if (res != undefined && res.status == false) {
                common.showMessage(res.error.message, true);
            } else if (error != undefined && error.status == false) {
                common.showMessage(error.error.message, true);
            }
        });
    },
    getTrainingCenter: function (trainingpartnerid, callBack) {
        var headers = {
            Authorization: $.cookie(Constants.User.authToken)
        };
        Api.post(Constants.Api.getTrainingCenterMaster + '/' + trainingpartnerid, headers, '', function (error, res) {
            if (res != undefined && res.status == true) {
                callBack({
                    status: true,
                    data: res.data
                });
            } else if (res != undefined && res.status == false) {
                common.showMessage(res.error.message, true);
            } else if (error != undefined && error.status == false) {
                common.showMessage(error.error.message, true);
            }
        });
    },
    getMinistry: function (schemeId, callBack) {
        var headers = {
            Authorization: $.cookie(Constants.User.authToken)
        };
        Api.post(Constants.Api.getSchemeCentralMinistry + '/' + schemeId, headers, '', function (error, res) {
            if (res != undefined && res.status == true) {
                callBack({
                    status: true,
                    data: res.data
                });
            } else if (res != undefined && res.status == false) {
                common.showMessage(res.error.message, true);
            } else if (error != undefined && error.status == false) {
                common.showMessage(error.error.message, true);
            }
        });
    },
    getState: function (callBack) {
        var headers = {
            Authorization: $.cookie(Constants.User.authToken)
        };
        Api.post(Constants.Api.getState, headers, '', function (error, res) {
            if (res != undefined && res.status == true) {
                callBack({
                    status: true,
                    data: res.data
                });
            } else if (res != undefined && res.status == false) {
                common.showMessage(res.error.message, true);
            } else if (error != undefined && error.status == false) {
                common.showMessage(error.error.message, true);
            }
        });
    },
    getCity: function (stateid, callBack) {
        var headers = {
            Authorization: $.cookie(Constants.User.authToken)
        };
        Api.post(Constants.Api.getCity + '/' + stateid, headers, '', function (error, res) {
            if (res != undefined && res.status == true) {
                callBack({
                    status: true,
                    data: res.data
                });
            } else if (res != undefined && res.status == false) {
                common.showMessage(res.error.message, true);
            } else if (error != undefined && error.status == false) {
                common.showMessage(error.error.message, true);
            }
        });
    },
    /**
     * CBT:this function is for show/hide loader image
     * @return {[type]} [description]
     */
    blockUI: function () {
        $.blockUI({
            message: '<div class="loader"></div>'
        });
        var navHieght = $(".navbar").height();
        $(".loader").css("top", navHieght);
        $(".loader").fadeIn();
    },
    unblockUI: function () {
        $.unblockUI();
        $(".loader").fadeOut();
    },
    /**
     * CBT:common method for bind data table
     * @param  {[type]} data        [description]
     * @param  {[type]} objColumns  [description]
     * @param  {[type]} datatableID [description]
     * @param  {[type]} moduleName  [description]
     * @return {[type]}             [description]
     */
    bindCommonDatatable: function (data, objColumns, datatableID, moduleName) {
        var columns = [];
        var columnDefs = [];
        var events = [];
        objColumns.forEach(function (column) {
            var objColumnDefs = {};
            if (column.isVisible == undefined || column.isVisible == true) {
                if (column.targets != undefined) {
                    objColumnDefs.targets = column.targets;
                }
                if (column.orderable != undefined) {
                    objColumnDefs.orderable = column.orderable;
                }
                if (column.searchable != undefined) {
                    objColumnDefs.searchable = column.searchable;
                }
                if (column.className != undefined) {
                    objColumnDefs.className = column.className;
                }
                if (column.isActionButton != undefined && column.isActionButton == true) {
                    objColumnDefs.render = function (data, type, full, meta) {
                        var buttons = "";
                        for (var i = 0; i < column.buttons.length; i++) {
                            if (column.buttons[i].dataRowField == "" || column.buttons[i].dataRowField == undefined && column.buttons[i].compareValue == "" || column.buttons[i].compareValue == undefined) {
                                if (column.buttons[i].checkboxObj !== undefined) {
                                    switch (column.buttons[i].dataRowField) {
                                        case 'trainingpartnerid':
                                            buttons += '<div class="custom-control custom-checkbox pmd-checkbox"><input class="custom-control-input cb-element" type="checkbox" name="row[]" value="' + full.trainingpartnerid + '" id="customCheckBox' + full.trainingpartnerid + '"><label class="custom-control-label" for="customCheckBox' + full.trainingpartnerid + '"></label></div>';
                                            break;
                                        case 'industrypartnerid':
                                            buttons += '<div class="custom-control custom-checkbox pmd-checkbox"><input class="custom-control-input cb-element" type="checkbox" name="row[]" value="' + full.industrypartnerid + '" id="customCheckBox' + full.industrypartnerid + '"><label class="custom-control-label" for="customCheckBox' + full.industrypartnerid + '"></label></div>';
                                            break;
                                        case 'jobroleid':
                                            buttons += '<div class="custom-control custom-checkbox pmd-checkbox"><input class="custom-control-input cb-element" type="checkbox" name="row[]" value="' + full.jobroleid + '" id="customCheckBox' + full.jobroleid + '"><label class="custom-control-label" for="customCheckBox' + full.jobroleid + '"></label></div>';
                                            break;
                                        case 'sectorid':
                                            buttons += '<div class="custom-control custom-checkbox pmd-checkbox"><input class="custom-control-input cb-element" type="checkbox" name="row[]" value="' + full.sectorid + '" id="customCheckBox' + full.sectorid + '"><label class="custom-control-label" for="customCheckBox' + full.sectorid + '"></label></div>';
                                            break;
                                        case 'trainingcenterid':
                                            buttons += '<div class="custom-control custom-checkbox pmd-checkbox"><input class="custom-control-input cb-element" type="checkbox" name="row[]" value="' + full.trainingcenterid + '" id="customCheckBox' + full.trainingcenterid + '"><label class="custom-control-label" for="customCheckBox' + full.trainingcenterid + '"></label></div>';
                                            break;
                                        case 'candidateid':
                                            buttons += '<div class="custom-control custom-checkbox pmd-checkbox"><input class="custom-control-input cb-element" type="checkbox" name="row[]" value="' + full.candidateid + '" id="customCheckBox' + full.candidateid + '"><label class="custom-control-label" for="customCheckBox' + full.candidateid + '"></label></div>';
                                            break;
                                        case 'request_id':
                                            buttons += '<div class="custom-control custom-checkbox pmd-checkbox"><input class="custom-control-input cb-element" type="checkbox" name="row[]" value="' + full.request_id + '" id="customCheckBox' + full.request_id + '"><label class="custom-control-label" for="customCheckBox' + full.request_id + '"></label></div>';
                                            break;
                                    }
                                }
                                if (column.buttons[i].buttonObj !== undefined) {
                                    buttons += column.buttons[i].buttonObj.html;
                                }
                                if (column.buttons[i].idName != undefined && column.buttons[i].idName != null && column.buttons[i].idName != "") {
                                    buttons = $(buttons).attr("id", column.buttons[i].idName + full[column.buttons[i].idField]);
                                    buttons = buttons.prop("outerHTML");
                                }
                            } else {
                                if (full[column.buttons[i].dataRowField] == column.buttons[i].compareValue) {
                                    buttons += column.buttons[i].buttonObj.html;
                                }
                            }
                        }
                        return buttons;
                    }
                }
                columnDefs.push(objColumnDefs);
            }
            var objColumn = {};
            objColumn.data = column.name;
            columns.push(objColumn);
        });
        //To Hide the Reply Btn once email is sent for reply
        if(datatableID === 'gridContact'){
            columnDefs = [
                {
                    targets: [4],
                    render: function (data, type, row) {
                        return row.is_mail_sent === 1 ? '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary btnView" type="button" title="Detail"><i class="material-icons pmd-sm">remove_red_eye</i></button>' : '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary btnView" type="button" title="Detail"><i class="material-icons pmd-sm">remove_red_eye</i></button> <button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary btnReply" title="Reply" type="button"><i class="material-icons pmd-sm">reply</i></button>'
                    }
                }
            ]
        }
        if(datatableID === 'gridbulkUploadError'){
            columnDefs = [
                {
                    targets: [3],
                    render: function (data, type, row) {
                        // console.log(data, "DATA" , type, "TYPE" , row,"ROW")
                        return '<a href=downloadErrorFile/'+row.error_file_name +' class="text-primary"  title="Click to Download File">'+ row.error_file_name +' </a>' ;
                    }
                }
            ]
        }
        //To Hide the Reply Btn once email is sent for reply
        var tableObj = $('#' + datatableID).DataTable({
            data: data,
            columns: columns,
            //stateSave: true,
            //"processing": true,
            responsive: false,
            /* responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            }, */
            /* "sScrollY": "50vh",
            "sScrollX": "100%",
            "sScrollXInner": "110%", */
            "bScrollCollapse": true,
            /* scrollY:        '50vh',
            scrollCollapse: true, */
            /* serverSide: true,
            "ajax":{
                url : apiURL, // json datasource
                headers: headers,
                type: methodType,  // method  , by default get
            }, */
            columnDefs: columnDefs,
            'select': {
                'style': 'multi',
                selector: 'td:first-child'
            },
            //order: [1, 'desc'],
            bFilter: true,
            bLengthChange: false,
            pagingType: "simple",
            "paging": true,
            "searching": true,
            "language": {
                "info": " _START_ - _END_ of _TOTAL_ ",
                "sLengthMenu": "<span class='custom-select-title'>Rows per page:</span> <span class='pmd-custom-select'> _MENU_ </span>",
                "sSearch": "",
                "sSearchPlaceholder": "Search",
                "paginate": {
                    "sNext": " ",
                    "sPrevious": " "
                },
            },
            dom:
                "<'card-header d-flex flex-row'<'data-table-title-responsive'><'search-paper pmd-textfield ml-auto'f>>" +
                "<'row'<'col-sm-12 datatableCustom'tr>>" +
                "<'card-footer' <'pmd-datatable-pagination' l i p>>",
        });

        var btnClear = $('<a class="btnClearDataTableFilter cursor-pointer hidden" style="position: absolute;right: 20px;z-index: 9999;top: 25px;"><i class="material-icons md-dark pmd-xs">cancel</i></a>');
        btnClear.appendTo($('#' + datatableID).parents('.dataTables_wrapper').find('.dataTables_filter'));
        $('#' + datatableID + '_wrapper .btnClearDataTableFilter').click(function () {
            $('#' + datatableID).dataTable().fnFilter('');
            $('.btnClearDataTableFilter').addClass('hidden');
        });


        $("div.dataTables_filter input").keyup(function (e) {
            if ($(this).val() != '') {
                $('.btnClearDataTableFilter').removeClass('hidden');
            } else {
                $('.btnClearDataTableFilter').addClass('hidden');
            }
        });

        $('.custom-select-info').hide();

        //$('#' + datatableID + '_wrapper').find("div.data-table-title-responsive").html();

        $('#' + datatableID + ' tbody').unbind("click");
        $(".custom-select-action").html('<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary" type="button"><i class="material-icons pmd-sm">delete</i></button><button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary" type="button"><i class="material-icons pmd-sm">more_vert</i></button>');

        $(".cb-element").change(function () {
            if ($(".cb-element").length == $(".cb-element:checked").length)
                $("#customCheckBox0").prop('checked', true);
            else
                $("#customCheckBox0").prop('checked', false);
        });

        $('#' + datatableID).on('page.dt', function () {
            //var info = tableObj.page.info();
            $("#customCheckBox0").prop('checked', false);
        });

        /* if (!tableObj.data().any()) {
            $('#' + datatableID + "_filter").hide();
            $('#' + datatableID + "_info").hide();
            $('#' + datatableID + "_paginate").hide();
            $('.customSelectAll').hide();
            $('#selectAction').hide();
        } else {
            $('#' + datatableID + "_filter").show();
            $('#' + datatableID + "_info").show();
            $('#' + datatableID + "_paginate").show();
            $('.customSelectAll').show();
            $('#selectAction').show();
        } */

        objColumns.forEach(function (column) {
            if (column.isActionButton != undefined && column.isActionButton == true) {
                for (var i = 0; i < column.buttons.length; i++) {
                    if (column.buttons[i].onClickEvent != "" && column.buttons[i].onClickEvent != undefined) {
                        clickHandler(column.buttons[i])
                    }
                }
            }
        })

        function clickHandler(btnObj) {
            $('#' + datatableID + ' tbody').on('click', btnObj.buttonObj.targetClass, function () {
                btnObj.onClickEvent(tableObj.row($(this).parents('tr')));
            });
        }
        return tableObj;
    },
    getUDID : function(){
        return window.navigator.userAgent.replace(/\D+/g, '');
    },
    getDeviceType : function() {
        var Sys = {};
        var ua = navigator.userAgent.toLowerCase();
        var s;
        (s = ua.match(/msie ([\d.]+)/)) ? Sys.ie = s[1] :
          (s = ua.match(/firefox\/([\d.]+)/)) ? Sys.firefox = s[1] :
            (s = ua.match(/chrome\/([\d.]+)/)) ? Sys.chrome = s[1] :
              (s = ua.match(/opera.([\d.]+)/)) ? Sys.opera = s[1] :
                (s = ua.match(/version\/([\d.]+).*safari/)) ? Sys.safari = s[1] : 0;
    
    
        if (Sys.ie) return ('IE: ' + Sys.ie);
        if (Sys.firefox) return ('Firefox: ' + Sys.firefox);
        if (Sys.chrome) return ('Chrome: ' + Sys.chrome);
        if (Sys.opera) return ('Opera: ' + Sys.opera);
        if (Sys.safari) return ('Safari: ' + Sys.safari);
        return "web";
    },
    bindTrainingDatatable: function (headers, gettrainingPartnerUrl, datatableID) {

       // console.log('#NEW GRID Headers', headers);
       // console.log('#NEW GRID API url', gettrainingPartnerUrl);
        //console.log('#NEW GRID datatabelID', datatableID);


        var gridtrainingPartnerObj = $('#' + datatableID).DataTable({
            serverSide: true,
            processing: true,
            bFilter: true,
            bLengthChange: true,
            stateSave: true,
            order: [0, 'desc'],
            // "searching": true,
            "bSearchable": true,

            "ajax": {
                "url": gettrainingPartnerUrl,
                "type": "POST",
                "headers": headers,
                'beforeSend': function (request) {
                    // debugger
                },
                "data": function (d) {
                    d.status = $('#customCheckBox0').is(":checked");
                    // console.log("RESPONSE VAL",d.search.value);
                    // console.log("RESPONSE VAL",d);
                    d.contact_mobile = d.search.value;
                    d.contact_email_address = d.search.value;
                    d.training_partner_name = d.search.value;
                    d.nsdc_registration_number = d.search.value;
                    d.tpid = d.search.value;
                    d.contact_person_name = d.search.value;

                }
            },
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            "columns": [{
                name: "pk_trainingpartnerid",
                "data": "pk_trainingpartnerid",
                "width": "5%"
            }, {
                name: "nsdc_registration_number",
                "data": "nsdc_registration_number",
                "width": "20%"
            }, {
                name: "tpid",
                "data": "tpid",
                "width": "10%"
            },
            {
                name: "training_partner_name",
                "data": "training_partner_name",
                "width": "15%"
            },
            {
                name: "contact_email_address",
                "data": "contact_email_address",
                "width": "10%"
            },
            {
                name: "status",
                "data": "status",
                "width": "10%"
            },
            {
                "data": "Actions",
                "width": "10%"
            }

            ],
            columnDefs: [{
                orderable: false,
                //  className: 'select-checkbox',
                targets: 0,
                'render': function (data, type, full, meta) {
                    // debugger;
                    return buttons = '<div class="custom-control custom-checkbox pmd-checkbox"><input class="custom-control-input cb-element" type="checkbox" name="row[]" value="' + full.pk_trainingpartnerid + '" id="customCheckBox' + full.pk_trainingpartnerid + '"><label class="custom-control-label" for="customCheckBox' + full.pk_trainingpartnerid + '"></label></div>';

                }
            }, {
                orderable: true,
                targets: 1
            }, {
                orderable: true,
                targets: 2
            },
            {
                orderable: true,
                targets: 3
            },
            {
                orderable: true,
                targets: 4
            },
            {
                orderable: false,
                targets: 5,
                'render': function (data, type, full, meta) {
                    if (data == 1) var buttonHTML = '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary btn-success btnApprove" title="Status" type="button" onclick="trainingPartner.onActiveClick(' + meta.row + ');" value="' + data + '"><i class="material-icons pmd-sm">check</i></button>';
                    if (data == 0) var buttonHTML = '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary btn-danger btnReject" title="Status" type="button" onclick="trainingPartner.onActiveClick(' + meta.row + ');" value="' + data + '"><i class="material-icons pmd-sm">clear</i></button>';
                    return buttonHTML;
                }
            },
            {
                orderable: false,
                targets: 6,
                'render': function (data, type, full, meta) {
                    var buttonHTML = '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary btnEdit" onclick="trainingPartner.edit(' + meta.row + ');" type="button" title="Edit"><i class="material-icons pmd-sm">edit</i></button> &nbsp;';
                    buttonHTML += '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary btnView" type="button" onclick="trainingPartner.view(' + meta.row + ');" value="' + data + '" title="Detail"><i class="material-icons pmd-sm">remove_red_eye</i></button>';
                    buttonHTML += '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-danger btnDelete"  type="button" onclick="trainingPartner.delete(' + meta.row + ');" value="' + data + '" title="Delete"><i class="material-icons pmd-sm">delete</i></button>';
                    return buttonHTML;
                }
            }
            ]
            ,
            //For checkall..
            select: {
                style: 'multi',
                selector: 'td:first-child'
            },
            //order: [1, 'asc'],
            "language": {
                "info": " _START_ - _END_ of _TOTAL_ ",
                "sLengthMenu": "<span class='custom-select-title hidden'>Rows per page:</span> <span class='pmd-custom-select hidden'> _MENU_ </span>",
                processing: "<span class='loading'></span>",
                "sSearch": "",
                "sSearchPlaceholder": "Search",
                "paginate": {
                    "sNext": " ",
                    "sPrevious": " "
                },
            },
            dom:
                "<'card-header d-flex flex-row'<'data-table-title-responsive'><'search-paper pmd-textfield ml-auto'f>>" +
                "<'row'<'col-sm-12 datatableCustom'tr>>" +
                "<'card-footer' <'pmd-datatable-pagination' l i p>>",
        });

        // Apply the filter
        $('.custom-select-info').hide();

        //$('#' + datatableID + '_wrapper').find("div.data-table-title-responsive").html();

        $('#' + datatableID + ' tbody').unbind("click");
        $(".custom-select-action").html('<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary" type="button"><i class="material-icons pmd-sm">delete</i></button><button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary" type="button"><i class="material-icons pmd-sm">more_vert</i></button>');

        $(".cb-element").change(function () {
            if ($(".cb-element").length == $(".cb-element:checked").length)
                $("#customCheckBox0").prop('checked', true);
            else
                $("#customCheckBox0").prop('checked', false);
        });

        $('#' + datatableID).on('page.dt', function () {
            //var info = tableObj.page.info();
            $("#customCheckBox0").prop('checked', false);
        });

        var btnClear = $('<a class="btnClearDataTableFilter cursor-pointer hidden" style="position: absolute;right: 20px;z-index: 9999;top: 25px;"><i class="material-icons md-dark pmd-xs">cancel</i></a>');
        btnClear.appendTo($('#' + datatableID).parents('.dataTables_wrapper').find('.dataTables_filter'));

        $('#' + datatableID + '_wrapper .btnClearDataTableFilter').click(function () {
            $('#' + datatableID).dataTable().fnFilter('');
            $('.btnClearDataTableFilter').addClass('hidden');
        });

        if (gridtrainingPartnerObj.data().any()) {
            $('#' + datatableID + "_filter").hide();
            $('#' + datatableID + "_info").hide();
            $('#' + datatableID + "_paginate").hide();
            $('.customSelectAll').hide();
            $('#selectAction').hide();
        } else {
            $('#' + datatableID + "_filter").show();
            $('#' + datatableID + "_info").show();
            $('#' + datatableID + "_paginate").show();
            $('.customSelectAll').show();
            $('#selectAction').show();
        }
        function clickHandler(btnObj) {
            $('#' + datatableID + ' tbody').on('click', btnObj.buttonObj.targetClass, function () {
                btnObj.onClickEvent(tableObj.row($(this).parents('tr')));
            });
        }
        $("div.dataTables_filter input").keyup(function (e) {
            if ($(this).val() != '') {
                $('.btnClearDataTableFilter').removeClass('hidden');
            } else {
                $('.btnClearDataTableFilter').addClass('hidden');
            }
        });

        return gridtrainingPartnerObj;
    },
    /**
     * CBT:this function clear values of form given as argument
     * @param  {[type]} formObject [description]
     * @return {[type]}            [description]
     */
    clearValues: function (formObject) {
        var values = {};
        formObject.find("input,select,textarea").each(function () {
            $(this).val("");
        });
        formObject.find("input[type=checkbox],input[type=radio]").each(function (e) {
            this.checked = false;
        });
        formObject.find("span").each(function () {
            if ($(this).attr("id") != undefined && $(this).attr("id") != null && $(this).attr("id") != "") {
                var elementId = $(this).attr("id").split("_")[1];
                if (elementId.toLowerCase() == "validation") {
                    $(this).hide();
                }
            }

        });
    },
    /**
     * CBT:this function fill values in form's components using values given as argument
     * @param  {[type]} formObject [description]
     * @param  {[type]} objData    [description]
     * @return {[type]}            [description]
     */
    fillFormValues: function (formObject, objData) {
        var values = {};
        formObject.find("input,select,textarea").each(function () {
            var inputObject = $(this);
            var fieldName = inputObject.attr("name");
            if (inputObject.attr("type") == "checkbox" || inputObject.attr("type") == "radio") {
                $("#" + objData[fieldName]).prop("checked", true);
                $("#male").val('male');
                $("#female").val('female');
                $("#other").val('other');
                $("#yes").val('yes');
                $("#no").val('no');
            } else if (inputObject.attr("type") == "hidden") {
                $("input[name='" + fieldName + "']").val(objData[fieldName]);
            }
            else if (inputObject.attr("type") == "text") {
                $("input[name='" + fieldName + "']").val(objData[fieldName]);
                $("input[name='" + fieldName + "']").closest('div').addClass('pmd-textfield-floating-label-completed');
            } else if (inputObject.is("textarea")) {
                $("textarea[name$='" + fieldName + "']").val(objData[fieldName]);
            } else if (inputObject.is("select")) {
                //console.log(fieldName, objData[fieldName]);
                if ($("select[name='" + fieldName + "']").hasClass("select-tags")) {
                    if (objData[fieldName] != null) {
                        if (typeof objData[fieldName] == "number") {
                            $("select[name='" + fieldName + "']").val(objData[fieldName]).trigger('change');
                        } else {
                            $("select[name='" + fieldName + "']").val(objData[fieldName].split(',')).trigger('change');
                        }
                    } else {
                        $("select[name='" + fieldName + "']").val('').trigger('change');
                    }
                } else {
                    $("select[name='" + fieldName + "']").val(objData[fieldName]);
                    // console.log('the fld name',fieldName,'objdtaa fld name',objData[fieldName]);
                    //console.log($("select[name='" + fieldName + "']").val('3'));
                }
            }
        });
        return;
    },
    /**
     * CBT:get values of form given as argument in JSON format
     * @param  {[type]} formObject [description]
     * @return {[type]}            [description]
     */
    getFormValues: function (formObject) {
        var values = {};
        formObject.find("input,select,textarea").each(function () {
            var inputObject = $(this);
            var fieldName = inputObject.attr("name");
            var fieldValue = $.trim(inputObject.val());
            if (inputObject.attr("type") == "checkbox") {
                // auto set data type for checkbox
                if (!inputObject.attr("data-type")) {
                    // single checkbox with that name means dataType="BOOL" else it is "ARRAY"
                    if (formObject.find("input[name='" + fieldName + "']").length == 1) {
                        dataType = "BOOL";
                    } else {
                        dataType = "ARRAY";
                    }
                }
                if (dataType == "BOOL") fieldValue = inputObject.is(":checked");
                if (dataType == "ARRAY") fieldValue = inputObject.is(":checked") ? fieldValue : "";
                if (dataType == "ARRAY") {
                    if ($.isArray(values[fieldName]) && values[fieldName].length != 0) {
                        if (fieldValue != "") {
                            values[fieldName].push(fieldValue)
                        }
                    } else {
                        values[fieldName] = [];
                        if (fieldValue != "") {
                            values[fieldName].push(fieldValue)
                        }
                    }
                } else {
                    values[fieldName] = fieldValue;
                }
            } else if (inputObject.attr("type") == "radio") {
                if (inputObject.is(":checked")) {
                    values[fieldName] = $.trim(fieldValue);
                }
            } else {
                values[fieldName] = $.trim(fieldValue);
            }
        });

        return values;
    },
    /**
     * CBT:this function is used to show hide div based on argument
     * @param  {[type]} isshowGrid [description]
     * @param  {[type]} moduleName [description]
     * @return {[type]}            [description]
     */
    showHideDiv: function (isshowGrid, moduleName) {
        if (isshowGrid == true) {
            $('#divAdd' + moduleName.displayName).hide();
            $('#import' + moduleName.displayName).show();
            $('#add' + moduleName.displayName).show();
            $('#div' + moduleName.displayName).show();
            $('#divImport' + moduleName.displayName).hide();
        } else {
            $('#divAdd' + moduleName.displayName).show();
            $('#import' + moduleName.displayName).hide();
            $('#add' + moduleName.displayName).hide();
            $('#div' + moduleName.displayName).hide();
        }
    },
    /**
     * CBT:common function to remove data based on id
     * @param  {[type]} moduleName [description]
     * @param  {[type]} APIURL     [description]
     * @param  {[type]} dataID     [description]
     * @param  {[type]} callBack   [description]
     * @return {[type]}            [description]
     */
    deleteData: function (moduleName, APIURL, dataID, callBack) {
        if (confirm("Are you sure you want to delete " + moduleName.displayName + "?")) {

            if (dataID > 0) {
                var deleteURL = APIURL + dataID;
                var headers = {
                    Authorization: $.cookie(Constants.User.authToken)
                };

                Api.post(deleteURL, headers, "", function (error, res) {
                    if (res != undefined && res.status == true) {
                        if (callBack) {
                            callBack({
                                status: true,
                                message: res.data.message
                            });
                        }
                    } else if (error.status == false) {
                        if (callBack) {
                            callBack({
                                status: false,
                                errorMsg: error.error.message
                            });
                        }
                    }
                });
            } else {
                if (callBack) {
                    callBack({
                        status: false,
                        errorMsg: "Invalid" + moduleName.displayName + " ID"
                    });
                }
            }
        }
    },
    /**
     * CBT:common method to show error and success message
     * @param  {[type]}  message [description]
     * @param  {Boolean} isError [description]
     * @return {[type]}          [description]
     */
    showMessage: function (message, isError) {
        if (isError == true) {
            $("#btnError").attr("data-message", message);
            $('#btnError').click();
        } else {
            $("#btnSuccess").attr("data-message", message);
            $("#btnSuccess").click();
        }
    },
    /**
     * CBT:common method to fill dropdown values
     * @param  {[type]} data      [description]
     * @param  {[type]} element   [description]
     * @param  {[type]} keyName   [description]
     * @param  {[type]} valueName [description]
     * @return {[type]}           [description]
     */
    fillDropdown: function (data, element, keyName, valueName) {
        for (var i = 0; i < data.length; i++) {
            $("#" + element).append('<option value="' + data[i][keyName] + '">' + data[i][valueName] + '</option>');
        }
    },
    /**
     * CBT:this function is used to show loading message
     * @param  {[type]} status  [description]
     * @param  {[type]} message [description]
     * @return {[type]}         [description]
     */
    showLoader: function (status, message) {
        if (message == "") {
            message = "Please Wait...";
        }
        var loader = "<div id='loaderOverlay' style='position: fixed;top: 0;width: 100%;height: 100%;background: rgba(255, 255, 255, 0.78);z-index: 1000;'>" +
            "<div style='left: 40%;top:40%;position:fixed;width: 250px;text-align: center;'>" +
            "<img src='themes/images/newloader.gif' style='width:70px;'>" +
            "<p>" + message + "</p>" +
            "</div>" +
            "</div>";
        if ($("#loaderOverlay").length == 0) {
            $("body").append(loader);
        }
        if (status) {
            $("#loaderOverlay").show();
        } else {
            $("#loaderOverlay").hide();
        }
    },
    /**
     * CBT:common method to make status active/deactive
     * @param  {[type]}   id        [description]
     * @param  {[type]}   tableCode [description]
     * @param  {[type]}   status    [description]
     * @param  {Function} cb        [description]
     * @return {[type]}             [description]
     */
    updateActiveStatus: function (id, tableCode, status, cb) {
        var headers = {
            Authorization: $.cookie(Constants.User.authToken)
        };
        var data = {
            "id": id,
            "tbl_code": tableCode,
            "is_active": status
        }
        Api.post(Constants.Api.updateActiveStatus, headers, data, function (error, res) {
            if (res != undefined && res.status == true) {
                common.showMessage(res.data.message, false);
                cb(res);
            } else if (res != undefined && res.status == false) {
                common.showMessage(res.error.message, true);
                cb(res);
            } else if (error != undefined && error.status == false) {
                common.showMessage(error.error.message, true);
                cb(res);
            }
        });
    },
    deleteRecordsApi: function (id, tableCode, delete_type, cb) {
        if (confirm("Are you sure you want to delete this record ?")) {
            if (id > 0) {
                var headers = {
                    Authorization: $.cookie(Constants.User.authToken)
                };
                var data = {
                    "id": id,
                    "tbl_code": tableCode,
                    "delete_type": delete_type
                }
                Api.post(Constants.Api.deleteRecordCommon + '/admin', headers, data, function (error, res) {
                    if (res != undefined && res.status == true) {
                        common.showMessage(res.data.message, false);
                        cb(res);
                    } else if (res != undefined && res.status == false) {
                        common.showMessage(res.error.message, true);
                        cb(res);
                    } else if (error != undefined && error.status == false) {
                        common.showMessage(error.error.message, true);
                        cb(res);
                    }
                });
            } else {
                if (cb) {
                    cb({
                        status: false,
                        errorMsg: "Invalid" + moduleName.displayName + " ID"
                    });
                }
            }
        }
    },
    validateFileSize: function (tableName,filesize) {
        var FileSize = filesize / 1024 / 1024; // in MB
        var maxFileSizeLimit = 2;
        if (tableName == "IndustryPartners" || tableName == "TrainingPartners") {
            maxFileSizeLimit = 30;
        }
        if (FileSize > maxFileSizeLimit) {
            return false;
        } else {
            return true;
        }
    },
    uploadMedia: function (mediaFiles, fileType, tableName, callBack) {
        if (mediaFiles != undefined && mediaFiles.constructor === File) {
            var fileName = mediaFiles.name;
            var ext = fileName.substr(fileName.lastIndexOf(".") + 1, fileName.length);
            var fileTypeObj = Constants.supportedFile[fileType];
            if (fileTypeObj.extension.indexOf(ext) == -1) {
                var errMsg = (fileTypeObj.errorMsg != undefined && fileTypeObj.errorMsg != "") ? fileTypeObj.errorMsg : Constants.supportedFile.defaultMessage;
                callBack({
                    status: false,
                    errorMsg: errMsg
                });
                return;
            }
            if (!common.validateFileSize(tableName,mediaFiles.size)) {
                var errMsg = Constants.supportedFile.fileSizeExceedMessage;
                callBack({
                    status: false,
                    errorMsg: errMsg
                });
                return;
            }
            var mediaData = new FormData();
            mediaData.append("file", mediaFiles);
            if (tableName !== undefined && tableName !== '') {
                mediaData.append("table", tableName);
            }
            Constants.ImageObj.data = mediaData;
            Constants.ImageObj.headers = HeaderSettings.headerSetting;
            var requestMedia = $.ajax(Constants.ImageObj);
            requestMedia.done(function (data) {
                console.log(data);
                if (data.status === false) {
                    callBack({
                        status: false,
                        errorMsg: data.error.message
                    });
                } else {
                    callBack({
                        status: true,
                        content: data
                    });
                }
            });
            requestMedia.fail(function (jqXHR, textStatus) {
                if (callBack) {
                    callBack({
                        status: false,
                        errorMsg: data.error.message
                    });
                }
            });
        } else {
            var errMsg = Constants.supportedFile.invalidFileUploadMessage;
            callBack({
                status: false,
                errorMsg: errMsg
            });
            return;
        }
    },

}

$.extend({
    getUrlVars: function () {
        var vars = [],
            hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    getUrlVar: function (name) {
        return $.getUrlVars()[name];
    }
});