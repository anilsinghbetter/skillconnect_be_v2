const maxProductImage = 10;
const maxProductHeight = 1000;
const maxProductWidth = 1000;
const maxBannerWidth = 924;
const maxBannerHeight = 254;
const baseURLServer = 'https://admin.skillconnect.in/';
const APIBASEURL="https://admin.skillconnect.in/api/v1";
Constants = {
    Api: {
        baseUrl: "/api/v1",
        apiKey: "12345!@#$%",
        signin: "/user/user-signin-admin/",
        signout: "/user/signout",
        signup: "/user/user-signup",
        getCategory: "/category/get-category/",
        addUpdateCategory: "/category/addupdate-category/",
        deleteCategory: "/category/remove-category/",
        getAdminUser: "/user/get-adminuser/",
        removeAdminUser: "/user/remove-adminuser/",
        getRoles: "/user/get-userrole/",
        addUpdateAdminUser: "/user/addupdate-adminuser",
        getUserByRole: "/user/get-userlist/",
        getUsertType: "/user/get-usertype",
        updateActiveStatus: "/other/active",
        deleteRecordCommon: "/other/delete",
        getModule: "/other/getModule",
        addRole: "/role/addupdate-role",
        getRoleModuleMapping: "/role/get-roleModuleMapping/",
        removeRole: "/role/remove-role/",
        getTrainingPartner: "/trainingpartner/get-trainingpartner/",
        getIndustryPartner: "/industrypartner/get-industrypartner/",
        getCandidates: "/candidate/getCandidates/",
        getCandidatesForAdmin: "/candidate/getCandidatesForAdmin",
        getTrainingCenter : "/trainingcenter/getTrainingCenter/",
        getTrainingCenterMaster : "/other/getTrainingCenterMaster",
        trainingPartnerSignin : "/trainingpartner/trainingpartner-signin",
        emailSend : "/other/emailSend",
        getIndustryPartnerDetail : "/industrypartner/get-industrypartner-detail/",
        getTrainingPartnerDetail : "/trainingpartner/get-trainingpartner-detail/",
        removeIndustryPartner : "/industrypartner/remove-industrypartner/",
        removeTrainingPartner : "/trainingpartner/remove-trainingpartner/",
        uploadCsv : "/other/upload",
        getState : "/other/getState",
        getCity : "/other/getCity",
        getSectorJobrole : "/other/getJobRoles",
        getSchemeMaster: "/other/getScheme",
        getSchemeCentralMinistry: "/other/getSchemeCentralMinistry",
        getStateWiseFutureRequestSearchResult : "/futurehiringrequest/getStateWiseFutureRequestSearchResult",
        addUpdatetrainingPartner : "/trainingpartner/addupdate-trainingpartner",
        addUpdatetrainingCenter : "/trainingcenter/addupdateTrainingCenter/",
        updateContactEnquiry : "/other/updateContactEnquiry/",
        addUpdateCandidate : "/candidate/addUpdateCandidate/",
        trainingPartnerUpdateMultipleRecords: "/trainingpartner/addupdate-trainingpartner-multiplerecords",
        addUpdateindustryPartner : "/industrypartner/addupdate-industrypartner",
        industryPartnerUpdateMultipleRecords: "/industrypartner/addupdate-industrypartner-multiplerecords",
        getSectorJobRoleMapping: "/sectorjobrole/get-sectorjobrole",
        getSector: "/sector/get-sectormaster",
        getJobRole: "/jobrole/get-jobrolemaster",
        removeSectorJobRoleMapping: "/sectorjobrole/remove-sectorjobrole/",
        removeSector: "/sector/remove-sector/",
        sectorUpdateMultipleRecords: "/sector/addupdate-sector-multiplerecords",
        removeJobRole: "/jobrole/remove-jobrole/",
        jobRoleUpdateMultipleRecords: "/jobrole/addupdate-jobrole-multiplerecords",
        addUpdateSector : "/sector/addupdate-sectormaster",
        addUpdateJobRole : "/jobrole/addupdate-jobrolemaster",
        getTrainingCenterDetail : "/trainingcenter/get-trainingcenter-detail/",
        trainingCenterUpdateMultipleRecords: "/trainingcenter/addupdate-trainingcenter-multiplerecords",
        candidateUpdateMultipleRecords: "/candidate/addupdate-candidate-multiplerecords",
        futureHiringRequestsUpdateMultipleRecords: "/futurehiringrequest/addupdate-futurerequest-multiplerecords",
        getCandidateDetail : "/candidate/get-candidate-viewmoredetail/",
        getCandidateDetailFull : "/candidate/getCandidateDetail/",
        getHiringRequest : "/futurehiringrequest/get-futurehiringrequest",
        getHiringRequestDetail : "/futurehiringrequest/get-futurehiringrequestdetail/",
        getcontact: "/other/getContact",
        getcontactDetail: "/other/getcontactDetail/",
        getDemands: "/demands/get-demands",
        getErrorFileList: "/other/errorFileList",
    },
    User: {
        currentUser: "current_user",
        authToken: "Authorization",
        udid: "udid"
    },
    walletRequestStatus: {
        0: "Pending",
        1: "Approved",
        2: "Rejected",
    },
    storeType: [{
        type: "Vegetable",
        shortDesc: "VE",
        image: ""
    }, {
        type: "Mall",
        shortDesc: "MA",
        image: ""
    }, {
        type: "Grocery",
        shortDesc: "GR",
        image: ""
    }, {
        type: "Other",
        shortDesc: "OT",
        image: ""
    }],
    ImageObj: {
        url: baseURLServer+"api/v1/other/upload",
        type: "POST",
        data: '',
        dataType: "json",
        cache: false,
        processData: false,
        contentType: false,
        headers: ''
    },
    productMeasure: [{
        id: "1",
        measure: "Kg/Gm"
    }, {
        id: "2",
        measure: "Lt/Ml"
    }, {
        id: "3",
        measure: "Unit"
    }],
    maxProductImage: maxProductImage,
    maxProductWidth: maxProductWidth,
    maxProductHeight: maxProductHeight,
    maxBannerWidth: maxBannerWidth,
    validImageFormats: ["jpeg", "jpg", "png"],
    commonMessage: {
        "MAX_PRODUCT_IMAGE_LIMIT": "Maximum " + maxProductImage + " images are allowed",
        "ERR_INVALID_PRODUCT_IMAGE_RESOLUTION": "Image resolution should be " + maxProductWidth + " X " + maxProductHeight,
        "ERR_INVALID_BANNER_IMAGE_RESOLUTION": "Image resolution should be " + maxBannerWidth + " X " + maxBannerHeight,
        "ERR_NO_IMAGE_SELECTED": "No Image Selected",
        "ERR_INVALID_IMAGE_FILE": "Please select image file only"
    },
    offerType: [{
        id: "1",
        offer: "Pramotional"
    }],
    discountType: [{
        id: "1",
        discount: "Cashback"
    },
    {
        id: "2",
        discount: "Instant Discount"
    }
    ],
    discountIn: [{
        id: "1",
        name: "In Rs"
    },
    {
        id: "2",
        name: "In Percent"
    }
    ],
    staticHtml: {
        replyButton: {
            html: '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary btnReply" title="Reply" type="button"><i class="material-icons pmd-sm">reply</i></button>',
            targetClass: 'button.btnReply'
        },
        approveButton: {
            html: '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary btn-success btnApprove" title="Status" type="button"><i class="material-icons pmd-sm">check</i></button>',
            targetClass: 'button.btnApprove'
        },
        rejectButton: {
            html: '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary btn-danger btnReject" title="Status" type="button" ><i class="material-icons pmd-sm">clear</i></button>',
            targetClass: 'button.btnReject'
        },
         editButton: {
            html: '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary btnEdit" type="button" title="Edit"><i class="material-icons pmd-sm">edit</i></button>',
            targetClass: 'button.btnEdit'
        }, 
        viewButton: {
            html: '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary btnView" type="button" title="Detail"><i class="material-icons pmd-sm">remove_red_eye</i></button>',
            targetClass: 'button.btnView'
        },
        deleteButton: {
            html: '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-danger btnDelete"  type="button" title="Delete"><i class="material-icons pmd-sm">delete</i></button>',
            targetClass: 'button.btnDelete'
        },
        checkBoxCanView: {
            html: "<input type='checkBox' class='chkCanView'/>"
        },
        checkBoxCanAdd: {
            html: "<input type='checkBox' class='chkCanAdd'/>"
        },
        checkBoxCanEdit: {
            html: "<input type='checkBox' class='chkCanEdit'/>"
        },
        checkBoxCanDelete: {
            html: "<input type='checkBox' class='chkCanDelete'/>"
        },
        checkBoxSelectAll: {
            html: '<div class="custom-control custom-checkbox pmd-checkbox"><input class="custom-control-input" type="checkbox" id="defaultCheck16"><label class="custom-control-label" for="defaultCheck16"></label></div>'
        },
    },
    supportedFile: {
        spreadSheet: {
            extension: ["csv", "CSV"],
            errorMsg: "Invalid File please select .csv file"
        },
        image: {
            extension: ["jpg", "JPG", "png", "PNG"],
            errorMsg: "Invalid Image file please select .jpg or .png file"
        },
        defaultMessage: "Invalid File Selected",
        fileSizeExceedMessage : "File size exceeds. Please upload valid size file.",
        invalidFileUploadMessage :"Invalid file upload request.",
    }
}
