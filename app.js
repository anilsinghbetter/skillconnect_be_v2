var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var session = require('express-session');
var compression = require('compression');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');

var https = require('https');
var fs = require('fs');

var app = express();
app.use(compression());
app.use(session({
  secret: 'Th!sI$E9!aYP6Y',
  cookie: {
    maxAge: 24 * 60 * 60 * 1000 // 24 hours
  },
  resave: true,
  saveUninitialized: true
})); 
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
// app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'views')));
app.use('/api-doc', express.static(__dirname + '/api-doc'));
app.use(function(request, response, next) {
  response.header("Access-Control-Allow-Origin", "*");
  response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, api-key,udid,device-type,Authorization");
  next();
});
// API related routes
app.use('/apidoc', express.static(__dirname + '/apidoc'));
require('./server/routes')(app);
app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
   app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('pages/others/pageNotFound', {
      message: err.message,
      error: err,
      title : '404: Page not found'
    });
  }); 
}

/*var https_options = {
  key: fs.readFileSync("./ssl/skillconnect.in.key"),
  cert: fs.readFileSync("./ssl/skillconnect.in.crt"),
};
*/

//https.createServer(https_options, function (req, res) {
//  res.writeHead(200);
//  res.end("Welcome to Node.js HTTPS Servern");
//}).listen(8443)

//https.createServer(https_options, app).listen(8443);

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('pages/others/pageNotFound', {
    message: err.message,
    error: err,
    title : '404: Page not found'
  });
});

process.on('unhandledRejection', (reason) => {
  console.log("Unhandeled Rejection.",reason);
});

module.exports = app;
