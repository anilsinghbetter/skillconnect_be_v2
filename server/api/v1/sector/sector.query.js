var tbl_UserMaster = "tbl_UserMaster";
var tbl_RoleMaster = "tbl_RoleMaster";
var tbl_UserType = "tbl_UserType";
var tbl_AccessToken = "tbl_AccessToken";
var tbl_transactionMaster = "tbl_transactionMaster";
var view_GetUserDetail = "view_GetUserDetail";
var tbl_IndustryPartners = "tbl_IndustryPartners";
var tbl_TrainingPartners = "tbl_TrainingPartners";
var view_GetLocationDetail = "view_GetLocationDetail";
var sector_jobrole_csv = "sector_jobrole_csv";
var view_SectorJobRoleDetail = "view_SectorJobRoleDetail";
var tbl_SectorMaster = "tbl_SectorMaster";
var trainingpartners_bulk_upload = "trainingpartners_bulk_upload";
var industrypartners_bulk_upload = "industrypartners_bulk_upload";
var candidate_bulk_upload = "candidate_bulk_upload";

var query = {
    getSectorQuery: {
        table: tbl_SectorMaster,
        select: [{
            field: 'pk_sectorID',
            alias: 'sectorid',
        }, {
            field: 'sectorName',
            alias: 'sectorName',
        }, {
            field: 'sectorSkillCouncil',
            alias: 'sectorSkillCouncil',
        }, {
            field: 'status',
            alias: 'status',
        }],
        filter: {
        },
        sortby: [{
            field: 'sectorName',
            order: 'ASC'
        }]
    },
    updateSectorQuery: {
        table: tbl_SectorMaster,
        update: [],
        filter: {
            and: [{
                field: 'pk_sectorID',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    checkSectorIsExistQuery: {
        table: tbl_SectorMaster,
        select: [{
            field: 'pk_sectorID',
            alias: 'sectorid'
        }, {
            field: 'sectorName',
            alias: 'sectorName'
        }, {
            field: 'sectorSkillCouncil',
            alias: 'sectorSkillCouncil'
        }],
        filter: {
            and: [{
                field: 'pk_sectorID',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    checkEntriesAlreayExistQuery: {
        table: tbl_SectorMaster,
        select: [{
            field: 'pk_sectorID',
            alias: 'sectorid'
        }],
        filter: {
            and: [{
                field: 'pk_sectorID',
                operator: 'NOTEQ',
                value: ''
            }, {
                or: [{
                    field: 'sectorName',
                    operator: 'EQ',
                    value: ''
                }, {
                    field: 'sectorSkillCouncil',
                    operator: 'EQ',
                    value: ''
                }]
            }]
        }
    },
    getSectorsByIndustryPartnerIDQuery: {
        join: {
            table: tbl_IndustryPartners,
            alias: 'IP',
            joinwith: [ {
                table: tbl_SectorMaster,
                alias: 'SEC',
                joincondition: {
                    table: 'SEC',
                    field: 'pk_sectorID',
                    operator: 'find_in_set',
                    value: {
                        table: 'IP',
                        field: 'fk_sectorID'
                    }
                }
            }]
        },
        select: [{
            field: 'GROUP_CONCAT(SEC.sectorName SEPARATOR "#")',
            encloseField: false,
            alias: 'sectorName'
        },{
            field: 'GROUP_CONCAT(SEC.pk_sectorID)',
            encloseField: false,
            alias: 'sectorid'
        }],
        filter: {
            and: [{
                field: 'IP.pk_industrypartnerid',
                encloseField: false,
                operator: 'EQ',
                value: ''
            },{
                field: 'IP.status',
                encloseField: false,
                operator: 'EQ',
                value: 1
            },{
                field: 'SEC.status',
                encloseField: false,
                operator: 'EQ',
                value: 1
            }]
        }
    },
    getSectorJobroleByTrainingPartnerIDQuery : {
        join: {
            table: tbl_TrainingPartners,
            alias: 'TP',
            joinwith: [ {
                table: tbl_SectorMaster,
                alias: 'SEC',
                joincondition: {
                    table: 'SEC',
                    field: 'pk_sectorID',
                    operator: 'find_in_set',
                    value: {
                        table: 'TP',
                        field: 'fk_sectorID'
                    }
                }
             },{
                table: view_SectorJobRoleDetail,
                alias: 'JBRL',
                joincondition: {
                    table: 'SEC',
                    field: 'pk_sectorID',
                    operator: 'find_in_set',
                    value: {
                        table: 'JBRL',
                        field: 'sectorid'
                    }
                }
            }]
        },
        select: [{
            field: 'SEC.sectorName',
            encloseField: false,
            alias: 'sectorName'
        },{
            field: 'SEC.pk_sectorID',
            encloseField: false,
            alias: 'sectorid'
        },{
            field: 'JBRL.jobroleid',
            encloseField: false,
            alias: 'jobroleid'
        },{
            field: 'JBRL.jobroleName',
            encloseField: false,
            alias: 'jobroleName'
        }],
        filter: {
            and: [{
                field: 'TP.pk_trainingpartnerid',
                encloseField: false,
                operator: 'EQ',
                value: ''
            },{
                field: 'TP.status',
                encloseField: false,
                operator: 'EQ',
                value: 1
            },{
                field: 'TP.is_deleted',
                encloseField: false,
                operator: 'EQ',
                value: 0
            },{
                field: 'JBRL.isSectorActive',
                encloseField: false,
                operator: 'EQ',
                value: 1
            }]
        }
    }, 
    getSectorsByTrainingPartnerIDQuery : {
        join: {
            table: tbl_TrainingPartners,
            alias: 'TP',
            joinwith: [ {
                table: tbl_SectorMaster,
                alias: 'SEC',
                joincondition: {
                    table: 'SEC',
                    field: 'pk_sectorID',
                    operator: 'find_in_set',
                    value: {
                        table: 'TP',
                        field: 'fk_sectorID'
                    }
                }
            }]
        },
        select: [{
            field: 'GROUP_CONCAT(SEC.sectorName SEPARATOR "#")',
            encloseField: false,
            alias: 'sectorName'
        },{
            field: 'GROUP_CONCAT(SEC.pk_sectorID)',
            encloseField: false,
            alias: 'sectorid'
        }],
        filter: {
            and: [{
                field: 'TP.pk_trainingpartnerid',
                encloseField: false,
                operator: 'EQ',
                value: ''
            },{
                field: 'TP.status',
                encloseField: false,
                operator: 'EQ',
                value: 1
            },{
                field: 'SEC.status',
                encloseField: false,
                operator: 'EQ',
                value: 1
            }]
        }
    },
    getTrainingPartnerSectorFromBulkUploadQuery: {
        table: trainingpartners_bulk_upload,
        select: [{
            field: 'id',
        }, {
            field: 'sector',
        }],
        filter: {
            and: [{
                field: 'is_processed',
                operator: 'EQ',
                value: 0
            },{
                field: 'is_sector_done',
                operator: 'EQ',
                value: 0
            }]
        },
        sortby: [{
            field: 'id',
            order: 'ASC'
        }]
    },
    getIndustryPartnerSectorFromBulkUploadQuery: {
        table: industrypartners_bulk_upload,
        select: [{
            field: 'id',
        }, {
            field: 'sector',
        }],
        filter: {
            and: [{
                field: 'is_processed',
                operator: 'EQ',
                value: 0
            },{
                field: 'is_sector_done',
                operator: 'EQ',
                value: 0
            }]
        },
        sortby: [{
            field: 'id',
            order: 'ASC'
        }]
    },
    getCandidateSectorJobRoleFromBulkUploadQuery: {
        table: candidate_bulk_upload,
        select: [{
            field: 'id',
        }, {
            field: 'sector',
        },{
            field: 'jobrole'
        }],
        filter: {
            and: [{
                field: 'is_processed',
                operator: 'EQ',
                value: 0
            },{
                field: 'is_sector_jobrole_done',
                operator: 'EQ',
                value: 0
            }]
        },
        sortby: [{
            field: 'id',
            order: 'ASC'
        }]
    },
    getSectorJobroleByIndustryPartnerIDQuery : {
        join: {
            table: tbl_IndustryPartners,
            alias: 'IP',
            joinwith: [ {
                table: tbl_SectorMaster,
                alias: 'SEC',
                joincondition: {
                    table: 'SEC',
                    field: 'pk_sectorID',
                    operator: 'find_in_set',
                    value: {
                        table: 'IP',
                        field: 'fk_sectorID'
                    }
                }
             },{
                table: view_SectorJobRoleDetail,
                alias: 'JBRL',
                joincondition: {
                    table: 'SEC',
                    field: 'pk_sectorID',
                    operator: 'find_in_set',
                    value: {
                        table: 'JBRL',
                        field: 'sectorid'
                    }
                }
            }]
        },
        select: [{
            field: 'SEC.sectorName',
            encloseField: false,
            alias: 'sectorName'
        },{
            field: 'SEC.pk_sectorID',
            encloseField: false,
            alias: 'sectorid'
        },{
            field: 'JBRL.jobroleid',
            encloseField: false,
            alias: 'jobroleid'
        },{
            field: 'JBRL.jobroleName',
            encloseField: false,
            alias: 'jobroleName'
        }],
        filter: {
            and: [{
                field: 'IP.pk_industrypartnerid',
                encloseField: false,
                operator: 'EQ',
                value: ''
            },{
                field: 'IP.status',
                encloseField: false,
                operator: 'EQ',
                value: 1
            },{
                field: 'IP.is_deleted',
                encloseField: false,
                operator: 'EQ',
                value: 0
            },{
                field: 'JBRL.isSectorActive',
                encloseField: false,
                operator: 'EQ',
                value: 1
            }]
        }
    },
};

module.exports = query
