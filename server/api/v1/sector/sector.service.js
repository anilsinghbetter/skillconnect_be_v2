var debug = require('debug')('server:api:v1:sector:service');
var common = require('../common');
var constant = require('../constant');
var sectorDAL = require('./sector.DAL');
var otherDAL = require('../other/other.DAL');
var trainingPartnerDAL = require('../trainingpartner/trainingpartner.DAL');
var industryPartnerDAL = require('../industrypartner/industrypartner.DAL');
var config = require('../../../../config');
var connection = require('../../../helper/connection');
var candidateDAL = require('../candidate/candidate.DAL');
/**
 * get Sector Service
 * 
 * @param {object} request 
 * @param {object} response
 * @return {object} 
 */
var getSectorService = async function (request, response) {
  debug("sector.service -> getSectorService");
  var isValid = common.validateParams([request.params.type]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_GET_SECTOR_REQUEST, false);
  }
  try {
    let result = await sectorDAL.getSector(request.params.type);
    return common.sendResponse(response, result.content, true);
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

/**
 * remove sector
 * 
 * @param {*} request 
 * @param {*} response 
 */
var removeSectorService = async function (request, response) {
  debug("sector.service -> removeSectorService");
  var isValid = common.validateParams([request.params.id]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_DELETE_SECTOR_REQUEST, false);
  }
  try {
    var fieldValueUpdate = [];
    fieldValueUpdate.push({
      field: "is_deleted",
      fValue: 1
    });
    let result = await sectorDAL.removeSector(request.params.id, fieldValueUpdate);
    if (result.status === false) {
      return common.sendResponse(response, result);
    } else {
      return common.sendResponse(response, constant.userMessages.SECTOR_DELETED_SUCCESSFULLY, true);
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

/**
 * When user wants to perform the actions like active/inactice/delete with multiple records
 * 
 * @param {*} request 
 * @param {*} response
 * @return {*} object 
 */
var updateMultipleRecordsService = async function (request, response) {
  debug("sector.service -> updateMultipleRecordsService");
  var isValid = common.validateParams([request.body.ids, request.body.action]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.INVALID_ADD_UPDATE_SECTOR_REQUEST, false);
  }
  var fieldValueInsert = [];

  var fieldValueObj = {};
  switch (request.body.action) {
    case 'active':
      fieldValueObj = {
        field: 'status',
        fValue: 1
      }
      break;
    case 'inactive':
      fieldValueObj = {
        field: 'status',
        fValue: 0
      }
      break;
    case 'delete':
      fieldValueObj = {
        field: 'is_deleted',
        fValue: 1
      }
      break;
  }

  fieldValueInsert.push(fieldValueObj);

  try {
    request.body.ids.forEach(async function (id, index) {
      var result = await sectorDAL.updateSectorInfoById(id, fieldValueInsert);
      if (request.body.ids.length - 1 == index) {
        return common.sendResponse(response, constant.userMessages.MSG_SECTOR_UPDATE_SUCCESSFULLY, true);
      }
    });
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

/**
 * Add Update sector
 * 
 * @param {*} request 
 * @param {*} response 
 */
var addUpdateSectorService = async function (request, response) {
  debug("sector.service -> addUpdateSectorService");
  var isValid = common.validateParams([request.body.sectorName, request.body.sectorSkillCouncil]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.INVALID_ADD_UPDATE_SECTOR_REQUEST, false);
  }
  common.sanitizeAll(request.body);
  var sectorID = request.body.sectorid;

  var sectorinfo = {};

  sectorinfo.sectorName = request.body.sectorName;
  sectorinfo.sectorSkillCouncil = request.body.sectorSkillCouncil;

  var sectorKeys = Object.keys(sectorinfo);
  var fieldValueInsert = [];
  sectorKeys.forEach(function (sectorKeys) {
    if (sectorinfo[sectorKeys] !== undefined) {
      var fieldValueObj = {};
      fieldValueObj = {
        field: sectorKeys,
        fValue: sectorinfo[sectorKeys]
      }
      fieldValueInsert.push(fieldValueObj);
    }
  });

  try {
    //While add new sector we are sending sectorid as -1
    if (sectorID == -1) {

    } else {
      var result = await sectorDAL.checkEditSectorIsExist(sectorID);
      if (result.content.length == 0) {
        return common.sendResponse(response, constant.userMessages.ERR_SECTOR_NOT_EXIST, false);
      } else if (result.content.length > 0 && result.content[0].sectorid == sectorID) {
        var result = await sectorDAL.checkEntriesAlreayExist(sectorinfo.sectorName, sectorinfo.sectorSkillCouncil, sectorID);
        if (result.content.length > 0) {
          return common.sendResponse(response, constant.userMessages.ERR_SECTOR_IS_ALREADY_EXIST, false);
        } else {
          var res_update_user = await sectorDAL.updateSectorInfoById(sectorID, fieldValueInsert);
          return common.sendResponse(response, constant.userMessages.MSG_SECTOR_UPDATE_SUCCESSFULLY, true);
        }
      }
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

var trainingPartnerBulkUploadCheckSectorService = async function () {
  var getUsers = await sectorDAL.getTrainingPartnerSectorFromBulkUpload();
  
  if (getUsers.status == true && getUsers.content.length > 0) {
    for (var i = 0; i < getUsers.content.length; i++) {
      var autoIncId = getUsers.content[i].id;
      var sectors = getUsers.content[i].sector.split(constant.appConfig.CSV_SEPERATOR); 

      if (sectors.length > 0) {
        var commaSeperatedSectorIds = '';
        for (var sectorname of sectors) {
          var sectorResponse = await otherDAL.checkSectorExistIntoMaster(sectorname);
          if (sectorResponse.status == true && sectorResponse.content.length > 0 && sectorResponse.content[0].sectorid > 0) {
            commaSeperatedSectorIds += sectorResponse.content[0].sectorid + ',';
          }
        }
        if (commaSeperatedSectorIds != '') {
          commaSeperatedSectorIds = commaSeperatedSectorIds.substring(0, commaSeperatedSectorIds.lastIndexOf(","));
        }
      }
      console.log(commaSeperatedSectorIds);
      if (commaSeperatedSectorIds != '') {
        var sectorId = commaSeperatedSectorIds;//sectorIds
        
        if(sectorId){
          var fieldValueUpdate = [];
          fieldValueUpdate.push({
            field: "sector",
            fValue: sectorId
          });
          var result = await trainingPartnerDAL.updateTrainingPartnerBulkUpload(autoIncId,fieldValueUpdate);
          if(result.status == true && result.content.changedRows == 1){
            var fieldValueUpdate = [];
            fieldValueUpdate.push({
              field: "is_sector_done",
              fValue: 1
            });
            var result = await trainingPartnerDAL.updateTrainingPartnerBulkUpload(autoIncId,fieldValueUpdate);
            console.log(result);
          }
        } else {
          console.log('No Sector Found');
        }
      } else {
        console.log('No Sector Found');
      }
    }
  } else {
    console.log('No records');
  }
}

var industryPartnerBulkUploadCheckSectorService = async function () {
  var getUsers = await sectorDAL.getIndustryPartnerSectorFromBulkUpload();
  console.log(getUsers);
  
  if (getUsers.status == true && getUsers.content.length > 0) {
    for (var i = 0; i < getUsers.content.length; i++) {
      var autoIncId = getUsers.content[i].id;
      var sectors = getUsers.content[i].sector.split(constant.appConfig.CSV_SEPERATOR); 

      if (sectors.length > 0) {
        var commaSeperatedSectorIds = '';
        for (var sectorname of sectors) {
          var sectorResponse = await otherDAL.checkSectorExistIntoMaster(sectorname);
          if (sectorResponse.status == true && sectorResponse.content.length > 0 && sectorResponse.content[0].sectorid > 0) {
            commaSeperatedSectorIds += sectorResponse.content[0].sectorid + ',';
          }
        }
        if (commaSeperatedSectorIds != '') {
          commaSeperatedSectorIds = commaSeperatedSectorIds.substring(0, commaSeperatedSectorIds.lastIndexOf(","));
        }
      }
      console.log(commaSeperatedSectorIds);
      if (commaSeperatedSectorIds != '') {
        var sectorId = commaSeperatedSectorIds;//sectorIds
        
        if(sectorId){
          var fieldValueUpdate = [];
          fieldValueUpdate.push({
            field: "sector",
            fValue: sectorId
          });
          var result = await industryPartnerDAL.updateIndustryPartnerBulkUpload(autoIncId,fieldValueUpdate);
          if(result.status == true && result.content.changedRows == 1){
            var fieldValueUpdate = [];
            fieldValueUpdate.push({
              field: "is_sector_done",
              fValue: 1
            });
            var result = await industryPartnerDAL.updateIndustryPartnerBulkUpload(autoIncId,fieldValueUpdate);
            console.log(result);
          }
        } else {
          console.log('No Sector Found');
        }
      } else {
        console.log('No Sector Found');
      }
    }
  } else {
    console.log('No records');
  }
}

var candidateBulkUploadCheckSectorJobRoleService = async function () {
  var getUsers = await sectorDAL.getCandidateSectorJobRoleFromBulkUpload();
  
  if (getUsers.status == true && getUsers.content.length > 0) {
    for (var i = 0; i < getUsers.content.length; i++) {
      var autoIncId = getUsers.content[i].id;
      var sectors = getUsers.content[i].sector.split(constant.appConfig.CSV_SEPERATOR); 
      var jobroleName = getUsers.content[i].jobrole;
      if (sectors.length > 0) {
        var commaSeperatedSectorIds = '';
        for (var sectorname of sectors) {
          var sectorResponse = await otherDAL.checkSectorJobRoleView(sectorname,jobroleName);
          if (sectorResponse.status == true && sectorResponse.content.length > 0 && sectorResponse.content[0].sectorid > 0) {
            commaSeperatedSectorIds += sectorResponse.content[0].sectorid + ',';
            JobRoleId = sectorResponse.content[0].jobroleid ;
          }
        }
        if (commaSeperatedSectorIds != '') {
          commaSeperatedSectorIds = commaSeperatedSectorIds.substring(0, commaSeperatedSectorIds.lastIndexOf(","));
        }
      }
       console.log(commaSeperatedSectorIds);
      if (commaSeperatedSectorIds != '') {
        var sectorId = commaSeperatedSectorIds;//sectorIds
        
        if(sectorId && JobRoleId){
          var fieldValueUpdate = [];
          fieldValueUpdate.push({
            field: "sector",
            fValue: sectorId
          },
          {
            field: "jobrole",
            fValue: JobRoleId
          });
          var result = await candidateDAL.updateCandidateBulkUpload(autoIncId,fieldValueUpdate);
          if(result.status == true && result.content.changedRows == 1){
            var fieldValueUpdate = [];
            fieldValueUpdate.push({
              field: "is_sector_jobrole_done",
              fValue: 1
            });
            var result = await candidateDAL.updateCandidateBulkUpload(autoIncId,fieldValueUpdate);
            console.log(result);
          }
        } else {
          console.log('No Sector/JobRole Found');
        }
      } else {
        console.log('No Sector/JobRole Found');
      }
    }
  } else {
    console.log('No records');
  }
}

module.exports = {
  getSectorService: getSectorService,
  removeSectorService: removeSectorService,
  updateMultipleRecordsService: updateMultipleRecordsService,
  addUpdateSectorService: addUpdateSectorService,
  trainingPartnerBulkUploadCheckSectorService : trainingPartnerBulkUploadCheckSectorService,
  industryPartnerBulkUploadCheckSectorService : industryPartnerBulkUploadCheckSectorService,
  candidateBulkUploadCheckSectorJobRoleService : candidateBulkUploadCheckSectorJobRoleService
}