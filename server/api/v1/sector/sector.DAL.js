var debug = require('debug')('server:api:v1:sector:DAL');
var common = require('../common');
var constant = require('../constant');
var query = require('./sector.query');


/**
 * Get Sector
 * 
 */
var getSector = async function (requestType) {
  debug("sector.DAL -> getSector");
  var getSectorQuery = common.cloneObject(query.getSectorQuery);
  var getSectorQueryFilter = { and: [] };
  getSectorQueryFilter.and.push({
    field: 'is_deleted',
    operator: 'EQ',
    value: 0
  });
  if(requestType == 'admin'){
    delete getSectorQuery.filter;
  } else {
    getSectorQueryFilter.and.push({
      field: 'status',
      operator: 'EQ',
      value: 1
    });
  }
  getSectorQuery.filter = getSectorQueryFilter;
  return await common.executeQuery(getSectorQuery);
}

var removeSector = async function (id,fieldValueUpdate) {
  debug("sector.DAL -> removeSector");
  var removeSectorQuery = common.cloneObject(query.updateSectorQuery);
  removeSectorQuery.filter.and[0].value = id;
  removeSectorQuery.update = fieldValueUpdate;
  return await common.executeQuery(removeSectorQuery);
}

var updateSectorInfoById = async function(sectorId, fieldValue){
  debug("sector.DAL -> updateSectorInfoById");
  var updateSectorQuery = common.cloneObject(query.updateSectorQuery);
  updateSectorQuery.filter.and[0].value = sectorId;
  updateSectorQuery.update = fieldValue;
  return await common.executeQuery(updateSectorQuery);
}

var checkEditSectorIsExist = async function (id) {
  debug("sector.DAL -> checkEditSectorIsExist");
  var checkEditSectorIsExist = common.cloneObject(query.checkSectorIsExistQuery);
  checkEditSectorIsExist.filter.and[0].value = id;
  return await common.executeQuery(checkEditSectorIsExist);
};

var checkEntriesAlreayExist = async function (sectorName,sectorSkillCouncil,sectorID) {
  debug("sector.DAL -> checkEntriesAlreayExist");
  var checkEntriesAlreayExist = common.cloneObject(query.checkEntriesAlreayExistQuery);
  checkEntriesAlreayExist.filter.and[0].value = sectorID;
  checkEntriesAlreayExist.filter.and[1].or[0].value = sectorName;
  checkEntriesAlreayExist.filter.and[1].or[1].value = sectorSkillCouncil;
  return await common.executeQuery(checkEntriesAlreayExist);
};

var getSectorJobroleByTrainingPartnerID = async function(trainingpartnerid){
  debug("sector.DAL -> getSectorJobroleByTrainingPartnerID");
  var getSectorJobroleByTrainingPartnerID = common.cloneObject(query.getSectorJobroleByTrainingPartnerIDQuery);
  getSectorJobroleByTrainingPartnerID.filter.and[0].value = trainingpartnerid;
  return await common.executeQuery(getSectorJobroleByTrainingPartnerID);
}

var getSectorsByTrainingPartnerID = async function(trainingpartnerid){
  debug("sector.DAL -> getSectorsByTrainingPartnerID");
  var getSectorsByTrainingPartnerID = common.cloneObject(query.getSectorsByTrainingPartnerIDQuery);
  getSectorsByTrainingPartnerID.filter.and[0].value = trainingpartnerid;
  return await common.executeQuery(getSectorsByTrainingPartnerID);
}

var getSectorsByIndustryPartnerID = async function(ipid){
  debug("sector.DAL -> getSectorsByTrainingPartnerID");
  var getSectorsByIndustryPartnerID = common.cloneObject(query.getSectorsByIndustryPartnerIDQuery);
  getSectorsByIndustryPartnerID.filter.and[0].value = ipid;
  return await common.executeQuery(getSectorsByIndustryPartnerID);
}

var getTrainingPartnerSectorFromBulkUpload = async function(){
  debug("sector.DAL -> getTrainingPartnerSectorFromBulkUpload");
  var getTrainingPartnerSectorFromBulkUpload = common.cloneObject(query.getTrainingPartnerSectorFromBulkUploadQuery);
  getTrainingPartnerSectorFromBulkUpload.limit = constant.appConfig.CRON_TRAININGPARTNER_BULK_RECORDS_LIMIT;
  return await common.executeQuery(getTrainingPartnerSectorFromBulkUpload);
}

var getIndustryPartnerSectorFromBulkUpload = async function(){
  debug("sector.DAL -> getIndustryPartnerSectorFromBulkUpload");
  var getIndustryPartnerSectorFromBulkUpload = common.cloneObject(query.getIndustryPartnerSectorFromBulkUploadQuery);
  getIndustryPartnerSectorFromBulkUpload.limit = constant.appConfig.CRON_INDUSTRYPARTNER_BULK_RECORDS_LIMIT;
  return await common.executeQuery(getIndustryPartnerSectorFromBulkUpload);
}

var getCandidateSectorJobRoleFromBulkUpload = async function(){
  debug("sector.DAL -> getCandidateSectorJobRoleFromBulkUpload");
  var getCandidateSectorJobRoleFromBulkUpload = common.cloneObject(query.getCandidateSectorJobRoleFromBulkUploadQuery);
  getCandidateSectorJobRoleFromBulkUpload.limit = constant.appConfig.CRON_CANDIDATE_BULK_RECORDS_LIMIT;
  return await common.executeQuery(getCandidateSectorJobRoleFromBulkUpload);
}

var getSectorJobroleByIndustryPartnerID  = async function(industrypartnerid){
  debug("sector.DAL -> getSectorJobroleByIndustryPartnerID");
  var getSectorJobroleByIndustryPartnerID = common.cloneObject(query.getSectorJobroleByIndustryPartnerIDQuery);
  getSectorJobroleByIndustryPartnerID.filter.and[0].value = industrypartnerid;
  return await common.executeQuery(getSectorJobroleByIndustryPartnerID);
}

module.exports = {
  getSector : getSector,
  removeSector : removeSector,
  updateSectorInfoById : updateSectorInfoById,
  checkEditSectorIsExist : checkEditSectorIsExist,
  checkEntriesAlreayExist : checkEntriesAlreayExist,
  getSectorsByTrainingPartnerID : getSectorsByTrainingPartnerID,
  getSectorsByIndustryPartnerID : getSectorsByIndustryPartnerID,
  getTrainingPartnerSectorFromBulkUpload : getTrainingPartnerSectorFromBulkUpload,
  getIndustryPartnerSectorFromBulkUpload : getIndustryPartnerSectorFromBulkUpload,
  getCandidateSectorJobRoleFromBulkUpload : getCandidateSectorJobRoleFromBulkUpload,
  getSectorJobroleByTrainingPartnerID: getSectorJobroleByTrainingPartnerID,
  getSectorJobroleByIndustryPartnerID: getSectorJobroleByIndustryPartnerID,
}
