var express = require('express');
var services = require('./sector.service');
var middleware = require('../../../middleware');

var router = express.Router();
module.exports = router;

router.post('/get-sectormaster/:type', middleware.logger, services.getSectorService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/remove-sector/:id',middleware.logger, services.removeSectorService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/addupdate-sector-multiplerecords',middleware.logger, services.updateMultipleRecordsService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/addupdate-sectormaster',middleware.logger, services.addUpdateSectorService);//middleware.checkAccessToken,middleware.userRightsByAPI,


//cron urls
router.post('/trainingPartnerBulkUploadCheckSectorService', services.trainingPartnerBulkUploadCheckSectorService);
router.post('/industryPartnerBulkUploadCheckSectorService', services.industryPartnerBulkUploadCheckSectorService);
router.post('/candidateBulkUploadCheckSectorJobRoleService', services.candidateBulkUploadCheckSectorJobRoleService);
