var debug = require('debug')('server:api:v1:common');
var constant = require('./constant');
var queryExecutor = require('../../helper/mySql');
var dbDateFormat = constant.appConfig.DB_DATE_FORMAT;
var pageSize = constant.appConfig.PAGE_SIZE;
var d3 = require("d3");
var config = require('../../../config');
var crypto = require("crypto");
var key = 'betterplace@123';
var otherDAL = require('./other/other.DAL');

let cloneObject = function (obejct) {
  return JSON.parse(JSON.stringify(obejct));
};
module.exports.cloneObject = cloneObject;

let executeQuery = async function (jsonQuery, cb) {
  // try{

  if (cb) {
    await queryExecutor.executeQuery(jsonQuery, cb);
  }
  else {
    return await queryExecutor.executeQuery(jsonQuery);
  }
};
module.exports.executeQuery = executeQuery;
module.exports.executeRawQuery = async (jsonQuery, cb) => {
  if (cb) {
    await queryExecutor.executeRawQuery(jsonQuery, cb);
  } else {
    return await queryExecutor.executeRawQuery(jsonQuery);
  }
};

module.exports.executeQueryWithTransactions = async (queryArrayJSON) => {
  return await queryExecutor.executeQueryWithTransactions(queryArrayJSON);
};


module.exports.getGetMediaURL = function (request) {
  debug("common -> getGetMediaURL");
  var fullUrl = request.protocol + '://' + request.get('host') + constant.appConfig.MEDIA_GET_STATIC_URL;
  return fullUrl;
};

module.exports.getNoImageURL = function (request) {
  debug("common -> getGetMediaURL");
  var fullUrl = request.protocol + '://' + request.get('host') + constant.appConfig.MEDIA_DEFAULT_IMAGES_PATH + "noimage.jpg";
  return fullUrl;
};

module.exports.getPaginationObject = function (request,customPageSize) {
  var paginationObj = {};
  var serverDateTime;
  var pageNo;
  if (request.query.pageno === undefined) {//|| request.query.datetime === undefined
    pageNo = 1;
    serverDateTime = (new Date()).getTime();
  } else {
    pageNo = parseInt(request.query.pageno);
    serverDateTime = parseInt(request.query.datetime);
  }
  paginationObj.pageNo = pageNo;
  paginationObj.serverDateTime = serverDateTime;
  paginationObj.dbServerDateTime = d3.timeFormat(dbDateFormat)(new Date(serverDateTime));
  if(customPageSize !== undefined && customPageSize > 0){
    paginationObj.limit = [customPageSize * (pageNo - 1), customPageSize];
  } else {
    paginationObj.limit = [pageSize * (pageNo - 1), pageSize];
  }
  return paginationObj;
};

module.exports.getPaginationOptions = function (request, totalNumberOfRecords,cutomPageSize,hiring_request_count_obj) {
  var pagesize = constant.appConfig.PAGE_SIZE;
  if(cutomPageSize !== undefined && cutomPageSize > 0){
    pagesize = cutomPageSize;
  }
  let pageCount = Math.ceil(totalNumberOfRecords / pagesize);
  let paginationOptions = {};
  paginationOptions.totalPages = pageCount;
  paginationOptions.curPage = request.query.pageno !== undefined ? parseInt(request.query.pageno) : 1;
  paginationOptions.perPage = pagesize;
  paginationOptions.totalcount = totalNumberOfRecords;
  if(hiring_request_count_obj != undefined){
    paginationOptions.totalPendingHiringRequests = hiring_request_count_obj.totalPendingHiringRequests;
    paginationOptions.totalAcceptedHiringRequests = hiring_request_count_obj.totalAcceptedHiringRequests;
    paginationOptions.totalCompletedHiringRequests = hiring_request_count_obj.totalCompletedHiringRequests;
    paginationOptions.totalDeclinedHiringRequests = hiring_request_count_obj.totalDeclinedHiringRequests;
    paginationOptions.totalCancelledHiringRequests = hiring_request_count_obj.totalCancelledHiringRequests;
    paginationOptions.internalStatus = hiring_request_count_obj.internalStatus;
  }
  return paginationOptions;
}

module.exports.sendResponse = function (response, obj, isSuccess, paginationOptions, user_type) {
  if (isSuccess != undefined) {
    if (isSuccess == true) {
      if (paginationOptions != undefined) {
        response.send({ status: true, data: obj, paginationOptions: paginationOptions });
      } else {
        response.send({ status: true, data: obj });
      }

    }
    else {
      response.send({ status: false, error: obj });
    }
  }
  else {
    response.send(obj);
  }
}

module.exports.validateObject = function (arrParam) {
  arrParam.forEach(function (param) {
    if (param == undefined || typeof param != "object") {
      return false;
    }
  });
  return true;
}

module.exports.validateParams = function (arrParam) {
  for ([index,param] of arrParam.entries()) {
    if (param === undefined || param === "") {
      return false;
    }
  }
  
  return true;
}
/**
 * sanitize the body params 
 * @param {*} arrParam 
 */
module.exports.sanitizeAll = function (arrParam) {
  var striptags = require('striptags');

  for (item in arrParam) {
    let newstring = arrParam[item].toString();
    newstring = newstring.trim();
    newstring = striptags(newstring);
    arrParam[item] = newstring;
  }
  return true;
}

module.exports.encrypt = function (text) {
  debug("common -> encrypt");
  var cipher = crypto.createCipher('aes-256-cbc', key);
  var crypted = cipher.update(text, 'utf-8', 'hex');
  crypted += cipher.final('hex');

  return crypted;
}
module.exports.decrypt = function (text) {
  debug("common -> decrypt");
  try {
    var decipher = crypto.createDecipher('aes-256-cbc', key);
    var decrypted = decipher.update(text, 'hex', 'utf-8');
    decrypted += decipher.final('utf-8');
    return decrypted;
  } catch (ex) {

  }
}
module.exports.compareDateString = function (dateString1, dateString2) {
  return dateString1 >= dateString2;
}

module.exports.getEmailTemplateFromDB = async function (emailType) {
  debug("common -> getEmailTemplateFromDB");
  var query = {
    getEmailTemplateFromDBQuery: {
      table: 'tbl_EmailTemplates',
      select: [{
        field: 'subject',
      }, {
        field: 'content',
      }],
      filter: {
        and: [{
          field: 'type',
          operator: 'EQ',
          value: ''
        }, {
          field: 'status',
          operator: 'EQ',
          value: ''
        }]
      }
    }
  };
  var getEmailTemplateFromDBQuery = cloneObject(query.getEmailTemplateFromDBQuery);
  getEmailTemplateFromDBQuery.filter.and[0].value = emailType;
  getEmailTemplateFromDBQuery.filter.and[1].value = 1;
  return await executeQuery(getEmailTemplateFromDBQuery);
}

module.exports.sendEmailToUserWithNodeMailer = function (to, from, subject, content) {
  debug("common -> sendEmailToUserWithNodeMailer");
  'use strict';
  const nodemailer = require('nodemailer');

  return new Promise(function (resolve, reject) {
    try {
      nodemailer.createTestAccount((err, account) => {
        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport(config.emailConfig);

        // setup email data with unicode symbols
        let mailOptions = {
          from: from, // sender address
          to: to, // list of receivers
          subject: subject, // Subject line
          //text: 'This is nodejs email', // plain text body
          html: content // html body
        };
        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            reject({
              status: false,
              error: error
            });
          }
          resolve({
            status: true,
            data: info.messageId
          });
        });
      });
    } catch (e) {
      debug("error", e);
      reject({
        status: false,
        error: e
      });
    }
  });
}
/**
 * Send Email to user with sendgrid.net
 * 
 * @param {string} to 
 * @param {string} subject 
 * @param {string} content 
 */
module.exports.sendEmailToUser = async function (to, subject, content) {
  debug("common -> sendEmailToUser");
  var helper = require('sendgrid').mail;
  var fromEmail = new helper.Email(config.sendgridConfig.fromUser);
  var toEmail = new helper.Email(to);
  var emailContent = new helper.Content('text/html', content);
  var mail = new helper.Mail(fromEmail, subject, toEmail, emailContent);

  var result_get_api = await otherDAL.getApiKey('sendgrid');
    // console.log(result_get_api.content[0].api_key);
  var sg = require('sendgrid')(result_get_api.content[0].api_key);

  var request = sg.emptyRequest({
    method: 'POST',
    path: '/v3/mail/send',
    body: mail.toJSON()
  });

  // With promise
  return new Promise(function (resolve, reject) {
    try {
      sg.API(request)
        .then(function (response) {
           console.log(response.statusCode);
           console.log(response.body);
           console.log(response.headers);
          resolve({
            status: true,
          });
        })
        .catch(function (error) {
          // error is an instance of SendGridError
          // The full response is attached to error.response
          console.log('error',error.response);
          reject({
            status: false,
          });
        });
    } catch (e) {
      debug("error", e);
      reject({
        status: false,
      });
    }
  });
}

module.exports.setSearchParams = function (arrParam, filter, searchOn) {
  var operatorToUse = 'eq';
  var aliasToUse = '';
  switch (searchOn) {
    case 'candidate':
      aliasToUse = 'CD.';
      break;
    case 'trainingcenter':
      aliasToUse = 'TC.';
      break;
  }

  for (var key in arrParam) {
    if (arrParam[key] !== undefined && arrParam[key] != "" && arrParam[key] != null) {
      if (key == 'candidate_name' || key == 'trainingcenter_name' || key == 'batch_id' || key == 'phone') {
        operatorToUse = 'match';
      } else {
        operatorToUse = 'eq';
      }

      if (key == 'batch_start_date' || key == 'batch_end_date') {
        filter.and.push({
          field: 'DATE_FORMAT('+aliasToUse+key+',"%Y-%m-%d")',
          encloseField: false,
          operator: operatorToUse,
          value: arrParam[key]
        });
      } else if(key === 'defaultSectors'){
        //Employer Dashboard Search will do default sector search whenever he has not selected any particular sector search [code start]
        filter.and.push({
          field: aliasToUse + 'fk_sectorID',
          encloseField: false,
          operator: 'eq',
          value: arrParam[key].split(',')
        });
        //Employer Dashboard Search will do default sector search whenever he has not selected any particular sector search [code over]
      } else {
        filter.and.push({
          field: aliasToUse + key,
          encloseField: false,
          operator: operatorToUse,
          value: arrParam[key]
        });
      }
      
    }
  }
}

module.exports.getStateFromView = async function () {
  debug("common -> getStateFromView");
  var query = {
    getStateFromViewQuery: {
      table: 'view_GetLocationDetail',
      select: [{
        field: 'fk_countryID',
      }, {
        field: 'stateid',
      }, {
        field: 'stateName',
      }],
      filter: {
        and: [{
          field: 'isStateActive',
          operator: 'EQ',
          value: 1
        }]
      },
      groupby: [{
        field: 'stateid'
      }],
    }
  };
  var getStateFromViewQuery = cloneObject(query.getStateFromViewQuery);
  return await executeQuery(getStateFromViewQuery);
}

module.exports.getCityFromView = async function (stateid) {
  debug("common -> getCityFromView");
  var query = {
    getCityFromViewQuery: {
      table: 'view_GetLocationDetail',
      select: [{
        field: 'fk_countryID',
      }, {
        field: 'cityid',
      }, {
        field: 'cityName',
      }],
      filter: {
        and: [{
          field: 'fk_stateID',
          operator: 'EQ',
          value: ''
        }, {
          field: 'isStateActive',
          operator: 'EQ',
          value: 1
        }, {
          field: 'isCityActive',
          operator: 'EQ',
          value: 1
        }]
      }
    }
  };
  var getCityFromViewQuery = cloneObject(query.getCityFromViewQuery);
  getCityFromViewQuery.filter.and[0].value = stateid;
  return await executeQuery(getCityFromViewQuery);
}

module.exports.getSchemeTypes = async function () {
  debug("common -> getSchemeTypes");
  var query = {
    getSchemeTypesQuery: {
      table: 'tbl_SchemeMaster',
      select: [{
        field: 'pk_schemeID',
      }, {
        field: 'schemeType',
      }],
      filter: {
        and: [{
          field: 'status',
          operator: 'EQ',
          value: 1
        }]
      }
    }
  };
  var getSchemeTypesQuery = cloneObject(query.getSchemeTypesQuery);
  return await executeQuery(getSchemeTypesQuery);
}

module.exports.getJobroleFromView = async function (sectorid) {
  debug("common -> getJobroleFromView");
  var query = {
    getJobroleFromViewQuery: {
      table: 'view_SectorJobRoleDetail',
      select: [{
        field: 'jobroleid',
      }, {
        field: 'jobroleName',
      }],
      filter: {
        and: [{
          field: 'fk_sectorID',
          operator: 'EQ',
          value: ''
        }, {
          field: 'isSectorActive',
          operator: 'EQ',
          value: 1
        }, {
          field: 'isJobRoleActive',
          operator: 'EQ',
          value: 1
        }]
      }
    }
  };
  var getJobroleFromViewQuery = cloneObject(query.getJobroleFromViewQuery);
  getJobroleFromViewQuery.filter.and[0].value = sectorid;
  return await executeQuery(getJobroleFromViewQuery);
}

module.exports.getSchemeCentralMinistryMapping = async function (schemeid) {
  debug("common -> getSchemeCentralMinistryMapping");
  var query = {
    getSchemeCentralMinistryMappingQuery: {
      table: 'tbl_SchemeCentralMinistryMapping',
      select: [{
        field: 'fk_centralministryID',
        alias: 'centralministryid'
      }],
      filter: {
        and: [{
          field: 'fk_schemeID',
          operator: 'EQ',
          value: ''
        }]
      }
    }
  };
  var getSchemeCentralMinistryMappingQuery = cloneObject(query.getSchemeCentralMinistryMappingQuery);
  getSchemeCentralMinistryMappingQuery.filter.and[0].value = schemeid;
  return await executeQuery(getSchemeCentralMinistryMappingQuery);
}

module.exports.getSchemeCentralMinistry = async function (centralministryid) {
  debug("common -> getSchemeCentralMinistry");
  var query = {
    getCentralMinistryQuery: {
      table: 'tbl_CentralministriesMaster',
      select: [{
        field: 'name',
        alias: 'centralministryname'
      }, {
        field: 'pk_ID',
        alias: 'centralministryid'
      }],
      filter: {
        and: [{
          field: 'pk_ID',
          operator: 'EQ',
          value: ''
        }, {
          field: 'status',
          operator: 'EQ',
          value: 1
        }]
      }
    }
  };
  var getCentralMinistryQuery = cloneObject(query.getCentralMinistryQuery);
  getCentralMinistryQuery.filter.and[0].value = centralministryid;
  return await executeQuery(getCentralMinistryQuery);
}