var debug = require('debug')('server:api:v1:candidate:DAL');
var d3 = require("d3");
var DateLibrary = require('date-management');
var common = require('../common');
var constant = require('../constant');
var query = require('./candidate.query');
var dbDateFormat = constant.appConfig.DB_DATE_FORMAT;

/**
 * Check candidate exists or not
 * 
 * @param {string} candidate_id 
 * @return {object}
 */
var checkCandidateIsExist = async function (sdmsnsdc_enrollment_number) {
  debug("candidate.DAL -> checkCandidateIsExist");
  var checkCandidateIsExistQuery = common.cloneObject(query.checkCandidateIsExistQuery);
  checkCandidateIsExistQuery.filter.and[0].value = sdmsnsdc_enrollment_number;
  return await common.executeQuery(checkCandidateIsExistQuery);
}

/**
 * Add new candidate 
 * 
 * @param  {Array of object}   fieldValue [the array object of the request body]
 * @return {object}
 */
var createCandidate = async function (fieldValue) {
  debug("candidate.DAL -> createCandidate");
  var createCandidateQuery = common.cloneObject(query.createCandidateQuery);
  createCandidateQuery.insert = fieldValue;
  return await common.executeQuery(createCandidateQuery);
}

/**
 * Get candidate count
 * 
 * @param {object} searchObj
 * @return {object} 
 */
var getCandidatesCount = async function (searchObj,requestType) {
  debug("candidate.DAL -> getCandidatesCount");
  var getCandidatesCountQuery = common.cloneObject(query.getCandidatesCountQuery);
  var candidatesFilter = { and: [] };
  if(requestType == 'admin'){
    candidatesFilter.and.push({
      field: 'CD.is_deleted',
      encloseField: false,
      operator: 'EQ',
      value: 0
    });
    getCandidatesCountQuery.filter = candidatesFilter;
  } else {
    if (Object.keys(searchObj).length !== 0) {
      common.setSearchParams(searchObj,candidatesFilter,'candidate');
      getCandidatesCountQuery.filter = candidatesFilter;
    } else {
      delete getCandidatesCountQuery.filter;
    }
    candidatesFilter.and.push({
      field: 'CD.status',
      encloseField: false,
      operator: 'EQ',
      value: 1
    });
    candidatesFilter.and.push({
      field: 'CD.is_deleted',
      encloseField: false,
      operator: 'EQ',
      value: 0
    });
    getCandidatesCountQuery.filter = candidatesFilter;
  }
  return await common.executeQuery(getCandidatesCountQuery);
}

/**
 * Get Candidates
 * 
 * @param {*} limit 
 * @param {*} searchObj 
 * @param {*} requestType admin / web
 */
var getCandidates = async function (limit, searchObj,requestType) {
  debug("candidate.DAL -> getCandidates");
  var getCandidateQuery = common.cloneObject(query.getCandidateQuery);
  var candidatesFilter = { and: [] };
  if(requestType == 'admin'){
    candidatesFilter.and.push({
      field: 'CD.is_deleted',
      encloseField: false,
      operator: 'EQ',
      value: 0
    });
    getCandidateQuery.filter = candidatesFilter;
  } else {
    if (Object.keys(searchObj).length !== 0) {
      common.setSearchParams(searchObj,candidatesFilter,'candidate');
      getCandidateQuery.filter = candidatesFilter;
    } else {
      delete getCandidateQuery.filter;
    }
    candidatesFilter.and.push({
      field: 'CD.status',
      encloseField: false,
      operator: 'EQ',
      value: 1
    });
    candidatesFilter.and.push({
      field: 'CD.is_deleted',
      encloseField: false,
      operator: 'EQ',
      value: 0
    });
    getCandidateQuery.filter = candidatesFilter;
  }
  //curpage
  //perpage
  //totalpages
  //console.log('limit',limit);
  if(requestType != 'admin'){
    getCandidateQuery.limit = limit;
  }
  return await common.executeQuery(getCandidateQuery);
}

var updateCandidateById = async function (candidateID,fieldValueUpdate) {
  debug("candidate.DAL -> updateCandidateById");
  var updateCandidate = common.cloneObject(query.updateCandidateQuery);
  updateCandidate.filter.and[0].value = candidateID;
  updateCandidate.update = fieldValueUpdate;
  return await common.executeQuery(updateCandidate);
}

var checkCandidateIDIsExist = async function(candidateID){
  debug("candidate.DAL -> checkCandidateIDIsExist");
  var checkCandidateIDIsExist = common.cloneObject(query.checkCandidateIDIsExistQuery);
  checkCandidateIDIsExist.filter.and[0].value = candidateID;
  return await common.executeQuery(checkCandidateIDIsExist);
}

var getCandidateDetail = async function(candidateID){
  debug("candidate.DAL -> getCandidateDetail");
  var getCandidateDetail = common.cloneObject(query.getCandidateDetailQuery);
  getCandidateDetail.filter.and[0].value = candidateID;
  return await common.executeQuery(getCandidateDetail);
}

var getTrainingPartnerIdFromCandidateID = async function(candidateid){
  debug("candidate.DAL -> getTrainingPartnerIdFromCandidateID");
  var getTrainingPartnerIdFromCandidateID = common.cloneObject(query.getTrainingPartnerIdFromCandidateIDQuery);
  getTrainingPartnerIdFromCandidateID.filter.and[0].value = candidateid;
  return await common.executeQuery(getTrainingPartnerIdFromCandidateID);
}

var updateCandidateBulkUpload = async function(id, fieldValue){
  debug("candidate.DAL -> updateCandidateBulkUpload");
  var updateCandidateBulkUpload = common.cloneObject(query.updateCandidateBulkUploadQuery);
  updateCandidateBulkUpload.filter.and[0].value = id;
  updateCandidateBulkUpload.update = fieldValue;
  return await common.executeQuery(updateCandidateBulkUpload);
}

var getCandidateStateCityFromBulkUplod = async function(){
  debug("candidate.DAL -> getCandidateStateCityFromBulkUplod");
  var getCandidateStateCityFromBulkUplod = common.cloneObject(query.getCandidateStateCityFromBulkUplodQuery);
  getCandidateStateCityFromBulkUplod.limit = constant.appConfig.CRON_CANDIDATE_BULK_RECORDS_LIMIT;
  return await common.executeQuery(getCandidateStateCityFromBulkUplod);
}

var getCandidateSchemeTypeFromBulkUpload = async function(){
  debug("candidate.DAL -> getCandidateSchemeTypeFromBulkUpload");
  var getCandidateSchemeTypeFromBulkUpload = common.cloneObject(query.getCandidateSchemeTypeFromBulkUploadQuery);
  getCandidateSchemeTypeFromBulkUpload.limit = constant.appConfig.CRON_CANDIDATE_BULK_RECORDS_LIMIT;
  return await common.executeQuery(getCandidateSchemeTypeFromBulkUpload);
}

var getCandidatesFromBulkUpload = async function () {
  debug("candidate.DAL -> getCandidatesFromBulkUpload");
  var getCandidatesFromBulkUpload = common.cloneObject(query.getCandidatesFromBulkUploadQuery);
  getCandidatesFromBulkUpload.limit = constant.appConfig.CRON_CANDIDATE_BULK_RECORDS_LIMIT;
  return await common.executeQuery(getCandidatesFromBulkUpload);
}

var getCandidateViewMoreDetail = async function(candidateID){
  debug("candidate.DAL -> getCandidateViewMoreDetail");
  var getCandidateViewMoreDetail = common.cloneObject(query.getCandidateViewMoreDetailQuery);
  getCandidateViewMoreDetail.filter.and[0].value = candidateID;
  return await common.executeQuery(getCandidateViewMoreDetail);
}

module.exports = {
  checkCandidateIsExist: checkCandidateIsExist,
  createCandidate: createCandidate,
  getCandidatesCount : getCandidatesCount,
  getCandidates: getCandidates,
  updateCandidateById : updateCandidateById,
  checkCandidateIDIsExist : checkCandidateIDIsExist,
  getCandidateDetail : getCandidateDetail,
  getTrainingPartnerIdFromCandidateID : getTrainingPartnerIdFromCandidateID,
  getCandidateStateCityFromBulkUplod : getCandidateStateCityFromBulkUplod,
  updateCandidateBulkUpload : updateCandidateBulkUpload,
  getCandidateSchemeTypeFromBulkUpload : getCandidateSchemeTypeFromBulkUpload,
  getCandidatesFromBulkUpload : getCandidatesFromBulkUpload,
  getCandidateViewMoreDetail : getCandidateViewMoreDetail,
}
