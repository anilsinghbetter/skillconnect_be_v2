var express = require('express');
var services = require('./candidate.service');
var middleware = require('../../../middleware');

var router = express.Router();
module.exports = router;

router.post('/addUpdateCandidate/:type', middleware.logger,middleware.checkAccessToken, services.addUpdateCandidateService);
router.post('/getCandidates/:type', middleware.logger, services.getCandidateService);
router.post('/getCandidatesForAdmin', middleware.checkAccessToken,middleware.userRightsByAPI,middleware.logger,services.getCandidatesForAdminService); 

router.post('/getCandidateAddFormData/:type', middleware.logger, services.getCandidateAddFormDataService);
router.post('/getCandidateDetail/:id/:type', middleware.logger, services.getCandidateDetailForWebService);//Get candidate detail for EDIT Candidate only
router.post('/get-candidate-viewmoredetail/:id/:type', middleware.logger, services.getCandidateDetailService);//View More Popup detail for candidate
router.post('/addupdate-candidate-multiplerecords', middleware.logger, services.updateMultipleRecordsService);
router.post('/get-zipwise-candidate', middleware.logger, services.getZipwiseCandidateService);
router.post('/get-zipwise-drivers', middleware.logger, services.getZipwiseDriversService);


//cron urls
router.post('/candidateBulkUploadCheckStateCityService', services.candidateBulkUploadCheckStateCityService);
router.post('/candidateBulkUploadCheckSchemeTypeService', services.candidateBulkUploadCheckSchemeTypeService);
router.post('/processCandidateCSVCronService', services.processCandidateCSVCronService);