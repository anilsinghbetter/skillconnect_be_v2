var debug = require('debug')('server:api:v1:candidate:service');
var d3 = require("d3");
var DateLibrary = require('date-management');
var bcrypt = require('bcrypt');
var randomize = require('randomatic');
var uuid = require('uuid');
var common = require('../common');
var constant = require('../constant');
var candidateDAL = require('./candidate.DAL');
var otherDAL = require('../other/other.DAL');
var sectorDAL = require('../sector/sector.DAL');
var trainingCenterDAL = require('../trainingcenter/trainingcenter.DAL');
var dbDateFormat = constant.appConfig.DB_DATE_FORMAT;
var dateFormat = require('dateformat');
var config = require('../../../../config');
var connection = require('../../../helper/connection');
var QueryBuilder = require('../../../helper/node-datatable-master');
var async = require("async");


var getCandidatesForAdminService = async function (request, response) {
  debug("candidate.service -> getCandidatesForAdminService");
  var params = request.body;

  var queryString = "";
   if(!!params.phone) queryString +=" AND (`phone` = "+ "'"+params.phone+"'";
   if(!!params.candidate_name) queryString +=" OR `candidate_name` LIKE "+ "'%"+params.candidate_name+"%'";
   if(!!params.sdmsnsdc_enrollment_number) queryString +=" OR `sdmsnsdc_enrollment_number` LIKE "+ "'%"+params.sdmsnsdc_enrollment_number+"%'";
   //if(!!params.email_address) queryString +=" OR `email_address` = "+ "'"+params.email_address+"'";
   if(!!params.training_status) queryString +=" OR `training_status` = "+ "'"+params.training_status+"'";
   if(!!params.batch_id) queryString +=" OR `batch_id` LIKE "+ "'%"+params.batch_id+"%')";

  //return;
  var oTableDef = {
    sCountColumnName: "distinct pk_candidateid",
    sTableName: 'tbl_Candidates',
    sWhereAndSql: "is_deleted = '0'" + queryString,
    sGroupColumnName: "pk_candidateid",
    sSelectSql: "pk_candidateid as candidateid,candidate_name,sdmsnsdc_enrollment_number,batch_id,training_status,status",
    //sSelectSql: "SCHEME.schemeType as `schemeType`,SEC.sectorid as `sectorid`,SEC.sectorName as `sectorName`,SEC.jobroleid as `jobroleid`,SEC.jobroleName as `jobroleName`,STATE.stateName as `stateministry`,SCM.fk_centralministryID as `centralministryid`,TRC.trainingcenter_name as `trainingcenter_name`,TRC.pk_trainingcenterid as `trainingcenter_id`,CEMI.name as `centralministry`,LD.stateid as `stateid`,LD.stateName as `stateName`,LD.cityid as `cityid`,LD.cityName as `cityName`,CD.pk_candidateid as candidateid,CD.candidate_name as candidate_name,CD.aadhaar_number as aadhaar_number,CD.aadhaar_enrollment_id as aadhaar_enrollment_id,CD.alternateidtype as alternateidtype,CD.alternate_id_number as alternate_id_number,CD.phone as phone,CD.email_address as email_address,DATE_FORMAT(CD.dob,'%d %b %Y') as dob,CD.gender as gender,CD.address as address,CD.pincode as pincode,CD.experience_in_years as experience_in_years,CD.sdmsnsdc_enrollment_number as sdmsnsdc_enrollment_number,CD.training_status as training_status,CD.attendence_percentage as attendence_percentage,CD.placement_status as placement_status,CD.employment_type as employment_type,CD.assessment_score as assessment_score,CD.max_assessment_score as max_assessment_score,DATE_FORMAT(CD.assessment_date,'%d %b %Y') as assessment_date,CD.training_type as training_type,CD.batch_id as batch_id,DATE_FORMAT(CD.batch_start_date,'%d %b %Y') as batch_start_date,DATE_FORMAT(CD.batch_end_date,'%d %b %Y') as batch_end_date,CD.is_willing_to_relocate as willing_to_relocate,CD.status as status,CD.fk_schemeID as fk_schemeID,CD.fk_ssdm as fk_ssdm,CD.fk_trainingpartnerid as fk_trainingpartnerid",
    //sFromSql: "tbl_Candidates as CD LEFT JOIN `view_GetLocationDetail` as LD ON (`LD`.`stateid` = `CD`.`fk_stateID` and `LD`.`cityid` = `CD`.`fk_cityID`) LEFT JOIN `view_SectorJobRoleDetail` as SEC ON ( FIND_IN_SET (`SEC`.`sectorid`,CD.fk_sectorID) and `SEC`.`jobroleid` = `CD`.`fk_jobroleID`) LEFT JOIN `tbl_SchemeMaster` as SCHEME ON (`SCHEME`.`pk_schemeID`= `CD`.`fk_schemeID`) LEFT JOIN `tbl_StateMaster` as STATE ON (`STATE`.`pk_stateID` = `CD`.`fk_ssdm`) LEFT JOIN `tbl_SchemeCentralMinistryMapping` as SCM ON (`SCM`.`fk_schemeID` = `CD`.`fk_schemeID`) LEFT JOIN `tbl_CentralministriesMaster` as CEMI ON (`CEMI`.`pk_ID` = `SCM`.`fk_centralministryID`) LEFT JOIN `tbl_TrainingCenters` as TRC ON (`TRC`.`pk_trainingcenterid` = `CD`.`fk_trainingcenterid`)",
  };
  // console.log(params,"PARAMS #2");
  var queryBuilder = new QueryBuilder(oTableDef);
  var queries = queryBuilder.buildQuery(params);
  queries.recordsTotal = "SELECT COUNT(*) as totalCount FROM `tbl_Candidates` WHERE is_deleted = '0'";
  var newres = {};
  if (queries.recordsFiltered) {
    var resFiltered1 = await connection.executeRawQuery(queries.recordsFiltered);
    //queries.recordsTotal = result.content;
    var resFilterNew = Object.values(resFiltered1.content[0]);
    //console.log('#1', resFilterNew);
  }
  var resTotal2 = await connection.executeRawQuery(queries.recordsTotal);
  //queries.recordsTotal = result.content;
  //console.log('#2', resTotal2.content[0].totalCount);
  // console.log(queries.select,"PARAMS #2");
  var resSelect3 = await connection.executeRawQuery(queries.select);
  //console.log('#3', resSelect3.content);

  newres = { recordsFiltered: resFilterNew[0], recordsTotal: resTotal2.content[0].totalCount, data: resSelect3.content }
  return common.sendResponse(response, newres);
}

/**
 * Add candidate
 * 
 * @param {object} request 
 * @param {object} response
 * @return {object} 
 */
var addUpdateCandidateService = async function (request, response) {
  debug("candidate.service -> addUpdateCandidateService");
  //console.log('# req body',request.body);
  if (request.body.candidateid == '-1') {
    var isValid = common.validateParams([
      request.body.districtname, //district
      request.body.statename, //state
      request.body.sectorname,
      request.body.jobrole,
      request.body.schemename,
      request.body.trainingpartnerid,
      request.body.training_center,
      request.body.name, //candidate name
      request.body.age, //age
      request.body.phone, //mobile no
      request.body.sex, //gender
      request.body.address, //address
      request.body.pincode, //pincode
      request.body.nsdc_enrollment_no, //nsdc_enrollment_no
      request.body.training_status, //training_status
      request.body.training_type, //training_type
      request.body.assessment_date,
      request.body.batch_id,
      request.body.batch_start_date,
      request.body.batch_end_date,
    ]);
    if (!isValid) {
      return common.sendResponse(response, constant.requestMessages.ERR_INVALID_ADD_CANDIDATE_REQUEST, false);
    }
  }
  common.sanitizeAll(request.body);

  //Check Aadhar Card Number length
  if (request.body.aadhar_card_no != undefined && request.body.aadhar_card_no != '' && request.body.aadhar_card_no.length != constant.appConfig.AADHAR_CARD_NUMBER_LENGTH) {
    return common.sendResponse(response, constant.requestMessages.ERR_AADHAR_CARD_LENGTH, false);
  }

  //Check Phone Number length
  if (request.body.phone != undefined && request.body.phone.length != constant.appConfig.MOBILE_NUMBER_LENGTH) {
    return common.sendResponse(response, constant.requestMessages.ERR_MOBILE_NUMBER_LENGTH, false);
  }

  //check state district combination
  if (request.body.statename !== undefined) {
    if(request.body.districtname === undefined){
      return common.sendResponse(response, constant.requestMessages.ERR_SELECT_STATEDISTRICT, false);
    }
    var checkStateCityQuery = 'SELECT count(stateid) as totalCount FROM `view_GetLocationDetail` WHERE  `stateid` = '+request.body.statename+' AND `cityid` = '+request.body.districtname;
    var StateCityRes =  await connection.executeRawQuery(checkStateCityQuery);
   
    if(StateCityRes.status === true){
      if(StateCityRes.content[0].totalCount == 0){
        return common.sendResponse(response, constant.requestMessages.ERR_COMBINATION_DISTRICT, false);
      } 
    }
  }

  //check sector jobrole combination
  if (request.body.sectorname !== undefined) {
    if(request.body.jobrole === undefined){
      return common.sendResponse(response, constant.requestMessages.ERR_SELECT_JOBROLE, false);
    }
    var checkSectorJobroleQuery = 'SELECT count(sectorid) as totalCount FROM `view_SectorJobRoleDetail` WHERE  `sectorid` = '+request.body.sectorname+' AND `jobroleid` = '+request.body.jobrole;
    var SectorJobroleRes =  await connection.executeRawQuery(checkSectorJobroleQuery);
   
    if(SectorJobroleRes.status === true){
      if(SectorJobroleRes.content[0].totalCount == 0){
        return common.sendResponse(response, constant.requestMessages.ERR_COMBINATION_JOBROLE, false);
      } 
    }
  }

  //Check if schemename == 5 then validate state scheme name
  if(request.body.schemename != undefined && request.body.schemename === '5'){
    if(request.body.state_scheme_name == undefined){
      return common.sendResponse(response, constant.requestMessages.ERR_STATE_SCHEME_NAME, false);
    }
    if(request.body.state_scheme_name === '' || request.body.state_scheme_name === null || request.body.state_scheme_name === 'null'){
      return common.sendResponse(response, constant.requestMessages.ERR_STATE_SCHEME_NAME, false);
    }
  }

  var candidateID = request.body.candidateid;
  var candidateinfo = {};
  candidateinfo.fk_trainingpartnerid = request.body.trainingpartnerid;
  if (request.body.training_center != undefined) {
    candidateinfo.fk_trainingcenterid = request.body.training_center;
  }
  if (request.body.districtname != undefined) {
    candidateinfo.fk_cityID = request.body.districtname;
  }
  if (request.body.statename != undefined) {
    candidateinfo.fk_stateID = request.body.statename;
  }
  if (request.body.sectorname != undefined) {
    candidateinfo.fk_sectorID = request.body.sectorname;
  }
  if (request.body.jobrole != undefined) {
    candidateinfo.fk_jobroleID = request.body.jobrole;
  }
  if (request.body.schemename != undefined) {
    candidateinfo.fk_schemeID = request.body.schemename;
  }
  if (request.body.state_scheme_name != undefined) {
    candidateinfo.fk_ssdm = request.body.state_scheme_name;
  }
  if (request.body.name != undefined) {
    candidateinfo.candidate_name = request.body.name;
  }
  if (request.body.aadhar_card_no != undefined) {
    candidateinfo.aadhaar_number = request.body.aadhar_card_no;
  }
  if (request.body.age != undefined) {
    candidateinfo.age = request.body.age;
  }
  if (request.body.candidate_id != undefined) {
    candidateinfo.candidate_id = request.body.candidate_id;
  }
  if (request.body.aadhaar_enrollment_id != undefined) {
    candidateinfo.aadhaar_enrollment_id = request.body.aadhaar_enrollment_id;
  }
  if (request.body.alternate_id_type != undefined) {
    candidateinfo.alternateidtype = request.body.alternate_id_type;
  }
  if (request.body.altername_id_no != undefined) {
    candidateinfo.alternate_id_number = request.body.altername_id_no;
  }
  if (request.body.phone != undefined) {
    candidateinfo.phone = request.body.phone;
  }
  if (request.body.email_address != undefined) {
    candidateinfo.email_address = request.body.email_address;
  }
  if (request.body.dob != undefined) {
    candidateinfo.dob = request.body.dob != '' ? d3.timeFormat(dbDateFormat)(new Date(request.body.dob)) : null;
  }
  if (request.body.sex != undefined) {
    candidateinfo.gender = request.body.sex;
  }
  if (request.body.address != undefined) {
    candidateinfo.address = request.body.address;
  }
  if (request.body.pincode != undefined) {
    candidateinfo.pincode = request.body.pincode;
  }
  if (request.body.exp_in_years != undefined) {
    candidateinfo.experience_in_years = request.body.exp_in_years != '' ? parseFloat(request.body.exp_in_years).toFixed(1) : null;
  }
  if (request.body.nsdc_enrollment_no != undefined) {
    candidateinfo.sdmsnsdc_enrollment_number = request.body.nsdc_enrollment_no;
  }
  if (request.body.training_status != undefined) {
    candidateinfo.training_status = request.body.training_status;
  }
  if (request.body.attendance_percent != undefined) {
    candidateinfo.attendence_percentage = request.body.attendance_percent != '' ? parseFloat(request.body.attendance_percent).toFixed(2) : null;
  }
  if (request.body.placement_status != undefined) {
    candidateinfo.placement_status = request.body.placement_status != '' ? request.body.placement_status : null;
  }
  if (request.body.employment_type != undefined) {
    candidateinfo.employment_type = request.body.employment_type != '' ? request.body.employment_type : null;
  }
  if (request.body.assessment_score != undefined) {
    candidateinfo.assessment_score = request.body.assessment_score != '' ? parseFloat(request.body.assessment_score).toFixed(2) : null;
  }
  if (request.body.max_assessment_score != undefined) {
    candidateinfo.max_assessment_score = request.body.max_assessment_score != '' ? parseFloat(request.body.max_assessment_score).toFixed(2) : null;
  }
  if (request.body.assessment_date != undefined) {
    candidateinfo.assessment_date = d3.timeFormat(dbDateFormat)(new Date(request.body.assessment_date));
  }
  if (request.body.training_type != undefined) {
    candidateinfo.training_type = request.body.training_type;
  }
  if (request.body.batch_id != undefined) {
    candidateinfo.batch_id = request.body.batch_id;
  }
  if (request.body.willing_to_relocate != undefined) {
    candidateinfo.is_willing_to_relocate = request.body.willing_to_relocate;
  }
  if (request.body.batch_start_date != undefined) {
    candidateinfo.batch_start_date = d3.timeFormat(dbDateFormat)(new Date(request.body.batch_start_date));
  }
  if (request.body.batch_end_date != undefined) {
    candidateinfo.batch_end_date = d3.timeFormat(dbDateFormat)(new Date(request.body.batch_end_date));
  }
  if (request.body.is_willing_to_relocate != undefined) {
    candidateinfo.is_willing_to_relocate = request.body.is_willing_to_relocate;
  }
  console.log(candidateinfo);
  
  var candidateKeys = Object.keys(candidateinfo);

  var fieldValueInsert = [];
  candidateKeys.forEach(function (candidateKeys) {
    if (candidateinfo[candidateKeys] !== undefined) {
      var fieldValueObj = {};
      fieldValueObj = {
        field: candidateKeys,
        fValue: candidateinfo[candidateKeys]
      }
      fieldValueInsert.push(fieldValueObj);
    }
  });

  try {
    //While adding new candidate we are sending candidate as -1
    if (candidateID == -1) {
      var result = await candidateDAL.checkCandidateIsExist(candidateinfo.sdmsnsdc_enrollment_number);
      if (result.content.length > 0 && result.content[0].candidateid > 0) {
        return common.sendResponse(response, constant.userMessages.ERR_CANDIDATE_IS_ALREADY_EXIST, false);
      }
      var res_create_candidate = await candidateDAL.createCandidate(fieldValueInsert);
      return common.sendResponse(response, constant.userMessages.MSG_CANDIDATE_ADD_SUCCESSFULLY, true);
    } else {
      var result = await candidateDAL.checkCandidateIDIsExist(candidateID);
      if (result.content.length == 0) {
        return common.sendResponse(response, constant.userMessages.ERR_CANDIDATE_NOT_EXIST, false);
      } else if (result.content.length > 0 && result.content[0].candidateid == candidateID) {
        if (candidateinfo.sdmsnsdc_enrollment_number != undefined) {
          var result = await candidateDAL.checkCandidateIsExist(candidateinfo.sdmsnsdc_enrollment_number);
          if (result.content.length > 0) {
            if (result.content[0].candidateid != candidateID) {
              return common.sendResponse(response, constant.userMessages.ERR_CANDIDATE_IS_ALREADY_EXIST, false);
            } else {
              var res_update_candidate = await candidateDAL.updateCandidateById(candidateID, fieldValueInsert);
              return common.sendResponse(response, constant.userMessages.MSG_CANDIDATE_UPDATE_SUCCESSFULLY, true);
            }
          } else {
            var res_update_candidate = await candidateDAL.updateCandidateById(candidateID, fieldValueInsert);
            return common.sendResponse(response, constant.userMessages.MSG_CANDIDATE_UPDATE_SUCCESSFULLY, true);
          }
        } else {
          var res_update_candidate = await candidateDAL.updateCandidateById(candidateID, fieldValueInsert);
          return common.sendResponse(response, constant.userMessages.MSG_CANDIDATE_UPDATE_SUCCESSFULLY, true);
        }
      }
    }
  } catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

/**
 * get candidate
 * 
 * @param {object} request 
 * @param {object} response
 * @return {object} 
 */
var getCandidateService = async function (request, response) {
  debug("candidate.service -> getCandidateService");

  //Employer Search for the State and city will go to the Training Center
  //SELECT GROUP_CONCAT(pk_trainingcenterid)  FROM `tbl_TrainingCenters` WHERE `fk_stateID` = 27 and fk_cityID = 464 and status = 1 and is_deleted = 0
  //SELECT * FROM `tbl_Candidates` WHERE fk_trainingcenterid IN (6,7,8,9,10,11,12,13,14,15,16,17,18,19)
  //Query Ends here

  var getPaginationObject = common.getPaginationObject(request);
  var limit = getPaginationObject.limit;
  var searchObj = {};
  if (request.body !== undefined) {
    common.sanitizeAll(request.body);
    //Update the search obj according to the table's Colmun name
    if (request.body.trainingpartnerid !== undefined) {
      request.body.fk_trainingpartnerid = request.body.trainingpartnerid;
      delete request.body.trainingpartnerid;
    }
    if (request.body.relocate !== undefined) {
      request.body.is_willing_to_relocate = request.body.relocate;
      delete request.body.relocate;
    }
    if (request.body.willing_to_relocate !== undefined) {
      request.body.is_willing_to_relocate = request.body.willing_to_relocate;
      delete request.body.willing_to_relocate;
    }
    if (request.body.sex !== undefined) {
      request.body.gender = request.body.sex;
      delete request.body.sex;
    }
    if (request.body.sectorid !== undefined) {
      request.body.fk_sectorID = request.body.sectorid;
      delete request.body.sectorid;
    }
    if (request.body.jobroleid !== undefined) {
      request.body.fk_jobroleID = request.body.jobroleid;
      delete request.body.jobroleid;
    }
    if (request.body.training_centre !== undefined) {
      request.body.fk_trainingcenterid = request.body.training_centre;
      delete request.body.training_centre;
    }
    if (request.body.trainingcenter_id !== undefined) {
      request.body.fk_trainingcenterid = request.body.trainingcenter_id;
      delete request.body.trainingcenter_id;
    }
    if (request.body.statename !== undefined) {
      request.body.fk_stateID = request.body.statename;
      delete request.body.statename;
    }
    if (request.body.stateid !== undefined) {
      request.body.fk_stateID = request.body.stateid;
      delete request.body.stateid;
    }
    if (request.body.districtname !== undefined) {
      request.body.fk_cityID = request.body.districtname;
      delete request.body.districtname;
    }
    if (request.body.cityid !== undefined) {
      request.body.fk_cityID = request.body.cityid;
      delete request.body.cityid;
    }
    if (request.body.batch_start_date !== undefined) {
      request.body.batch_start_date = d3.timeFormat(constant.appConfig.FP_DATE_FORMAT)(new Date(request.body.batch_start_date));
    }
    if (request.body.batch_end_date !== undefined) {
      request.body.batch_end_date = d3.timeFormat(constant.appConfig.FP_DATE_FORMAT)(new Date(request.body.batch_end_date));
    }
    if (request.body.has_joined !== undefined) {
      request.body.has_joined = request.body.has_joined; // Candidate should not come in search results if he has already joined for any Employer so it should be always 0 
    }  
    searchObj = request.body;
  }
  try {
    let res_count = await candidateDAL.getCandidatesCount(searchObj, request.params.type);
    var totalCandidates = res_count.content[0].candidatecount;
    let result = await candidateDAL.getCandidates(limit, searchObj, request.params.type);
    // console.log(result.content,"RETURNED CANDIDDATES");
    return common.sendResponse(response, result.content, true, common.getPaginationOptions(request, totalCandidates));
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.ERR_NO_CANDIDATES_FOUND, false);
  }
}
var getCandidateAddFormDataService = async function (request, response) {
  debug("candidate.service -> getCandidateAddFormDataService");
  if (request.body.type == 'EMP') {
    var getSectorsResponse = await sectorDAL.getSectorsByIndustryPartnerID(request.body.trainingpartnerid);
  } else {
    var getSectorsResponse = await sectorDAL.getSectorsByTrainingPartnerID(request.body.trainingpartnerid);
  }
  if (getSectorsResponse.status == true && getSectorsResponse.content.length > 0 && getSectorsResponse.content[0].sectorName != null) {
    var result = { state: [], sector: [], scheme: [], alternateidtype: [], gender: [], training_status: [], placement_status: [], employment_status: [], training_type: [], training_centre: [] };
    //state
    var stateResult = await common.getStateFromView();
    if (stateResult.status == true && stateResult.content.length > 0) {
      result.state.push(stateResult.content);
    }
    //city
    /* var cityResult = await common.getCityFromView(1);
    if (cityResult.status == true && cityResult.content.length > 0){
      result.city.push(cityResult.content);
    } */

    //sector
    var sectorsOfTrainingPartner = getSectorsResponse.content[0].sectorName.split('#');
    var sectorsIdsOfTrainingPartner = getSectorsResponse.content[0].sectorid.split(',');
    var fieldValueUpdate = [];
    sectorsIdsOfTrainingPartner.forEach(function (id, index) {
      fieldValueUpdate.push({
        "sectorid": id,
        "sectorname": sectorsOfTrainingPartner[index]
      });
    });
    result.sector.push(fieldValueUpdate);

    //Training center
    var getTrainingCentersResponse = await trainingCenterDAL.getTrainingCentersByTrainingPartnerID(request.body.trainingpartnerid);
    if (getTrainingCentersResponse.status == true && getTrainingCentersResponse.content.length > 0) {
      result.training_centre.push(getTrainingCentersResponse.content);
    }

    //var sectorResult = await sectorDAL.getSector('web');

    //jobroles
    //var jobroleResult = await common.getJobroleFromView(1);

    //scheme master
    var schemeResult = await common.getSchemeTypes();
    if (schemeResult.status == true && schemeResult.content.length > 0) {
      result.scheme.push(schemeResult.content);
    }

    //alternateidtype
    var alternateidtypeResult = constant.appConfig.ALTERNATE_ID_TYPE;
    result.alternateidtype.push(alternateidtypeResult);
    //gender
    var gendertypeResult = constant.appConfig.GENDER_TYPE;
    result.gender.push(gendertypeResult);

    //TRAINING_STATUS
    var trainingstatustypeResult = constant.appConfig.TRAINING_STATUS_TYPE;
    result.training_status.push(trainingstatustypeResult);

    //placement status
    var placementstatustypeResult = constant.appConfig.PLACEMENT_STATUS_TYPE;
    result.placement_status.push(placementstatustypeResult);
    //employment status
    var employmentstatustypeResult = constant.appConfig.EMPLOYMENT_STATUS_TYPE;
    result.employment_status.push(employmentstatustypeResult);

    //training type
    var trainingtypeResult = constant.appConfig.TRAINING_TYPE;
    result.training_type.push(trainingtypeResult);

    return common.sendResponse(response, result, true);
  } else {
    return common.sendResponse(response, constant.userMessages.ERR_NO_ADD_CANDIDATE_FORM_DATA_FOUND, false);
  }

}


/**
 * When user wants to perform the actions like active/inactice/delete with multiple records
 * 
 * @param {*} request 
 * @param {*} response
 * @return {*} object 
 */
var updateMultipleRecordsService = async function (request, response) {
  debug("candidate.service -> updateMultipleRecordsService");
  var isValid = common.validateParams([request.body.ids, request.body.action]);
  if (!isValid) {
    return common.sendResponse(response, constant.userMessages.INVALID_ADD_UPDATE_CANDIDATE_REQUEST, false);
  }
  var fieldValueInsert = [];

  var fieldValueObj = {};
  switch (request.body.action) {
    case 'active':
      fieldValueObj = {
        field: 'status',
        fValue: 1
      }
      break;
    case 'inactive':
      fieldValueObj = {
        field: 'status',
        fValue: 0
      }
      break;
    case 'delete':
      fieldValueObj = {
        field: 'is_deleted',
        fValue: 1
      }
      break;
  }

  fieldValueInsert.push(fieldValueObj);

  try {
    request.body.ids.forEach(async function (id, index) {
      var result = await candidateDAL.updateCandidateById(id, fieldValueInsert);
      if (request.body.ids.length - 1 == index) {
        return common.sendResponse(response, constant.userMessages.MSG_CANDIDATE_UPDATE_SUCCESSFULLY, true);
      }
    });
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

var getCandidateDetailService = async function (request, response) {
  debug("candidate.service -> getCandidateDetailService");
  var isValid = common.validateParams([request.params.id]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_GET_DETAIL_REQUEST, false);
  }
  try {
    let result = await candidateDAL.getCandidateViewMoreDetail(request.params.id);
    if (result.status === false) {
      return common.sendResponse(response, constant.userMessages.ERR_NO_CANDIDATE_FOUND, false);
    } else if (result.status === true && result.content.length > 0) {
      function setDisplayName(displayValue){
      
        displayValue = displayValue.charAt(0).toUpperCase() + displayValue.substr(1);
        
        switch(displayValue){
          case 'Voterid':
            return 'Voter ID';
          case 'Drivinglicense':
            return 'Driving License';
          case 'Duplicateenrollment':
            return 'Duplicate Enrollment';
          case 'Notplaced':
            return 'Not Placed';
          case 'Notinterested':
            return 'Not Interested';
          case 'Selfemployment':
            return 'Self Employment';
          case 'Wageemployment':
            return 'Wage Employment';
          case 'Selfemployed':
            return 'Self Employed';
          case 'Upskilled':
            return 'Up Skilled';
          case 'Higherstudies':
            return 'Higher Studies'
          case 'Freshskilling':
            return 'Fresh Skilling'
          default:
            return displayValue;
        }
      }
      //to convert value into display values in candidate details
      for(candidateData of result.content){
        {(candidateData.alternateidtype !== null && candidateData.alternateidtype !== undefined) ? candidateData.alternateidtype = setDisplayName(candidateData.alternateidtype) : candidateData.alternateidtype};
        {(candidateData.gender !== null & candidateData.gender !== undefined) ? candidateData.gender = setDisplayName(candidateData.gender) : candidateData.gender};
        {(candidateData.placement_status !== null && candidateData.placement_status !== undefined)  ? candidateData.placement_status = setDisplayName(candidateData.placement_status) : candidateData.placement_status};
        {(candidateData.training_type !== null && candidateData.training_type !== undefined) ? candidateData.training_type = setDisplayName(candidateData.training_type) : candidateData.training_type};
        {(candidateData.training_status !== null && candidateData.training_status !== undefined) ? candidateData.training_status = setDisplayName(candidateData.training_status) : candidateData.training_status};
        {(candidateData.employment_type !== null && candidateData.employment_type !== undefined) ? candidateData.employment_type = setDisplayName(candidateData.employment_type) : candidateData.employment_type};
        {(candidateData.willing_to_relocate !== null && candidateData.willing_to_relocate !== undefined) ? candidateData.willing_to_relocate = setDisplayName(candidateData.willing_to_relocate) : candidateData.willing_to_relocate};
      }
      if(request.params.type == 'admin'){
        response.render("partials/candidatedetail.ejs", { data: result.content[0] }, function (err, html) {
          response.send(html);
        });         
      } else {
        return common.sendResponse(response, result.content, true);
      }
    } else {
      return common.sendResponse(response, constant.userMessages.ERR_NO_CANDIDATE_FOUND, false);
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

var getCandidateDetailForWebService = async function (request, response) {
  debug("candidate.service -> getCandidateDetailForWebService");
  var isValid = common.validateParams([request.params.id]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_GET_DETAIL_REQUEST, false);
  }
  try {
    let result = await candidateDAL.getCandidateDetail(request.params.id);
    if (result.status === false) {
      return common.sendResponse(response, constant.userMessages.ERR_NO_CANDIDATE_FOUND, false);
    } else if (result.status === true && result.content.length > 0) {
      return common.sendResponse(response, result.content, true);
    } else {
      return common.sendResponse(response, constant.userMessages.ERR_NO_CANDIDATE_FOUND, false);
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

//cron to check state and city ids from candidate_bulk_upload table
var candidateBulkUploadCheckStateCityService = async function () {
  var getUsers = await candidateDAL.getCandidateStateCityFromBulkUplod();
  //console.log(getUsers);
  if (getUsers.status == true && getUsers.content.length > 0) {
    for (var i = 0; i < getUsers.content.length; i++) {
      var autoIncId = getUsers.content[i].id;
      var statename = getUsers.content[i].state;
      var cityname = getUsers.content[i].district;

      var result = await otherDAL.checkCityStateView(statename, cityname);

      if (result.status == true && result.content.length > 0 && result.content[0].stateid > 0 && result.content[0].cityid > 0) {
        var state = result.content[0].stateid;//state
        var district = result.content[0].cityid;//city
        if (state > 0 && district > 0) {
          var fieldValueUpdate = [];
          fieldValueUpdate.push({
            field: "state",
            fValue: state
          });
          fieldValueUpdate.push({
            field: "district",
            fValue: district
          });

          var result = await candidateDAL.updateCandidateBulkUpload(autoIncId, fieldValueUpdate);
          if (result.status == true && result.content.changedRows == 1) {
            var fieldValueUpdate = [];
            fieldValueUpdate.push({
              field: "is_statecity_done",
              fValue: 1
            });
            var result = await candidateDAL.updateCandidateBulkUpload(autoIncId, fieldValueUpdate);
            console.log(result);
          }
        } else {
          console.log('No state / city found');

        }
      } else {
        console.log('No state / city found');
      }
    }
  } else {
    console.log('No records');
  }
}

var candidateBulkUploadCheckSchemeTypeService = async function () {
  var getUsers = await candidateDAL.getCandidateSchemeTypeFromBulkUpload();

  if (getUsers.status == true && getUsers.content.length > 0) {
    for (var i = 0; i < getUsers.content.length; i++) {
      var autoIncId = getUsers.content[i].id;
      var SchemeType = getUsers.content[i].scheme_type;
      var StateMinistry = getUsers.content[i].state_skill_development_mission;

      var result = await otherDAL.checkSchemeExistIntoMaster(SchemeType);
      if (result.status == true && result.content.length > 0 && result.content[0].pk_schemeID > 0) {
        var SchemeId = result.content[0].pk_schemeID;//Scheme Type
        var state_skill_development_mission = 0;
        if (SchemeId > 0) {
          if (SchemeId == 5) {
            //Check for the state ministry
            var stateSkillDevelopmentMissionResponse = await otherDAL.checkState(StateMinistry);
            if (stateSkillDevelopmentMissionResponse.status == true && stateSkillDevelopmentMissionResponse.content.length > 0 && stateSkillDevelopmentMissionResponse.content[0].stateid > 0) {
              state_skill_development_mission = stateSkillDevelopmentMissionResponse.content[0].stateid;
            }

            var fieldValueUpdate = [];
            fieldValueUpdate.push({
              field: "state_skill_development_mission",
              fValue: state_skill_development_mission
            });
            var result = await candidateDAL.updateCandidateBulkUpload(autoIncId, fieldValueUpdate);
          }

          var fieldValueUpdate = [];
          fieldValueUpdate.push({
            field: "scheme_type",
            fValue: SchemeId
          });

          var result = await candidateDAL.updateCandidateBulkUpload(autoIncId, fieldValueUpdate);
          if (result.status == true && result.content.changedRows == 1) {
            var fieldValueUpdate = [];
            fieldValueUpdate.push({
              field: "is_scheme_state_done",
              fValue: 1
            });
            var result = await candidateDAL.updateCandidateBulkUpload(autoIncId, fieldValueUpdate);
            console.log(result);
          }
        } else {
          console.log('No Scheme Found');

        }
      } else {
        console.log('No Scheme Found');
      }
    }
  } else {
    console.log('No records');
  }
}

var processCandidateCSVCronService = async function () {
  //Query:SELECT * FROM `candidate_bulk_upload` WHERE `is_processed` = 0 AND `is_sector_done` = 1 AND `is_statecity_done` = 1 AND 'is_scheme_state_done' ORDER BY id ASC
  //Step 1 :  Dump into tbl_Candidates  the records
  //Step 3 : Update is_processed flag to 1 which are successfully inserted to the tbl_Candidates.

  debug("candidate.service -> processCandidateCSVCronService");
  var getUsers = await candidateDAL.getCandidatesFromBulkUpload();
  //console.log(getUsers);
  if (getUsers.status == true && getUsers.content.length > 0) {

    for (var i = 0; i < getUsers.content.length; i++) {
      var autoIncId = getUsers.content[i].id;
      //Remove spaces from Alternate ID Type 
      getUsers.content[i].alternateidtype = (getUsers.content[i].alternateidtype != '' && getUsers.content[i].alternateidtype != null)  ? getUsers.content[i].alternateidtype.replace(/\s+/g, '') : '';
      //Remove spaces from Gender 
      getUsers.content[i].gender = getUsers.content[i].gender.replace(/\s+/g, '');
      //Remove spaces from Training Status
      getUsers.content[i].training_status = getUsers.content[i].training_status.replace(/\s+/g, '');
      //Remove spaces from Placement Status
      getUsers.content[i].placement_status = getUsers.content[i].placement_status.replace(/\s+/g, '');
      //Remove spaces from Employment Type
      getUsers.content[i].employment_type = getUsers.content[i].employment_type.replace(/\s+/g, '');
      //Remove spaces from Training Type
      getUsers.content[i].training_type = getUsers.content[i].training_type.replace(/\s+/g, '');

      var sector = getUsers.content[i].sector;
      var jobrole = getUsers.content[i].jobrole;
      var candidate_name = getUsers.content[i].candidate_name;
      getUsers.content[i].aadhaar_number = getUsers.content[i].aadhaar_number.replace(/[“”""]/g, '');
      var aadhaar_number = getUsers.content[i].aadhaar_number;
      var aadhaar_enrollment_id = (getUsers.content[i].aadhaar_enrollment_id != '' && getUsers.content[i].aadhaar_enrollment_id != null) ? getUsers.content[i].aadhaar_enrollment_id : ''; 
      var alternateidtype = (getUsers.content[i].alternateidtype != '' && getUsers.content[i].alternateidtype != null) ? getUsers.content[i].alternateidtype : '';
      var alternate_id_number = (getUsers.content[i].alternate_id_number != '' && getUsers.content[i].alternate_id_number != null) ? getUsers.content[i].alternate_id_number : '';
      var phone = (getUsers.content[i].phone != '' && getUsers.content[i].phone != null) ? getUsers.content[i].phone : '';
      var email_address = (getUsers.content[i].email_address != '' && getUsers.content[i].email_address != null) ? getUsers.content[i].email_address : '';
      var dob = getUsers.content[i].dob != '' ? d3.timeFormat(dbDateFormat)(new Date(getUsers.content[i].dob)) : null;
      var gender = getUsers.content[i].gender;
      var address = getUsers.content[i].address;
      var state = getUsers.content[i].state;
      var district = getUsers.content[i].district;
      var pincode = getUsers.content[i].pincode;
      var experience_in_years = getUsers.content[i].experience_in_years;
      var sdmsnsdc_enrollment_number = getUsers.content[i].sdmsnsdc_enrollment_number;
      var training_status = getUsers.content[i].training_status;
      var attendence_percentage = getUsers.content[i].attendence_percentage;
      var placement_status = getUsers.content[i].placement_status;
      var employment_type = getUsers.content[i].employment_type;
      var assessment_score = getUsers.content[i].assessment_score;
      var max_assessment_score = getUsers.content[i].max_assessment_score;
      var assessment_date = getUsers.content[i].assessment_date != '' ? d3.timeFormat(dbDateFormat)(new Date(getUsers.content[i].assessment_date)) : null;
      var training_type = getUsers.content[i].training_type;
      var batch_id = getUsers.content[i].batch_id;
      var batch_start_date = getUsers.content[i].batch_start_date != '' ? d3.timeFormat(dbDateFormat)(new Date(getUsers.content[i].batch_start_date)) : null;
      var batch_end_date = getUsers.content[i].batch_end_date != '' ? d3.timeFormat(dbDateFormat)(new Date(getUsers.content[i].batch_end_date)) : null;
      var scheme_type = getUsers.content[i].scheme_type;
      var state_skill_development_mission = getUsers.content[i].state_skill_development_mission;
      var trainingpartner_id = getUsers.content[i].trainingpartner_id;
      var trainingcenter_id = getUsers.content[i].training_center;
      var is_willing_to_relocate = (getUsers.content[i].willing_to_relocate != '' && getUsers.content[i].willing_to_relocate != null && getUsers.content[i].willing_to_relocate != 'null') ? getUsers.content[i].willing_to_relocate : 'no';
      var sqlQuery = constant.appConfig.CANDIDATES_INSERT_QUERY;
      sqlQuery = sqlQuery + '("' + sector + '","' + jobrole + '","' + candidate_name + '","' + aadhaar_number + '","' + aadhaar_enrollment_id + '","' + alternateidtype + '","' + alternate_id_number + '","' + phone + '","' + email_address + '","' + dob + '","' + gender + '","' + address + '","' + state + '","' + district + '","' + pincode + '","' + experience_in_years + '","' + sdmsnsdc_enrollment_number + '","' + training_status + '","' + attendence_percentage + '","' + placement_status + '","' + employment_type + '","' + assessment_score + '","' + max_assessment_score + '","' + assessment_date + '","' + training_type + '","' + batch_id + '","' + batch_start_date + '","' + batch_end_date + '","' + scheme_type + '","' + state_skill_development_mission + '","' + trainingpartner_id + '","' + trainingcenter_id + '","' + is_willing_to_relocate + '" );';

      //"CANDIDATES_INSERT_QUERY": 'INSERT INTO `tbl_Candidates` (`fk_sectorID`, `fk_jobroleID`, `candidate_name`, `aadhaar_number`,
      // `aadhaar_enrollment_id`,`alternateidtype`,`alternate_id_number`,`phone`,`email_address`,`dob`,`gender`,`address`,`fk_stateID`,
      //`fk_cityID`,`pincode`, `experience_in_years`, `sdmsnsdc_enrollment_number`, `training_status`, `attendence_percentage`,
      // `placement_status`, `employment_type`, `assessment_score`, `max_assessment_score`, `assessment_date`, `training_type`,
      // `batch_id`, `batch_start_date`, `batch_end_date`, `fk_schemeID`, `fk_ssdm`, `fk_trainingpartnerid`,`fk_trainingcenterid`) VALUES ', 

      try {
        var res = await connection.executeRawQuery(sqlQuery);
        if (res.status === true && res.content !== undefined && res.content.insertId > 0) {
          var fieldValueUpdate = [];
          fieldValueUpdate.push({
            field: "is_processed",
            fValue: 1
          });
          var result = await candidateDAL.updateCandidateBulkUpload(autoIncId, fieldValueUpdate);
          console.log(result);
        } else {

        }
      } catch (ex) {

      }
    }
  }
  else {
    console.log("No records to update");
  }
}

var getZipwiseCandidateService = async function (request, response) {
  debug("futurehiringrequest.service -> getZipwiseCandidateService");
  try {
    var resultAll = { demands: [] };
    var totalFutureRequests = 0;

    
    var demandObj = {};
    

    //Get API KEY FOR g_maps
    var result_get_api = await otherDAL.getApiKey('g_maps');
    if (result_get_api.status === true && result_get_api.content.length > 0) {
      demandObj.apiKey = result_get_api.content[0].api_key;
      // totalFutureRequests = totalfuturereqRes.content[0].TotalFutureHiringRequests !== null ? totalfuturereqRes.content[0].TotalFutureHiringRequests : 0;
    }
    // return;
    
    
    var getZipwiseCandidate = 'SELECT SUM(count1) as total,t1.stateid,t1.statename,t1.lat,t1.lng from (SELECT COUNT(pincode) AS count1, candidates.fk_stateID as stateid,st.lat,st.lng,st.stateName as statename FROM tbl_Candidates as candidates INNER JOIN tbl_StateMaster as st ON st.pk_stateID =candidates.fk_stateID GROUP BY candidates.pincode) as t1 GROUP BY t1.stateid';
    console.log('# mysql', getZipwiseCandidate);
    var result = await connection.executeRawQuery(getZipwiseCandidate);
    
    //let result = await futurehiringrequestsDAL.getStatewiseFutureHiringRequest();

    if (result.status === true && result.content !== undefined) {
      demandObj.StatewiseCandidate = result.content;

    } else {
      //return common.sendResponse(response, constant.userMessages.ERR_FETCH_STATEWISE_FUTURREQUEST, false);
    }
    //console.log('# demandObj',demandObj);
    resultAll.demands.push(demandObj);
    //console.log('# res',result);
    return common.sendResponse(response, resultAll, true);
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

var getZipwiseDriversService = async function (request, response) {
  debug("futurehiringrequest.service -> getZipwiseDriversService");
  try {
    var resultAll = { demands: [] };
    var totalFutureRequests = 0;

    
    var demandObj = {};
    

    //Get API KEY FOR g_maps
    var result_get_api = await otherDAL.getApiKey('g_maps');
    if (result_get_api.status === true && result_get_api.content.length > 0) {
      demandObj.apiKey = result_get_api.content[0].api_key;
      // totalFutureRequests = totalfuturereqRes.content[0].TotalFutureHiringRequests !== null ? totalfuturereqRes.content[0].TotalFutureHiringRequests : 0;
    }
    // return;
    
    
    var getZipwiseDrivers = "SELECT SUM(count1) as total,t1.stateid,t1.statename,t1.lat,t1.lng,t1.jobrolename from (SELECT COUNT(pincode) AS count1, candidates.fk_stateID as stateid ,st.lat,st.lng,st.stateName as statename,jb.jobroleName as jobrolename  FROM tbl_Candidates as candidates INNER JOIN tbl_StateMaster as st ON st.pk_stateID =candidates.fk_stateID INNER JOIN tbl_JobroleMaster as jb ON jb.pk_jobroleID =candidates.fk_jobroleID WHERE jb.jobroleName LIKE '%driver%' GROUP BY candidates.pincode) as t1 GROUP BY t1.stateid";
    console.log('# mysql', getZipwiseDrivers);
    var result = await connection.executeRawQuery(getZipwiseDrivers);
    
    //let result = await futurehiringrequestsDAL.getStatewiseFutureHiringRequest();

    if (result.status === true && result.content !== undefined) {
      demandObj.StatewiseCandidate = result.content;

    } else {
      //return common.sendResponse(response, constant.userMessages.ERR_FETCH_STATEWISE_FUTURREQUEST, false);
    }
    //console.log('# demandObj',demandObj);
    resultAll.demands.push(demandObj);
    //console.log('# res',result);
    return common.sendResponse(response, resultAll, true);
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}



module.exports = {
  getCandidatesForAdminService: getCandidatesForAdminService,
  addUpdateCandidateService: addUpdateCandidateService,
  getCandidateService: getCandidateService,
  getCandidateAddFormDataService: getCandidateAddFormDataService,
  updateMultipleRecordsService: updateMultipleRecordsService,
  getCandidateDetailService: getCandidateDetailService,
  getCandidateDetailForWebService: getCandidateDetailForWebService,
  candidateBulkUploadCheckStateCityService: candidateBulkUploadCheckStateCityService,
  candidateBulkUploadCheckSchemeTypeService: candidateBulkUploadCheckSchemeTypeService,
  processCandidateCSVCronService: processCandidateCSVCronService,
  getZipwiseCandidateService: getZipwiseCandidateService,
  getZipwiseDriversService: getZipwiseDriversService,

}
