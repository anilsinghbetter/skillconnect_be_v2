var tbl_UserMaster = "tbl_UserMaster";
var tbl_RoleMaster = "tbl_RoleMaster";
var tbl_UserType = "tbl_UserType";
var tbl_OTP = "tbl_OTP";
var tbl_AccessToken = "tbl_AccessToken";
var tbl_transactionMaster = "tbl_transactionMaster";
var tbl_TrainingPartners = "tbl_TrainingPartners";
var tbl_TrainingCenters = "tbl_TrainingCenters";
var tbl_Candidates = "tbl_Candidates";
var view_GetUserDetail = "view_GetUserDetail";
var view_GetLocationDetail = "view_GetLocationDetail";
var view_SectorJobRoleDetail = "view_SectorJobRoleDetail";
var tbl_SchemeMaster = "tbl_SchemeMaster";
var tbl_StateMaster = "tbl_StateMaster";
var tbl_SchemeCentralMinistryMapping = "tbl_SchemeCentralMinistryMapping";
var tbl_CentralministriesMaster = "tbl_CentralministriesMaster";
var tbl_TrainingCenters = "tbl_TrainingCenters";
var candidate_bulk_upload = "candidate_bulk_upload";
var view_Candidates = "view_Candidates";

var query = {
    checkCandidateIsExistQuery: {
        table: tbl_Candidates,
        select: [{
            field: 'pk_candidateid',
            alias: 'candidateid'
        }],
        filter: {
            and: [{
                field: 'sdmsnsdc_enrollment_number',
                operator: 'EQ',
                value: ''
            }, {
                field: 'is_deleted',
                operator: 'EQ',
                value: 0
            }]
        }
    },
    createCandidateQuery: {
        table: tbl_Candidates,
        insert: []
    },
    getCandidatesCountQuery: {
        table: tbl_Candidates,
        alias: 'CD',
        select: [{
            field: 'CD.pk_candidateid',
            encloseField: false,
            alias: 'candidatecount',
            aggregation: 'count',
        }],
        filter: {}
    },
    getCandidateQuery: {
        join: {
            table: tbl_Candidates,
            alias: 'CD',
            joinwith: [
                /* {
                table: view_GetLocationDetail,
                alias: 'LD',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'LD',
                        field: 'stateid',
                        operator: 'eq',
                        value: {
                            table: 'CD',
                            field: 'fk_stateID'
                        }
                    }, {
                        table: 'LD',
                        field: 'cityid',
                        operator: 'eq',
                        value: {
                            table: 'CD',
                            field: 'fk_cityID'
                        }
                    }]
                }
            }, */ 
            {
                table: view_SectorJobRoleDetail,
                alias: 'SEC',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'SEC',
                        field: 'sectorid',
                        operator: 'find_in_set',
                        value: {
                            table: 'CD',
                            field: 'fk_sectorID'
                        }
                    }, {
                        table: 'SEC',
                        field: 'jobroleid',
                        operator: 'eq',
                        value: {
                            table: 'CD',
                            field: 'fk_jobroleID'
                        }
                    }]
                }
            },
            /* {
                table: tbl_SchemeMaster,
                alias: 'SCHEME',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'SCHEME',
                        field: 'pk_schemeID',
                        operator: 'eq',
                        value: {
                            table: 'CD',
                            field: 'fk_schemeID'
                        }
                    }]
                }
            }, */ 
            /* {
                table: tbl_StateMaster,
                alias: 'STATE',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'STATE',
                        field: 'pk_stateID',
                        operator: 'eq',
                        value: {
                            table: 'CD',
                            field: 'fk_ssdm'
                        }
                    }]
                }
            }, */ 
            /* {
                table: tbl_SchemeCentralMinistryMapping,
                alias: 'SCM',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'SCM',
                        field: 'fk_schemeID',
                        operator: 'eq',
                        value: {
                            table: 'CD',
                            field: 'fk_schemeID'
                        }
                    }]
                }
            }, */ 
            /* {
                table: tbl_CentralministriesMaster,
                alias: 'CEMI',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'CEMI',
                        field: 'pk_ID',
                        operator: 'eq',
                        value: {
                            table: 'SCM',
                            field: 'fk_centralministryID'
                        }
                    }]
                }
            }, */ 
            /* {
                table: tbl_TrainingCenters,
                alias: 'TRC',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'TRC',
                        field: 'pk_trainingcenterid',
                        operator: 'eq',
                        value: {
                            table: 'CD',
                            field: 'fk_trainingcenterid'
                        }
                    }]
                }
            } */
        ]
        },
        select: [
           /*  {
            field: 'SCHEME.schemeType',
            encloseField: false,
            alias: 'schemeType'
        }, */ 
        {
            field: 'SEC.sectorid',
            encloseField: false,
            alias: 'sectorid'
        }, 
        {
            field: 'SEC.sectorName',
            encloseField: false,
            alias: 'sectorName'
        }, {
            field: 'SEC.jobroleid',
            encloseField: false,
            alias: 'jobroleid'
        }, {
            field: 'SEC.jobroleName',
            encloseField: false,
            alias: 'jobroleName'
        }, 
       /*  {
            field: 'STATE.stateName',
            encloseField: false,
            alias: 'stateministry'
        }, {
            field: 'SCM.fk_centralministryID',
            encloseField: false,
            alias: 'centralministryid'
        }, {
            field: 'TRC.trainingcenter_name',
            encloseField: false,
            alias: 'trainingcenter_name'
        }, {
            field: 'TRC.pk_trainingcenterid',
            encloseField: false,
            alias: 'trainingcenter_id'
        }, {
            field: 'CEMI.name',
            encloseField: false,
            alias: 'centralministry'
        }, {
            field: 'LD.stateid',
            encloseField: false,
            alias: 'stateid'
        }, {
            field: 'LD.stateName',
            encloseField: false,
            alias: 'stateName'
        }, {
            field: 'LD.cityid',
            encloseField: false,
            alias: 'cityid'
        }, {
            field: 'LD.cityName',
            encloseField: false,
            alias: 'cityName'
        },  */
        {
            field: 'CD.pk_candidateid',
            encloseField: false,
            alias: 'candidateid'
        }, {
            field: 'CD.candidate_name',
            encloseField: false,
            alias: 'candidate_name'
        }, 
        /* {
            field: 'CD.aadhaar_number',
            encloseField: false,
            alias: 'aadhaar_number'
        }, {
            field: 'CD.aadhaar_enrollment_id',
            encloseField: false,
            alias: 'aadhaar_enrollment_id'
        }, {
            field: 'CD.alternateidtype',
            encloseField: false,
            alias: 'alternateidtype'
        }, {
            field: 'CD.alternate_id_number',
            encloseField: false,
            alias: 'alternate_id_number'
        },  */
        {
            field: 'CD.phone',
            encloseField: false,
            alias: 'phone'
        },
        /*  {
            field: 'CD.email_address',
            encloseField: false,
            alias: 'email_address'
        }, {
            field: 'DATE_FORMAT(CD.dob,"%d %b %Y")',
            encloseField: false,
            alias: 'dob'
        }, {
            field: 'CD.gender',
            encloseField: false,
            alias: 'gender'
        }, {
            field: 'CD.address',
            encloseField: false,
            alias: 'address'
        }, {
            field: 'CD.pincode',
            encloseField: false,
            alias: 'pincode'
        }, {
            field: 'CD.experience_in_years',
            encloseField: false,
            alias: 'experience_in_years'
        }, {
            field: 'CD.sdmsnsdc_enrollment_number',
            encloseField: false,
            alias: 'sdmsnsdc_enrollment_number'
        }, */
        {
            field: 'CD.training_status',
            encloseField: false,
            alias: 'training_status'
        }, 
        /* {
            field: 'CD.attendence_percentage',
            encloseField: false,
            alias: 'attendence_percentage'
        }, {
            field: 'CD.placement_status',
            encloseField: false,
            alias: 'placement_status'
        }, {
            field: 'CD.employment_type',
            encloseField: false,
            alias: 'employment_type'
        }, {
            field: 'CD.assessment_score',
            encloseField: false,
            alias: 'assessment_score'
        }, {
            field: 'CD.max_assessment_score',
            encloseField: false,
            alias: 'max_assessment_score'
        }, {
            field: 'DATE_FORMAT(CD.assessment_date,"%d %b %Y")',
            encloseField: false,
            alias: 'assessment_date'
        }, {
            field: 'CD.training_type',
            encloseField: false,
            alias: 'training_type'
        },  */
        {
            field: 'CD.batch_id',
            encloseField: false,
            alias: 'batch_id'
        },
        /* {
            field: 'DATE_FORMAT(CD.batch_start_date,"%d %b %Y")',
            encloseField: false,
            alias: 'batch_start_date'
        }, {
            field: 'DATE_FORMAT(CD.batch_end_date,"%d %b %Y")',
            encloseField: false,
            alias: 'batch_end_date'
        }, {
            field: 'CD.is_willing_to_relocate',
            encloseField: false,
            alias: 'willing_to_relocate'
        }, {
            field: 'CD.status',
            encloseField: false,
            alias: 'status'
        }, {
            field: 'CD.fk_schemeID',
            encloseField: false,
            alias: 'fk_schemeID'
        }, {
            field: 'CD.fk_ssdm',
            encloseField: false,
            alias: 'fk_ssdm'
        }, {
            field: 'CD.fk_trainingpartnerid',
            encloseField: false,
            alias: 'fk_trainingpartnerid'
        } */
    ],
        filter: {},
        sortby: [{
            field: 'CD.pk_candidateid',
            encloseField: false,
            order: 'DESC'
        }]
    },
    
    getCandidateQueryWithView: {
        table: view_Candidates,
        select: [
            /* {
            field: 'schemeType',
        }, {
            field: 'sectorid',
        }, */
         {
            field: 'sectorName',
        }, 
        /* {
            field: 'jobroleid',
        }, */ 
        {
            field: 'jobroleName',
        }, 
        /* {
            field: 'stateministry',
        }, {
            field: 'centralministryid',
        }, {
            field: 'trainingcenter_name',
        }, {
            field: 'trainingcenter_id',
        }, {
            field: 'centralministry',
        }, {
            field: 'stateid',
        }, {
            field: 'stateName',
        }, {
            field: 'cityid',
        }, {
            field: 'cityName',
        }, */ 
        {
            field: 'candidateid',
        }, 
        {
            field: 'candidate_name',
        }, 
        /* {
            field: 'aadhaar_number',
        }, {
            field: 'aadhaar_enrollment_id',
        }, {
            field: 'alternateidtype',
        }, {
            field: 'alternate_id_number',
        }, */ 
        {
            field: 'phone',
        }, 
        /* {
            field: 'email_address',
        }, {
            field: 'DATE_FORMAT(dob,"%d %b %Y")',
            encloseField: false,
            alias: 'dob'
        }, {
            field: 'gender',
        }, {
            field: 'address',
        }, {
            field: 'pincode',
        }, {
            field: 'experience_in_years',
        }, {
            field: 'sdmsnsdc_enrollment_number',
        }, */ 
        {
            field: 'training_status',
        }, 
        /* {
            field: 'attendence_percentage',
        }, {
            field: 'placement_status',
        }, {
            field: 'employment_type',
        }, {
            field: 'assessment_score',
        }, {
            field: 'max_assessment_score',
        }, {
            field: 'DATE_FORMAT(assessment_date,"%d %b %Y")',
            encloseField: false,
            alias: 'assessment_date'
        }, {
            field: 'training_type',
        }, */ 
        {
            field: 'batch_id',
        }, 
        /* {
            field: 'DATE_FORMAT(batch_start_date,"%d %b %Y")',
            encloseField: false,
            alias: 'batch_start_date'
        }, {
            field: 'DATE_FORMAT(batch_end_date,"%d %b %Y")',
            encloseField: false,
            alias: 'batch_end_date'
        }, {
            field: 'willing_to_relocate',
        }, {
            field: 'status'
        }, {
            field: 'fk_schemeID',
        }, {
            field: 'fk_ssdm',
        }, {
            field: 'fk_trainingpartnerid',
        } */
    ],
        filter: {},
        sortby: [{
            field: 'candidateid',
            order: 'DESC'
        }]
    },
    updateCandidateQuery: {
        table: tbl_Candidates,
        update: [],
        filter: {
            and: [{
                field: 'pk_candidateid',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    checkCandidateIDIsExistQuery: {
        table: tbl_Candidates,
        select: [{
            field: 'pk_candidateid',
            alias: 'candidateid'
        }],
        filter: {
            and: [{
                field: 'pk_candidateid',
                operator: 'EQ',
                value: ''
            }, {
                field: 'is_deleted',
                operator: 'EQ',
                value: 0
            }]
        }
    },
    getCandidateDetailQuery: {
        table: view_Candidates,
        select: [{
            field: 'schemeType',
        }, {
            field: 'sectorid',
        }, {
            field: 'sectorName',
        }, {
            field: 'jobroleid',
        }, {
            field: 'jobroleName',
        }, {
            field: 'stateministry',
        }, {
            field: 'centralministryid',
        }, {
            field: 'centralministry',
        }, {
            field: 'stateid',
        }, {
            field: 'stateName',
        },{
            field: 'age',
        },{
            field: 'candidate_id',
        }, {
            field: 'cityid',
        }, {
            field: 'cityName',
        }, {
            field: 'trainingcenter_name',
        }, {
            field: 'trainingcenter_id',
        }, {
            field:'candidateid',
        }, {
            field: 'candidate_name',
        }, {
            field: 'aadhaar_number',
        }, {
            field: 'aadhaar_enrollment_id',
        }, {
            field: 'alternateidtype',
        }, {
            field: 'alternate_id_number',
        }, {
            field: 'phone',
        }, {
            field: 'email_address',
        }, {
            field: 'DATE_FORMAT(dob,"%Y-%m-%d")',
            encloseField: false,
            alias: 'dob'
        }, {
            field: 'gender',
        }, {
            field: 'address',
        }, {
            field: 'pincode',
        }, {
            field: 'experience_in_years',
        }, {
            field: 'sdmsnsdc_enrollment_number',
        }, {
            field: 'training_status',
        }, {
            field: 'attendence_percentage',
        }, {
            field: 'placement_status',
        }, {
            field: 'employment_type',
        }, {
            field: 'assessment_score',
        }, {
            field: 'max_assessment_score',
        }, {
            field: 'DATE_FORMAT(assessment_date,"%Y-%m-%d")',
            encloseField: false,
            alias: 'assessment_date'
        }, {
            field: 'training_type',
        }, {
            field: 'batch_id',
        }, {
            field: 'DATE_FORMAT(batch_start_date,"%Y-%m-%d")',
            encloseField: false,
            alias: 'batch_start_date'
        }, {
            field: 'DATE_FORMAT(batch_end_date,"%Y-%m-%d")',
            encloseField: false,
            alias: 'batch_end_date'
        }, {
            field: 'willing_to_relocate',
        }, {
            field: 'status',
        }, {
            field: 'fk_schemeID',
            encloseField: false,
            alias: 'schemeid'
        }, {
            field: 'fk_ssdm',
            encloseField: false,
            alias: 'stateministryid'
        },{
            field: 'fk_trainingpartnerid',
        }],
        filter: {
             and: [{
                field: 'candidateid',
                operator: 'EQ',
                value: ''
            }, {
                field: 'is_deleted',
                operator: 'EQ',
                value: 0
            }, {
                field: 'status',
                operator: 'EQ',
                value: 1
            }]
        }
    },
    getCandidateViewMoreDetailQuery: {
        table: view_Candidates,
        select: [{
            field: 'schemeType',
        }, {
            field: 'sectorid',
        }, {
            field: 'sectorName',
        }, {
            field: 'jobroleid',
        }, {
            field: 'jobroleName',
        }, {
            field: 'stateministry',
        }, {
            field: 'centralministryid',
        }, {
            field: 'centralministry',
        }, {
            field: 'stateid',
        }, {
            field: 'stateName',
        }, {
            field: 'cityid',
        }, {
            field: 'cityName',
        }, {
            field: 'trainingcenter_name',
        }, {
            field: 'trainingcenter_id',
        },{
            field: 'age',
        },{
            field: 'candidate_id',
        }, {
            field:'candidateid',
        }, {
            field: 'candidate_name',
        }, {
            field: 'aadhaar_number',
        }, {
            field: 'aadhaar_enrollment_id',
        }, {
            field: 'alternateidtype',
        }, {
            field: 'alternate_id_number',
        }, {
            field: 'phone',
        }, {
            field: 'email_address',
        }, {
            field: 'DATE_FORMAT(dob,"%d %b %Y")',
            encloseField: false,
            alias: 'dob'
        }, {
            field: 'gender',
        }, {
            field: 'address',
        }, {
            field: 'pincode',
        }, {
            field: 'experience_in_years',
        }, {
            field: 'sdmsnsdc_enrollment_number',
        }, {
            field: 'training_status',
        }, {
            field: 'attendence_percentage',
        }, {
            field: 'placement_status',
        }, {
            field: 'employment_type',
        }, {
            field: 'assessment_score',
        }, {
            field: 'max_assessment_score',
        }, {
            field: 'DATE_FORMAT(assessment_date,"%d %b %Y")',
            encloseField: false,
            alias: 'assessment_date'
        }, {
            field: 'training_type',
        }, {
            field: 'batch_id',
        }, {
            field: 'DATE_FORMAT(batch_start_date,"%d %b %Y")',
            encloseField: false,
            alias: 'batch_start_date'
        }, {
            field: 'DATE_FORMAT(batch_end_date,"%d %b %Y")',
            encloseField: false,
            alias: 'batch_end_date'
        }, {
            field: 'willing_to_relocate',
        }, {
            field: 'status',
        }, {
            field: 'fk_schemeID',
            encloseField: false,
            alias: 'schemeid'
        }, {
            field: 'fk_ssdm',
            encloseField: false,
            alias: 'stateministryid'
        },{
            field: 'fk_trainingpartnerid',
        }],
        filter: {
             and: [{
                field: 'candidateid',
                operator: 'EQ',
                value: ''
            }, {
                field: 'is_deleted',
                operator: 'EQ',
                value: 0
            }, {
                field: 'status',
                operator: 'EQ',
                value: 1
            }]
        }
    },
    getTrainingPartnerIdFromCandidateIDQuery : {
        table: tbl_Candidates,
        select: [{
            field: 'fk_trainingpartnerid',
            alias: 'trainingpartnerid'
        }],
        filter: {
            and: [{
                field: 'pk_candidateid',
                operator: 'EQ',
                value: ''
            }, {
                field: 'status',
                operator: 'EQ',
                value: 1
            }, {
                field: 'is_deleted',
                operator: 'EQ',
                value: 0
            }]
        }
    },
    getCandidateStateCityFromBulkUplodQuery: {
        table: candidate_bulk_upload,
        select: [{
            field: 'id',
        }, {
            field: 'state',
        }, {
            field: 'district',
        }],
        filter: {
            and: [{
                field: 'is_processed',
                operator: 'EQ',
                value: 0
            }, {
                field: 'is_statecity_done',
                operator: 'EQ',
                value: 0
            }]
        },
        sortby: [{
            field: 'id',
            order: 'ASC'
        }]
    },
    getCandidateSchemeTypeFromBulkUploadQuery: {
        table: candidate_bulk_upload,
        select: [{
            field: 'id',
        }, {
            field: 'scheme_type',
        }, {
            field: 'state_skill_development_mission',
        }],
        filter: {
            and: [{
                field: 'is_processed',
                operator: 'EQ',
                value: 0
            }, {
                field: 'is_scheme_state_done',
                operator: 'EQ',
                value: 0
            }]
        },
        sortby: [{
            field: 'id',
            order: 'ASC'
        }]
    },
    updateCandidateBulkUploadQuery: {
        table: candidate_bulk_upload,
        update: [],
        filter: {
            and: [{
                field: 'id',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    getCandidatesFromBulkUploadQuery:{
        table: candidate_bulk_upload,
        select: [{
            field: 'id',
        },{
            field: 'trainingpartner_id',
        },{
            field: 'training_center',
        },{
            field: 'district',
        },{
            field: 'state',
        },{
            field: 'sector',
        },{
            field: 'jobrole',
        },{
            field: 'scheme_type',
        },{
            field: 'state_skill_development_mission',
        },{
            field: 'candidate_name',
        },{
            field: 'aadhaar_number',
        },{
            field: 'aadhaar_enrollment_id',
        },{
            field: 'alternateidtype',
        },{
            field: 'alternate_id_number',
        },{
            field: 'phone',
        },{
            field: 'email_address',
        },{
            field: 'dob',
        },{
            field: 'gender',
        },{
            field: 'address',
        },{
            field: 'pincode',
        },{
            field: 'experience_in_years',
        },{
            field: 'sdmsnsdc_enrollment_number',
        },{
            field: 'training_status',
        },{
            field: 'attendence_percentage',
        },{
            field: 'placement_status',
        },{
            field: 'employment_type',
        },{
            field: 'assessment_score',
        },{
            field: 'max_assessment_score',
        },{
            field: 'assessment_date',
        },{
            field: 'training_type',
        },{
            field: 'batch_id',
        },{
            field: 'batch_start_date',
        },{
            field: 'batch_end_date',
        },{
            field: 'willing_to_relocate',
        }],
        filter: {
            and: [{
                field: 'is_processed',
                operator: 'EQ',
                value: 0
            }, {
                field: 'is_sector_jobrole_done',
                operator: 'EQ',
                value: 1
            }, {
                field: 'is_statecity_done',
                operator: 'EQ',
                value: 1
            }, {
                field: 'is_scheme_state_done',
                operator: 'EQ',
                value: 1
            }]  

        },
        sortby: [{
            field: 'id',
            order: 'ASC'
        }]
    }
};


module.exports = query