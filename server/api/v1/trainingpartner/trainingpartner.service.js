var debug = require('debug')('server:api:v1:trainingpartner:service');
var d3 = require("d3");
var DateLibrary = require('date-management');
var bcrypt = require('bcrypt');
var randomize = require('randomatic');
var uuid = require('uuid');
var common = require('../common');
var otherService = require('../other/other.service');
var constant = require('../constant');
var trainingPartnerDAL = require('./trainingpartner.DAL');
var industryPartnerDAL = require('../industrypartner/industrypartner.DAL');
var candidateDAL = require('../candidate/candidate.DAL');
var otherDAL = require('../other/other.DAL');
var dbDateFormat = constant.appConfig.DB_DATE_FORMAT;
var config = require('../../../../config');
var connection = require('../../../helper/connection');
var QueryBuilder = require('../../../helper/node-datatable-master');
var async = require("async");

/**
 * get training partners NEW
 * 
 * @param {object} request 
 * @param {object} cb//callback
 * @return {object} 
 */

var getTrainingPartnerService = async function (request, response) {
  debug("Traingin Partner New.service -> getTrainingPartnerService");
  var params = request.body;

  var queryString="";
  if(!!params.contact_mobile) queryString +=" AND (`contact_mobile` = "+ "'"+params.contact_mobile+"'";
  if(!!params.contact_email_address) queryString +=" OR `contact_email_address` LIKE "+ "'%"+params.contact_email_address+"%'";
  if(!!params.training_partner_name) queryString +=" OR `training_partner_name` LIKE "+ "'%"+params.training_partner_name+"%'";
  if(!!params.nsdc_registration_number) queryString +=" OR `nsdc_registration_number` = "+ "'"+params.nsdc_registration_number+"'";
  if(!!params.tpid) queryString +=" OR `tpid` = "+ "'"+params.tpid+"'";
  if(!!params.contact_person_name) queryString +=" OR `contact_person_name` LIKE "+ "'%"+params.contact_person_name+"%')";
  // console.log(params,"PARAMS #2");
  //return;
  var oTableDef = {
    sCountColumnName: "distinct pk_trainingpartnerid",
    sGroupColumnName: "pk_trainingpartnerid",
    sTableName: 'tbl_TrainingPartners',
    sWhereAndSql: "is_deleted = 0"+queryString,
    sSelectSql: "pk_trainingpartnerid,tpid,nsdc_registration_number,training_partner_name,contact_mobile,contact_email_address,contact_person_name,is_email_sent,legal_entity_name,fk_sectorID as sectorid,fk_stateID as stateid,fk_cityID as cityid,pincode,address,status",
    //sFromSql: "tbl_UserMaster as UM LEFT JOIN tbl_MatchPlayingSquad as MPS ON (UM.pk_userID = MPS.fk_playerID) LEFT JOIN tbl_MatchMaster as MM ON (MM.pk_matchID = MPS.fk_matchID and MM.isDeleted = '0')",
  };
  var queryBuilder = new QueryBuilder(oTableDef);
  var queries = queryBuilder.buildQuery(params);
  queries.recordsTotal = "SELECT COUNT(*) as totalCount FROM `tbl_TrainingPartners` WHERE is_deleted = '0'";
//  console.log('#Queries', queries);
  var newres = {};
  if (queries.recordsFiltered) {
    var resFiltered1 = await connection.executeRawQuery(queries.recordsFiltered);
    //queries.recordsTotal = result.content;
    var resFilterNew = Object.values(resFiltered1.content[0]);
    //console.log('#1', resFilterNew);
  }
  var resTotal2 = await connection.executeRawQuery(queries.recordsTotal);
  //queries.recordsTotal = result.content;
  //console.log('#2', resTotal2.content[0].totalCount);

  var resSelect3 = await connection.executeRawQuery(queries.select);
  //console.log('#3', resSelect3.content);

  newres = { recordsFiltered: resFilterNew[0], recordsTotal: resTotal2.content[0].totalCount, data: resSelect3.content }
  return common.sendResponse(response, newres);
};
/**
 * signup
 * 
 * @param  {object} request
 * @param  {object} response
 * @return {object}
 */
var signupService = async function (request, response) {
  debug("trainingpartner.service -> signupService");
  var isValid = common.validateParams([request.body.nsdc_registration_number, request.body.training_partner_name, request.body.legal_entity_name, request.body.contact_person_name, request.body.contact_email_address, request.body.sectors]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_SIGNUP_REQUEST, false);
  }
  common.sanitizeAll(request.body);

  var trainingpartnerinfo = {};
  trainingpartnerinfo.tpid = 'TP-' + randomize('0', 5);
  trainingpartnerinfo.nsdc_registration_number = request.body.nsdc_registration_number;
  trainingpartnerinfo.password = bcrypt.hashSync(constant.appConfig.SYSTEM_PASSWORD, 10);
  trainingpartnerinfo.training_partner_name = request.body.training_partner_name;
  trainingpartnerinfo.legal_entity_name = request.body.legal_entity_name;
  trainingpartnerinfo.contact_person_name = request.body.contact_person_name;
  trainingpartnerinfo.contact_email_address = request.body.contact_email_address;
  trainingpartnerinfo.contact_mobile = request.body.contact_mobile;
  trainingpartnerinfo.address = request.body.address;
  trainingpartnerinfo.fk_sectorID = request.body.sectors;

  var trainingpartnerKeys = Object.keys(trainingpartnerinfo);
  var fieldValueInsert = [];
  trainingpartnerKeys.forEach(function (trainingpartnerKeys) {
    if (trainingpartnerinfo[trainingpartnerKeys] !== undefined) {
      var fieldValueObj = {};
      fieldValueObj = {
        field: trainingpartnerKeys,
        fValue: trainingpartnerinfo[trainingpartnerKeys]
      }
      fieldValueInsert.push(fieldValueObj);
    }
  });

  try {
    var result = await trainingPartnerDAL.checkUserIsExist(trainingpartnerinfo.tpid, trainingpartnerinfo.nsdc_registration_number);
    if (result.content.length > 0 && result.content[0].trainingpartnerid > 0) {
      return common.sendResponse(response, constant.userMessages.ERR_USER_IS_ALREADY_EXIST, false);
    }
    var res_create_user = await trainingPartnerDAL.createUser(fieldValueInsert);
    otherService.emailSignUpSendService("tp_registration", trainingpartnerinfo);
    return common.sendResponse(response, constant.userMessages.MSG_SIGNUP_SUCCESSFULLY, true);
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
};

/**
 * signin
 *
 * @param  {object}   request
 * @param  {object} response
 * @return {object}
 */
var signinService = async function (request, response) {
  debug("trainingpartner.service -> signinService");
  var isValid = common.validateParams([request.body.tpid, request.body.password]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_SIGNIN_REQUEST, false);
  }
  common.sanitizeAll(request.body);
  var tpid = request.body.tpid;
  var password = request.body.password;
  let result = await trainingPartnerDAL.userLogin(tpid);
  if (result.status == false) {
    return common.sendResponse(response, result);
  } else if (result.content.length == 0) {
    return common.sendResponse(response, constant.userMessages.ERR_INVALID_EMAIL_AND_PASSWORD, false);
  } else if (result.content[0].status == 0) {
    return common.sendResponse(response, constant.userMessages.ERR_USER_IS_NOT_ACTIVE, false);
  }
  bcrypt.compare(password, result.content[0].password, async function (err, ress) {//First argurment is the input string to be compared
    if (ress) {
      let res_access_token = await checkAndCreateAccessToken(request, result.content);
      if (res_access_token.status === true) {
        /* var session = request.session;
        session.userInfo = {
          trainingpartnerid: res_access_token.data[0].trainingpartnerid,
          training_partner_name: res_access_token.data[0].training_partner_name,
          role: res_access_token.data[0].role,
          userTypeID: res_access_token.data[0].user_type_id
        };
        var userRights = [];
        for (var i = 0; i < res_access_token.data.length; i++) {
          var objRights = {};
          objRights.moduleName = res_access_token.data[i].module_name;
          objRights.canView = res_access_token.data[i].can_view;
          objRights.canAddEdit = res_access_token.data[i].can_add_edit;
          objRights.canDelete = res_access_token.data[i].can_delete;
          objRights.adminCreated = res_access_token.data[i].admin_created;
          userRights.push(objRights);
        }
        request.session.userInfo.userRights = userRights; */
        ['password', 'status'].forEach(e => delete result.content[0][e]);
        return common.sendResponse(response, res_access_token);
      } else {
        return common.sendResponse(response, res_access_token);
      }
    } else {
      return common.sendResponse(response, constant.userMessages.ERR_INVALID_EMAIL_AND_PASSWORD, false);
    }
  });
};

/**
 * check and create access token
 * 
 * @param  {Object}   request  [description]
 * @param  {Object}   userInfo [description]
 * @return {object}            [description]
 */
async function checkAndCreateAccessToken(request, userInfo) {
  try {
    var userId = userInfo[0].trainingpartnerid;
    var token = uuid.v1();
    var deviceId = request.headers["udid"];
    var deviceType = (request.headers["device-type"]).toLowerCase();
    var expiryDateTime = DateLibrary.getRelativeDate(new Date(), {
      operationType: "Absolute_DateTime",
      granularityType: "hours",
      value: constant.appConfig.MAX_ACCESS_TOKEN_EXPIRY_HOURS
    });
    var host = request.hostname;
    let result = await trainingPartnerDAL.exprieAccessToken(userId, deviceId, host);

    let res_access_token = await trainingPartnerDAL.createAccessToken(userId, token, expiryDateTime, deviceId, host);

    let res_user_tran = await trainingPartnerDAL.checkUserTransaction(deviceId, deviceType);

    if (res_user_tran.content[0].totalCount > 0) {
      var fieldValueUpdate = [];
      fieldValueUpdate.push({
        field: "isLogedIn",
        fValue: 1
      });
      fieldValueUpdate.push({
        field: "lastLoginDatetime",
        fValue: d3.timeFormat(dbDateFormat)(new Date())
      });
      let res_transaction = trainingPartnerDAL.updateUserTransaction(deviceId, deviceType, fieldValueUpdate);
    }
    else {
      let res_transaction = trainingPartnerDAL.createUserTransaction(deviceId, deviceType);
    }
    return { status: true, user_type: 'TP', "access_token": token, data: userInfo };
  }
  catch (ex) {
    console.log(" error in checkAndCreateAccessToken");
    throw ex;
  }
};

/**
 * changePasswordService check first userId and compare old password with db password and then chnage password
 *
 * @param  {object}   request
 * @param  {object} response
 * @return {object}
 */
var changePasswordService = async function (request, response) {
  debug("trainingpartner.service -> changePasswordService");
  var isValid = common.validateParams([request.body.trainingpartnerid, request.body.old_password, request.body.new_password]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_CHANGE_PASSWORD_REQUEST, false);
  }
  common.sanitizeAll(request.body);

  //Password should be minimum of 8 and maximum of 50 characters.
  var passwordLength = request.body.new_password.length;
  if (passwordLength < constant.appConfig.RESET_PASSWORD_MINIMUM_CHARACTERS_LIMIT) {
    return common.sendResponse(response, constant.requestMessages.ERR_RESET_PASSWORD_MIN_LENGTH, false);
  }
  if (passwordLength > constant.appConfig.RESET_PASSWORD_MAXIMUM_CHARACTERS_LIMIT) {
    return common.sendResponse(response, constant.requestMessages.ERR_RESET_PASSWORD_MAX_LENGTH, false);
  }

  var userId = request.body.trainingpartnerid;
  var oldPassword = bcrypt.hashSync(request.body.old_password, 10);
  var newPassword = bcrypt.hashSync(request.body.new_password, 10);
  var result = await trainingPartnerDAL.validateUser(userId);
  if (result.status === false) {
    return common.sendResponse(response, result);
  } else if (result.content.length == 0) {
    return common.sendResponse(response, constant.userMessages.ERR_USER_NOT_EXIST, false);
  } else if (result.content.length > 0) {
    if (result.content[0].status == 0) {
      return common.sendResponse(response, constant.userMessages.ERR_USER_IS_NOT_ACTIVE, false);
    }
  }
  bcrypt.compare(request.body.old_password, result.content[0].password, async function (err, ress) {//First argurment is the input string to be compared
    if (ress) {
      var fieldValueUpdate = [];
      fieldValueUpdate.push({
        field: "password",
        fValue: newPassword
      });
      var result = await trainingPartnerDAL.updateUserInfoById(userId, fieldValueUpdate);
      if (result.status === false) {
        return common.sendResponse(response, result);
      } else {
        return common.sendResponse(response, constant.userMessages.MSG_PASSWORD_CHANGE_SUCCESSFULLY, true);
      }
    } else {
      return common.sendResponse(response, constant.userMessages.ERR_OLD_PASSWORD_NOT_MATCH, false);
    }
  });
};

/**
 * update users profile
 *
 * @param  {object}   request
 * @param  {object} response
 * @return {object}
 */
var updateProfileService = async function (request, response) {
  debug("trainingpartner.service -> updateProfileService");
  var isValid = common.validateParams([request.body.trainingpartnerid]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_PROFILE_UPDATE_REQUEST, false);
  }
  common.sanitizeAll(request.body);

  var userId = request.body.trainingpartnerid;

  var result = await trainingPartnerDAL.validateUser(userId);
  if (result.status === false) {
    return common.sendResponse(response, result);
  } else if (result.content.length == 0) {
    return common.sendResponse(response, constant.userMessages.ERR_NO_TRAININGPARTNER_FOUND, false);
  } else if (result.content.length > 0) {
    if (result.content[0].status == 0) {
      return common.sendResponse(response, constant.userMessages.ERR_USER_IS_NOT_ACTIVE, false);
    }
  }

  var userInfo = {};
  if (request.body.contact_person_name != undefined) {
    userInfo.contact_person_name = request.body.contact_person_name;
  }
  if (request.body.contact_email_address != undefined) {
    userInfo.contact_email_address = request.body.contact_email_address;
  }
  if (request.body.contact_mobile != undefined) {
    userInfo.contact_mobile = request.body.contact_mobile;
  }
  if (request.body.address != undefined) {
    userInfo.address = request.body.address;
  }

  var userKeys = Object.keys(userInfo);
  var fieldValueUpdate = [];
  userKeys.forEach(function (userKey) {
    if (userInfo[userKey] !== undefined) {
      var fieldValueObj = {};
      fieldValueObj = {
        field: userKey,
        fValue: userInfo[userKey]
      };
      fieldValueUpdate.push(fieldValueObj);
    }
  });
  if (fieldValueUpdate.length > 0) {
    var result = await trainingPartnerDAL.updateUserInfoById(userId, fieldValueUpdate);
    if (result.status === false) {
      return common.sendResponse(response, result);
    } else {
      return common.sendResponse(response, constant.userMessages.UPDATE_USER_PROFILE_SUCCESSFULLY, true);
    }
  } else {
    return common.sendResponse(response, constant.userMessages.UPDATE_USER_PROFILE_SUCCESSFULLY, true);
  }
}




/**
 * get training partner detail service
 * 
 * @param {object} request 
 * @param {object} response 
 * @return {object} 
 */
var getTrainingPartnerDetailService = async function (request, response) {
  debug("trainingpartner.service -> getTrainingPartnerDetailService");
  var isValid = common.validateParams([request.params.id]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_GET_DETAIL_REQUEST, false);
  }
  try {
    let result = await trainingPartnerDAL.getTrainingPartnerDetail(request.params.id);
    if (result.status === false) {
      return common.sendResponse(response, result);
    } else {
      response.render("partials/trainingpartnerdetails.ejs", { data: result.content[0] }, function (err, html) {
        response.send(html);
      });
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.ERR_NO_TRAININGPARTNER_FOUND, false);
  }
}

/**
 * remove training partner
 * 
 * @param {object} request 
 * @param {object} response 
 * @return {object} 
 */
var removeTrainingPartnerService = async function (request, response) {
  debug("trainingpartner.service -> removeTrainingPartnerService");
  var isValid = common.validateParams([request.params.id]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_REMOVE_TP_REQUEST, false);
  }
  try {
    var fieldValueUpdate = [];
    fieldValueUpdate.push({
      field: "is_deleted",
      fValue: 1
    });
    let result = await trainingPartnerDAL.removeTrainingPartner(request.params.id, fieldValueUpdate);
    if (result.status === false) {
      return common.sendResponse(response, result);
    } else {
      return common.sendResponse(response, constant.userMessages.TP_DELETED_SUCCESSFULLY, true);
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.ERR_NO_TRAININGPARTNER_FOUND, false);
  }
}

/**
 * add/update Training Partner
 * 
 * @param {object} request 
 * @param {object} response
 * @return {object} 
 */
var addUpdateTrainingPartnerService = async function (request, response) {
  debug("trainingpartner.service -> addUpdateTrainingPartnerService");
  var isValid = common.validateParams([request.body.nsdc_registration_number, request.body.training_partner_name, request.body.legal_entity_name, request.body.contact_person_name, request.body.contact_email_address, request.body.pincode, request.body.sectorid]);
  if (!isValid) {
    return common.sendResponse(response, constant.userMessages.INVALID_ADD_UPDATE_TP_REQUEST, false);
  }
  common.sanitizeAll(request.body);
  var userID = request.body.trainingpartnerid;

  var trainingpartnerinfo = {};
  if (userID == -1) {
    trainingpartnerinfo.tpid = 'TP-' + randomize('0', 5);
    trainingpartnerinfo.password = bcrypt.hashSync(constant.appConfig.SYSTEM_PASSWORD, 10);
  }

  trainingpartnerinfo.nsdc_registration_number = request.body.nsdc_registration_number;
  trainingpartnerinfo.training_partner_name = request.body.training_partner_name;
  trainingpartnerinfo.legal_entity_name = request.body.legal_entity_name;
  trainingpartnerinfo.contact_person_name = request.body.contact_person_name;
  trainingpartnerinfo.contact_email_address = request.body.contact_email_address;
  trainingpartnerinfo.contact_mobile = request.body.contact_mobile;
  trainingpartnerinfo.address = request.body.address;
  trainingpartnerinfo.fk_stateID = request.body.stateid;
  trainingpartnerinfo.fk_cityID = request.body.cityid;
  trainingpartnerinfo.pincode = request.body.pincode;
  trainingpartnerinfo.fk_sectorID = request.body.sectorid;

  var trainingpartnerKeys = Object.keys(trainingpartnerinfo);
  var fieldValueInsert = [];
  trainingpartnerKeys.forEach(function (trainingpartnerKeys) {
    if (trainingpartnerinfo[trainingpartnerKeys] !== undefined) {
      var fieldValueObj = {};
      fieldValueObj = {
        field: trainingpartnerKeys,
        fValue: trainingpartnerinfo[trainingpartnerKeys]
      }
      fieldValueInsert.push(fieldValueObj);
    }
  });

  try {
    //While add new user we are sending user id as -1
    if (userID == -1) {
      var result = await trainingPartnerDAL.checkUserIsExist(trainingpartnerinfo.tpid, trainingpartnerinfo.nsdc_registration_number);
      if (result.content.length > 0 && result.content[0].trainingpartnerid > 0) {
        return common.sendResponse(response, constant.userMessages.ERR_USER_IS_ALREADY_EXIST, false);
      }
      var res_create_user = await trainingPartnerDAL.createUser(fieldValueInsert);
      return common.sendResponse(response, constant.userMessages.MSG_TP_ADD_SUCCESSFULLY, true);
    } else {
      var result = await trainingPartnerDAL.checkEditUserIsExist(userID);

      if (result.content.length == 0) {
        return common.sendResponse(response, constant.userMessages.ERR_USER_NOT_EXIST, false);
      } else if (result.content.length > 0 && result.content[0].trainingpartnerid == userID) {
        if (result.content[0].nsdc_registration_number != trainingpartnerinfo.nsdc_registration_number) {
          var result = await trainingPartnerDAL.checkUserIsExist(trainingpartnerinfo.tpid, trainingpartnerinfo.nsdc_registration_number);
          if (result.status == true && result.content.length > 0 && result.content[0].trainingpartnerid > 0) {
            return common.sendResponse(response, constant.userMessages.ERR_USER_IS_ALREADY_EXIST, false);
          } else if (result.status == true && result.content.length == 0) {
            var res_update_user = await trainingPartnerDAL.updateUserInfoById(userID, fieldValueInsert);
            return common.sendResponse(response, constant.userMessages.MSG_TP_UPDATE_SUCCESSFULLY, true);
          }
        } else {
          var res_update_user = await trainingPartnerDAL.updateUserInfoById(userID, fieldValueInsert);
          return common.sendResponse(response, constant.userMessages.MSG_TP_UPDATE_SUCCESSFULLY, true);
        }
      }
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
};

/**
 * When user wants to perform the actions like active/inactice/delete with multiple records
 * 
 * @param {*} request 
 * @param {*} response
 * @return {*} object 
 */
var updateMultipleRecordsService = async function (request, response) {
  debug("trainingpartner.service -> updateMultipleRecordsService");
  var isValid = common.validateParams([request.body.ids, request.body.action]);
  if (!isValid) {
    return common.sendResponse(response, constant.userMessages.INVALID_ADD_UPDATE_TP_REQUEST, false);
  }
  var fieldValueInsert = [];

  var fieldValueObj = {};
  switch (request.body.action) {
    case 'active':
      fieldValueObj = {
        field: 'status',
        fValue: 1
      }
      break;
    case 'inactive':
      fieldValueObj = {
        field: 'status',
        fValue: 0
      }
      break;
    case 'delete':
      fieldValueObj = {
        field: 'is_deleted',
        fValue: 1
      }
      break;
  }

  fieldValueInsert.push(fieldValueObj);

  try {
    request.body.ids.forEach(async function (id, index) {
      var result = await trainingPartnerDAL.updateUserInfoById(id, fieldValueInsert);
      if (request.body.ids.length - 1 == index) {
        return common.sendResponse(response, constant.userMessages.MSG_TP_UPDATE_SUCCESSFULLY, true);
      }
    });
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

//cron to check state and city ids from trainingpartners_bulk_upload table
var trainingPartnerBulkUploadCheckStateCityService = async function () {
  var getUsers = await trainingPartnerDAL.getTrainingPartnerStateCityFromBulkUplod();
  if (getUsers.status == true && getUsers.content.length > 0) {
    for (var i = 0; i < getUsers.content.length; i++) {
      var autoIncId = getUsers.content[i].id;
      var statename = getUsers.content[i].state;
      var cityname = getUsers.content[i].district;

      var result = await otherDAL.checkCityStateView(statename, cityname);

      if (result.status == true && result.content.length > 0 && result.content[0].stateid > 0 && result.content[0].cityid > 0) {
        var state = result.content[0].stateid;//state
        var district = result.content[0].cityid;//city
        if (state > 0 && district > 0) {
          var fieldValueUpdate = [];
          fieldValueUpdate.push({
            field: "state",
            fValue: state
          });
          fieldValueUpdate.push({
            field: "district",
            fValue: district
          });

          var result = await trainingPartnerDAL.updateTrainingPartnerBulkUpload(autoIncId, fieldValueUpdate);
          if (result.status == true && result.content.changedRows == 1) {
            var fieldValueUpdate = [];
            fieldValueUpdate.push({
              field: "is_statecity_done",
              fValue: 1
            });
            var result = await trainingPartnerDAL.updateTrainingPartnerBulkUpload(autoIncId, fieldValueUpdate);
            console.log(result);
          }
        } else {
          console.log('No state / city found');

        }
      } else {
        console.log('No state / city found');
      }
    }
  } else {
    console.log('No records');
  }
}

var processTrainingPartnerCSVCronService = async function () {
  //Query:SELECT * FROM `trainingpartners_bulk_upload` WHERE `is_processed` = 0 AND `is_sector_done` = 1 AND `is_statecity_done` = 1 ORDER BY id ASC
  //Step 1 :  Dump into tbl_TrainingPartners  the records
  //Step 3 : Update is_processed flag to 1 which are successfully inserted to the tbl_TrainingPartners.
  debug("other.service -> processTrainingPartnerCSVCronService");
  var getUsers = await trainingPartnerDAL.getTrainingPartnerFromBulkUplod();

  if (getUsers.status == true && getUsers.content.length > 0) {

    for (var i = 0; i < getUsers.content.length; i++) {
      var autoIncId = getUsers.content[i].id;

      var nsdc_registration_number = getUsers.content[i].nsdc_ssc_registration_number;
      var training_partner_name = getUsers.content[i].training_partner_name;
      var legal_entity_name = getUsers.content[i].legal_entity_name;
      var contact_person_name = getUsers.content[i].contact_person_name;
      var contact_email_address = getUsers.content[i].contact_email_address;
      var contact_mobile = getUsers.content[i].contact_mobile;
      var address = getUsers.content[i].address;
      var fk_sectorID = getUsers.content[i].sector;
      var fk_stateID = getUsers.content[i].state;
      var fk_cityID = getUsers.content[i].district;
      var pincode = getUsers.content[i].pincode;
      var tpid = 'TP-' + randomize('0', 5);
      var password = bcrypt.hashSync(constant.appConfig.SYSTEM_PASSWORD, 10);

      var sqlQuery = constant.appConfig.TRAINING_PARTNERS_INSERT_QUERY;
      sqlQuery = sqlQuery + '("' + nsdc_registration_number + '","' + training_partner_name + '","' + legal_entity_name + '","' + contact_person_name + '","' + contact_email_address + '","' + contact_mobile + '","' + address + '","' + fk_stateID + '","' + fk_cityID + '","' + pincode + '","' + fk_sectorID + '","' + tpid + '","' + password + '");';



      try {
        var res = await connection.executeRawQuery(sqlQuery);
        if (res.status === true && res.content !== undefined && res.content.insertId > 0) {
          var fieldValueUpdate = [];
          fieldValueUpdate.push({
            field: "is_processed",
            fValue: 1
          });
          var result = await trainingPartnerDAL.updateTrainingPartnerBulkUpload(autoIncId, fieldValueUpdate);
          console.log(result);
        } else {


        }
      } catch (ex) {

      }
    }
  }
  else {
    console.log("No records to update");
  }
}

var updateHiringRequestService = async function (request, response) {
  debug("trainingpartner.service -> updateHiringRequestService");
  var isValid = common.validateParams([request.body.hiringrequestid, request.body.hiringrequeststatus]);
  if (!isValid) {
    return common.sendResponse(response, constant.userMessages.INVALID_UPDATE_HIRING_REQUEST, false);
  }
  if (request.body.hiringrequeststatus == 5) { // For particular cancel request
    var isValid = common.validateParams([request.body.hiringrequestid, request.body.hiringrequeststatus, request.body.cancelreason]);
    if (!isValid) {
      return common.sendResponse(response, constant.userMessages.INVALID_UPDATE_HIRING_REQUEST, false);
    }
  }
  var totalHiringRequests = request.body.hiringrequestid.toString().split(',');
  var successCounter = 0;
  var hiring_request_detail;
  for (let i = 0; i < totalHiringRequests.length; i++) {
    var fieldValueUpdate = [];
    fieldValueUpdate.push({
      field: 'log_status',
      fValue: '0'
    });
    try {
      var result = await trainingPartnerDAL.checkHiringRequestExist(totalHiringRequests[i]);

      if (result.status === false) {
        return common.sendResponse(response, constant.userMessages.ERR_NO_HIRING_REQUEST_RECORD_FOUND, false);
      } else if (result.status == true && result.content !== undefined && result.content.length == 0) {
        return common.sendResponse(response, constant.userMessages.ERR_NO_HIRING_REQUEST_RECORD_FOUND, false);
      } else if (result.status == true && result.content !== undefined && result.content.length > 0) {
        var result_update = await trainingPartnerDAL.updateHiringRequestById(totalHiringRequests[i], fieldValueUpdate);
        if (result_update.status === false) {
          return common.sendResponse(response, constant.userMessages.ERR_NO_HIRING_REQUEST_RECORD_FOUND, false);
        } else {
          //Insert the new entry for the hiring request afer updaiting the log_status to 0
          var hiringrequestdetailinfo = {};
          hiringrequestdetailinfo.fk_candidatedetailid = result.content[0].candidatedetailid;
          hiringrequestdetailinfo.fk_industrypartnerid = result.content[0].industrypartnerid;
          hiringrequestdetailinfo.fk_trainingpartnerid = result.content[0].trainingpartnerid;
          hiringrequestdetailinfo.fk_candidateid = result.content[0].candidateid;
          hiringrequestdetailinfo.hiring_request_status = request.body.hiringrequeststatus;
          hiring_request_detail = hiringrequestdetailinfo;
          if (request.body.cancelreason) {
            hiringrequestdetailinfo.cancel_reason = request.body.cancelreason;
          }

          var hiringrequestdetailKeys = Object.keys(hiringrequestdetailinfo);
          var fieldValueInsert = [];
          hiringrequestdetailKeys.forEach(function (hiringrequestdetailKeys) {
            if (hiringrequestdetailinfo[hiringrequestdetailKeys] !== undefined) {
              var fieldValueObj = {};
              fieldValueObj = {
                field: hiringrequestdetailKeys,
                fValue: hiringrequestdetailinfo[hiringrequestdetailKeys]
              }
              fieldValueInsert.push(fieldValueObj);
            }
          });
          var res_create_hiringrequest = await industryPartnerDAL.createHiringRequest(fieldValueInsert);
          if (res_create_hiringrequest.status == true && res_create_hiringrequest.content !== undefined && res_create_hiringrequest.content.insertId > 0) {
            successCounter++;
          }
        }
      }
    }
    catch (ex) {
      return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
    }
  }
  hiring_request_detail.hiringrequestdetailid = result.content[0].hiringrequestdetailid;
  if (successCounter > 0) {
    otherService.sendHiringRequestStatusEmailToTP(hiring_request_detail);
    return common.sendResponse(response, constant.userMessages.HIRING_REQUEST_STATUS_CHANGED_SUCCESSFULLY, true);
  } else {
    return common.sendResponse(response, constant.userMessages.ERR_HIRING_STATUS_CHANGE, false);
  }
}

var updateInternalStatusForHiringRequestService = async function (request, response) {
  debug("trainingpartner.service -> updateInternalStatusForHiringRequestService");
  var isValid = common.validateParams([request.body.hiringrequestid, request.body.internalstatus]);
  if (!isValid) {
    return common.sendResponse(response, constant.userMessages.INVALID_UPDATE_HIRING_REQUEST, false);
  }
  var totalHiringRequests = request.body.hiringrequestid.split(',');
  var successCounter = 0;
  for (let i = 0; i < totalHiringRequests.length; i++) {
    var fieldValueUpdate = [];
    fieldValueUpdate.push({
      field: 'internal_log_status',
      fValue: '0'
    });
    fieldValueUpdate.push({
      field: 'log_status',
      fValue: '0'
    });
    try {
      var result = await trainingPartnerDAL.checkHiringRequestExist(totalHiringRequests[i]);

      if (result.status === false) {
        return common.sendResponse(response, constant.userMessages.ERR_NO_HIRING_REQUEST_RECORD_FOUND, false);
      } else if (result.status == true && result.content !== undefined && result.content.length == 0) {
        return common.sendResponse(response, constant.userMessages.ERR_NO_HIRING_REQUEST_RECORD_FOUND, false);
      } else if (result.status == true && result.content !== undefined && result.content.length > 0) {
        var result_update = await trainingPartnerDAL.updateInternalHiringRequestById(totalHiringRequests[i], fieldValueUpdate);
        if (result_update.status === false) {
          return common.sendResponse(response, constant.userMessages.ERR_NO_HIRING_REQUEST_RECORD_FOUND, false);
        } else {
          //Insert the new entry for the hiring request afer updaiting the internal_log_status to 0
          var hiringrequestdetailinfo = {};
          hiringrequestdetailinfo.fk_candidatedetailid = result.content[0].candidatedetailid;
          hiringrequestdetailinfo.fk_industrypartnerid = result.content[0].industrypartnerid;
          hiringrequestdetailinfo.fk_trainingpartnerid = result.content[0].trainingpartnerid;
          hiringrequestdetailinfo.fk_candidateid = result.content[0].candidateid;
          hiringrequestdetailinfo.hiring_request_status = result.content[0].hiring_request_status;
          hiringrequestdetailinfo.internal_status = request.body.internalstatus;

          var hiringrequestdetailKeys = Object.keys(hiringrequestdetailinfo);
          var fieldValueInsert = [];
          hiringrequestdetailKeys.forEach(function (hiringrequestdetailKeys) {
            if (hiringrequestdetailinfo[hiringrequestdetailKeys] !== undefined) {
              var fieldValueObj = {};
              fieldValueObj = {
                field: hiringrequestdetailKeys,
                fValue: hiringrequestdetailinfo[hiringrequestdetailKeys]
              }
              fieldValueInsert.push(fieldValueObj);
            }
          });
          var res_create_hiringrequest = await industryPartnerDAL.createHiringRequest(fieldValueInsert);
          // Update candidate to 'has_joined' == 1,if the status is joined [Code Start]
          var fieldValueUpdateForCandidate = [];
          if (request.body.internalstatus === '5') {
            fieldValueUpdateForCandidate.push({
              field: "has_joined",
              fValue: '1'
            });
          } else {
            fieldValueUpdateForCandidate.push({
              field: "has_joined",
              fValue: '0'
            });
          }
          var res_update_hasJoined = await candidateDAL.updateCandidateById(result.content[0].candidateid,fieldValueUpdateForCandidate);

          // Update candidate to 'has_joined' == 1,if the status is joined [Code Over]
          if (res_create_hiringrequest.status == true && res_create_hiringrequest.content !== undefined && res_create_hiringrequest.content.insertId > 0) {
            successCounter++;
          }
        }
      }
    }
    catch (ex) {
      return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
    }
  }
  if (successCounter > 0) {
    return common.sendResponse(response, constant.userMessages.HIRING_REQUEST_STATUS_CHANGED_SUCCESSFULLY, true);
  } else {
    return common.sendResponse(response, constant.userMessages.ERR_INTERNAL_STATUS_CHANGE, false);
  }
}

var getTrainingPartnerSettingsService = async function (request, response) {
  debug("trainingpartner.service -> getTrainingPartnerSettingsService");
  var isValid = common.validateParams([request.params.id]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_GET_DETAIL_REQUEST, false);
  }
  try {
    let result = await trainingPartnerDAL.getTrainingPartnerDetail(request.params.id);
    if (result.status === true && result.content.length > 0) {
      return common.sendResponse(response, result.content, true);
    } else {
      return common.sendResponse(response, constant.userMessages.ERR_NO_TRAININGPARTNER_FOUND, false);
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

var getTrainingPartnerDashboardDataService = async function (request, response) {
  debug("trainingpartner.service -> getTrainingPartnerDashboardDataService");
  var isValid = common.validateParams([request.body.trainingpartnerid]);
  if (!isValid) {
    return common.sendResponse(response, constant.userMessages.INVALID_GET_TP_DASHBOARD_DATA_REQUEST, false);
  }
  try {
    var result = { counts: [], candidate_statistics: [], recent_hiring_requests: [], recent_activities: [], recruitment_history: [] };
    var totalCadidates = 0;
    var totalTrainingCenters = 0;
    var totalPendingHiringRequests = 0;
    var totalJoinedCandidates = 0;

    //Get Total Number of Candidates
    let totalcandidatesRes = await trainingPartnerDAL.getTotalNumberOfCandidates(request.body.trainingpartnerid);
    if (totalcandidatesRes.status === true && totalcandidatesRes.content.length > 0) {
      totalCadidates = totalcandidatesRes.content[0].totalCandidateCount;
    }

    //Get Total Number of Training Centers
    let totalcentersRes = await trainingPartnerDAL.getTotalNumberOfTrainingcenters(request.body.trainingpartnerid);
    if (totalcentersRes.status === true && totalcentersRes.content.length > 0) {
      totalTrainingCenters = totalcentersRes.content[0].totalTrainingCenterCount;
    }

    //Get Total Number of Pending Hiring Requests
    var filterObj = {};
    filterObj.hiring_request_status = 1;//Pending
    filterObj.trainingpartnerid = request.body.trainingpartnerid;
    let totalpendingrequestRes = await industryPartnerDAL.getHiringRequestsCount(filterObj);
    if (totalpendingrequestRes.status === true && totalpendingrequestRes.content.length > 0) {
      totalPendingHiringRequests = totalpendingrequestRes.content[0].totalCount;
    }

    var countObj = {};
    countObj.totalcandidates = totalCadidates;
    countObj.totaltrainingcenters = totalTrainingCenters;
    countObj.totalhiringrequests = totalPendingHiringRequests;
    result.counts.push(countObj);

    //Get Candidate Statistics [availablecandidates,hiredcandidates,totalcandidates] 
    //get joined cadidates
    let joinedCandidatesRes = await trainingPartnerDAL.getTotalNumberOfJoinedCandidates(request.body.trainingpartnerid);
    if (joinedCandidatesRes.status === true && joinedCandidatesRes.content.length > 0) {
      totalJoinedCandidates = joinedCandidatesRes.content[0].totalJoinedCandidatesCount;
    }

    var availableCandidates = totalCadidates - totalJoinedCandidates;

    var candidateStatisticsObj = {};
    candidateStatisticsObj.availablecandidates = availableCandidates; //availablecandidates = total - joined
    candidateStatisticsObj.hiredcandidates = totalJoinedCandidates; //hired means actually joined candidates
    result.candidate_statistics.push(candidateStatisticsObj);

    //Get Recent Pending Hiring Requests - limit 10 [it means the recent pending requests created for loggedin TP's candidates]
    let recentPendingRequests = await trainingPartnerDAL.getRecentPendingRequests(request.body.trainingpartnerid);
    if (recentPendingRequests.status === true && recentPendingRequests.content.length > 0) {
      result.recent_hiring_requests = recentPendingRequests.content
    }

    //Get Recent activities performed by the logged in TP - limit 10
    let recentActivities = await otherDAL.getRecentActivities(request.body.trainingpartnerid, 'TP');
    if (recentActivities.status === true && recentActivities.content.length > 0) {
      result.recent_activities = recentActivities.content
    }

    //Get Recruitment History - limit 10 [employers who have hired (joined) loggedin TP's candidates]
    let recentRecruitmentres = await trainingPartnerDAL.getRecentRecruitments(request.body.trainingpartnerid);
    if (recentRecruitmentres.status === true && recentRecruitmentres.content.length > 0) {
      result.recruitment_history = recentRecruitmentres.content
    }
    return common.sendResponse(response, result, true);
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}


module.exports = {
  signupService: signupService,
  signinService: signinService,
  changePasswordService: changePasswordService,
  updateProfileService: updateProfileService,
  getTrainingPartnerService: getTrainingPartnerService,
  getTrainingPartnerDetailService: getTrainingPartnerDetailService,
  removeTrainingPartnerService: removeTrainingPartnerService,
  addUpdateTrainingPartnerService: addUpdateTrainingPartnerService,
  updateMultipleRecordsService: updateMultipleRecordsService,
  trainingPartnerBulkUploadCheckStateCityService: trainingPartnerBulkUploadCheckStateCityService,
  processTrainingPartnerCSVCronService: processTrainingPartnerCSVCronService,
  updateHiringRequestService: updateHiringRequestService,
  updateInternalStatusForHiringRequestService: updateInternalStatusForHiringRequestService,
  getTrainingPartnerSettingsService: getTrainingPartnerSettingsService,
  getTrainingPartnerDashboardDataService: getTrainingPartnerDashboardDataService
}
