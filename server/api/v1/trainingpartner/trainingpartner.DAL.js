var debug = require('debug')('server:api:v1:trainingpartner:DAL');
var d3 = require("d3");
var DateLibrary = require('date-management');
var common = require('../common');
var constant = require('../constant');
var query = require('./trainingpartner.query');
var dbDateFormat = constant.appConfig.DB_DATE_FORMAT;

/**
 * checkUserIsExistQuery
 * 
 * @param {String} id trainig partner autoincrement id
 * @returns {object} 
 */

var checkEditUserIsExist = async function (id) {
  debug("trainingpartner.DAL -> checkEditUserIsExist");
  var checkEditUserIsExist = common.cloneObject(query.checkUserIsExistQuery);
  checkEditUserIsExist.filter.and[0].value = id;
  return await common.executeQuery(checkEditUserIsExist);
};

/**
 * checkUserIsExist
 * 
 * @param  {string}  tpid [trainingpartner id]
 * @param  {string}  nsdc_registration_number [nsds registration number]
 * @return {object}
 */

var checkUserIsExist = async function (tpid, nsdc_registration_number) {
  debug("trainingpartner.DAL -> checkUserIsExist");
  var checkTrainingPartnerIsExistQuery = common.cloneObject(query.checkTrainingPartnerIsExistQuery);
  checkTrainingPartnerIsExistQuery.filter.and[1].or[0].value = tpid;
  checkTrainingPartnerIsExistQuery.filter.and[1].or[1].value = nsdc_registration_number;
  return await common.executeQuery(checkTrainingPartnerIsExistQuery);
};
/**
 * create new user
 * 
 * @param  {Array of object}   fieldValue [the array object of the request body]
 * @return {object}
 */
var createUser = async function (fieldValue) {
  debug("trainingpartner.DAL -> createUser");
  var createUserQuery = common.cloneObject(query.createUserQuery);
  createUserQuery.insert = fieldValue;
  return await common.executeQuery(createUserQuery);
};


/**
 *
 * signin using tpid
 *
 * @param  {string}  tpid [trainingpartner id]
 * @return {object}
 */
var userLogin = async function (tpid) {
  debug("trainingpartner.DAL -> userLogin");
  var getUserInfoQuery = common.cloneObject(query.getUserInfoQuery);
  getUserInfoQuery.filter.and[0].value = tpid;
  return await common.executeQuery(getUserInfoQuery);
};

/**
 * expire access token
 * 
 * @param  {string}   userId
 * @param  {string}   deviceId
 * @param  {string}   host
 * @return {object}
 */
var exprieAccessToken = async function (userId, deviceId, host) {
  debug("trainingpartner.DAL -> exprieAccessToken");
  var updateAccessTokenQuery = common.cloneObject(query.updateAccessTokenQuery);
  if (userId === undefined) {
    updateAccessTokenQuery.filter.or[1].value = deviceId;
    updateAccessTokenQuery.filter.or[2].value = host;
  } else {
    updateAccessTokenQuery.filter.or[0].and[0].value = userId;
    updateAccessTokenQuery.filter.or[0].and[1].value = deviceId;
    updateAccessTokenQuery.filter.or[0].and[2].value = host;
  }
  return await common.executeQuery(updateAccessTokenQuery);
};

/**
 * create access token
 *
 * @param  {string}   userId
 * @param  {string}   token
 * @param  {string}   expiryDateTime
 * @param  {string}   deviceId
 * @param  {string}   host
 * @return {object}
 */
var createAccessToken = async function (userId, token, expiryDateTime, deviceId, host) {
  debug("trainingpartner.DAL -> accessTokenGenerate");
  var insertAccessTokenQuery = common.cloneObject(query.insertAccessTokenQuery);
  var dbExpiryDateTime = d3.timeFormat(dbDateFormat)(new Date(expiryDateTime));
  insertAccessTokenQuery.insert.fValue = [userId, token, dbExpiryDateTime, deviceId, host];
  return await common.executeQuery(insertAccessTokenQuery);
};

/**
 * check users login log(transction)
 *
 * @param  {string}   deviceId
 * @param  {string}   deviceType
 * @return {object}
 */
var checkUserTransaction = async function (deviceId, deviceType) {
  debug("trainingpartner.DAL -> checkUserTransaction");
  var checkUserTransactionQuery = common.cloneObject(query.checkUserTransactionQuery);
  checkUserTransactionQuery.filter.and[0].value = deviceId;
  checkUserTransactionQuery.filter.and[1].value = deviceType;
  return await common.executeQuery(checkUserTransactionQuery);
};

/**
 * Update user transaction
 *
 * @param  {string}   deviceId
 * @param  {string}   deviceType
 * @param  {Array of object}   fieldValue
 * @return {object}
 */
var updateUserTransaction = function (deviceId, deviceType, fieldValue) {
  debug("trainingpartner.DAL -> updateUserTransaction");
  var updateUserTransactionQuery = common.cloneObject(query.updateUserTransactionQuery);
  updateUserTransactionQuery.filter.and[0].value = deviceId;
  updateUserTransactionQuery.filter.and[1].value = deviceType;
  updateUserTransactionQuery.update = fieldValue;
  return common.executeQuery(updateUserTransactionQuery);
};

/**
 * 
 * create users login log(transaction)
 *
 * @param  {string}   deviceId
 * @param  {string}   deviceType
 * @return {object} 
 */
var createUserTransaction = function (deviceId, deviceType) {
  debug("trainingpartner.DAL -> createUserTransaction");
  var insertUserTransactionQuery = common.cloneObject(query.insertUserTransactionQuery);
  insertUserTransactionQuery.insert.fValue = [deviceId, deviceType];
  return common.executeQuery(insertUserTransactionQuery);
};

/**
 * 
 * check the users id if it is valid or not
 *  
 * @param  {int}   userId   [description]
 * @return {object}
 */
var validateUser = async function (userId) {
  debug("trainingpartner.DAL -> validateUser");
  var validateUserQuery = common.cloneObject(query.validateUserQuery);
  validateUserQuery.filter.and[0].value = userId;
  return await common.executeQuery(validateUserQuery);
};

/**
 * 
 * update user information by id
 * 
 * @param  {int}   userId
 * @param  {Array of object}   fieldValue [the array object of the request body]
 * @return {object}
 */
var updateUserInfoById = async function (userId, fieldValue) {
  debug("trainingpartner.DAL -> updateUserInfoById");
  var updateUserQuery = common.cloneObject(query.updateUserQuery);
  updateUserQuery.filter.and[0].value = userId;
  updateUserQuery.update = fieldValue;
  return await common.executeQuery(updateUserQuery);
};

/**
 * Get Training Partners count
 * 
 * @param {object} searchObj
 * @return {object} 
 */
var getTrainingPartnersCount = async function (searchObj) {
  debug("trainingpartner.DAL -> getTrainingPartnersCount");
  var getTrainingPartnersCountQuery = common.cloneObject(query.getTrainingPartnersCountQuery);
  var trainingpartnerFilter = { and: [] };
  if (Object.keys(searchObj).length !== 0) {
    common.setSearchParams(searchObj,trainingpartnerFilter);
    getTrainingPartnersCountQuery.filter = trainingpartnerFilter;
  } else {
    delete getTrainingPartnersCountQuery.filter;
  }
  return await common.executeQuery(getTrainingPartnersCountQuery);
}

/**
 * Get Training Partners
 * 
 * @param {Array of object} searchinfo [optional]
 */
var getTrainingPartners = async function (limit, searchObj) {
  debug("trainingpartner.DAL -> getTrainingPartners");
  var getTrainingPartnersQuery = common.cloneObject(query.getTrainingPartnersQuery);
  var trainingpartnerFilter = { and: [] };
  /* if (Object.keys(searchObj).length !== 0) {
    common.setSearchParams(searchObj,trainingpartnerFilter);
    getTrainingPartnersQuery.filter = trainingpartnerFilter;
  } else {
    delete getTrainingPartnersQuery.filter;
  }*/
  //getTrainingPartnersQuery.limit = limit; 
  return await common.executeQuery(getTrainingPartnersQuery);
}

/**
 * Get training partner detail by id
 * 
 * @param {int} trainingpartnerid 
 * @return {object}
 */
var getTrainingPartnerDetail = async function (trainingpartnerid) {
  debug("trainingpartner.DAL -> getTrainingPartnerDetail");
  var getTrainingPartnerDetailQuery = common.cloneObject(query.getTrainingPartnerDetailQuery);
  getTrainingPartnerDetailQuery.filter.and[0].value = trainingpartnerid;
  return await common.executeQuery(getTrainingPartnerDetailQuery);
}


/**
 * Get training partner detail by id for email
 * 
 * @param {int} trainingpartnerid 
 * @return {object}
 */
var getTrainingPartnerInfo = async function (trainingpartnerid) {
  debug("trainingpartner.DAL -> getTrainingPartnerInfo");
  var getTrainingPartnerInfoQuery = common.cloneObject(query.getTrainingPartnerInfoQuery);
  getTrainingPartnerInfoQuery.filter.and[0].value = trainingpartnerid;
  return await common.executeQuery(getTrainingPartnerInfoQuery);
}


/**
 * Remove training partner(soft delete) by id
 * 
 * @param {int} trainingpartnerid
 * @param {array of object} fieldValue 
 * @return {object}
 */
var removeTrainingPartner = async function(trainingpartnerid,fieldValue){
  debug("trainingpartner.DAL -> removeTrainingPartner");
  var removeTrainingPartnerQuery = common.cloneObject(query.removeTrainingPartnerQuery);
  removeTrainingPartnerQuery.filter.and[0].value = trainingpartnerid;
  removeTrainingPartnerQuery.update = fieldValue;
  return await common.executeQuery(removeTrainingPartnerQuery);
}

var getTrainingPartnerStateCityFromBulkUplod = async function(){
  debug("trainingpartner.DAL -> getTrainingPartnerStateCityFromBulkUplod");
  var getTrainingPartnerStateCityFromBulkUplod = common.cloneObject(query.getTrainingPartnerStateCityFromBulkUplodQuery);
  getTrainingPartnerStateCityFromBulkUplod.limit = constant.appConfig.CRON_TRAININGPARTNER_BULK_RECORDS_LIMIT;
  return await common.executeQuery(getTrainingPartnerStateCityFromBulkUplod);
}

var updateTrainingPartnerBulkUpload = async function(id, fieldValue){
  debug("trainingpartner.DAL -> updateTrainingPartnerBulkUpload");
  var updateTrainingPartnerBulkUpload = common.cloneObject(query.updateTrainingPartnerBulkUploadQuery);
  updateTrainingPartnerBulkUpload.filter.and[0].value = id;
  updateTrainingPartnerBulkUpload.update = fieldValue;
  return await common.executeQuery(updateTrainingPartnerBulkUpload);
}

var getTrainingPartnerFromBulkUplod = async function () {
  debug("trainingpartner.DAL -> getTrainingPartnerFromBulkUplod");
  var getTrainingPartnerFromBulkUplod = common.cloneObject(query.getTrainingPartnerFromBulkUplodQuery);
  getTrainingPartnerFromBulkUplod.limit = constant.appConfig.CRON_TRAININGPARTNER_BULK_RECORDS_LIMIT;
  return await common.executeQuery(getTrainingPartnerFromBulkUplod);
}

var checkHiringRequestExist = async function (hiringrequestid) {
  debug("trainingpartner.DAL -> updateHiringRequestById");
  var checkHiringRequestExist = common.cloneObject(query.checkHiringRequestExistQuery);
  checkHiringRequestExist.filter.and[0].value = hiringrequestid;
  return await common.executeQuery(checkHiringRequestExist);
}

var updateHiringRequestById = async function (hiringrequestid, fieldValue) {
  debug("trainingpartner.DAL -> updateHiringRequestById");
  var updateHiringRequestById = common.cloneObject(query.updateHiringRequestByIdQuery);
  updateHiringRequestById.filter.and[0].value = hiringrequestid;
  updateHiringRequestById.update = fieldValue;
  return await common.executeQuery(updateHiringRequestById);
}

var updateInternalHiringRequestById = async function(hiringrequestid, fieldValue){
  debug("trainingpartner.DAL -> updateInternalHiringRequestById");
  var updateInternalHiringRequestById = common.cloneObject(query.updateInternalHiringRequestByIdQuery);
  updateInternalHiringRequestById.filter.and[0].value = hiringrequestid;
  updateInternalHiringRequestById.update = fieldValue;
  return await common.executeQuery(updateInternalHiringRequestById);
}

var getTotalNumberOfCandidates = async function(trainingpartnerid){
  debug("trainingpartner.DAL -> getTotalNumberOfCandidates");
  var getTotalNumberOfCandidates = common.cloneObject(query.getTotalNumberOfCandidatesQuery);
  getTotalNumberOfCandidates.filter.and[0].value = trainingpartnerid;
  return await common.executeQuery(getTotalNumberOfCandidates);
}

var getTotalNumberOfTrainingcenters = async function(trainingpartnerid){
  debug("trainingpartner.DAL -> getTotalNumberOfTrainingcenters");
  var getTotalNumberOfTrainingcenters = common.cloneObject(query.getTotalNumberOfTrainingcentersQuery);
  getTotalNumberOfTrainingcenters.filter.and[0].value = trainingpartnerid;
  return await common.executeQuery(getTotalNumberOfTrainingcenters);
}

var getTotalNumberOfJoinedCandidates = async function(trainingpartnerid){
  debug("trainingpartner.DAL -> getTotalNumberOfJoinedCandidates");
  var getTotalNumberOfJoinedCandidates = common.cloneObject(query.getTotalNumberOfJoinedCandidatesQuery);
  getTotalNumberOfJoinedCandidates.filter.and[0].value = trainingpartnerid;
  return await common.executeQuery(getTotalNumberOfJoinedCandidates);
}

var getRecentPendingRequests = async function(trainingpartnerid){
  debug("trainingpartner.DAL -> getRecentPendingRequests");
  var getRecentPendingRequests = common.cloneObject(query.getRecentPendingRequestsQuery);
  getRecentPendingRequests.filter.and[0].value = trainingpartnerid;
  getRecentPendingRequests.limit = constant.appConfig.TP_DASHBOARD_DATA_RECENT_HIRING_REQUEST_LIMIT;
  return await common.executeQuery(getRecentPendingRequests);
}

var getRecentRecruitments = async function(trainingpartnerid){
  debug("trainingpartner.DAL -> getRecentRecruitments");
  var getRecentRecruitments = common.cloneObject(query.getRecentRecruitmentsQuery);
  getRecentRecruitments.filter.and[0].value = trainingpartnerid;
  getRecentRecruitments.limit = constant.appConfig.TP_DASHBOARD_DATA_RECENT_RECRUITMENT_LIMIT;
  return await common.executeQuery(getRecentRecruitments);
}

module.exports = {
  checkEditUserIsExist : checkEditUserIsExist,
  checkUserIsExist: checkUserIsExist,
  createUser: createUser,
  userLogin: userLogin,
  exprieAccessToken: exprieAccessToken,
  createAccessToken: createAccessToken,
  checkUserTransaction: checkUserTransaction,
  updateUserTransaction: updateUserTransaction,
  createUserTransaction: createUserTransaction,
  validateUser: validateUser,
  updateUserInfoById: updateUserInfoById,
  getTrainingPartnersCount : getTrainingPartnersCount,
  getTrainingPartners : getTrainingPartners,
  getTrainingPartnerDetail : getTrainingPartnerDetail,
  getTrainingPartnerInfo: getTrainingPartnerInfo,
  removeTrainingPartner : removeTrainingPartner,
  getTrainingPartnerStateCityFromBulkUplod : getTrainingPartnerStateCityFromBulkUplod,
  updateTrainingPartnerBulkUpload : updateTrainingPartnerBulkUpload,
  getTrainingPartnerFromBulkUplod : getTrainingPartnerFromBulkUplod,
  updateHiringRequestById : updateHiringRequestById,
  checkHiringRequestExist : checkHiringRequestExist,
  updateInternalHiringRequestById : updateInternalHiringRequestById,
  getTotalNumberOfCandidates : getTotalNumberOfCandidates,
  getTotalNumberOfTrainingcenters : getTotalNumberOfTrainingcenters,
  getTotalNumberOfJoinedCandidates : getTotalNumberOfJoinedCandidates,
  getRecentPendingRequests : getRecentPendingRequests,
  getRecentRecruitments : getRecentRecruitments
}
