var tbl_UserMaster = "tbl_UserMaster";
var tbl_RoleMaster = "tbl_RoleMaster";
var tbl_UserType = "tbl_UserType";
var tbl_OTP = "tbl_OTP";
var tbl_AccessToken = "tbl_AccessToken";
var tbl_transactionMaster = "tbl_transactionMaster";
var tbl_TrainingPartners = "tbl_TrainingPartners";
var tbl_TrainingCenters = "tbl_TrainingCenters";
var tbl_Candidates = "tbl_Candidates";
var view_GetUserDetail = "view_GetUserDetail";
var view_GetLocationDetail = "view_GetLocationDetail";
var tbl_SectorMaster = "tbl_SectorMaster";
var trainingpartners_bulk_upload = "trainingpartners_bulk_upload";
var tbl_HiringRequestsDetail = "tbl_HiringRequestsDetail";
var view_HiringDetail = 'view_HiringDetail';

var query = {
    checkUserIsExistQuery: {
        table: tbl_TrainingPartners,
        select: [{
            field: 'pk_trainingpartnerid',
            alias: 'trainingpartnerid'
        }, {
            field: 'nsdc_registration_number',
            alias: 'nsdc_registration_number'
        }],
        filter: {
            and: [{
                field: 'pk_trainingpartnerid',
                operator: 'EQ',
                value: ''
            }]
        }
    }, // check user is exist query end
    checkTrainingPartnerIsExistQuery: {
        table: tbl_TrainingPartners,
        select: [{
            field: 'pk_trainingpartnerid',
            alias: 'trainingpartnerid'
        }],
        filter: {
            and: [{
                field: 'is_deleted',
                operator: 'EQ',
                value: 0
            }, {
                or: [{
                    field: 'tpid',
                    operator: 'EQ',
                    value: ''
                }, {
                    field: 'nsdc_registration_number',
                    operator: 'EQ',
                    value: ''
                }]
            }],
        }
    },
    createUserQuery: {
        table: tbl_TrainingPartners,
        insert: []
    },
    getUserInfoQuery: {
        table: tbl_TrainingPartners,
        alias: 'TP',
        select: [{
            field: 'TP.pk_trainingpartnerid',
            encloseField: false,
            alias: 'trainingpartnerid'
        }, {
            field: 'TP.tpid',
            encloseField: false,
            alias: 'tpid'
        }, {
            field: 'TP.password',
            encloseField: false,
            alias: 'password'
        }, {
            field: 'TP.nsdc_registration_number',
            encloseField: false,
            alias: 'nsdc_registration_number'
        }, {
            field: 'TP.training_partner_name',
            encloseField: false,
            alias: 'training_partner_name'
        }, {
            field: 'TP.legal_entity_name',
            encloseField: false,
            alias: 'legal_entity_name'
        }, {
            field: 'TP.contact_person_name',
            encloseField: false,
            alias: 'contact_person_name'
        }, {
            field: 'TP.contact_email_address',
            encloseField: false,
            alias: 'contact_email_address'
        }, {
            field: 'TP.contact_mobile',
            encloseField: false,
            alias: 'contact_mobile'
        }, {
            field: 'TP.address',
            encloseField: false,
            alias: 'address'
        }, {
            field: 'TP.pincode',
            encloseField: false,
            alias: 'pincode'
        }, {
            field: 'TP.status',
            encloseField: false,
            alias: 'status'
        }, {
            field: 'TP.created_date',
            encloseField: false,
            alias: 'created_date'
        }],
        filter: {
            and: [{
                field: 'TP.tpid',
                encloseField: false,
                operator: 'EQ',
                value: ''
            },{
                field: 'TP.is_deleted',
                encloseField: false,
                operator: 'EQ',
                value: 0
            }]
        }

    },
    updateAccessTokenQuery: {
        table: tbl_AccessToken,
        update: [{
            field: 'isExpired',
            fValue: 1
        }],
        filter: {
            or: [{
                and: [{
                    field: 'fk_trainingpartnerid',
                    operator: 'EQ',
                    value: ''
                }, {
                    field: 'deviceID',
                    operator: 'EQ',
                    value: ''
                }, {
                    field: 'requestHost',
                    operator: 'EQ',
                    value: ''
                }]
            }, {
                field: 'deviceID',
                operator: 'EQ',
                value: ''
            }, {
                field: 'requestHost',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    insertAccessTokenQuery: {
        table: tbl_AccessToken,
        insert: {
            field: ["fk_trainingpartnerid", "token", "expiryDateTime", "deviceID", "requestHost"],
            fValue: []
        }
    },
    checkUserTransactionQuery: {
        table: tbl_transactionMaster,
        select: [{
            field: 'pk_transactionID',
            aggregation: 'count',
            alias: 'totalCount'
        }],
        filter: {
            and: [{
                field: 'deviceID',
                operator: 'EQ',
                value: ''
            }, {
                field: 'deviceType',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    updateUserTransactionQuery: {
        table: tbl_transactionMaster,
        update: [{
            field: 'isLogedIn',
            fValue: 1
        }],
        filter: {
            and: [{
                field: 'deviceID',
                operator: 'EQ',
                value: ''
            }, {
                field: 'deviceType',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    insertUserTransactionQuery: {
        table: tbl_transactionMaster,
        insert: {
            field: ["deviceID", "deviceType"],
            fValue: []
        }
    },
    validateUserQuery: {
        table: tbl_TrainingPartners,
        select: [{
            field: 'pk_trainingpartnerid',
            alias: 'trainingpartnerid'
        }, {
            field: 'tpid',
            alias: 'tpid'
        }, {
            field: 'password',
            alias: 'password'
        }, {
            field: 'status',
            alias: 'status'
        }],
        filter: {
            and: [{
                field: 'pk_trainingpartnerid',
                operator: 'EQ',
                value: ''
            }, {
                field: 'is_deleted',
                operator: 'EQ',
                value: 0
            }]
        }
    },
    updateUserQuery: {
        table: tbl_TrainingPartners,
        update: [],
        filter: {
            and: [{
                field: 'pk_trainingpartnerid',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    getTrainingPartnersCountQuery: {
        table: tbl_TrainingPartners,
        select: [{
            field: 'pk_trainingpartnerid',
            aggregation: 'count',
            alias: 'totalCount'
        }],
        filter: {}
    },
    getTrainingPartnersQuery: {
        table: tbl_TrainingPartners,
        select: [{
            field: 'pk_trainingpartnerid',
            alias: 'trainingpartnerid'
        }, {
            field: 'tpid',
            alias: 'tpid'
        }, {
            field: 'nsdc_registration_number',
            alias: 'nsdc_registration_number'
        }, {
            field: 'training_partner_name',
            alias: 'training_partner_name'
        }, {
            field: 'legal_entity_name',
            alias: 'legal_entity_name'
        }, {
            field: 'contact_person_name',
            alias: 'contact_person_name'
        }, {
            field: 'contact_email_address',
            alias: 'contact_email_address'
        }, {
            field: 'contact_mobile',
            alias: 'contact_mobile'
        }, {
            field: 'address',
            alias: 'address'
        }, {
            field: 'fk_sectorID',
            alias: 'sectorid'
        }, {
            field: 'fk_stateID',
            alias: 'stateid'
        }, {
            field: 'fk_cityID',
            alias: 'cityid'
        }, {
            field: 'pincode',
            alias: 'pincode'
        }, {
            field: 'status',
            alias: 'status'
        }, {
            field: 'is_email_sent',
            alias: 'is_email_sent'
        }],
        filter: {
            and: [{
                field: 'is_deleted',
                operator: 'EQ',
                value: 0
            }]
        },
        sortby: [{
            field: 'pk_trainingpartnerid',
            order: 'DESC'
        }]
    },
    getTrainingPartnerDetailQuery: {
        join: {
            table: tbl_TrainingPartners,
            alias: 'TP',
            joinwith: [{
                table: view_GetLocationDetail,
                alias: 'LD',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'LD',
                        field: 'stateid',
                        operator: 'eq',
                        value: {
                            table: 'TP',
                            field: 'fk_stateID'
                        }
                    }, {
                        table: 'LD',
                        field: 'cityid',
                        operator: 'eq',
                        value: {
                            table: 'TP',
                            field: 'fk_cityID'
                        }
                    }]
                }
            },{
                table: tbl_SectorMaster,
                alias: 'SEC',
                joincondition: {
                    table: 'SEC',
                    field: 'pk_sectorID',
                    operator: 'find_in_set',
                    value: {
                        table: 'TP',
                        field: 'fk_sectorID'
                    }
                }
            }]
        },
        select: [{
            field: 'GROUP_CONCAT(SEC.sectorName)',
            encloseField: false,
            alias: 'sectorname'
        }, {
            field: 'LD.stateName',
            encloseField: false,
            alias: 'stateName'
        }, {
            field: 'LD.stateid',
            encloseField: false,
            alias: 'stateid'
        }, {
            field: 'LD.cityid',
            encloseField: false,
            alias: 'cityid'
        }, {
            field: 'LD.cityName',
            encloseField: false,
            alias: 'cityName'
        }, {
            field: 'TP.pk_trainingpartnerid',
            encloseField: false,
            alias: 'trainingpartnerid'
        }, {
            field: 'TP.tpid',
            encloseField: false,
            alias: 'tpid'
        }, {
            field: 'TP.nsdc_registration_number',
            encloseField: false,
            alias: 'nsdc_registration_number'
        }, {
            field: 'TP.training_partner_name',
            encloseField: false,
            alias: 'training_partner_name'
        }, {
            field: 'TP.legal_entity_name',
            encloseField: false,
            alias: 'legal_entity_name'
        }, {
            field: 'TP.contact_person_name',
            encloseField: false,
            alias: 'contact_person_name'
        }, {
            field: 'TP.contact_email_address',
            encloseField: false,
            alias: 'contact_email_address'
        }, {
            field: 'TP.contact_mobile',
            encloseField: false,
            alias: 'contact_mobile'
        }, {
            field: 'TP.address',
            encloseField: false,
            alias: 'address'
        }, {
            field: 'TP.pincode',
            encloseField: false,
            alias: 'pincode'
        }, {
            field: 'TP.status',
            encloseField: false,
            alias: 'status'
        }, {
            field: 'TP.is_email_sent',
            encloseField: false,
            alias: 'is_email_sent'
        }, {
            field: 'TP.created_date',
            encloseField: false,
            alias: 'created_date'
        }],
        filter: {
            and: [{
                field: 'TP.pk_trainingpartnerid',
                encloseField: false,
                operator: 'EQ',
                value: ''
            }, {
                field: 'TP.is_deleted',
                encloseField: false,
                operator: 'EQ',
                value: 0
            },{
                field: 'SEC.status',
                encloseField: false,
                operator: 'EQ',
                value: 1
            }]
        }
    },
    removeTrainingPartnerQuery: {
        table: tbl_TrainingPartners,
        update: [],
        filter: {
            and: [{
                field: 'pk_trainingpartnerid',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    getTrainingPartnerInfoQuery: {
        table: tbl_TrainingPartners,
        select: [
        {
            field: 'contact_person_name',
            alias: 'contact_person_name'
        }, {
            field: 'contact_email_address',
            alias: 'contact_email_address'
        }
    ],
        filter: {
            and: [
            {
                field: 'pk_trainingpartnerid',
                operator: 'EQ',
                value: ''
            },
                {
                field: 'is_deleted',
                operator: 'EQ',
                value: 0
            }]
        },
        sortby: [{
            field: 'pk_trainingpartnerid',
            order: 'DESC'
        }]
    },
    getTrainingPartnerStateCityFromBulkUplodQuery: {
        table: trainingpartners_bulk_upload,
        select: [{
            field: 'id',
        }, {
            field: 'state',
        }, {
            field: 'district',
        }],
        filter: {
            and: [{
                field: 'is_processed',
                operator: 'EQ',
                value: 0
            }, {
                field: 'is_statecity_done',
                operator: 'EQ',
                value: 0
            }]
        },
        sortby: [{
            field: 'id',
            order: 'ASC'
        }]
    },
    getTrainingPartnerFromBulkUplodQuery: {
        table: trainingpartners_bulk_upload,
        select: [{
            field: 'id',
        }, {
            field: 'nsdc_ssc_registration_number',
        }, {
            field: 'training_partner_name',
        }, {
            field: 'legal_entity_name',
        }, {
            field: 'contact_person_name',
        }, {
            field: 'contact_email_address',
        }, {
            field: 'contact_mobile',
        }, {
            field: 'address',
        }, {
            field: 'sector',
        }, {
            field: 'state',
        }, {
            field: 'district',
        }, {
            field: 'pincode',
        }],
        filter: {
            and: [{
                field: 'is_processed',
                operator: 'EQ',
                value: 0
            }, {
                field: 'is_sector_done',
                operator: 'EQ',
                value: 1
            }, {
                field: 'is_statecity_done',
                operator: 'EQ',
                value: 1
            }]
        },
        sortby: [{
            field: 'id',
            order: 'ASC'
        }]
    },
    updateTrainingPartnerBulkUploadQuery: {
        table: trainingpartners_bulk_upload,
        update: [],
        filter: {
            and: [{
                field: 'id',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    checkHiringRequestExistQuery: {
        table: tbl_HiringRequestsDetail,
        select: [{
            field: 'pk_hiringrequestdetailid',
            alias: 'hiringrequestdetailid'
        }, {
            field: 'fk_candidatedetailid',
            alias: 'candidatedetailid'
        }, {
            field: 'fk_industrypartnerid',
            alias: 'industrypartnerid'
        }, {
            field: 'fk_trainingpartnerid',
            alias: 'trainingpartnerid'
        }, {
            field: 'fk_candidateid',
            alias: 'candidateid'
        }, {
            field: 'hiring_request_status',
            alias: 'hiring_request_status'
        }],
        filter: {
            and: [{
                field: 'pk_hiringrequestdetailid',
                operator: 'EQ',
                value: ''
            }, {
                field: 'log_status',
                operator: 'EQ',
                value: 1
            }]
        }
    },
    updateHiringRequestByIdQuery: {
        table: tbl_HiringRequestsDetail,
        update: [],
        filter: {
            and: [{
                field: 'pk_hiringrequestdetailid',
                operator: 'EQ',
                value: ''
            }, {
                field: 'log_status',
                operator: 'EQ',
                value: 1
            }]
        }
    },
    updateInternalHiringRequestByIdQuery: {
        table: tbl_HiringRequestsDetail,
        update: [],
        filter: {
            and: [{
                field: 'pk_hiringrequestdetailid',
                operator: 'EQ',
                value: ''
            }, {
                field: 'log_status',
                operator: 'EQ',
                value: 1
            }, {
                field: 'internal_log_status',
                operator: 'EQ',
                value: 1
            }]
        }
    },
    getTotalNumberOfCandidatesQuery: {
        table: tbl_Candidates,
        select: [{
            field: 'pk_candidateid',
            aggregation: 'count',
            alias: 'totalCandidateCount'
        }],
        filter: {
            and: [{
                field: 'fk_trainingpartnerid',
                operator: 'EQ',
                value: ''
            }, {
                field: 'status',
                operator: 'EQ',
                value: 1
            }, {
                field: 'is_deleted',
                operator: 'EQ',
                value: 0
            }]
        }
    },
    getTotalNumberOfTrainingcentersQuery: {
        table: tbl_TrainingCenters,
        select: [{
            field: 'pk_trainingcenterid',
            aggregation: 'count',
            alias: 'totalTrainingCenterCount'
        }],
        filter: {
            and: [{
                field: 'fk_trainingpartnerid',
                operator: 'EQ',
                value: ''
            }, {
                field: 'status',
                operator: 'EQ',
                value: 1
            }, {
                field: 'is_deleted',
                operator: 'EQ',
                value: 0
            }]
        }
    },
    getTotalNumberOfJoinedCandidatesQuery: {
        table: view_HiringDetail,
        select: [{
            field: 'hiringrequestdetailid',
            aggregation: 'count',
            alias: 'totalJoinedCandidatesCount'
        }],
        filter: {
            and: [{
                field: 'trainingpartnerid',
                operator: 'EQ',
                value: ''
            }, {
                field: 'hiring_request_status',
                operator: 'EQ',
                value: 2
            },{
                field: 'internal_status',
                operator: 'EQ',
                value: 5
            },{
                field: 'log_status',
                operator: 'EQ',
                value: 1
            }, {
                field: 'internal_log_status',
                operator: 'EQ',
                value: 1
            }]
        }
    },
    getRecentPendingRequestsQuery: {
        table: view_HiringDetail,
        select: [{
            field: 'hiringrequestdetailid',
        }, {
            field: 'industry_partner_name',
            alias: 'employername'
        }, {
            field: 'candidate_name',
            alias: 'candidatename'
        }, {
            field: 'statename',
        }, {
            field: 'jobroleName',
            alias: 'jobrolename'
        },
        ],
        filter: {
            and: [{
                field: 'trainingpartnerid',
                operator: 'EQ',
                value: ''
            }, {
                field: 'hiring_request_status',
                operator: 'EQ',
                value: 1
            }, {
                field: 'log_status',
                operator: 'EQ',
                value: 1
            }]
        },
        sortby: [{
            field: 'hiringrequestdetailid',
            order: 'DESC'
        }]
    },
    getRecentRecruitmentsQuery: {
        table: view_HiringDetail,
        select: [{
            field: 'industry_partner_name',
            alias: 'employername'
        }, {
            field: 'DATE_FORMAT(modified_date,"%d %b %Y")',
            encloseField: false,
            alias: 'date'
        }, {
            field: 'DISTINCT(candidateid)',
            encloseField: false,
            aggregation: 'count',
            alias: 'count'
        }],
        filter: {
            and: [{
                field: 'trainingpartnerid',
                operator: 'EQ',
                value: ''
            }, {
                field: 'internal_status',
                operator: 'EQ',
                value: 5
            }, {
                field: 'internal_log_status',
                operator: 'EQ',
                value: 1
            }]
        },
        groupby: [{
            field: 'industrypartnerid'
        }],
        sortby: [{
            field: 'modified_date',
            order: 'DESC'
        }]
    }
};


module.exports = query
