var express = require('express');
var services = require('./trainingpartner.service');
var middleware = require('../../../middleware');

var router = express.Router();
module.exports = router;
 
router.post('/trainingpartner-signup', middleware.logger, services.signupService);
router.post('/trainingpartner-signin', middleware.logger, services.signinService);
router.post('/trainingpartner-changePassword', middleware.logger, services.changePasswordService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/trainingpartner-updateProfile', middleware.logger, services.updateProfileService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/get-trainingpartner', middleware.checkAccessToken,middleware.userRightsByAPI,middleware.logger,services.getTrainingPartnerService);
router.get('/get-trainingpartner-detail/:id', middleware.checkAccessToken,middleware.userRightsByAPI,middleware.logger, services.getTrainingPartnerDetailService);
router.post('/remove-trainingpartner/:id', middleware.checkAccessToken,middleware.userRightsByAPI,middleware.logger, services.removeTrainingPartnerService);
router.post('/addupdate-trainingpartner', middleware.checkAccessToken,middleware.userRightsByAPI,middleware.logger, services.addUpdateTrainingPartnerService);
router.post('/addupdate-trainingpartner-multiplerecords', middleware.checkAccessToken,middleware.userRightsByAPI,middleware.logger, services.updateMultipleRecordsService);

router.post('/updateHiringRequest', middleware.logger, services.updateHiringRequestService);
router.post('/updateInternalStatusForHiringRequest', middleware.logger, services.updateInternalStatusForHiringRequestService);
router.post('/getTrainingPartnerSettings/:id', middleware.logger, services.getTrainingPartnerSettingsService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/getTrainingPartnerDashboardData', middleware.logger,services.getTrainingPartnerDashboardDataService);
//cron urls
router.post('/trainingPartnerBulkUploadCheckStateCityService', services.trainingPartnerBulkUploadCheckStateCityService);
router.post('/processTrainingPartnerCSVCronService', services.processTrainingPartnerCSVCronService);