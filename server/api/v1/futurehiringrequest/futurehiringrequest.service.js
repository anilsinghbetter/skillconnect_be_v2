var debug = require('debug')('server:api:v1:futurehiringrequest:service');
var d3 = require("d3");
var DateLibrary = require('date-management');
var uuid = require('uuid');
var common = require('../common');
var constant = require('../constant');
var futurehiringrequestsDAL = require('./futurehiringrequest.DAL');
var otherDAL = require('../other/other.DAL');
var dbDateFormat = constant.appConfig.DB_DATE_FORMAT;
var config = require('../../../../config');
var connection = require('../../../helper/connection');
var fs = require('fs');
var QueryBuilder = require('../../../helper/node-datatable-master');
var async = require("async");

/**
 * add/update Future Hiring Request
 * 
 * @param  {object} request
 * @param  {object} response
 * @return {object}
 */
var addUpdateFutureHiringRequestService = async function (request, response) {
  debug("futurehiringrequests.service -> addUpdateFutureHiringRequestService");
  var isValid = common.validateParams([
    request.body.sectorid,
    request.body.industrypartnerid
    /* request.body.jobrole,
   request.body.number_of_positions,
   request.body.stateid,
   request.body.districtid,
   request.body.gender */
  ]);
  if (!isValid) {
    return common.sendResponse(response, constant.userMessages.INVALID_ADD_UPDATE_FUTURE_HIRING_REQUEST, false);
  }
  //common.sanitizeAll(request.body);

  var successCounter = 0;
  var errorCounter = 0;
  //request.body.members = request.body.members.filter(function (e) { return e });
  request.body.members = request.body.members.filter(value => Object.keys(value).length !== 0);

  var errorMsgs = [];
  if (request.body.members !== null && request.body.members !== undefined) {
    for (var key in request.body.members) {
      if (request.body.members.hasOwnProperty(key)) {
        item = request.body.members[key];

        var futurerequestinfo = {};
        if (item.month !== undefined && typeof item.month === 'object') {
          /* var month = item.month.join();
          futurerequestinfo.month = month; */
          var currentDate = new Date();
          var currentYear = currentDate.getFullYear();
          var nextYear = currentDate.getFullYear() + 1;
          var currentMonth = currentDate.getMonth() + 1;
          for (var monthkey in item.month) {
            switch (item.month[monthkey]) {
              case 'Jan-' + currentYear:
                if (1 === currentMonth) {
                  futurerequestinfo.currentmonth = item.number_of_positions;
                } else {
                  futurerequestinfo.January = item.number_of_positions;
                }
                break;
              case 'Jan-' + nextYear:
                futurerequestinfo.January = item.number_of_positions;
                break;
              case 'Feb-' + currentYear:
                if (2 === currentMonth) {
                  futurerequestinfo.currentmonth = item.number_of_positions;
                } else {
                  futurerequestinfo.February = item.number_of_positions;
                }
                break;
              case 'Feb-' + nextYear:
                futurerequestinfo.February = item.number_of_positions;
                break;
              case 'Mar-' + currentYear:
                if (3 === currentMonth) {
                  futurerequestinfo.currentmonth = item.number_of_positions;
                } else {
                  futurerequestinfo.March = item.number_of_positions;
                }
                break;
              case 'Mar-' + nextYear:
                futurerequestinfo.March = item.number_of_positions;
                break;
              case 'Apr-' + currentYear:
                if (4 === currentMonth) {
                  futurerequestinfo.currentmonth = item.number_of_positions;
                } else {
                  futurerequestinfo.April = item.number_of_positions;
                }
                break;
              case 'Apr-' + nextYear:
                futurerequestinfo.April = item.number_of_positions;
                break;
              case 'May-' + currentYear:
                if (5 === currentMonth) {
                  futurerequestinfo.currentmonth = item.number_of_positions;
                } else {
                  futurerequestinfo.May = item.number_of_positions;
                }
                break;
              case 'May-' + nextYear:
                futurerequestinfo.May = item.number_of_positions;
                break;
              case 'Jun-' + currentYear:
                if (6 === currentMonth) {
                  futurerequestinfo.currentmonth = item.number_of_positions;
                } else {
                  futurerequestinfo.June = item.number_of_positions;
                }
                break;
              case 'Jun-' + nextYear:
                futurerequestinfo.June = item.number_of_positions;
                break;
              case 'Jul-' + currentYear:
                if (7 === currentMonth) {
                  futurerequestinfo.currentmonth = item.number_of_positions;
                } else {
                  futurerequestinfo.July = item.number_of_positions;
                }
                break;
              case 'Jul-' + nextYear:
                futurerequestinfo.July = item.number_of_positions;
                break;
              case 'Aug-' + currentYear:
                if (8 === currentMonth) {
                  futurerequestinfo.currentmonth = item.number_of_positions;
                } else {
                  futurerequestinfo.August = item.number_of_positions;
                }
                break;
              case 'Aug-' + nextYear:
                futurerequestinfo.August = item.number_of_positions;
                break;
              case 'Sep-' + currentYear:
                if (9 === currentMonth) {
                  futurerequestinfo.currentmonth = item.number_of_positions;
                } else {
                  futurerequestinfo.September = item.number_of_positions;
                }
                break;
              case 'Sep-' + nextYear:
                futurerequestinfo.September = item.number_of_positions;
                break;
              case 'Oct-' + currentYear:
                if (10 === currentMonth) {
                  futurerequestinfo.currentmonth = item.number_of_positions;
                } else {
                  futurerequestinfo.October = item.number_of_positions;
                }
                break;
              case 'Oct-' + nextYear:
                futurerequestinfo.October = item.number_of_positions;
                break;
              case 'Nov-' + currentYear:
                if (11 === currentMonth) {
                  futurerequestinfo.currentmonth = item.number_of_positions;
                } else {
                  futurerequestinfo.November = item.number_of_positions;
                }
                break;
              case 'Nov-' + nextYear:
                futurerequestinfo.November = item.number_of_positions;
                break;
              case 'Dec-' + currentYear:
                if (12 === currentMonth) {
                  futurerequestinfo.currentmonth = item.number_of_positions;
                } else {
                  futurerequestinfo.December = item.number_of_positions;
                }
                break;
              case 'Dec-' + nextYear:
                futurerequestinfo.December = item.number_of_positions;
                break;
              default:
                break;
            }
          }
        }
        futurerequestinfo.fk_industrypartnerid = request.body.industrypartnerid;
        futurerequestinfo.fk_sectorID = request.body.sectorid;
        futurerequestinfo.fk_jobroleID = item.jobroleid;
        futurerequestinfo.fk_stateID = item.stateid;
        futurerequestinfo.fk_cityID = item.districtid;
        futurerequestinfo.year = request.body.year;

        //futurerequestinfo.number_of_positions = item.number_of_positions;
        if (item.gender !== undefined) {
          futurerequestinfo.gender = item.gender;
          if (item.gender == "") {
            delete futurerequestinfo.gender;
          }
        }

        var futurerequestinfoKeys = Object.keys(futurerequestinfo);
        var fieldValueInsert = [];
        futurerequestinfoKeys.forEach(function (futurerequestinfoKeys) {
          if (futurerequestinfo[futurerequestinfoKeys] !== undefined) {
            var fieldValueObj = {};
            fieldValueObj = {
              field: futurerequestinfoKeys,
              fValue: futurerequestinfo[futurerequestinfoKeys]
            }
            fieldValueInsert.push(fieldValueObj);
          }
        });
        try {
          var res_create = await futurehiringrequestsDAL.createFutureHiringRequest(fieldValueInsert);
          if (res_create.status == true && res_create.content !== undefined && res_create.content.insertId > 0) {
            successCounter++;

            var updateAllNullToZeroQuery = 'UPDATE `tbl_FutureHiringRequests`  SET currentmonth = IFNULL(currentmonth, 0),January = IFNULL(January, 0),February = IFNULL(February, 0),March = IFNULL(March, 0),April = IFNULL(April, 0),May = IFNULL(May, 0),June = IFNULL(June, 0),July = IFNULL(July, 0),August = IFNULL(August, 0),September = IFNULL(September, 0),October = IFNULL(October, 0),November = IFNULL(November, 0),December = IFNULL(December, 0)';
            var resultAfterupdateAllNullToZeroQuery = await connection.executeRawQuery(updateAllNullToZeroQuery);

            var updateQuery = 'UPDATE `tbl_FutureHiringRequests` SET `total`= (SELECT SUM(`currentmonth` + `January` + `February` + `March` + `April` + `May` + `June` + `July` + `August` + `September` + `October` + `November` + `December`) as `numberOfPositions`)';
            var updateQueryRes = await connection.executeRawQuery(updateQuery);

            var updateQuery = 'UPDATE `tbl_FutureHiringRequests` set `unique_key` = (SELECT  CONCAT(`fk_industrypartnerid`,`fk_sectorID`,`fk_jobroleID`,`fk_stateID`,`fk_cityID`,`currentmonth`,`January`,`February`,`March`,`April`,`May`,`June`,`July`,`August`,`September`,`October`,`November`,`December`) AS FIRSTNAME)';
            var resultAfterupdateQuery = await connection.executeRawQuery(updateQuery);

          } else {
            errorCounter++;
            errorMsgs.push(constant.userMessages.ERR_FUTURE_HIRING_REQUEST_ADD);
          }
        }
        catch (ex) {
          errorCounter++;
          errorMsgs.push(ex.error.message);
          //return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
        }
      }
    }
  }

  if (successCounter > 0 && errorCounter === 0) {
    return common.sendResponse(response, constant.userMessages.MSG_FUTURE_HIRING_REQUEST_ADD_SUCCESSFULLY, true);
  } else {
    var totalrecords = successCounter + errorCounter;
    /* finalData = {
      successrecords: successCounter,
      totalrecords : successCounter + errorCounter,
      errorrecords: errorMsgs,
      message: successCounter > 0 ? 'Total '+totalrecords + 'out of '+successrecords + 'requests added successfully.' : constant.userMessages.ERR_FUTURE_HIRING_REQUEST_ADD,
    }; */
    var message = successCounter > 0 ? 'Total ' + totalrecords + ' out of ' + successCounter + ' requests added successfully.' : constant.userMessages.ERR_FUTURE_HIRING_REQUEST_ADD;
    if (successCounter > 0) {
      return common.sendResponse(response, constant.userMessages.MSG_FUTURE_HIRING_REQUEST_ADD_SUCCESSFULLY, true);
      //return common.sendResponse(response, message, true);
    } else {
      return common.sendResponse(response, message, false);
    }
  }
};

var getFutureHiringRequest = async function (request, response) {
  debug("getFutureHiringRequest.service -> getFutureHiringRequest");
  var params = request.body;

  var queryString = "";
  if (!!params.employerName) queryString += " AND (`IP`.`legal_entity_name` LIKE " + "'%" + params.employerName + "%'";
  if (!!params.sectorName) queryString += " OR `JR`.`sectorName` LIKE " + "'%" + params.sectorName + "%'";
  if (!!params.jobroleName) queryString += " OR `JR`.`jobroleName` LIKE " + "'%" + params.jobroleName + "%'";
  if (!!params.stateName) queryString += " OR `LD`.`stateName` LIKE " + "'%" + params.stateName + "%'";
  if (!!params.cityName) queryString += " OR `LD`.`cityName` LIKE " + "'%" + params.cityName + "%'";
  if (!!params.year) queryString += " OR `FHR`.`year` = " + "'" + params.year + "'";
  if (!!params.gender) queryString += " OR `FHR`.`gender` = " + "'" + params.gender + "'";
  if (!!params.total) queryString += " OR `FHR`.`total` = " + "'" + params.total + "')";

  //return;
  var oTableDef = {
    sCountColumnName: "distinct pk_futurerequestID",
    //sTableName: 'tbl_FutureHiringRequests',
    sWhereAndSql: "FHR.is_deleted = 0" + queryString,
    sGroupColumnName: "pk_futurerequestID",
    sSelectSql: "LD.stateName as `stateName`,LD.stateid as `stateid`,LD.cityName as `cityName`,LD.cityid as `cityid`,JR.sectorName as `sectorName`,JR.sectorid as `sectorid`,JR.jobroleid as `jobroleid`,JR.jobroleName as `jobroleName`,IP.pk_industrypartnerid as `employerrID`,IP.legal_entity_name as `employerName`,FHR.year as `year`,FHR.currentmonth as `currentmonth`,FHR.January as `January`,FHR.February as `February`,FHR.March as `March`,FHR.April as `April`,FHR.May as `May`,FHR.June as `June`,FHR.July as `July`,FHR.August as `August`,FHR.September as `September`,FHR.October as `October`,FHR.November as `November`,FHR.December as `December`,FHR.pk_futurerequestID as `request_id`,FHR.gender as `gender`,FHR.total as `numberOfPositions`",
    sFromSql: "`tbl_FutureHiringRequests` as FHR LEFT JOIN `view_GetLocationDetail` as LD ON(`LD`.`stateid` = `FHR`.`fk_stateID` and `LD`.`cityid` = `FHR`.`fk_cityID`) LEFT JOIN `view_SectorJobRoleDetail` as JR ON (`JR`.`sectorid` = `FHR`.`fk_sectorID` and `JR`.`jobroleid` = `FHR`.`fk_jobroleID`) LEFT JOIN `tbl_IndustryPartners` as IP ON (`IP`.`pk_industrypartnerid` = `FHR`.`fk_industrypartnerid`)",
  };
  var queryBuilder = new QueryBuilder(oTableDef);
  var queries = queryBuilder.buildQuery(params);
  queries.recordsTotal = "SELECT COUNT(*) as totalCount FROM `tbl_FutureHiringRequests` WHERE is_deleted = '0'";
  console.log('#Queries', queries);
  var newres = {};
  if (queries.recordsFiltered) {
    console.log('#', queries.recordsFiltered);
    var resFiltered1 = await connection.executeRawQuery(queries.recordsFiltered);
    //queries.recordsTotal = result.content;
    var resFilterNew = Object.values(resFiltered1.content[0]);
    //console.log('#1', resFilterNew);
  }
  console.log('#111', queries.recordsTotal);
  var resTotal2 = await connection.executeRawQuery(queries.recordsTotal);
  //queries.recordsTotal = result.content;
  //console.log('#2', resTotal2.content[0].totalCount);
  console.log('#222', queries.select);
  var resSelect3 = await connection.executeRawQuery(queries.select);
  //console.log('#3', resSelect3.content);

  newres = { recordsFiltered: resFilterNew[0], recordsTotal: resTotal2.content[0].totalCount, data: resSelect3.content }
  return common.sendResponse(response, newres);
}

var getFutureHiringRequestDetailService = async function (request, response) {
  debug("futurehiringrequest.service -> getFutureHiringRequestDetailService");
  var isValid = common.validateParams([request.params.id]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_USER_PROFILE_UPDATE_REQUEST, false);
  }
  try {
    let result = await futurehiringrequestsDAL.getFutureHiringRequestDetail(request.params.id);
    if (result.status === false) {
      return common.sendResponse(response, result);
    } else {
      response.render("partials/hiringrequestdetails.ejs", { data: result.content[0] }, function (err, html) {
        response.send(html);
      });
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.ERR_NO_HIRING_REQUEST_RECORD_FOUND, false);
  }
}

/**
 * When user wants to perform the actions like active/inactice/delete with multiple records
 * 
 * @param {*} request 
 * @param {*} response
 * @return {*} object 
 */
var updateMultipleRecordsService = async function (request, response) {
  debug("futurehiringrequest.service -> updateMultipleRecordsService");
  var isValid = common.validateParams([request.body.ids, request.body.action]);
  if (!isValid) {
    return common.sendResponse(response, constant.userMessages.INVALID_ADD_UPDATE_FUTURE_HIRING_REQUEST, false);
  }
  var fieldValueInsert = [];

  var fieldValueObj = {};
  switch (request.body.action) {
    case 'active':
      fieldValueObj = {
        field: 'status',
        fValue: 1
      }
      break;
    case 'inactive':
      fieldValueObj = {
        field: 'status',
        fValue: 0
      }
      break;
    case 'delete':
      fieldValueObj = {
        field: 'is_deleted',
        fValue: 1
      }
      break;
  }

  fieldValueInsert.push(fieldValueObj);

  try {
    request.body.ids.forEach(async function (id, index) {
      var result = await futurehiringrequestsDAL.updateFutureRequestById(id, fieldValueInsert);
      if (request.body.ids.length - 1 == index) {
        return common.sendResponse(response, constant.userMessages.MSG_FUTRUE_HIRING_UPDATE_SUCCESSFULLY, true);
      }
    });
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

var getStatewiseFutureHiringRequestService = async function (request, response) {
  debug("futurehiringrequest.service -> getStatewiseFutureHiringRequestService");
  try {
    var resultAll = { demands: [] };
    var totalFutureRequests = 0;

    //Get Total Number of Future requests
    let totalfuturereqRes = await otherDAL.getTotalNumberOfFutureRequests();
    if (totalfuturereqRes.status === true && totalfuturereqRes.content.length > 0) {
      totalFutureRequests = totalfuturereqRes.content[0].TotalFutureHiringRequests !== null ? totalfuturereqRes.content[0].TotalFutureHiringRequests : 0;
    }
    var demandObj = {};
    demandObj.totalFutureRequests = totalFutureRequests;

    //Get API KEY FOR g_maps
    var result_get_api = await otherDAL.getApiKey('g_maps');
    if (result_get_api.status === true && result_get_api.content.length > 0) {
      demandObj.apiKey = result_get_api.content[0].api_key;
      // totalFutureRequests = totalfuturereqRes.content[0].TotalFutureHiringRequests !== null ? totalfuturereqRes.content[0].TotalFutureHiringRequests : 0;
    }
    // return;
    //Get All the Employers from future hiring request table
    let employersResQuery = 'SELECT IP.legal_entity_name as `employerName`,FHR.`fk_industrypartnerid` FROM `tbl_FutureHiringRequests` as FHR LEFT JOIN `tbl_IndustryPartners` as IP ON (`IP`.`pk_industrypartnerid` = `FHR`.`fk_industrypartnerid`) WHERE (FHR.`status` = 1 AND FHR.`is_deleted` = 0) GROUP BY `fk_industrypartnerid`';
    var employersRes = await connection.executeRawQuery(employersResQuery);
    if (employersRes.status === true && employersRes.content !== undefined) {
      demandObj.employers = employersRes.content;
    }

    //Get All the Jobroles from future hiring request table
    let jobroleQuery = 'SELECT JB.jobroleName,FHR.`fk_jobroleID` FROM `tbl_FutureHiringRequests` as FHR LEFT JOIN `tbl_JobroleMaster` as JB ON (`JB`.`pk_jobroleID` = `FHR`.`fk_jobroleID`) WHERE (FHR.`status` = 1 AND FHR.`is_deleted` = 0) GROUP BY `fk_jobroleID`';
    var jobroleQueryRes = await connection.executeRawQuery(jobroleQuery);
    if (jobroleQueryRes.status === true && jobroleQueryRes.content !== undefined) {
      demandObj.jobroles = jobroleQueryRes.content;
    }

    var getStatewiseFutureHiringRequest = 'SELECT `LD`.`stateName` as `statename`,`fk_stateID`,`fk_jobroleID`,GROUP_CONCAT(DISTINCT `JR`.jobroleName,"-[",cast(Ts as char),"]") as `jobroleName`,`LD`.lat as `lat`,`LD`.lng as `lng`,SUM(Ts) as `TotalFutureHiringRequests`,GROUP_CONCAT(cast(Ts as char)) as `IndividualTotalRequest` FROM (SELECT `LD`.stateName as `statename`,`fk_stateID`,`fk_jobroleID`,GROUP_CONCAT(DISTINCT `JR`.jobroleName) as `jobroleName`,`LD`.lat as `lat`,`LD`.lng as `lng`,SUM(`total`) Ts FROM `tbl_FutureHiringRequests` as `FHR` LEFT JOIN `tbl_StateMaster` as `LD` ON (`LD`.`pk_stateID` = `FHR`.`fk_stateID`) LEFT JOIN `tbl_JobroleMaster` as `JR` ON (`JR`.`pk_jobroleID` = `FHR`.`fk_jobroleID`) WHERE (`FHR`.status = "1" and `FHR`.is_deleted = "0") GROUP BY `FHR`.fk_stateID,`FHR`.fk_jobroleID)`FHR` LEFT JOIN `tbl_StateMaster` as `LD` ON (`LD`.`pk_stateID` = `FHR`.`fk_stateID`) LEFT JOIN `tbl_JobroleMaster` as `JR` ON (`JR`.`pk_jobroleID` = `FHR`.`fk_jobroleID`) GROUP BY `FHR`.fk_stateID';
    console.log('# mysql', getStatewiseFutureHiringRequest);
    var result = await connection.executeRawQuery(getStatewiseFutureHiringRequest);
    //let result = await futurehiringrequestsDAL.getStatewiseFutureHiringRequest();

    if (result.status === true && result.content !== undefined) {
      demandObj.StatewiseFutureHiringRequest = result.content;

    } else {
      //return common.sendResponse(response, constant.userMessages.ERR_FETCH_STATEWISE_FUTURREQUEST, false);
    }
    //console.log('# demandObj',demandObj);
    resultAll.demands.push(demandObj);
    //console.log('# res',result);
    return common.sendResponse(response, resultAll, true);
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

var generateErrorFilesForFutureBulkUploadService = async function (request, response) {
  debug("futurehiringrequest.service -> generateErrorFilesForFutureBulkUploadService");
  try {
    let FilesToBeCreated = await futurehiringrequestsDAL.getBulkUploadFiles();
    if (FilesToBeCreated.status === true && FilesToBeCreated.content !== undefined) {
      for (file of FilesToBeCreated.content) {
        var errorFileName = 'futurehiringrequest_error_' + file.industrypartnerid + '_' + file.meta_id + '.csv';
        fs.writeFileSync('routes/bulk_error_files/' + errorFileName, 'sector,jobrole,state,district,currentmonth,January,February,March,April,May,June,July,August,September,October,November,December\r\n');

        let result = await futurehiringrequestsDAL.generateErrorFilesForFutureBulkUpload(file.industrypartnerid, file.meta_id);
        if (result.status == true && result.content.length > 0) {
          for (var errorFile of result.content) {
            // // replace(/(\r\n|\n|\r|,)/gm,"");
            for (let ErrorField in errorFile) {
              if (errorFile[ErrorField] === null) {
                errorFile[ErrorField] = '';
              }
            }

            try {
              var sector = errorFile.sector !== null ? errorFile.sector.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.sector + ',';
              var jobrole = errorFile.jobrole !== null ? errorFile.jobrole.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.jobrole + ',';
              var state = errorFile.state !== null ? errorFile.state.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.state + ',';
              var district = errorFile.district !== null ? errorFile.district.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.district + ',';
              var currentmonth = errorFile.currentmonth + ',';
              var January = errorFile.January + ',';
              var February = errorFile.February + ',';
              var March = errorFile.March + ',';
              var April = errorFile.April + ',';
              var May = errorFile.May + ',';
              var June = errorFile.June + ',';
              var July = errorFile.July + ',';
              var August = errorFile.August + ',';
              var September = errorFile.September + ',';
              var October = errorFile.October + ',';
              var November = errorFile.November + ',';
              var December = errorFile.December + ',';

              fs.appendFileSync('routes/bulk_error_files/' + errorFileName, sector + jobrole + state + district + currentmonth + January + February + March + April + May + June + July + August + September + October + November + December + '\r\n');
              // console.log('The "data to append" was appended to file!');
            } catch (err) {
              // console.log(err,'Error writing DATA');
            }
          }

          let flag_update_result = await otherDAL.updatefileCreatedFlag(file.meta_id);
          if (flag_update_result.status === true) {
            // console.log("flag updated successfully..");
          }
          else {
            // console.log("could not update flaag....");
          }
        }
      }
      return common.sendResponse(response, result.content, true);
    } else {
      return common.sendResponse(response, constant.userMessages.ERR_NO_BULKUPLOADERROR_FOUND, false);
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

var getStateWiseFutureRequestSearchResult = async function (request, response) {
  debug("futurehiringrequest.service -> getStateWiseFutureRequestSearchResult");
  //console.log('# request data',request.body);
  common.sanitizeAll(request.body);

  var params = request.body;

  var queryString = "";
  if (!!params.fk_jobroleID && params.fk_jobroleID > 0) queryString += " AND (`FHR`.`fk_jobroleID` = " + "'" + params.fk_jobroleID + "' )";
  if (!!params.fk_industrypartnerid && params.fk_industrypartnerid > 0) queryString += " AND (`FHR`.`fk_industrypartnerid` = " + "'" + params.fk_industrypartnerid + "' )";

  //console.log('# querystring',queryString);

  try {
    if (!!params.fromtomonths) {
      var monthsArr = params.fromtomonths.split(',');
      //console.log('# params',monthsArr);
      var query = '';
      for (var i = 0; i < monthsArr.length; i++) {
        //console.log('#',monthsArr[i]);
        query += 'SUM(' + monthsArr[i] + ')+';
      }
      if (query != '') {
        var lastChar = query.slice(-1);
        if (lastChar == '+') {
          query = query.slice(0, -1);
        }
      }
      //console.log('# query', query);
      var getStatewiseFutureHiringRequest = 'SELECT `LD`.`stateName` as `statename`,`fk_stateID`,`fk_jobroleID`,`LD`.lat as `lat`,`LD`.lng,GROUP_CONCAT(DISTINCT `JR`.jobroleName) as `jobroleName`,(' + query + ') as  `TotalFutureHiringRequests` FROM `tbl_FutureHiringRequests` as FHR LEFT JOIN `tbl_StateMaster` as `LD` ON (`LD`.`pk_stateID` = `FHR`.`fk_stateID`) LEFT JOIN `tbl_JobroleMaster` as `JR` ON (`JR`.`pk_jobroleID` = `FHR`.`fk_jobroleID`) WHERE (`FHR`.status = "1" and `FHR`.is_deleted = "0" ' + queryString + ') GROUP BY fk_stateID';
    } else if (!!params.monthfrom && params.monthto === '') {
      var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
      console.log('# m', months[params.monthfrom]);
      var getStatewiseFutureHiringRequest = 'SELECT `LD`.`stateName` as `statename`,`fk_stateID`,`fk_jobroleID`,`LD`.lat as `lat`,`LD`.lng,GROUP_CONCAT(DISTINCT `JR`.jobroleName) as `jobroleName`,(SUM(' + months[params.monthfrom] + ')) as  `TotalFutureHiringRequests` FROM `tbl_FutureHiringRequests` as FHR LEFT JOIN `tbl_StateMaster` as `LD` ON (`LD`.`pk_stateID` = `FHR`.`fk_stateID`) LEFT JOIN `tbl_JobroleMaster` as `JR` ON (`JR`.`pk_jobroleID` = `FHR`.`fk_jobroleID`) WHERE (`FHR`.status = "1" and `FHR`.is_deleted = "0" ' + queryString + ') GROUP BY fk_stateID';
    } else if (!!params.monthto && params.monthfrom === '') {
      var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
      var getStatewiseFutureHiringRequest = 'SELECT `LD`.`stateName` as `statename`,`fk_stateID`,`fk_jobroleID`,`LD`.lat as `lat`,`LD`.lng,GROUP_CONCAT(DISTINCT `JR`.jobroleName) as `jobroleName`,(SUM(' + months[params.monthto] + ')) as  `TotalFutureHiringRequests` FROM `tbl_FutureHiringRequests` as FHR LEFT JOIN `tbl_StateMaster` as `LD` ON (`LD`.`pk_stateID` = `FHR`.`fk_stateID`) LEFT JOIN `tbl_JobroleMaster` as `JR` ON (`JR`.`pk_jobroleID` = `FHR`.`fk_jobroleID`) WHERE (`FHR`.status = "1" and `FHR`.is_deleted = "0" ' + queryString + ') GROUP BY fk_stateID';
    } else {
      var getStatewiseFutureHiringRequest = 'SELECT `LD`.`stateName` as `statename`,`fk_stateID`,`fk_jobroleID`,GROUP_CONCAT(DISTINCT `JR`.jobroleName,"-[",cast(Ts as char),"]") as `jobroleName`,`LD`.lat as `lat`,`LD`.lng as `lng`,SUM(Ts) as `TotalFutureHiringRequests`,GROUP_CONCAT(cast(Ts as char)) as `IndividualTotalRequest` FROM (SELECT `LD`.stateName as `statename`,`fk_stateID`,`fk_jobroleID`,GROUP_CONCAT(DISTINCT `JR`.jobroleName) as `jobroleName`,`LD`.lat as `lat`,`LD`.lng as `lng`,SUM(`total`) Ts FROM `tbl_FutureHiringRequests` as `FHR` LEFT JOIN `tbl_StateMaster` as `LD` ON (`LD`.`pk_stateID` = `FHR`.`fk_stateID`) LEFT JOIN `tbl_JobroleMaster` as `JR` ON (`JR`.`pk_jobroleID` = `FHR`.`fk_jobroleID`) WHERE (`FHR`.status = "1" and `FHR`.is_deleted = "0" ' + queryString + ') GROUP BY `FHR`.fk_stateID,`FHR`.fk_jobroleID)`FHR` LEFT JOIN `tbl_StateMaster` as `LD` ON (`LD`.`pk_stateID` = `FHR`.`fk_stateID`) LEFT JOIN `tbl_JobroleMaster` as `JR` ON (`JR`.`pk_jobroleID` = `FHR`.`fk_jobroleID`) GROUP BY `FHR`.fk_stateID';
    }
    console.log('# mysql', getStatewiseFutureHiringRequest);
    var result = await connection.executeRawQuery(getStatewiseFutureHiringRequest);

    if (result.status === true && result.content !== undefined) {
      // console.log('# res con',result.content);
      return common.sendResponse(response, result.content, true);
    } else {
      return common.sendResponse(response, constant.userMessages.ERR_FETCH_STATEWISE_FUTURREQUEST, false);
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

var getZipWiseDriverFHRService = async function (request, response) {
  debug("futurehiringrequest.service -> getZipWiseDriverFHRService");
  try {
    var resultAll = { demands: [] };
    var totalFutureRequests = 0;

    var demandObj = {};

    //Get API KEY FOR g_maps
    var result_get_api = await otherDAL.getApiKey('g_maps');
    if (result_get_api.status === true && result_get_api.content.length > 0) {
      demandObj.apiKey = result_get_api.content[0].api_key;
      // totalFutureRequests = totalfuturereqRes.content[0].TotalFutureHiringRequests !== null ? totalfuturereqRes.content[0].TotalFutureHiringRequests : 0;
    }

    var getStatewiseFutureHiringRequest = 'SELECT `LD`.`stateName` as `statename`,`fk_stateID`,`fk_jobroleID`,GROUP_CONCAT(DISTINCT `JR`.jobroleName,"-[",cast(Ts as char),"]") as `jobroleName`,`LD`.lat as `lat`,`LD`.lng as `lng`,SUM(Ts) as `TotalFutureHiringRequests`,GROUP_CONCAT(cast(Ts as char)) as `IndividualTotalRequest` FROM (SELECT `LD`.stateName as `statename`,`fk_stateID`,`fk_jobroleID`,GROUP_CONCAT(DISTINCT `JR`.jobroleName) as `jobroleName`,`LD`.lat as `lat`,`LD`.lng as `lng`,SUM(`total`) Ts FROM `tbl_FutureHiringRequests` as `FHR` LEFT JOIN `tbl_StateMaster` as `LD` ON (`LD`.`pk_stateID` = `FHR`.`fk_stateID`) LEFT JOIN `tbl_JobroleMaster` as `JR` ON (`JR`.`pk_jobroleID` = `FHR`.`fk_jobroleID`) WHERE (`FHR`.status = "1" and `FHR`.is_deleted = "0" ) GROUP BY `FHR`.fk_stateID,`FHR`.fk_jobroleID)`FHR` LEFT JOIN `tbl_StateMaster` as `LD` ON (`LD`.`pk_stateID` = `FHR`.`fk_stateID`) LEFT JOIN `tbl_JobroleMaster` as `JR` ON (`JR`.`pk_jobroleID` = `FHR`.`fk_jobroleID`) WHERE JR.jobroleName LIKE "%driver%" GROUP BY `FHR`.fk_stateID';
    // console.log('# mysql', getStatewiseFutureHiringRequest);
    var result = await connection.executeRawQuery(getStatewiseFutureHiringRequest);
    //let result = await futurehiringrequestsDAL.getStatewiseFutureHiringRequest();

    if (result.status === true && result.content !== undefined) {
      demandObj.StatewiseFutureHiringRequest = result.content;

    } else {
      //return common.sendResponse(response, constant.userMessages.ERR_FETCH_STATEWISE_FUTURREQUEST, false);
    }
    //console.log('# demandObj',demandObj);
    resultAll.demands.push(demandObj);
    //console.log('# res',result);
    return common.sendResponse(response, resultAll, true);
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}


module.exports = {
  addUpdateFutureHiringRequestService: addUpdateFutureHiringRequestService,
  getFutureHiringRequest: getFutureHiringRequest,
  getFutureHiringRequestDetailService: getFutureHiringRequestDetailService,
  updateMultipleRecordsService: updateMultipleRecordsService,
  getStatewiseFutureHiringRequestService: getStatewiseFutureHiringRequestService,
  generateErrorFilesForFutureBulkUploadService: generateErrorFilesForFutureBulkUploadService,
  getStateWiseFutureRequestSearchResult: getStateWiseFutureRequestSearchResult,
  getZipWiseDriverFHRService: getZipWiseDriverFHRService
}