var express = require('express');
var services = require('./futurehiringrequest.service');
var middleware = require('../../../middleware');


var router = express.Router();
module.exports = router;

router.post('/addUpdateFutureHiringRequest/:type', middleware.logger, services.addUpdateFutureHiringRequestService);//middleware.checkAccessToken,middleware.userRightsByAPI,

router.post('/get-futurehiringrequest', middleware.logger, services.getFutureHiringRequest);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.get('/get-futurehiringrequestdetail/:id', middleware.logger, services.getFutureHiringRequestDetailService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/addupdate-futurerequest-multiplerecords',middleware.checkAccessToken,middleware.logger, services.updateMultipleRecordsService);//middleware.userRightsByAPI,
router.post('/get-statewise-futurehiringrequest', middleware.logger, services.getStatewiseFutureHiringRequestService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/generateErrorFilesForFutureBulkUpload', middleware.logger, services.generateErrorFilesForFutureBulkUploadService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/getStateWiseFutureRequestSearchResult', middleware.logger, services.getStateWiseFutureRequestSearchResult);
router.post('/get-zipwise-driverFHR', middleware.logger, services.getZipWiseDriverFHRService);