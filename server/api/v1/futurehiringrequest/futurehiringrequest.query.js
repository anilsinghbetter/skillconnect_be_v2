var tbl_UserMaster = "tbl_UserMaster";
var tbl_RoleMaster = "tbl_RoleMaster";
var tbl_UserType = "tbl_UserType";
var tbl_AccessToken = "tbl_AccessToken";
var tbl_transactionMaster = "tbl_transactionMaster";
var view_GetUserDetail = "view_GetUserDetail";
var tbl_IndustryPartners = "tbl_IndustryPartners";
var view_GetLocationDetail = "view_GetLocationDetail";
var tbl_SectorMaster = "tbl_SectorMaster";
var industrypartners_bulk_upload = "industrypartners_bulk_upload";
var tbl_CandidateDetail = "tbl_CandidateDetail";
var tbl_Candidates = "tbl_Candidates";
var tbl_TrainingPartners = "tbl_TrainingPartners";
var view_SectorJobRoleDetail = "view_SectorJobRoleDetail";
var tbl_HiringRequestsDetail = "tbl_HiringRequestsDetail";
var view_HiringDetail = "view_HiringDetail";
var tbl_FutureHiringRequests = "tbl_FutureHiringRequests";
var tbl_StateMaster = 'tbl_StateMaster';
var tbl_candidates_metadata = 'tbl_candidates_metadata';
var futurerequest_errors = 'futurerequest_errors';
var tbl_JobroleMaster = 'tbl_JobroleMaster';

var query = {
    createFutureHiringRequestQuery: {
        table: tbl_FutureHiringRequests,
        insert: []
    },
    getFutureHiringRequestQuery: {
        join: {
            table: tbl_FutureHiringRequests,
            alias: 'FHR',
            joinwith: [{
                table: view_GetLocationDetail,
                alias: 'LD',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'LD',
                        field: 'stateid',
                        operator: 'eq',
                        value: {
                            table: 'FHR',
                            field: 'fk_stateID'
                        }
                    }, {
                        table: 'LD',
                        field: 'cityid',
                        operator: 'eq',
                        value: {
                            table: 'FHR',
                            field: 'fk_cityID'
                        }
                    }]
                }
            }, {
                table: view_SectorJobRoleDetail,
                alias: 'JR',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'JR',
                        field: 'sectorid',
                        operator: 'eq',
                        value: {
                            table: 'FHR',
                            field: 'fk_sectorID'
                        }
                    }, {
                        table: 'JR',
                        field: 'jobroleid',
                        operator: 'eq',
                        value: {
                            table: 'FHR',
                            field: 'fk_jobroleID'
                        }
                    }]
                }
            }, {
                table: tbl_IndustryPartners,
                alias: 'IP',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'IP',
                        field: 'pk_industrypartnerid',
                        operator: 'eq',
                        value: {
                            table: 'FHR',
                            field: 'fk_industrypartnerid'
                        }
                    }]
                }
            }]
        },
        select: [{
            field: 'LD.stateName',
            encloseField: false,
            alias: 'stateName'
        }, {
            field: 'LD.stateid',
            encloseField: false,
            alias: 'stateid'
        }, {
            field: 'LD.cityName',
            encloseField: false,
            alias: 'cityName'
        }, {
            field: 'LD.cityid',
            encloseField: false,
            alias: 'cityid'
        }, {
            field: 'JR.sectorName',
            encloseField: false,
            alias: 'sectorName'
        }, {
            field: 'JR.sectorid',
            encloseField: false,
            alias: 'sectorid'
        }, {
            field: 'JR.jobroleid',
            encloseField: false,
            alias: 'jobroleid'
        }, {
            field: 'JR.jobroleName',
            encloseField: false,
            alias: 'jobroleName'
        }, {
            field: 'IP.pk_industrypartnerid',
            encloseField: false,
            alias: 'employerrID'
        }, {
            field: 'IP.legal_entity_name',
            encloseField: false,
            alias: 'employerName'
        }, {
            field: 'IP.employertype',
            encloseField: false,
            alias: 'employerType'
        }, {
            //field: 'SUM(`currentmonth` + `January` + `February` + `March` + `April` + `May` + `June` + `July` + `August` + `September` + `October` + `November` + `December`)',
            field: 'total',
            encloseField: false,
            alias: 'numberOfPositions'
        }, {
            field: 'FHR.year',
            encloseField: false,
            alias: 'year'
        }, {
            field: 'FHR.pk_futurerequestID',
            encloseField: false,
            alias: 'request_id'
        }, {
            field: 'FHR.status',
            encloseField: false,
            alias: 'status'
        }],
        groupby: [{
            field: 'pk_futurerequestID'
        }],
        filter: {
        },
    },
    getFutureHiringRequestDetailQuery: {
        join: {
            table: tbl_FutureHiringRequests,
            alias: 'FHR',
            joinwith: [{
                table: view_GetLocationDetail,
                alias: 'LD',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'LD',
                        field: 'stateid',
                        operator: 'eq',
                        value: {
                            table: 'FHR',
                            field: 'fk_stateID'
                        }
                    }, {
                        table: 'LD',
                        field: 'cityid',
                        operator: 'eq',
                        value: {
                            table: 'FHR',
                            field: 'fk_cityID'
                        }
                    }]
                }
            }, {
                table: view_SectorJobRoleDetail,
                alias: 'JR',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'JR',
                        field: 'sectorid',
                        operator: 'eq',
                        value: {
                            table: 'FHR',
                            field: 'fk_sectorID'
                        }
                    }, {
                        table: 'JR',
                        field: 'jobroleid',
                        operator: 'eq',
                        value: {
                            table: 'FHR',
                            field: 'fk_jobroleID'
                        }
                    }]
                }
            }, {
                table: tbl_IndustryPartners,
                alias: 'IP',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'IP',
                        field: 'pk_industrypartnerid',
                        operator: 'eq',
                        value: {
                            table: 'FHR',
                            field: 'fk_industrypartnerid'
                        }
                    }]
                }
            }]
        },
        select: [{
            field: 'LD.stateName',
            encloseField: false,
            alias: 'stateName'
        }, {
            field: 'LD.stateid',
            encloseField: false,
            alias: 'stateid'
        }, {
            field: 'LD.cityName',
            encloseField: false,
            alias: 'cityName'
        }, {
            field: 'LD.cityid',
            encloseField: false,
            alias: 'cityid'
        }, {
            field: 'JR.sectorName',
            encloseField: false,
            alias: 'sectorName'
        }, {
            field: 'JR.sectorid',
            encloseField: false,
            alias: 'sectorid'
        }, {
            field: 'JR.jobroleid',
            encloseField: false,
            alias: 'jobroleid'
        }, {
            field: 'JR.jobroleName',
            encloseField: false,
            alias: 'jobroleName'
        }, {
            field: 'IP.pk_industrypartnerid',
            encloseField: false,
            alias: 'employerrID'
        }, {
            field: 'IP.legal_entity_name',
            encloseField: false,
            alias: 'employerName'
        }, {
            field: 'IP.employertype',
            encloseField: false,
            alias: 'employerType'
        }, {
            field: 'FHR.year',
            encloseField: false,
            alias: 'year'
        }, {
            field: 'FHR.currentmonth',
            encloseField: false,
            alias: 'currentmonth'
        }, {
            field: 'FHR.January',
            encloseField: false,
            alias: 'January'
        }, {
            field: 'FHR.February',
            encloseField: false,
            alias: 'February'
        }, {
            field: 'FHR.March',
            encloseField: false,
            alias: 'March'
        }, {
            field: 'FHR.April',
            encloseField: false,
            alias: 'April'
        }, {
            field: 'FHR.May',
            encloseField: false,
            alias: 'May'
        }, {
            field: 'FHR.June',
            encloseField: false,
            alias: 'June'
        }, {
            field: 'FHR.July',
            encloseField: false,
            alias: 'July'
        }, {
            field: 'FHR.August',
            encloseField: false,
            alias: 'August'
        }, {
            field: 'FHR.September',
            encloseField: false,
            alias: 'September'
        }, {
            field: 'FHR.October',
            encloseField: false,
            alias: 'October'
        }, {
            field: 'FHR.November',
            encloseField: false,
            alias: 'November'
        }, {
            field: 'FHR.December',
            encloseField: false,
            alias: 'December'
        }, {
            field: 'FHR.pk_futurerequestID',
            encloseField: false,
            alias: 'request_id'
        }, {
            field: 'FHR.gender',
            encloseField: false,
            alias: 'gender'
        }, {
            field: 'FHR.total',
            encloseField: false,
            alias: 'total'
        }, {
            field: 'IP.contact_person_name',
            encloseField: false,
            alias: 'contact_person_name'
        }, {
            field: 'IP.contact_email_address',
            encloseField: false,
            alias: 'contact_email'
        }],
        filter: {
            and: [{
                field: 'FHR.pk_futurerequestID',
                encloseField: false,
                operator: 'EQ',
                value: ''
            }]
        },
    },
    updateFutureRequestByIdQuery: {
        table: tbl_FutureHiringRequests,
        update: [],
        filter: {
            and: [{
                field: 'pk_futurerequestID',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    getStatewiseFutureHiringRequestQuery: {
        join: {
            table: tbl_FutureHiringRequests,
            alias: 'FHR',
            joinwith: [{
                table: tbl_StateMaster,
                alias: 'LD',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'LD',
                        field: 'pk_stateID',
                        operator: 'eq',
                        value: {
                            table: 'FHR',
                            field: 'fk_stateID'
                        }
                    }]
                }
            }, {
                table: tbl_JobroleMaster,
                alias: 'JR',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'JR',
                        field: 'pk_jobroleID',
                        operator: 'eq',
                        value: {
                            table: 'FHR',
                            field: 'fk_jobroleID'
                        }
                    }]
                }
            }]
        },
        select: [{
            field: 'LD.stateName',
            encloseField: false,
            alias: 'statename'
        }, {
            field: 'JR.jobroleName',
            encloseField: false,
            alias: 'jobroleName'
        }, {
            field: 'LD.lat',
            encloseField: false,
            alias: 'lat'
        }, {
            field: 'LD.lng',
            encloseField: false,
            alias: 'lng'
        }, {
            field: 'SUM(total)',
            encloseField: false,
            alias: 'TotalFutureHiringRequests'
        }],
        filter: {
            and: [{
                field: 'FHR.status',
                encloseField: false,
                operator: 'EQ',
                value: 1
            }, {
                field: 'FHR.is_deleted',
                encloseField: false,
                operator: 'EQ',
                value: 0
            }]
        },
        groupby: [{
            field: 'FHR.fk_stateID',
            encloseField: false
        }]
    },
    getBulkUploadFilesQuery: {
        join: {
            table: futurerequest_errors,
            alias: 'ERLIST',
            joinwith: [{
                table: tbl_candidates_metadata,
                alias: 'MTDATA',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'ERLIST',
                        field: 'fk_industrypartnerid',
                        operator: 'eq',
                        value: {
                            table: 'MTDATA',
                            field: 'industrypartner_id'
                        }
                    }]
                }
            }]
        },
        select: [{
            field: 'ERLIST.fk_industrypartnerid',
            encloseField: false,
            alias: 'industrypartnerid'
        }, {
            field: 'ERLIST.meta_id',
            encloseField: false,
        }, {
            field: 'MTDATA.filename',
            encloseField: false,
        }],
        filter: {
            and: [{
                field: 'MTDATA.is_error_file_created',
                encloseField: false,
                operator: 'EQ',
                value: 0
            }, {
                field: 'ERLIST.meta_id',
                encloseField: false,
                operator: 'EQ',
                value: {
                    table: 'MTDATA',
                    field: 'id'
                }
            }]
        },
        groupby: [{
            field: 'ERLIST.fk_industrypartnerid',
            encloseField: false,
        }, {
            field: 'ERLIST.meta_id',
            encloseField: false,
        }],
        orderby: [{
            field: 'ERLIST.meta_id',
            encloseField: false,
        }]
    },
    generateErrorFilesForFutureBulkUploadQuery: {
        table: futurerequest_errors,
        alias: 'ERLIST',
        select: [{
            field: 'ERLIST.fk_industrypartnerid',
            encloseField: false,
            alias: 'industrypartnerid'
        }, {
            field: 'ERLIST.meta_id',
            encloseField: false,
        }, {
            field: 'ERLIST.sector_name',
            encloseField: false,
            alias: 'sector'
        }, {
            field: 'ERLIST.jobrole_name',
            encloseField: false,
            alias: 'jobrole'
        }, {
            field: 'ERLIST.state_name',
            encloseField: false,
            alias: 'state'
        }, {
            field: 'ERLIST.district_name',
            encloseField: false,
            alias: 'district'
        }, {
            field: 'ERLIST.currentmonth',
            encloseField: false,
            alias: 'currentmonth'
        }, {
            field: 'ERLIST.January',
            encloseField: false,
            alias: 'January'
        }, {
            field: 'ERLIST.February',
            encloseField: false,
            alias: 'February'
        }, {
            field: 'ERLIST.March',
            encloseField: false,
            alias: 'March'
        }, {
            field: 'ERLIST.April',
            encloseField: false,
            alias: 'April'
        }, {
            field: 'ERLIST.May',
            encloseField: false,
            alias: 'May'
        }, {
            field: 'ERLIST.June',
            encloseField: false,
            alias: 'June'
        }, {
            field: 'ERLIST.July',
            encloseField: false,
            alias: 'July'
        }, {
            field: 'ERLIST.August',
            encloseField: false,
            alias: 'August'
        }, {
            field: 'ERLIST.September',
            encloseField: false,
            alias: 'September'
        }, {
            field: 'ERLIST.October',
            encloseField: false,
            alias: 'October'
        }, {
            field: 'ERLIST.November',
            encloseField: false,
            alias: 'November'
        }, {
            field: 'ERLIST.December',
            encloseField: false,
            alias: 'December'
        }],
        filter: {
            and: [{
                field: 'ERLIST.fk_industrypartnerid',
                encloseField: false,
                operator: 'EQ',
                value: ''
            }, {
                field: 'ERLIST.meta_id',
                encloseField: false,
                operator: 'EQ',
                value: ''
            }]
        },
        orderby: [{
            field: 'ERLIST.meta_id',
            encloseField: false,
        }]
    },
};

module.exports = query
