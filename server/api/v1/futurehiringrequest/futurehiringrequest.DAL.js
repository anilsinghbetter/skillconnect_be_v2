var debug = require('debug')('server:api:v1:futurehiringrequest:DAL');
var d3 = require("d3");
var DateLibrary = require('date-management');
var common = require('../common');
var constant = require('../constant');
var query = require('./futurehiringrequest.query');
var dbDateFormat = constant.appConfig.DB_DATE_FORMAT;

/**
 * Create Future Request 
 */
var createFutureHiringRequest = async function (fieldValue) {
  debug("futurehiringrequests.DAL -> createFutureHiringRequest");
  var createFutureHiringRequest = common.cloneObject(query.createFutureHiringRequestQuery);
  createFutureHiringRequest.insert = fieldValue;
  return await common.executeQuery(createFutureHiringRequest);
}

var getFutureHiringRequest = async function () {
  debug("futurehiringrequest.DAL -> getFutureHiringRequest");
  var getFutureHiringRequestQuery = common.cloneObject(query.getFutureHiringRequestQuery);
  var filter = { and: [] };
  filter.and.push({
    field: 'FHR.is_deleted',
    encloseField: false,
    operator: 'EQ',
    value: 0
  });
  getFutureHiringRequestQuery.filter = filter;
  return await common.executeQuery(getFutureHiringRequestQuery);
}

/**
 * Get future hiring request detail by id
 * 
 * @param {int} request_id
 * @return {object}
 */
var getFutureHiringRequestDetail = async function (request_id) {
  debug("futurehiringrequest.DAL -> getFutureHiringRequestDetail");
 // console.log(await common.executeQuery(query.getFutureHiringRequestQuery),"reqid");

  var getFutureHiringRequestDetailQuery = common.cloneObject(query.getFutureHiringRequestDetailQuery);
  getFutureHiringRequestDetailQuery.filter.and[0].value = request_id;
  
  return await common.executeQuery(getFutureHiringRequestDetailQuery);
}

var updateFutureRequestById = async function (FutureRequestID,fieldValueUpdate) {
  debug("futurehiringrequest.DAL -> updateFutureRequestById");
  var updateFutureRequestById = common.cloneObject(query.updateFutureRequestByIdQuery);
  updateFutureRequestById.filter.and[0].value = FutureRequestID;
  updateFutureRequestById.update = fieldValueUpdate;
  return await common.executeQuery(updateFutureRequestById);
}

var getStatewiseFutureHiringRequest = async function () {
  debug("futurehiringrequest.DAL -> getStatewiseFutureHiringRequest");
  var getStatewiseFutureHiringRequest = common.cloneObject(query.getStatewiseFutureHiringRequestQuery);
  return await common.executeQuery(getStatewiseFutureHiringRequest);
}

var generateErrorFilesForFutureBulkUpload = async function (empid,meta_id) {
  debug("futurehiringrequest.DAL -> generateErrorFilesForFutureBulkUpload");
  var getMetaInfoForFutureHiringRequest = common.cloneObject(query.generateErrorFilesForFutureBulkUploadQuery);
  getMetaInfoForFutureHiringRequest.filter.and[0].value = empid;
  getMetaInfoForFutureHiringRequest.filter.and[1].value = meta_id;
  return await common.executeQuery(getMetaInfoForFutureHiringRequest);
}

var getBulkUploadFiles = async function () {
  debug("other.DAL -> getBulkUploadFiles");
  var getBulkUploadFiles = common.cloneObject(query.getBulkUploadFilesQuery);
  getBulkUploadFiles.limit = constant.appConfig.CRON_BULKUPLOAD_FILE_CREATION_LIMIT;
  return await common.executeQuery(getBulkUploadFiles);
}

module.exports = {
  createFutureHiringRequest : createFutureHiringRequest,
  getFutureHiringRequest : getFutureHiringRequest,
  getFutureHiringRequestDetail: getFutureHiringRequestDetail,
  updateFutureRequestById : updateFutureRequestById,
  getStatewiseFutureHiringRequest : getStatewiseFutureHiringRequest,
  generateErrorFilesForFutureBulkUpload : generateErrorFilesForFutureBulkUpload,
  getBulkUploadFiles : getBulkUploadFiles
}
