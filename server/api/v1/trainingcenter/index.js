var express = require('express');
var services = require('./trainingcenter.service');
var middleware = require('../../../middleware');

var router = express.Router();
module.exports = router;

router.post('/addupdateTrainingCenter/:type', middleware.logger, services.addupdateTrainingCenterService);
router.post('/getTrainingCenter/:type', middleware.logger, services.getTrainingCenterService);
router.post('/get-trainingcenter-detail/:id', middleware.logger, services.getTrainingCenterDetailService);
router.post('/addupdate-trainingcenter-multiplerecords', middleware.logger, services.updateMultipleRecordsService);
router.post('/getTrainingCenterDetail/:id', middleware.logger, services.getTrainingCenterDetailForWebService);//Get training center detail from web