var debug = require('debug')('server:api:v1:trainingcenter:service');
var d3 = require("d3");
var DateLibrary = require('date-management');
var bcrypt = require('bcrypt');
var randomize = require('randomatic');
var uuid = require('uuid');
var common = require('../common');
var constant = require('../constant');
var trainingCenterDAL = require('./trainingcenter.DAL');
var dbDateFormat = constant.appConfig.DB_DATE_FORMAT;
var config = require('../../../../config');
var connection = require('../../../helper/connection');


/**
 * add / update training center
 *
 * @param  {object}   request
 * @param  {object} response
 * @return {object}
 */
var addupdateTrainingCenterService = async function (request, response) {
  debug("trainingcenter.service -> addupdateTrainingCenterService");
  if (request.body.trainingcenterid == '-1') {
    var isValid = common.validateParams([
      request.body.trainingpartnerid,
      request.body.trainingcenter_name,
      request.body.sdms_trainingcenter_id,
      request.body.address,
      request.body.statename,
      request.body.districtname,
      request.body.pincode,
      request.body.contact_person_name,
      request.body.contact_email_address
    ]);
    if (!isValid) {
      return common.sendResponse(response, constant.requestMessages.ERR_INVALID_TRAININGCENTER_ADD_REQUEST, false);
    }
  }

  common.sanitizeAll(request.body);

  //check state district combination
  if (request.body.statename !== undefined) {
    if(request.body.districtname === undefined){
      return common.sendResponse(response, constant.requestMessages.ERR_SELECT_DISTRICT, false);
    }
    var checkStateCityQuery = 'SELECT count(stateid) as totalCount FROM `view_GetLocationDetail` WHERE  `stateid` = '+request.body.statename+' AND `cityid` = '+request.body.districtname;
    var StateCityRes =  await connection.executeRawQuery(checkStateCityQuery);
   
    if(StateCityRes.status === true){
      if(StateCityRes.content[0].totalCount == 0){
        return common.sendResponse(response, constant.requestMessages.ERR_COMBINATION_DISTRICT, false);
      } 
    }
  }

  var trainingcenterID = request.body.trainingcenterid;
  var trainingcenterinfo = {};
  if (request.body.trainingpartnerid != undefined) {
    trainingcenterinfo.fk_trainingpartnerid = request.body.trainingpartnerid;
  }
  if (request.body.sdms_trainingcenter_id != undefined) {
    trainingcenterinfo.sdms_trainingcenter_id = request.body.sdms_trainingcenter_id;
  }
  if (request.body.trainingcenter_name != undefined) {
    trainingcenterinfo.trainingcenter_name = request.body.trainingcenter_name;
  }
  if (request.body.address != undefined) {
    trainingcenterinfo.address = request.body.address;
  }
  if (request.body.districtname != undefined) {
    trainingcenterinfo.fk_cityID = request.body.districtname;
  }
  if (request.body.statename != undefined) {
    trainingcenterinfo.fk_stateID = request.body.statename;
  }
  if (request.body.pincode != undefined) {
    trainingcenterinfo.pincode = request.body.pincode;
  }
  if (request.body.contact_person_name != undefined) {
    trainingcenterinfo.contact_person_name = request.body.contact_person_name;
  }
  if (request.body.contact_email_address != undefined) {
    trainingcenterinfo.contact_email_address = request.body.contact_email_address;
  }
  if (request.body.contact_mobile != undefined) {
    trainingcenterinfo.contact_mobile = request.body.contact_mobile;
  }
  var trainingcenterKeys = Object.keys(trainingcenterinfo);
  var fieldValueInsert = [];
  trainingcenterKeys.forEach(function (trainingcenterKeys) {
    if (trainingcenterinfo[trainingcenterKeys] !== undefined) {
      var fieldValueObj = {};
      fieldValueObj = {
        field: trainingcenterKeys,
        fValue: trainingcenterinfo[trainingcenterKeys]
      }
      fieldValueInsert.push(fieldValueObj);
    }
  });

  try {
    //While adding new trainingCenter we are sending trainingcenterID as -1
    if (trainingcenterID == -1) {
      var result = await trainingCenterDAL.checkTrainingCenterAlreadyExist(trainingcenterinfo.fk_trainingpartnerid, trainingcenterinfo.trainingcenter_name, trainingcenterinfo.fk_stateID, trainingcenterinfo.fk_cityID, trainingcenterinfo.pincode);
      if (result.content.length > 0 && result.content[0].trainingcenterid > 0) {
        return common.sendResponse(response, constant.userMessages.ERR_TRAININGCENTER_IS_ALREADY_EXIST, false);
      }
      var res_create_trainingcenter = await trainingCenterDAL.createTrainingCenter(fieldValueInsert);
      return common.sendResponse(response, constant.userMessages.MSG_TRAININGCENTER_ADD_SUCCESSFULLY, true);
    } else {
      var result = await trainingCenterDAL.checkTrainingCenterIDIsExist(trainingcenterID);
      if (result.content.length == 0) {
        return common.sendResponse(response, constant.userMessages.ERR_TRAININGCENTER_NOT_EXIST, false);
      } else if (result.content.length > 0 && result.content[0].trainingcenterid == trainingcenterID) {
        if (trainingcenterinfo.fk_trainingpartnerid != undefined && trainingcenterinfo.trainingcenter_name != undefined && trainingcenterinfo.fk_stateID != undefined && trainingcenterinfo.fk_cityID != undefined && trainingcenterinfo.pincode != undefined) {
          var result = await trainingCenterDAL.checkTrainingCenterAlreadyExist(trainingcenterinfo.fk_trainingpartnerid, trainingcenterinfo.trainingcenter_name, trainingcenterinfo.fk_stateID, trainingcenterinfo.fk_cityID, trainingcenterinfo.pincode);
          if (result.content.length > 0) {
            if (result.content[0].trainingcenterid != trainingcenterID) {
              return common.sendResponse(response, constant.userMessages.ERR_TRAININGCENTER_IS_ALREADY_EXIST, false);
            } else {
              var res_update_trainingcenter = await trainingCenterDAL.updateTrainingCenterById(trainingcenterID, fieldValueInsert);
              return common.sendResponse(response, constant.userMessages.MSG_TRAININGCENTER_UPDATE_SUCCESSFULLY, true);
            }

          } else {
            var res_update_trainingcenter = await trainingCenterDAL.updateTrainingCenterById(trainingcenterID, fieldValueInsert);
            return common.sendResponse(response, constant.userMessages.MSG_TRAININGCENTER_UPDATE_SUCCESSFULLY, true);
          }
        } else {
          var res_update_trainingcenter = await trainingCenterDAL.updateTrainingCenterById(trainingcenterID, fieldValueInsert);
          return common.sendResponse(response, constant.userMessages.MSG_TRAININGCENTER_UPDATE_SUCCESSFULLY, true);
        }
      }
    }
  } catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

/**
 * 
 * @param {object} request 
 * @param {object} response
 * @return {object}
 */
var getTrainingCenterService = async function (request, response) {
  debug("trainingcenter.service -> getTrainingCenterService");
  var getPaginationObject = common.getPaginationObject(request,constant.appConfig.TRAININGCENTER_RECORDS_LIMIT);
  var limit = getPaginationObject.limit;

  var searchObj = {};
  if (request.body !== undefined) {
    common.sanitizeAll(request.body); 
    if (request.body.trainingpartnerid !== undefined) {
      request.body.fk_trainingpartnerid = request.body.trainingpartnerid;
      delete request.body.trainingpartnerid;
    }
    if (request.body.stateid !== undefined) {
      request.body.fk_stateID = request.body.stateid;
      delete request.body.stateid;
    }
    if (request.body.cityid !== undefined) {
      request.body.fk_cityID = request.body.cityid;
      delete request.body.cityid;
    }
    searchObj = request.body;
  }

  try {
    let res_count = await trainingCenterDAL.getTrainingCentersCount(searchObj,request.params.type);
    var totalTrainingCenters = res_count.content[0].totalCount;
    let result = await trainingCenterDAL.getTrainingCenters(limit, searchObj, request.params.type);

    return common.sendResponse(response, result.content, true, common.getPaginationOptions(request, totalTrainingCenters,constant.appConfig.TRAININGCENTER_RECORDS_LIMIT));
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.ERR_NO_TRAININGCENTER_FOUND, false);
  }
}

var getTrainingCenterDetailService = async function (request, response) {
  debug("trainingcenter.service -> getTrainingCenterDetailService");
  try {
    response.render("partials/trainingcenterdetails.ejs", { data: request.body.data }, function (err, html) {
      response.send(html);
    });
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.ERR_NO_TRAININGCENTER_FOUND, false);
  }
}

var getTrainingCenterDetailForWebService = async function (request, response) {
  debug("trainingcenter.service -> getTrainingCenterDetailForWebService");
  var isValid = common.validateParams([request.params.id]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_GET_DETAIL_REQUEST, false);
  }
  try {
    let result = await trainingCenterDAL.getTrainingCenterDetail(request.params.id);
    if (result.status === false) {
      return common.sendResponse(response, constant.userMessages.ERR_NO_TRAININGCENTER_FOUND, false);
    } else if(result.status === true && result.content.length > 0){
      return common.sendResponse(response, result.content, true);
    } else {
      return common.sendResponse(response, constant.userMessages.ERR_NO_TRAININGCENTER_FOUND, false);
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

/**
 * When user wants to perform the actions like active/inactice/delete with multiple records
 * 
 * @param {*} request 
 * @param {*} response
 * @return {*} object 
 */
var updateMultipleRecordsService = async function (request, response) {
  debug("trainingpartner.service -> updateMultipleRecordsService");
  var isValid = common.validateParams([request.body.ids, request.body.action]);
  if (!isValid) {
    return common.sendResponse(response, constant.userMessages.INVALID_ADD_UPDATE_TP_REQUEST, false);
  }
  var fieldValueInsert = [];

  var fieldValueObj = {};
  switch (request.body.action) {
    case 'active':
      fieldValueObj = {
        field: 'status',
        fValue: 1
      }
      break;
    case 'inactive':
      fieldValueObj = {
        field: 'status',
        fValue: 0
      }
      break;
    case 'delete':
      fieldValueObj = {
        field: 'is_deleted',
        fValue: 1
      }
      break;
  }

  fieldValueInsert.push(fieldValueObj);

  try {
    request.body.ids.forEach(async function (id, index) {
      var result = await trainingCenterDAL.updateTrainingCenterById(id, fieldValueInsert);
      if (request.body.ids.length - 1 == index) {
        return common.sendResponse(response, constant.userMessages.MSG_TRAININGCENTER_UPDATE_SUCCESSFULLY, true);
      }
    });
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

module.exports = {
  addupdateTrainingCenterService: addupdateTrainingCenterService,
  getTrainingCenterService: getTrainingCenterService,
  getTrainingCenterDetailService: getTrainingCenterDetailService,
  updateMultipleRecordsService: updateMultipleRecordsService,
  getTrainingCenterDetailForWebService: getTrainingCenterDetailForWebService
}
