var tbl_UserMaster = "tbl_UserMaster";
var tbl_RoleMaster = "tbl_RoleMaster";
var tbl_UserType = "tbl_UserType";
var tbl_OTP = "tbl_OTP";
var tbl_AccessToken = "tbl_AccessToken";
var tbl_transactionMaster = "tbl_transactionMaster";
var tbl_TrainingPartners = "tbl_TrainingPartners";
var tbl_TrainingCenters = "tbl_TrainingCenters";
var tbl_Candidates = "tbl_Candidates";
var view_GetUserDetail = "view_GetUserDetail";
var view_GetLocationDetail = "view_GetLocationDetail";

var query = {
    checkTrainingCenterAlreadyExistQuery: {
        table: tbl_TrainingCenters,
        select: [{
            field: 'pk_trainingcenterid',
            alias: 'trainingcenterid'
        }],
        filter: {
            and: [{
                field: 'fk_trainingpartnerid',
                operator: 'EQ',
                value: ''
            }, {
                field: 'trainingcenter_name',
                operator: 'EQ',
                value: ''
            }, {
                field: 'fk_stateID',
                operator: 'EQ',
                value: ''
            }, {
                field: 'fk_cityID',
                operator: 'EQ',
                value: ''
            }, {
                field: 'pincode',
                operator: 'EQ',
                value: ''
            }, {
                field: 'is_deleted',
                operator: 'EQ',
                value: 0
            }]
        }
    },
    createTrainingCenterQuery: {
        table: tbl_TrainingCenters,
        insert: []
    },
    getTrainingCentersCountQuery: {
        table: tbl_TrainingCenters,
        alias: 'TC',
        select: [{
            field: 'TC.pk_trainingcenterid',
            encloseField: false,
            aggregation: 'count',
            alias: 'totalCount'
        }],
        filter: {}
    },
    getTrainingCentersQuery: {
        join: {
            table: tbl_TrainingCenters,
            alias: 'TC',
            joinwith: [{
                table: view_GetLocationDetail,
                alias: 'LD',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'LD',
                        field: 'stateid',
                        operator: 'eq',
                        value: {
                            table: 'TC',
                            field: 'fk_stateID'
                        }
                    }, {
                        table: 'LD',
                        field: 'cityid',
                        operator: 'eq',
                        value: {
                            table: 'TC',
                            field: 'fk_cityID'
                        }
                    }]
                }
            }]
        },
        select: [{
            field: 'LD.stateName',
            encloseField: false,
            alias: 'stateName'
        }, {
            field: 'LD.cityName',
            encloseField: false,
            alias: 'cityName'
        }, {
            field: 'TC.pk_trainingcenterid',
            encloseField: false,
            alias: 'trainingcenterid'
        },{
            field: 'TC.fk_trainingpartnerid',
            encloseField: false,
            alias: 'trainingpartnerid'
        }, {
            field: 'TC.sdms_trainingcenter_id',
            encloseField: false,
            alias: 'sdms_trainingcenter_id'
        }, {
            field: 'TC.trainingcenter_name',
            encloseField: false,
            alias: 'trainingcenter_name'
        }, {
            field: 'TC.address',
            encloseField: false,
            alias: 'address'
        }, {
            field: 'TC.pincode',
            encloseField: false,
            alias: 'pincode'
        }, {
            field: 'TC.fk_stateid',
            encloseField: false,
            alias: 'stateid'
        }, {
            field: 'TC.fk_cityid',
            encloseField: false,
            alias: 'cityid'
        }, {
            field: 'TC.contact_person_name',
            encloseField: false,
            alias: 'contact_person_name'
        }, {
            field: 'TC.contact_email_address',
            encloseField: false,
            alias: 'contact_email_address'
        }, {
            field: 'TC.contact_mobile',
            encloseField: false,
            alias: 'contact_mobile'
        }, {
            field: 'TC.status',
            encloseField: false,
            alias: 'status'
        }],
        filter: {},
        sortby: [{
            field: 'TC.pk_trainingcenterid',
            encloseField: false,
            order: 'DESC'
        }]
    },
    checkTrainingCenterIDIsExistQuery: {
        table: tbl_TrainingCenters,
        select: [{
            field: 'pk_trainingcenterid',
            alias: 'trainingcenterid'
        }],
        filter: {
            and: [{
                field: 'pk_trainingcenterid',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    updateTrainingCenterQuery: {
        table: tbl_TrainingCenters,
        update: [],
        filter: {
            and: [{
                field: 'pk_trainingcenterid',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    checkTrainingCenterNameQuery: {
        table: tbl_TrainingCenters,
        select: [{
            field: 'pk_trainingcenterid',
            alias: 'trainingcenterid'
        }],
        filter: {
            and: [{
                field: 'trainingcenter_name',
                operator: 'EQ',
                value: ''
            }, {
                field: 'fk_trainingpartnerid',
                operator: 'EQ',
                value: ''
            }, {
                field: 'status',
                operator: 'EQ',
                value: 1
            }, {
                field: 'is_deleted',
                operator: 'EQ',
                value: 0
            }]
        }
    },
    getTrainingCenterDropdownForSearchQuery: {
        table: tbl_TrainingCenters,
        select: [{
            field: 'pk_trainingcenterid',
            alias: 'trainingcenterid'
        }, {
            field: 'trainingcenter_name',
            alias: 'trainingcenter_name'
        }],
        filter: {
            and: [{
                field: 'status',
                operator: 'EQ',
                value: 1
            }, {
                field: 'is_deleted',
                operator: 'EQ',
                value: 0
            }]
        }
    },
    getTrainingCentersByTrainingPartnerIDQuery: {
        table: tbl_TrainingCenters,
        select: [{
            field: 'pk_trainingcenterid',
            alias: 'trainingcenterid'
        }, {
            field: 'trainingcenter_name',
            alias: 'trainingcenter_name'
        }],
        filter: {
            and: [{
                field: 'fk_trainingpartnerid',
                operator: 'EQ',
                value: ''
            }, {
                field: 'status',
                operator: 'EQ',
                value: 1
            }, {
                field: 'is_deleted',
                operator: 'EQ',
                value: 0
            }]
        }
    },
    getTrainingCenterDetailQuery : {
        join: {
            table: tbl_TrainingCenters,
            alias: 'TC',
            joinwith: [{
                table: view_GetLocationDetail,
                alias: 'LD',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'LD',
                        field: 'stateid',
                        operator: 'eq',
                        value: {
                            table: 'TC',
                            field: 'fk_stateID'
                        }
                    }, {
                        table: 'LD',
                        field: 'cityid',
                        operator: 'eq',
                        value: {
                            table: 'TC',
                            field: 'fk_cityID'
                        }
                    }]
                }
            }]
        },
        select: [{
            field: 'LD.stateName',
            encloseField: false,
            alias: 'stateName'
        },{
            field: 'LD.stateid',
            encloseField: false,
            alias: 'stateid'
        }, {
            field: 'LD.cityName',
            encloseField: false,
            alias: 'cityName'
        }, {
            field: 'LD.cityid',
            encloseField: false,
            alias: 'cityid'
        }, {
            field: 'TC.pk_trainingcenterid',
            encloseField: false,
            alias: 'trainingcenterid'
        }, {
            field: 'TC.sdms_trainingcenter_id',
            encloseField: false,
            alias: 'sdms_trainingcenter_id'
        }, {
            field: 'TC.trainingcenter_name',
            encloseField: false,
            alias: 'trainingcenter_name'
        }, {
            field: 'TC.address',
            encloseField: false,
            alias: 'address'
        }, {
            field: 'TC.pincode',
            encloseField: false,
            alias: 'pincode'
        }, {
            field: 'TC.fk_stateid',
            encloseField: false,
            alias: 'stateid'
        }, {
            field: 'TC.fk_cityid',
            encloseField: false,
            alias: 'cityid'
        }, {
            field: 'TC.contact_person_name',
            encloseField: false,
            alias: 'contact_person_name'
        }, {
            field: 'TC.contact_email_address',
            encloseField: false,
            alias: 'contact_email_address'
        }, {
            field: 'TC.contact_mobile',
            encloseField: false,
            alias: 'contact_mobile'
        }, {
            field: 'TC.status',
            encloseField: false,
            alias: 'status'
        }],
        filter: {
            and: [{
                field: 'TC.pk_trainingcenterid',
                encloseField: false,
                operator: 'EQ',
                value: ''
            }, {
                field: 'TC.is_deleted',
                encloseField: false,
                operator: 'EQ',
                value: 0
            }, {
                field: 'TC.status',
                encloseField: false,
                operator: 'EQ',
                value: 1
            }] 
        },
    }
};


module.exports = query
