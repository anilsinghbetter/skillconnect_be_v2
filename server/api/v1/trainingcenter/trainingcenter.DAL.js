var debug = require('debug')('server:api:v1:trainingcenter:DAL');
var d3 = require("d3");
var DateLibrary = require('date-management');
var common = require('../common');
var constant = require('../constant');
var query = require('./trainingcenter.query');
var dbDateFormat = constant.appConfig.DB_DATE_FORMAT;

/**
 * 
 * @param {*} trainingpartnerid 
 * @param {*} trainingcenter_name 
 * @param {*} state 
 * @param {*} city 
 * @param {*} pincode 
 */
var checkTrainingCenterAlreadyExist = async function (trainingpartnerid, trainingcenter_name,stateid,cityid,pincode) {
  debug("trainingcenter.DAL -> checkTrainingCenterAlreadyExist");
  var checkTrainingCenterAlreadyExist = common.cloneObject(query.checkTrainingCenterAlreadyExistQuery);
  checkTrainingCenterAlreadyExist.filter.and[0].value = trainingpartnerid;
  checkTrainingCenterAlreadyExist.filter.and[1].value = trainingcenter_name;
  checkTrainingCenterAlreadyExist.filter.and[2].value = stateid;
  checkTrainingCenterAlreadyExist.filter.and[3].value = cityid;
  checkTrainingCenterAlreadyExist.filter.and[4].value = pincode;
  return await common.executeQuery(checkTrainingCenterAlreadyExist);
};

/**
 * Add new training center 
 * 
 * @param  {Array of object}   fieldValue [the array object of the request body]
 * @return {object}
 */
var createTrainingCenter = async function (fieldValue) {
  debug("trainingcenter.DAL -> createTrainingCenter");
  var createTrainingCenterQuery = common.cloneObject(query.createTrainingCenterQuery);
  createTrainingCenterQuery.insert = fieldValue;
  return await common.executeQuery(createTrainingCenterQuery);
}

/**
 * Get Training Centers count
 * 
 * @param {Array of object} searchObj
 * @return {object} 
 */
var getTrainingCentersCount = async function (searchObj,requestType) {
  debug("trainingcenter.DAL -> getTrainingCentersCount");
  var getTrainingCentersCountQuery = common.cloneObject(query.getTrainingCentersCountQuery);
  var trainingCentersFilter = { and: [] };
  if(requestType == 'admin'){
    trainingCentersFilter.and.push({
      field: 'TC.is_deleted',
      encloseField: false,
      operator: 'EQ',
      value: 0
    });
    getTrainingCentersCountQuery.filter = trainingCentersFilter;
  } else {
    if (Object.keys(searchObj).length !== 0) {
      common.setSearchParams(searchObj,trainingCentersFilter,'trainingcenter');
      getTrainingCentersCountQuery.filter = trainingCentersFilter;
    } else {
      delete getTrainingCentersCountQuery.filter;
    }
    trainingCentersFilter.and.push({
      field: 'TC.status',
      encloseField: false,
      operator: 'EQ',
      value: 1
    });
    trainingCentersFilter.and.push({
      field: 'TC.is_deleted',
      encloseField: false,
      operator: 'EQ',
      value: 0
    });
    getTrainingCentersCountQuery.filter = trainingCentersFilter;
  }
  return await common.executeQuery(getTrainingCentersCountQuery);
}

/** 
* get Training Centers
* 
* @param  {int}   limit
* @param  {Array of object}   searchObj
* @return {object}
*/
var getTrainingCenters = async function (limit, searchObj,requestType) {
  debug("trainingcenter.DAL -> getTrainingCenters");
  var getTrainingCentersQuery = common.cloneObject(query.getTrainingCentersQuery);
  var trainingCentersFilter = { and: [] };
  if(requestType == 'admin'){
    trainingCentersFilter.and.push({
      field: 'TC.is_deleted',
      encloseField: false,
      operator: 'EQ',
      value: 0
    });
    getTrainingCentersQuery.filter = trainingCentersFilter;
  } else {
    if (Object.keys(searchObj).length !== 0) {
      common.setSearchParams(searchObj,trainingCentersFilter,'trainingcenter');
      getTrainingCentersQuery.filter = trainingCentersFilter;
    } else {
      delete getTrainingCentersQuery.filter;
    }
    trainingCentersFilter.and.push({
      field: 'TC.status',
      encloseField: false,
      operator: 'EQ',
      value: 1
    });
    trainingCentersFilter.and.push({
      field: 'TC.is_deleted',
      encloseField: false,
      operator: 'EQ',
      value: 0
    });
    getTrainingCentersQuery.filter = trainingCentersFilter;
  }
  if(requestType !== 'admin'){
    getTrainingCentersQuery.limit = limit;
  }
  return await common.executeQuery(getTrainingCentersQuery);
}

var checkTrainingCenterIDIsExist = async function(id){
  debug("trainingcenter.DAL -> checkTrainingCenterIDIsExist");
  var checkTrainingCenterIDIsExist = common.cloneObject(query.checkTrainingCenterIDIsExistQuery);
  checkTrainingCenterIDIsExist.filter.and[0].value = id;
  return await common.executeQuery(checkTrainingCenterIDIsExist);
}

var updateTrainingCenterById = async function(trainingcenterID,fieldValueUpdate){
  debug("trainingcenter.DAL -> updateTrainingCenter");
  var updateTrainingCenter = common.cloneObject(query.updateTrainingCenterQuery);
  updateTrainingCenter.filter.and[0].value = trainingcenterID;
  updateTrainingCenter.update = fieldValueUpdate;
  return await common.executeQuery(updateTrainingCenter);
}

var checkTrainingCenterName = async function(trainingcentername,trainingpartnerid){
  debug("trainingcenter.DAL -> checkTrainingCenterName");
  var checkTrainingCenterName = common.cloneObject(query.checkTrainingCenterNameQuery);
  checkTrainingCenterName.filter.and[0].value = trainingcentername;
  checkTrainingCenterName.filter.and[1].value = trainingpartnerid;
  return await common.executeQuery(checkTrainingCenterName);
}

var getTrainingCenterDropdownForSearch = async function(){
  debug("trainingcenter.DAL -> getTrainingCenterDropdownForSearch");
  var getTrainingCenterDropdownForSearch = common.cloneObject(query.getTrainingCenterDropdownForSearchQuery);
  return await common.executeQuery(getTrainingCenterDropdownForSearch);
}

var getTrainingCentersByTrainingPartnerID = async function(trainingpartnerid){
  debug("trainingcenter.DAL -> getTrainingCentersByTrainingPartnerID");
  var getTrainingCentersByTrainingPartnerID = common.cloneObject(query.getTrainingCentersByTrainingPartnerIDQuery);
  getTrainingCentersByTrainingPartnerID.filter.and[0].value = trainingpartnerid;
  return await common.executeQuery(getTrainingCentersByTrainingPartnerID);
}

var getTrainingCenterDetail = async function(centerid){
  debug("trainingcenter.DAL -> getCandidateDetail");
  var getTrainingCenterDetail = common.cloneObject(query.getTrainingCenterDetailQuery);
  getTrainingCenterDetail.filter.and[0].value = centerid;
  return await common.executeQuery(getTrainingCenterDetail);
}

module.exports = {
  checkTrainingCenterAlreadyExist: checkTrainingCenterAlreadyExist,
  createTrainingCenter: createTrainingCenter,
  getTrainingCentersCount : getTrainingCentersCount,
  getTrainingCenters: getTrainingCenters,
  checkTrainingCenterIDIsExist : checkTrainingCenterIDIsExist,
  updateTrainingCenterById : updateTrainingCenterById,
  checkTrainingCenterName : checkTrainingCenterName,
  getTrainingCenterDropdownForSearch : getTrainingCenterDropdownForSearch,
  getTrainingCentersByTrainingPartnerID : getTrainingCentersByTrainingPartnerID,
  getTrainingCenterDetail : getTrainingCenterDetail
}
