var debug = require('debug')('server:api:v1:jobrole:service');
var common = require('../common');
var constant = require('../constant');
var jobroleDAL = require('./jobrole.DAL');
var config = require('../../../../config');
var connection = require('../../../helper/connection');

/**
 * get Job Role Service
 * 
 * @param {object} request 
 * @param {object} response
 * @return {object} 
 */
var getJobRoleService = async function (request, response) {
  debug("jobrole.service -> getJobRoleService");
  try {
    let result = await jobroleDAL.getJobRole();
    return common.sendResponse(response, result.content, true);
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

/**
 * remove jobrole
 * 
 * @param {*} request 
 * @param {*} response 
 */
var removeJobroleService = async function (request, response) {
  debug("jobrole.service -> removeJobroleService");
  var isValid = common.validateParams([request.params.id]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_DELETE_JOBROLE_REQUEST, false);
  }
  try {
    var fieldValueUpdate = [];
    fieldValueUpdate.push({
      field: "is_deleted",
      fValue: 1
    });
    let result = await jobroleDAL.removeJobRole(request.params.id, fieldValueUpdate);
    if (result.status === false) {
      return common.sendResponse(response, result);
    } else {
      return common.sendResponse(response, constant.userMessages.JOBROLE_DELETED_SUCCESSFULLY, true);
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

/**
 * When user wants to perform the actions like active/inactice/delete with multiple records
 * 
 * @param {*} request 
 * @param {*} response
 * @return {*} object 
 */
var updateMultipleRecordsService = async function (request, response) {
  debug("jobrole.service -> updateMultipleRecordsService");
  var isValid = common.validateParams([request.body.ids, request.body.action]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.INVALID_ADD_UPDATE_JOBROLE_REQUEST, false);
  }
  var fieldValueInsert = [];

  var fieldValueObj = {};
  switch (request.body.action) {
    case 'active':
      fieldValueObj = {
        field: 'status',
        fValue: 1
      }
      break;
    case 'inactive':
      fieldValueObj = {
        field: 'status',
        fValue: 0
      }
      break;
    case 'delete':
      fieldValueObj = {
        field: 'is_deleted',
        fValue: 1
      }
      break;
  }

  fieldValueInsert.push(fieldValueObj);

  try {
    request.body.ids.forEach(async function (id, index) {
      var result = await jobroleDAL.updateJobRoleInfoById(id, fieldValueInsert);
      if (request.body.ids.length - 1 == index) {
        return common.sendResponse(response, constant.userMessages.MSG_JOBROLE_UPDATE_SUCCESSFULLY, true);
      }
    });
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

/**
 * Add Update jobrole
 * 
 * @param {*} request 
 * @param {*} response 
 */
var addUpdateJobroleService = async function (request, response) {
  debug("jobrole.service -> addUpdateJobroleService");
  var isValid = common.validateParams([request.body.jobroleName, request.body.jobroleCode]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.INVALID_ADD_UPDATE_JOBROLE_REQUEST, false);
  }
  common.sanitizeAll(request.body);
  var jobroleID = request.body.jobroleid;

  var jobroleinfo = {};

  jobroleinfo.jobroleName = request.body.jobroleName;
  jobroleinfo.jobroleCode = request.body.jobroleCode;

  var jobroleKeys = Object.keys(jobroleinfo);
  var fieldValueInsert = [];
  jobroleKeys.forEach(function (jobroleKeys) {
    if (jobroleinfo[jobroleKeys] !== undefined) {
      var fieldValueObj = {};
      fieldValueObj = {
        field: jobroleKeys,
        fValue: jobroleinfo[jobroleKeys]
      }
      fieldValueInsert.push(fieldValueObj);
    }
  });

  try {
    //While add new jobrole we are sending jobroleID as -1
    if (jobroleID == -1) {

    } else {
      var result = await jobroleDAL.checkEditJobRoleIsExist(jobroleID);
      if (result.content.length == 0) {
        return common.sendResponse(response, constant.userMessages.ERR_JOBROLE_NOT_EXIST, false);
      } else if (result.content.length > 0 && result.content[0].jobroleid == jobroleID) {
        var result = await jobroleDAL.checkEntriesAlreayExist(jobroleinfo.jobroleName, jobroleinfo.jobroleCode, jobroleID);
        if (result.content.length > 0) {
          return common.sendResponse(response, constant.userMessages.ERR_JOBROLE_IS_ALREADY_EXIST, false);
        } else {
          var res_update_user = await jobroleDAL.updateJobRoleInfoById(jobroleID, fieldValueInsert);
          return common.sendResponse(response, constant.userMessages.MSG_JOBROLE_UPDATE_SUCCESSFULLY, true);
        }
      }
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

module.exports = {
  getJobRoleService: getJobRoleService,
  removeJobroleService : removeJobroleService,
  updateMultipleRecordsService : updateMultipleRecordsService,
  addUpdateJobroleService : addUpdateJobroleService
}