var express = require('express');
var services = require('./jobrole.service');
var middleware = require('../../../middleware');

var router = express.Router();
module.exports = router;

router.post('/get-jobrolemaster', middleware.checkAccessToken,middleware.userRightsByAPI,middleware.logger, services.getJobRoleService);
router.post('/remove-jobrole/:id',middleware.logger, services.removeJobroleService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/addupdate-jobrole-multiplerecords',middleware.logger, services.updateMultipleRecordsService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/addupdate-jobrolemaster',middleware.logger, services.addUpdateJobroleService);//middleware.checkAccessToken,middleware.userRightsByAPI,