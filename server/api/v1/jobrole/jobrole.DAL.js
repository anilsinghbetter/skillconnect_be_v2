var debug = require('debug')('server:api:v1:jobrole:DAL');
var common = require('../common');
var constant = require('../constant');
var query = require('./jobrole.query');


/**
 * Get Job Role
 * 
 */
var getJobRole = async function () {
  debug("jobrole.DAL -> getJobRole");
  var getJobRoleQuery = common.cloneObject(query.getJobRoleQuery);
  var getJobRoleQueryFilter = { and: [] };

  getJobRoleQueryFilter.and.push({
    field: 'is_deleted',
    operator: 'EQ',
    value: 0
  });
  getJobRoleQuery.filter = getJobRoleQueryFilter;

  return await common.executeQuery(getJobRoleQuery);
}

var removeJobRole = async function (id,fieldValueUpdate) {
  debug("jobrole.DAL -> removeJobRole");
  var removeJobRoleQuery = common.cloneObject(query.updateJobRoleQuery);
  removeJobRoleQuery.filter.and[0].value = id;
  removeJobRoleQuery.update = fieldValueUpdate;
  return await common.executeQuery(removeJobRoleQuery);
}

var updateJobRoleInfoById = async function(jobroleId, fieldValue){
  debug("jobrole.DAL -> updateJobRoleInfoById");
  var updateJobRoleQuery = common.cloneObject(query.updateJobRoleQuery);
  updateJobRoleQuery.filter.and[0].value = jobroleId;
  updateJobRoleQuery.update = fieldValue;
  return await common.executeQuery(updateJobRoleQuery);
}

var checkEditJobRoleIsExist = async function (id) {
  debug("jobrole.DAL -> checkEditJobRoleIsExist");
  var checkEditJobRoleIsExist = common.cloneObject(query.checkJobRoleIsExistQuery);
  checkEditJobRoleIsExist.filter.and[0].value = id;
  return await common.executeQuery(checkEditJobRoleIsExist);
};

var checkEntriesAlreayExist = async function (jobroleName,code,jobroleID) {
  debug("jobrole.DAL -> checkEntriesAlreayExist");
  var checkEntriesAlreayExist = common.cloneObject(query.checkEntriesAlreayExistQuery);
  checkEntriesAlreayExist.filter.and[0].value = jobroleID;
  checkEntriesAlreayExist.filter.and[1].or[0].value = jobroleName;
  checkEntriesAlreayExist.filter.and[1].or[1].value = code;
  return await common.executeQuery(checkEntriesAlreayExist);
};

module.exports = {
  getJobRole: getJobRole,
  removeJobRole : removeJobRole,
  checkEditJobRoleIsExist : checkEditJobRoleIsExist,
  checkEntriesAlreayExist : checkEntriesAlreayExist,
  updateJobRoleInfoById : updateJobRoleInfoById
}
