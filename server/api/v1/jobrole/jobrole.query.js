var tbl_UserMaster = "tbl_UserMaster";
var tbl_RoleMaster = "tbl_RoleMaster";
var tbl_UserType = "tbl_UserType";
var tbl_AccessToken = "tbl_AccessToken";
var tbl_transactionMaster = "tbl_transactionMaster";
var view_GetUserDetail = "view_GetUserDetail";
var tbl_IndustryPartners = "tbl_IndustryPartners";
var view_GetLocationDetail = "view_GetLocationDetail";
var sector_jobrole_csv = "sector_jobrole_csv";
var tbl_JobroleMaster = "tbl_JobroleMaster";

var query = {
    getJobRoleQuery: {
        table: tbl_JobroleMaster,
        select: [{
            field: 'pk_jobroleID',
            alias: 'jobroleid'
        }, {
            field: 'jobroleName'
        }, 
        /* {
            field: 'jobroleCode'
        }, */
        {
            field: 'status'
        }],
        sortby: [{
            field: 'pk_jobroleID',
            order: 'DESC'
        }]
    },
    updateJobRoleQuery:{
        table: tbl_JobroleMaster,
        update: [],
        filter: {
            and: [{
                field: 'pk_jobroleID',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    checkJobRoleIsExistQuery:{
        table: tbl_JobroleMaster,
        select: [{
            field: 'pk_jobroleID',
            alias: 'jobroleid'
        }, {
            field: 'jobroleName',
            alias: 'jobroleName'
        }, {
            field: 'jobroleCode',
            alias: 'jobroleCode'
        }],
        filter: {
            and: [{
                field: 'pk_jobroleID',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    checkEntriesAlreayExistQuery: {
        table: tbl_JobroleMaster,
        select: [{
            field: 'pk_jobroleID',
            alias: 'jobroleid'
        }],
        filter: {
            and: [{
                field: 'pk_jobroleID',
                operator: 'NOTEQ',
                value: ''
            }, {
                or: [{
                    field: 'jobroleName',
                    operator: 'EQ',
                    value: ''
                }, {
                    field: 'jobroleCode',
                    operator: 'EQ',
                    value: ''
                }]
            }]
        }
    }
};

module.exports = query
