var debug = require('debug')('server:api:v1:industrypartner:service');
var d3 = require("d3");
var DateLibrary = require('date-management');
var uuid = require('uuid');
var common = require('../common');
var otherService = require('../other/other.service');
var constant = require('../constant');
var industryPartnerDAL = require('./industrypartner.DAL');
var candidateDAL = require('../candidate/candidate.DAL');
var dbDateFormat = constant.appConfig.DB_DATE_FORMAT;
var config = require('../../../../config');
var connection = require('../../../helper/connection');
var bcrypt = require("bcrypt");
var randomize = require('randomatic');
var otherDAL = require('../other/other.DAL');
var QueryBuilder = require('../../../helper/node-datatable-master');
var async = require("async");

/**
 * signup
 * 
 * @param  {Object} request  
 * @param  {Object} response 
 * @return {object}
 */
var signupService = async function (request, response) {
  debug("industrypartner.service -> signupService");
  var isValid = common.validateParams([request.body.legal_entity_name, request.body.contact_email_address, request.body.contact_person_name, request.body.sectors]);//request.body.employer_type, 
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_SIGNUP_REQUEST, false);
  }
  common.sanitizeAll(request.body);

  var userinfo = {};
  userinfo.ipid = 'EMP-' + randomize('0', 5);
  userinfo.password = bcrypt.hashSync(constant.appConfig.SYSTEM_PASSWORD, 10);
  userinfo.employertype = request.body.employer_type;
  userinfo.legal_entity_name = request.body.legal_entity_name;
  userinfo.contact_person_name = request.body.contact_person_name;
  userinfo.contact_email_address = request.body.contact_email_address;
  userinfo.contact_mobile = request.body.contact_mobile;
  userinfo.office_phone = request.body.office_phone;
  userinfo.website = request.body.website;
  userinfo.address = request.body.address;
  userinfo.fk_sectorID = request.body.sectors;

  var userKeys = Object.keys(userinfo);
  var userfieldValueInsert = [];
  userKeys.forEach(function (userKeys) {
    if (userinfo[userKeys] !== undefined) {
      var fieldValueObj = {};
      fieldValueObj = {
        field: userKeys,
        fValue: userinfo[userKeys]
      }
      userfieldValueInsert.push(fieldValueObj);
    }
  });
  //console.log("Final Data:", userfieldValueInsert);
  try {
    var result = await industryPartnerDAL.checkUserIsExist(userinfo.ipid, userinfo.contact_email_address);
    if (result.content.length > 0 && result.content[0].industrypartnerid > 0) {
      return common.sendResponse(response, constant.userMessages.ERR_USER_IS_ALREADY_EXIST, false);
    }
    var res_create_user = await industryPartnerDAL.createIndustryUser(userfieldValueInsert);
    otherService.emailSignUpSendService("ip_registration", userinfo);
    return common.sendResponse(response, constant.userMessages.MSG_SIGNUP_SUCCESSFULLY, true);
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }

};

/**
 * signin
 * 
 * @param  {object}   request
 * @param  {object} response
 * @return {object}
 */
var signinService = async function (request, response) {
  debug("IndustryPartner.service -> signinService");
  var isValid = common.validateParams([request.body.tpid, request.body.password]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_SIGNIN_REQUEST, false);
  }
  common.sanitizeAll(request.body);
  var ipid = request.body.tpid;
  var password = request.body.password;
  let result = await industryPartnerDAL.signin(ipid);
  if (result.status == false) {
    return common.sendResponse(response, result);
  } else if (result.content.length == 0) {
    return common.sendResponse(response, constant.userMessages.ERR_INVALID_EMAIL_AND_PASSWORD, false);
  } else if (result.content[0].status == 0) {
    return common.sendResponse(response, constant.userMessages.ERR_USER_IS_NOT_ACTIVE, false);
  }
  bcrypt.compare(password, result.content[0].password, async function (err, ress) {//First argurment is the input string to be compared
    if (ress) {

      let res_access_token = await checkAndCreateAccessToken(request, result.content);
      if (res_access_token.status === true) {

        /*  var session = request.session;
         session.userInfo = {
           userId: res_access_token.data[0].user_id,
           name: res_access_token.data[0].name,
           mobile: res_access_token.data[0].mobile,
           role: res_access_token.data[0].role,
           userTypeID: res_access_token.data[0].user_type_id
         };
         var userRights = [];
         for (var i = 0; i < res_access_token.data.length; i++) {
           var objRights = {};
           objRights.moduleName = res_access_token.data[i].module_name;
           objRights.canView = res_access_token.data[i].can_view;
           objRights.canAddEdit = res_access_token.data[i].can_add_edit;
           objRights.canDelete = res_access_token.data[i].can_delete;
           objRights.adminCreated = res_access_token.data[i].admin_created;
           userRights.push(objRights);
         }
         request.session.userInfo.userRights = userRights; */
        ['password', 'status'].forEach(e => delete result.content[0][e]);
        return common.sendResponse(response, res_access_token);
      } else {
        return common.sendResponse(response, res_access_token);
      }
    } else {
      return common.sendResponse(response, constant.userMessages.ERR_INVALID_EMAIL_AND_PASSWORD, false);
    }
  });
};

/**
 * check and create access token
 * 
 * @param  {Object}   request  [description]
 * @param  {Object}   userInfo [description]
 * @return {object}            [description]
 */
async function checkAndCreateAccessToken(request, userInfo) {
  try {
    var userId = userInfo[0].industrypartnerid;
    var token = uuid.v1();
    var deviceId = request.headers["udid"];
    var deviceType = (request.headers["device-type"]).toLowerCase();
    var expiryDateTime = DateLibrary.getRelativeDate(new Date(), {
      operationType: "Absolute_DateTime",
      granularityType: "hours",
      value: constant.appConfig.MAX_ACCESS_TOKEN_EXPIRY_HOURS
    });
    var host = request.hostname;
    let result = await industryPartnerDAL.exprieAccessToken(userId, deviceId, host);

    let res_access_token = await industryPartnerDAL.createAccessToken(userId, token, expiryDateTime, deviceId, host);

    let res_user_tran = await industryPartnerDAL.checkUserTransaction(deviceId, deviceType);

    if (res_user_tran.content[0].totalCount > 0) {
      var fieldValueUpdate = [];
      fieldValueUpdate.push({
        field: "isLogedIn",
        fValue: 1
      });
      fieldValueUpdate.push({
        field: "lastLoginDatetime",
        fValue: d3.timeFormat(dbDateFormat)(new Date())
      });
      let res_transaction = industryPartnerDAL.updateUserTransaction(deviceId, deviceType, fieldValueUpdate);
    }
    else {
      let res_transaction = industryPartnerDAL.createUserTransaction(deviceId, deviceType);
    }
    return { status: true, user_type: 'EMP', "access_token": token, data: userInfo };
  }
  catch (ex) {
    console.log(" error in checkAndCreateAccessToken");
    throw ex;
  }
};

/**
 * changePasswordService check first userId and compare old password with db password and then chnage password
 *
 * @param  {object}   request
 * @param  {object} response
 * @return {object}
 */
var changePasswordService = async function (request, response) {
  debug("industrypartner.service -> changePasswordService");
  var isValid = common.validateParams([request.body.industrypartnerid, request.body.old_password, request.body.new_password]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_CHANGE_PASSWORD_REQUEST, false);
  }
  common.sanitizeAll(request.body);

  //Password should be minimum of 8 and maximum of 50 characters.
  var passwordLength = request.body.new_password.length;
  if (passwordLength < constant.appConfig.RESET_PASSWORD_MINIMUM_CHARACTERS_LIMIT) {
    return common.sendResponse(response, constant.requestMessages.ERR_RESET_PASSWORD_MIN_LENGTH, false);
  }
  if (passwordLength > constant.appConfig.RESET_PASSWORD_MAXIMUM_CHARACTERS_LIMIT) {
    return common.sendResponse(response, constant.requestMessages.ERR_RESET_PASSWORD_MAX_LENGTH, false);
  }

  var userId = request.body.industrypartnerid;
  var oldPassword = bcrypt.hashSync(request.body.old_password, 10);
  var newPassword = bcrypt.hashSync(request.body.new_password, 10);
  var result = await industryPartnerDAL.validateUser(userId);
  if (result.status === false) {
    return common.sendResponse(response, result);
  } else if (result.content.length == 0) {
    return common.sendResponse(response, constant.userMessages.ERR_USER_NOT_EXIST, false);
  } else if (result.content.length > 0) {
    if (result.content[0].status == 0) {
      return common.sendResponse(response, constant.userMessages.ERR_USER_IS_NOT_ACTIVE, false);
    }
  }
  bcrypt.compare(request.body.old_password, result.content[0].password, async function (err, ress) {//First argurment is the input string to be compared
    if (ress) {
      var fieldValueUpdate = [];
      fieldValueUpdate.push({
        field: "password",
        fValue: newPassword
      });
      var result = await industryPartnerDAL.updateUserInfoById(userId, fieldValueUpdate);
      if (result.status === false) {
        return common.sendResponse(response, result);
      } else {
        return common.sendResponse(response, constant.userMessages.MSG_PASSWORD_CHANGE_SUCCESSFULLY, true);
      }
    } else {
      return common.sendResponse(response, constant.userMessages.ERR_OLD_PASSWORD_NOT_MATCH, false);
    }
  });
};

/**
 * update users profile
 *
 * @param  {object}   request
 * @param  {object} response
 * @return {object}
 */
var updateProfileService = async function (request, response) {
  debug("industrypartner.service -> updateProfileService");
  var isValid = common.validateParams([request.body.industrypartnerid]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_PROFILE_UPDATE_REQUEST, false);
  }
  common.sanitizeAll(request.body);

  var userId = request.body.industrypartnerid;

  var result = await industryPartnerDAL.validateUser(userId);
  if (result.status === false) {
    return common.sendResponse(response, result);
  } else if (result.content.length == 0) {
    return common.sendResponse(response, constant.userMessages.ERR_NO_INDUSTRYPARTNER_FOUND, false);
  } else if (result.content.length > 0) {
    if (result.content[0].status == 0) {
      return common.sendResponse(response, constant.userMessages.ERR_USER_IS_NOT_ACTIVE, false);
    }
  }

  var userInfo = {};
  /* if(request.body.employertype != undefined){
    userInfo.employertype = request.body.employertype;
  } */
  /* if(request.body.legal_entity_name != undefined){
    userInfo.legal_entity_name = request.body.legal_entity_name;
  } */
  if (request.body.contact_person_name != undefined) {
    userInfo.contact_person_name = request.body.contact_person_name;
  }
  if (request.body.contact_email_address != undefined) {
    userInfo.contact_email_address = request.body.contact_email_address;
  }
  if (request.body.contact_mobile != undefined) {
    userInfo.contact_mobile = request.body.contact_mobile;
  }
  if (request.body.address != undefined) {
    userInfo.address = request.body.address;
  }
  if (request.body.office_phone != undefined) {
    userInfo.office_phone = request.body.office_phone;
  }
  if (request.body.website != undefined) {
    userInfo.website = request.body.website;
  }
  var userKeys = Object.keys(userInfo);
  var fieldValueUpdate = [];
  userKeys.forEach(function (userKey) {
    if (userInfo[userKey] !== undefined) {
      var fieldValueObj = {};
      fieldValueObj = {
        field: userKey,
        fValue: userInfo[userKey]
      };
      fieldValueUpdate.push(fieldValueObj);
    }
  });
  if (fieldValueUpdate.length > 0) {
    var result = await industryPartnerDAL.updateUserInfoById(userId, fieldValueUpdate);
    if (result.status === false) {
      return common.sendResponse(response, result);
    } else {
      return common.sendResponse(response, constant.userMessages.UPDATE_PROFILE_SUCCESSFULLY, true);
    }
  } else {
    return common.sendResponse(response, constant.userMessages.UPDATE_PROFILE_SUCCESSFULLY, true);
  }
}

/**
 * get Industry Partner Service
 * 
 * @param {object} request 
 * @param {object} response
 * @return {object} 
 */
var getIndustryPartnerService = async function (request, response) {
  debug("industrypartner.service -> getIndustryPartnerService");
  var params = request.body;
 
  var queryString="";
  if(!!params.contact_mobile) queryString +=" AND (`contact_mobile` = "+ "'"+params.contact_mobile+"'";
  if(!!params.contact_email_address) queryString +=" OR `contact_email_address` LIKE "+ "'%"+params.contact_email_address+"%'";
  if(!!params.legal_entity_name) queryString +=" OR `legal_entity_name` LIKE "+ "'%"+params.legal_entity_name+"%'";
  if(!!params.ipid) queryString +=" OR `ipid` = "+ "'"+params.ipid+"'";
  if(!!params.employertype) queryString +=" OR `employertype` = "+ "'"+params.employertype+"'";
  if(!!params.contact_person_name) queryString +=" OR `contact_person_name` LIKE "+ "'%"+params.contact_person_name+"%')";

  //return;
  var oTableDef = {
    sCountColumnName: "distinct pk_industrypartnerid",
    sTableName: 'tbl_IndustryPartners',
    sWhereAndSql: "is_deleted = 0"+queryString,
    sGroupColumnName: "pk_industrypartnerid",
    sSelectSql: "pk_industrypartnerid as industrypartnerid,ipid,employertype,legal_entity_name,contact_person_name,contact_email_address,contact_mobile,address,status,is_email_sent,office_phone,website,fk_sectorID as sectorid,fk_stateID as stateid,fk_cityID as cityid,pincode ",
    //sFromSql: "tbl_UserMaster as UM LEFT JOIN tbl_MatchPlayingSquad as MPS ON (UM.pk_userID = MPS.fk_playerID) LEFT JOIN tbl_MatchMaster as MM ON (MM.pk_matchID = MPS.fk_matchID and MM.isDeleted = '0')",
  };
  var queryBuilder = new QueryBuilder(oTableDef);
  var queries = queryBuilder.buildQuery(params);
  queries.recordsTotal = "SELECT COUNT(*) as totalCount FROM `tbl_IndustryPartners` WHERE is_deleted = '0'";
//  console.log('#Queries', queries);
  var newres = {};
  if (queries.recordsFiltered) {
    var resFiltered1 = await connection.executeRawQuery(queries.recordsFiltered);
    //queries.recordsTotal = result.content;
    var resFilterNew = Object.values(resFiltered1.content[0]);
    //console.log('#1', resFilterNew);
  }
  var resTotal2 = await connection.executeRawQuery(queries.recordsTotal);
  //queries.recordsTotal = result.content;
  //console.log('#2', resTotal2.content[0].totalCount);

  var resSelect3 = await connection.executeRawQuery(queries.select);
  //console.log('#3', resSelect3.content);

  newres = { recordsFiltered: resFilterNew[0], recordsTotal: resTotal2.content[0].totalCount, data: resSelect3.content }
  return common.sendResponse(response, newres);
}

/**
 * get industry partner detail service
 * 
 * @param {object} request 
 * @param {object} response 
 * @return {object} 
 */
var getIndustryPartnerDetailService = async function (request, response) {
  debug("industrypartner.service -> getIndustryPartnerDetailService");
  var isValid = common.validateParams([request.params.id]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_USER_PROFILE_UPDATE_REQUEST, false);
  }
  try {
    let result = await industryPartnerDAL.getIndustryPartnerDetail(request.params.id);
    //console.log(request.params.id, "EMP ID");
    if (result.status === false) {
      return common.sendResponse(response, result);
    } else {
      response.render("partials/industrypartnerdetails.ejs", { data: result.content[0] }, function (err, html) {
        response.send(html);
      });
    }
  }
  catch (ex) {
    //console.log(ex, "ERROR");
    return common.sendResponse(response, constant.userMessages.ERR_NO_INDUSTRYPARTNER_FOUND, false);
  }
}

/**
 * remove industry partner
 * 
 * @param {object} request 
 * @param {object} response 
 * @return {object} 
 */
var removeIndustryPartnerService = async function (request, response) {
  debug("industrypartner.service -> removeIndustryPartnerService");
  var isValid = common.validateParams([request.params.id]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_USER_PROFILE_UPDATE_REQUEST, false);
  }
  try {
    var fieldValueUpdate = [];
    fieldValueUpdate.push({
      field: "is_deleted",
      fValue: 1
    });
    let result = await industryPartnerDAL.removeIndustryPartner(request.params.id, fieldValueUpdate);
    if (result.status === false) {
      return common.sendResponse(response, result);
    } else {
      return common.sendResponse(response, constant.userMessages.IP_DELETED_SUCCESSFULLY, true);
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.ERR_NO_TRAININGPARTNER_FOUND, false);
  }
}

/**
 * add/update Industry Partner
 * 
 * @param  {object} request
 * @param  {object} response
 * @return {object}
 */
var addUpdateIndustryPartnerService = async function (request, response) {
  debug("industrypartner.service -> addUpdateIndustryPartnerService");
  var isValid = common.validateParams([request.body.employertype, request.body.legal_entity_name, request.body.contact_person_name, request.body.contact_email_address, request.body.pincode, request.body.sectorid]);
  if (!isValid) {
    return common.sendResponse(response, constant.userMessages.INVALID_ADD_UPDATE_IP_REQUEST, false);
  }
  common.sanitizeAll(request.body);
  var userID = request.body.industrypartnerid;

  var industrypartnerinfo = {};
  if (userID == -1) {
    industrypartnerinfo.ipid = 'EMP-' + randomize('0', 5);
    industrypartnerinfo.password = bcrypt.hashSync(constant.appConfig.SYSTEM_PASSWORD, 10);
  }

  industrypartnerinfo.employertype = request.body.employertype;
  industrypartnerinfo.legal_entity_name = request.body.legal_entity_name;
  industrypartnerinfo.contact_person_name = request.body.contact_person_name;
  industrypartnerinfo.contact_email_address = request.body.contact_email_address;
  industrypartnerinfo.contact_mobile = request.body.contact_mobile;
  industrypartnerinfo.address = request.body.address;
  industrypartnerinfo.office_phone = request.body.office_phone;
  industrypartnerinfo.website = request.body.website;
  industrypartnerinfo.fk_stateID = request.body.stateid;
  industrypartnerinfo.fk_cityID = request.body.cityid;
  industrypartnerinfo.pincode = request.body.pincode;
  industrypartnerinfo.fk_sectorID = request.body.sectorid;

  var industrypartnerKeys = Object.keys(industrypartnerinfo);
  var fieldValueInsert = [];
  industrypartnerKeys.forEach(function (industrypartnerKeys) {
    if (industrypartnerinfo[industrypartnerKeys] !== undefined) {
      var fieldValueObj = {};
      fieldValueObj = {
        field: industrypartnerKeys,
        fValue: industrypartnerinfo[industrypartnerKeys]
      }
      fieldValueInsert.push(fieldValueObj);
    }
  });

  try {
    //While add new user we are sending user id as -1
    if (userID == -1) {
      var result = await industryPartnerDAL.checkUserIsExist(industrypartnerinfo.ipid, industrypartnerinfo.contact_email_address);
      if (result.content.length > 0 && result.content[0].industrypartnerid > 0) {
        return common.sendResponse(response, constant.userMessages.ERR_USER_IS_ALREADY_EXIST, false);
      }
      var res_create_user = await industryPartnerDAL.createIndustryUser(fieldValueInsert);
      return common.sendResponse(response, constant.userMessages.MSG_IP_ADD_SUCCESSFULLY, true);
    } else {
      var result = await industryPartnerDAL.checkEditUserIsExist(userID);

      if (result.content.length == 0) {
        return common.sendResponse(response, constant.userMessages.ERR_USER_NOT_EXIST, false);
      } else if (result.content.length > 0 && result.content[0].industrypartnerid == userID) {
        if (result.content[0].contact_email_address != industrypartnerinfo.contact_email_address) {
          var result = await industryPartnerDAL.checkUserIsExist(industrypartnerinfo.ipid, industrypartnerinfo.contact_email_address);
          if (result.status == true && result.content.length > 0 && result.content[0].industrypartnerid > 0) {
            return common.sendResponse(response, constant.userMessages.ERR_USER_IS_ALREADY_EXIST, false);
          } else if (result.status == true && result.content.length == 0) {
            var res_update_user = await industryPartnerDAL.updateUserInfoById(userID, fieldValueInsert);
            return common.sendResponse(response, constant.userMessages.MSG_IP_UPDATE_SUCCESSFULLY, true);
          }
        } else {
          var res_update_user = await industryPartnerDAL.updateUserInfoById(userID, fieldValueInsert);
          return common.sendResponse(response, constant.userMessages.MSG_IP_UPDATE_SUCCESSFULLY, true);
        }
      }
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
};

/**
 * When user wants to perform the actions like active/inactice/delete with multiple records
 * 
 * @param {object} request 
 * @param {object} response
 * @return {object} 
 */
var updateMultipleRecordsService = async function (request, response) {
  debug("industrypartner.service -> updateMultipleRecordsService");
  var isValid = common.validateParams([request.body.ids, request.body.action]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.INVALID_ADD_UPDATE_IP_REQUEST, false);
  }
  var fieldValueInsert = [];

  var fieldValueObj = {};
  switch (request.body.action) {
    case 'active':
      fieldValueObj = {
        field: 'status',
        fValue: 1
      }
      break;
    case 'inactive':
      fieldValueObj = {
        field: 'status',
        fValue: 0
      }
      break;
    case 'delete':
      fieldValueObj = {
        field: 'is_deleted',
        fValue: 1
      }
      break;
  }

  fieldValueInsert.push(fieldValueObj);

  try {
    request.body.ids.forEach(async function (id, index) {
      var result = await industryPartnerDAL.updateUserInfoById(id, fieldValueInsert);
      if (request.body.ids.length - 1 == index) {
        return common.sendResponse(response, constant.userMessages.MSG_IP_UPDATE_SUCCESSFULLY, true);
      }
    });
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

//cron to check state and city ids from industrypartners_bulk_upload table
var industryPartnerBulkUploadCheckStateCityService = async function () {
  var getUsers = await industryPartnerDAL.getIndustryPartnerStateCityFromBulkUplod();
  if (getUsers.status == true && getUsers.content.length > 0) {
    for (var i = 0; i < getUsers.content.length; i++) {
      var autoIncId = getUsers.content[i].id;
      var statename = getUsers.content[i].state;
      var cityname = getUsers.content[i].district;

      var result = await otherDAL.checkCityStateView(statename, cityname);

      if (result.status == true && result.content.length > 0 && result.content[0].stateid > 0 && result.content[0].cityid > 0) {
        var state = result.content[0].stateid;//state
        var district = result.content[0].cityid;//city
        if (state > 0 && district > 0) {
          var fieldValueUpdate = [];
          fieldValueUpdate.push({
            field: "state",
            fValue: state
          });
          fieldValueUpdate.push({
            field: "district",
            fValue: district
          });

          var result = await industryPartnerDAL.updateIndustryPartnerBulkUpload(autoIncId, fieldValueUpdate);
          if (result.status == true && result.content.changedRows == 1) {
            var fieldValueUpdate = [];
            fieldValueUpdate.push({
              field: "is_statecity_done",
              fValue: 1
            });
            var result = await industryPartnerDAL.updateIndustryPartnerBulkUpload(autoIncId, fieldValueUpdate);
            console.log(result);
          }
        } else {
          console.log('No state / city found');

        }
      } else {
        console.log('No state / city found');
      }
    }
  } else {
    console.log('No records');
  }
}

var processIndustryPartnerCSVCronService = async function () {
  //Query:SELECT * FROM `industry_bulk_upload` WHERE `is_processed` = 0 AND `is_sector_done` = 1 AND `is_statecity_done` = 1 ORDER BY id ASC
  //Step 1 :  Dump into tbl_IndustryPartners  the records
  //Step 3 : Update is_processed flag to 1 which are successfully inserted to the tbl_TrainingPartners.
  debug("other.service -> processIndustryPartnerCSVCronService");

  var getUsers = await industryPartnerDAL.getIndustryPartnerFromBulkUpload();
  console.log(getUsers);
  if (getUsers.status == true && getUsers.content.length > 0) {

    for (var i = 0; i < getUsers.content.length; i++) {
      var autoIncId = getUsers.content[i].id;

      var employertype = getUsers.content[i].employertype;
      var legal_entity_name = getUsers.content[i].legal_entity_name;
      var contact_person_name = getUsers.content[i].contact_person_name;
      var contact_email_address = getUsers.content[i].contact_email_address;
      var contact_mobile = getUsers.content[i].contact_mobile;
      var address = getUsers.content[i].address;
      var fk_sectorID = getUsers.content[i].sector;
      var office_phone = getUsers.content[i].office_phone;
      var website = getUsers.content[i].website;
      var fk_stateID = getUsers.content[i].state;
      var fk_cityID = getUsers.content[i].district;
      var pincode = getUsers.content[i].pincode;
      var ipid = 'EMP-' + randomize('0', 5);
      var password = bcrypt.hashSync(constant.appConfig.SYSTEM_PASSWORD, 10);

      var sqlQuery = constant.appConfig.INDUSTRY_PARTNERS_INSERT_QUERY;
      sqlQuery = sqlQuery + '("' + employertype + '","' + legal_entity_name + '","' + contact_person_name + '","' + contact_email_address + '","' +
        contact_mobile + '","' + address + '","' + office_phone + '","' + fk_stateID + '","' + fk_cityID + '","' +
        pincode + '","' + fk_sectorID + '","' + website + '","' + ipid + '","' + password + '");';

      //(`employertype`, `legal_entity_name`, `contact_person_name`, `contact_email_address`, 
      //`contact_mobile`, `address`,`office_phone`, `fk_stateID`, `fk_cityID`, `pincode`,`fk_sectorID`, `website`,`ipid`,`password`)

      console.log(sqlQuery);

      try {
        var res = await connection.executeRawQuery(sqlQuery);
        if (res.status === true && res.content !== undefined && res.content.insertId > 0) {
          var fieldValueUpdate = [];
          fieldValueUpdate.push({
            field: "is_processed",
            fValue: 1
          });
          var result = await industryPartnerDAL.updateIndustryPartnerBulkUpload(autoIncId, fieldValueUpdate);
          console.log(result);
        } else {

        }
      } catch (ex) {

      }
    }
  }
  else {
    console.log("No records to update");
  }
}

var addCandidateDetailService = async function (request, response) {
  debug("industrypartner.service -> addCandidateDetailService");
  if (request.body.candidatedetailid > 0) {
    updateCandidateDetail(request, response);
  } else {
    var isValid = common.validateParams([
      request.body.candidatedetailid,
      request.body.candidateid,
      request.body.industrypartnerid,
      request.body.interview_date,
      request.body.interview_time,
      request.body.salary,
      request.body.statename,
      request.body.districtname,
      request.body.is_esic,
      request.body.is_pf,
      request.body.other_perks,
      request.body.contact_person_email,
      request.body.contact_person_name,
      request.body.contact_person_phone,
      /* 
      request.body.is_transportation, 
      request.body.other_information,  */
    ]);
    if (!isValid) {
      return common.sendResponse(response, constant.requestMessages.ERR_INVALID_ADD_CANDIDATE_DETIAL_REQUEST, false);
    }
    common.sanitizeAll(request.body);

    var candidatedetailID = request.body.candidatedetailid;

    var totalCandidates = request.body.candidateid.split(',');
    var successCounter = 0;
    for (let i = 0; i < totalCandidates.length; i++) {
      var candidatedetailinfo = {};
      candidatedetailinfo.fk_cityID = request.body.districtname;
      candidatedetailinfo.fk_stateID = request.body.statename;
      candidatedetailinfo.interview_date = d3.timeFormat(dbDateFormat)(new Date(request.body.interview_date));
      candidatedetailinfo.interview_time = request.body.interview_time + ":00";
      candidatedetailinfo.salary = request.body.salary;
      candidatedetailinfo.is_pf = request.body.is_pf;
      candidatedetailinfo.is_esic = request.body.is_esic;
      candidatedetailinfo.is_food = request.body.is_food;
      candidatedetailinfo.is_transportation = request.body.is_transportation;
      candidatedetailinfo.is_accomodation = request.body.is_accomodation;
      candidatedetailinfo.other_perks = request.body.other_perks;
      candidatedetailinfo.contact_person_name = request.body.contact_person_name;
      candidatedetailinfo.contact_person_email = request.body.contact_person_email;
      candidatedetailinfo.contact_person_phone = request.body.contact_person_phone;
      candidatedetailinfo.other_information = request.body.other_information;

      var candidatedetailKeys = Object.keys(candidatedetailinfo);
      var fieldValueInsert = [];
      candidatedetailKeys.forEach(function (candidatedetailKeys) {
        if (candidatedetailinfo[candidatedetailKeys] !== undefined) {
          var fieldValueObj = {};
          fieldValueObj = {
            field: candidatedetailKeys,
            fValue: candidatedetailinfo[candidatedetailKeys]
          }
          fieldValueInsert.push(fieldValueObj);
        }
      });

      try {
        //While adding new candidatedetail we are sending candidatedetailID as -1
        if (candidatedetailID == -1) {
          var resultTPID = await candidateDAL.getTrainingPartnerIdFromCandidateID(totalCandidates[i]);
          if (resultTPID.status == true && resultTPID.content[0].trainingpartnerid > 0) {

            var res_create_hiringrequest_detail = await industryPartnerDAL.createHiringRequestDetail(fieldValueInsert);

            if (res_create_hiringrequest_detail.status == true && res_create_hiringrequest_detail.content !== undefined && res_create_hiringrequest_detail.content.insertId > 0) {
              //Insert into Hiring Request Detail Table
              var hiringrequestdetailinfo = {};
              hiringrequestdetailinfo.fk_candidatedetailid = res_create_hiringrequest_detail.content.insertId;
              hiringrequestdetailinfo.fk_industrypartnerid = request.body.industrypartnerid;
              hiringrequestdetailinfo.fk_trainingpartnerid = resultTPID.content[0].trainingpartnerid;
              hiringrequestdetailinfo.fk_candidateid = totalCandidates[i];

              var hiringrequestdetailKeys = Object.keys(hiringrequestdetailinfo);
              var fieldValueInsert = [];
              hiringrequestdetailKeys.forEach(function (hiringrequestdetailKeys) {
                if (hiringrequestdetailinfo[hiringrequestdetailKeys] !== undefined) {
                  var fieldValueObj = {};
                  fieldValueObj = {
                    field: hiringrequestdetailKeys,
                    fValue: hiringrequestdetailinfo[hiringrequestdetailKeys]
                  }
                  fieldValueInsert.push(fieldValueObj);
                }
              });

              var res_create_hiringrequest = await industryPartnerDAL.createHiringRequest(fieldValueInsert);
              if (res_create_hiringrequest.status == true && res_create_hiringrequest.content !== undefined && res_create_hiringrequest.content.insertId > 0) {
                successCounter++;
                if (totalCandidates.length === 1) {
                  otherService.sendNewHiringRequestEmailToTP(0, hiringrequestdetailinfo);
                }
              }
            }
          } else {
            return common.sendResponse(response, constant.userMessages.ERR_CANDIDATE_DETAIL_ADD, false);
          }
        }
      } catch (ex) {
        return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
      }
    }
    if (successCounter > 0) {
      return common.sendResponse(response, constant.userMessages.MSG_CANDIDATE_DETAIL_ADD_SUCCESSFULLY, true);
    } else {
      return common.sendResponse(response, constant.userMessages.ERR_CANDIDATE_DETAIL_ADD, false);
    }
  }
}

var sendNewHiringRequestBulkEmail = async function (request, response) {
  debug("industrypartner.service -> sendNewHiringRequestBulkEmail");
  var result = await industryPartnerDAL.checkNewEmailToBeSent();
  if (result.status === true && result.content.length > 0) {
    for (everyNewHiringRequest of result.content) {
      otherService.sendNewHiringRequestEmailToTP(1, everyNewHiringRequest);
    }
  } else {
    return common.sendResponse(response, constant.userMessages.ERR_CANDIDATE_DETAIL_ADD, false);
  }
}

var updateCandidateDetail = async function (request, response) {
  debug("industrypartner.service -> updateCandidateDetail");
  var isValid = common.validateParams([
    request.body.candidatedetailid,
    request.body.industrypartnerid,
  ]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_UPDATE_CANDIDATE_DETIAL_REQUEST, false);
  }
  common.sanitizeAll(request.body);

  var candidatedetailID = request.body.candidatedetailid;

  var result = await industryPartnerDAL.checkCandidateDetailIDIsExist(candidatedetailID);

  //check state district combination
  if (request.body.statename !== undefined) {
    if(request.body.districtname === undefined){
      return common.sendResponse(response, constant.requestMessages.ERR_SELECT_STATEDISTRICT, false);
    }
    var checkStateCityQuery = 'SELECT count(stateid) as totalCount FROM `view_GetLocationDetail` WHERE  `stateid` = '+request.body.statename+' AND `cityid` = '+request.body.districtname;
    var StateCityRes =  await connection.executeRawQuery(checkStateCityQuery);
   
    if(StateCityRes.status === true){
      if(StateCityRes.content[0].totalCount == 0){
        return common.sendResponse(response, constant.requestMessages.ERR_COMBINATION_DISTRICT, false);
      } 
    }
  }

  var candidatedetailinfo = {};
  if (request.body.districtname != undefined) {
    candidatedetailinfo.fk_cityID = request.body.districtname;
  }
  if (request.body.statename != undefined) {
    candidatedetailinfo.fk_stateID = request.body.statename;
  }
  if (request.body.interview_date != undefined) {
    candidatedetailinfo.interview_date = d3.timeFormat(dbDateFormat)(new Date(request.body.interview_date));
  }
  if (request.body.interview_time != undefined) {
    candidatedetailinfo.interview_time = request.body.interview_time + ":00";
  }
  if (request.body.salary != undefined) {
    candidatedetailinfo.salary = request.body.salary;
  }
  if (request.body.is_pf != undefined) {
    candidatedetailinfo.is_pf = request.body.is_pf;
  }
  if (request.body.is_esic != undefined) {
    candidatedetailinfo.is_esic = request.body.is_esic;
  }
  if (request.body.is_food != undefined) {
    candidatedetailinfo.is_food = request.body.is_food;
  }
  if (request.body.is_transportation != undefined) {
    candidatedetailinfo.is_transportation = request.body.is_transportation;
  }
  if (request.body.is_accomodation != undefined) {
    candidatedetailinfo.is_accomodation = request.body.is_accomodation;
  }
  if (request.body.other_perks != undefined) {
    candidatedetailinfo.other_perks = request.body.other_perks;
  }
  if (request.body.contact_person_name != undefined) {
    candidatedetailinfo.contact_person_name = request.body.contact_person_name;
  }
  if (request.body.contact_person_email != undefined) {
    candidatedetailinfo.contact_person_email = request.body.contact_person_email;
  }
  if (request.body.contact_person_phone != undefined) {
    candidatedetailinfo.contact_person_phone = request.body.contact_person_phone;
  }
  if (request.body.other_information != undefined) {
    candidatedetailinfo.other_information = request.body.other_information;
  }

  var candidatedetailKeys = Object.keys(candidatedetailinfo);
  var fieldValueUpdate = [];
  candidatedetailKeys.forEach(function (candidatedetailKeys) {
    if (candidatedetailinfo[candidatedetailKeys] !== undefined) {
      var fieldValueObj = {};
      fieldValueObj = {
        field: candidatedetailKeys,
        fValue: candidatedetailinfo[candidatedetailKeys]
      }
      fieldValueUpdate.push(fieldValueObj);
    }
  });
  if (result.content.length == 0) {
    return common.sendResponse(response, constant.userMessages.ERR_NO_HIRING_REQUEST_DETAILS_FOUND, false);
  } else if (result.content.length > 0 && result.content[0].candidatedetailid == candidatedetailID) {
    if (fieldValueUpdate.length > 0) {
      var res_update_candidate_detail = await industryPartnerDAL.updateCandidateDetailById(candidatedetailID, fieldValueUpdate);
    } else {

    }
    return common.sendResponse(response, constant.userMessages.MSG_CANDIDATE_DETAIL_UPDATE_SUCCESSFULLY, true);
  }
}

var getHiringRequests = async function (request, response) {
  debug("industrypartner.service -> getHiringRequests");
  var getPaginationObject = common.getPaginationObject(request,constant.appConfig.EMP_HIRING_REQUEST_RECORDS_LIMIT);
  var limit = getPaginationObject.limit;
  var searchObj = {};

  if (request.body !== undefined) {
    /* if (request.body.trainingpartnerid !== undefined) {
        request.body.fk_trainingpartnerid = request.body.trainingpartnerid;
        delete request.body.trainingpartnerid;
    } */

    if (request.body.industrypartnerid !== undefined) {
      if (request.body.type === 'TrainingPartner') {
        request.body.trainingpartnerid = request.body.industrypartnerid;
        delete request.body.industrypartnerid;
      } else {
        request.body.industrypartnerid = request.body.industrypartnerid;
      }
      delete request.body.type;
    }
    searchObj = request.body;
  }
  try {
    let res_count = await industryPartnerDAL.getHiringRequestsCount(searchObj);
    var totalHiringRequests = res_count.content[0].totalCount;

    var filterObj = {};
    if (searchObj.industrypartnerid !== undefined) {
      filterObj.industrypartnerid = searchObj.industrypartnerid;
    } else {
      filterObj.trainingpartnerid = searchObj.trainingpartnerid;
    }
    filterObj.hiring_request_status = 1;
    var totalPendingHiringRequests = await industryPartnerDAL.getHiringRequestsCount(filterObj);
    totalPendingHiringRequests = totalPendingHiringRequests.content[0].totalCount;

    filterObj.hiring_request_status = 2;
    var totalAcceptedHiringRequests = await industryPartnerDAL.getHiringRequestsCount(filterObj);
    totalAcceptedHiringRequests = totalAcceptedHiringRequests.content[0].totalCount;

    filterObj.hiring_request_status = 3;
    var totalCompletedHiringRequests = await industryPartnerDAL.getHiringRequestsCount(filterObj);
    totalCompletedHiringRequests = totalCompletedHiringRequests.content[0].totalCount;

    filterObj.hiring_request_status = 4;
    var totalDeclinedHiringRequests = await industryPartnerDAL.getHiringRequestsCount(filterObj);
    totalDeclinedHiringRequests = totalDeclinedHiringRequests.content[0].totalCount;
    filterObj.hiring_request_status = 5;
    var totalCancelledHiringRequests = await industryPartnerDAL.getHiringRequestsCount(filterObj);
    totalCancelledHiringRequests = totalCancelledHiringRequests.content[0].totalCount;

    let hiring_request_count_obj = {};
    hiring_request_count_obj.totalPendingHiringRequests = totalPendingHiringRequests;
    hiring_request_count_obj.totalAcceptedHiringRequests = totalAcceptedHiringRequests;
    hiring_request_count_obj.totalCompletedHiringRequests = totalCompletedHiringRequests;
    hiring_request_count_obj.totalDeclinedHiringRequests = totalDeclinedHiringRequests;
    hiring_request_count_obj.totalCancelledHiringRequests = totalCancelledHiringRequests;
    hiring_request_count_obj.internalStatus = request.query.internalStatus !== undefined ? request.query.internalStatus : 0;

    let result = await industryPartnerDAL.getHiringRequests(limit, searchObj, request.params.type);
    return common.sendResponse(response, result.content, true, common.getPaginationOptions(request, totalHiringRequests, constant.appConfig.EMP_HIRING_REQUEST_RECORDS_LIMIT, hiring_request_count_obj));
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.ERR_NO_HIRING_REQUEST_DETAILS_FOUND, false);
  }
}

var getHiringRequestDetailsForCandidateService = async function (request, response) {
  debug("industrypartner.service -> getHiringRequestDetailsForCandidateService");
  var isValid = common.validateParams([request.params.id]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_GET_DETAIL_REQUEST, false);
  }
  try {
    let result = await industryPartnerDAL.getHiringRequestDetailsForCandidate(request.params.id);
    if (result.status === false) {
      return common.sendResponse(response, constant.userMessages.ERR_NO_HIRING_REQUEST_DETAILS_FOUND, false);
    } else {
      return common.sendResponse(response, result.content, true);
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

var getIndustryPartnerSettingsService = async function (request, response) {
  debug("industrypartner.service -> getIndustryPartnerSettingsService");
  var isValid = common.validateParams([request.params.id]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_GET_DETAIL_REQUEST, false);
  }
  try {
    let result = await industryPartnerDAL.getIndustryPartnerDetail(request.params.id);
    if (result.status === true && result.content.length > 0) {
      return common.sendResponse(response, result.content, true);
    } else {
      return common.sendResponse(response, constant.userMessages.ERR_NO_INDUSTRYPARTNER_FOUND, false);
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

var getIndustryPartnerDashboardDataService = async function (request, response) {
  debug("industrypartner.service -> getIndustryPartnerDashboardDataService");
  var isValid = common.validateParams([request.body.industrypartnerid]);
  if (!isValid) {
    return common.sendResponse(response, constant.userMessages.INVALID_GET_EMP_DASHBOARD_DATA_REQUEST, false);
  }
  try {
    var result = { counts: [], candidate_statistics: [], recent_hiring_requests: [], recent_activities: [], recruitment_history: [] };
    var totalHiredCadidates = 0;
    var totalFutureRequests = 0;
    var totalPendingHiringRequests = 0;
    var totalCompletedHiringRequests = 0;
    var totalAcceptedHiringRequests = 0;
    var totalDeclinedHiringRequests = 0;

    //Get count of total hired(joined) candidates hired by loggedin EMP
    let totalhiredcandidatessRes = await industryPartnerDAL.getTotalNumberOfJoinedCandidates(request.body.industrypartnerid);
    if (totalhiredcandidatessRes.status === true && totalhiredcandidatessRes.content.length > 0) {
      totalHiredCadidates = totalhiredcandidatessRes.content[0].totalJoinedCandidatesCount;
    }

    //Get total future requests posted by loggedin EMP
    let totalfuturereqRes = await industryPartnerDAL.getTotalNumberOfFutureRequests(request.body.industrypartnerid);
    if (totalfuturereqRes.status === true && totalfuturereqRes.content.length > 0) {
      totalFutureRequests = totalfuturereqRes.content[0].totalFutureRequestsCount !== null ? totalfuturereqRes.content[0].totalFutureRequestsCount : 0;
    }

    //Get Total Number of Pending Hiring Requests posted by loggedin EMP
    var filterObj = {};
    filterObj.hiring_request_status = 1;//Pending
    filterObj.industrypartnerid = request.body.industrypartnerid;
    let totalpendingrequestRes = await industryPartnerDAL.getHiringRequestsCount(filterObj);
    if (totalpendingrequestRes.status === true && totalpendingrequestRes.content.length > 0) {
      totalPendingHiringRequests = totalpendingrequestRes.content[0].totalCount;
    }

    //Get Total Number of Completed Hiring Requests posted by loggedin EMP
    var filterObj = {};
    filterObj.hiring_request_status = 3;//Completed
    filterObj.industrypartnerid = request.body.industrypartnerid;
    let totalcompletedrequestRes = await industryPartnerDAL.getHiringRequestsCount(filterObj);
    if (totalcompletedrequestRes.status === true && totalcompletedrequestRes.content.length > 0) {
      totalCompletedHiringRequests = totalcompletedrequestRes.content[0].totalCount;
    }

    //Get Total Number of Accepted Hiring Requests posted by loggedin EMP
    var filterObj = {};
    filterObj.hiring_request_status = 4;//Accepted
    filterObj.industrypartnerid = request.body.industrypartnerid;
    let totalacceptedrequestRes = await industryPartnerDAL.getHiringRequestsCount(filterObj);
    if (totalacceptedrequestRes.status === true && totalacceptedrequestRes.content.length > 0) {
      totalAcceptedHiringRequests = totalacceptedrequestRes.content[0].totalCount;
    }

    //Get Total Number of Declined Hiring Requests posted by loggedin EMP
    var filterObj = {};
    filterObj.hiring_request_status = 5;//Declined
    filterObj.industrypartnerid = request.body.industrypartnerid;
    let totaldeclinedrequestRes = await industryPartnerDAL.getHiringRequestsCount(filterObj);
    if (totaldeclinedrequestRes.status === true && totaldeclinedrequestRes.content.length > 0) {
      totalDeclinedHiringRequests = totaldeclinedrequestRes.content[0].totalCount;
    }

    var countObj = {};
    countObj.totalhiredcandidates = totalHiredCadidates;
    countObj.totalfuturerequests = totalFutureRequests;
    countObj.totalhiringrequests = totalPendingHiringRequests;
    result.counts.push(countObj);

    //Get Candidate Statistics [availablecandidates,hiredcandidates,totalhiringrequests] 
    var availableCandidates = totalPendingHiringRequests;

    var candidateStatisticsObj = {};
    candidateStatisticsObj.availablecandidates = availableCandidates; //availablecandidates = total - joined
    candidateStatisticsObj.hiredcandidates = totalHiredCadidates; //Total hired candidates
    candidateStatisticsObj.totalhiringrequestsdonebyemp = totalPendingHiringRequests + totalCompletedHiringRequests + totalAcceptedHiringRequests + totalDeclinedHiringRequests; //Total HiringRequests = [pending+completed+declined+cancelled]
    result.candidate_statistics.push(candidateStatisticsObj);

    //Get Recent Pending Hiring Requests - limit 10 [it means the recent pending requests created by loggedin EMP]
    let recentPendingRequests = await industryPartnerDAL.getRecentPendingRequests(request.body.industrypartnerid);
    if (recentPendingRequests.status === true && recentPendingRequests.content.length > 0) {
      result.recent_hiring_requests = recentPendingRequests.content
    }

    //Get Recent activities performed by the logged in EMP - limit 10
    let recentActivities = await otherDAL.getRecentActivities(request.body.industrypartnerid, 'EMP');
    if (recentActivities.status === true && recentActivities.content.length > 0) {
      result.recent_activities = recentActivities.content
    }

    //Get Recruitment History - limit 10 [training partners who marked no. of candidates as hired (joined)]
    let recentRecruitmentres = await industryPartnerDAL.getRecentRecruitments(request.body.industrypartnerid);
    if (recentRecruitmentres.status === true && recentRecruitmentres.content.length > 0) {
      result.recruitment_history = recentRecruitmentres.content
    }
    return common.sendResponse(response, result, true);
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

module.exports = {
  signupService: signupService,
  signinService: signinService,
  changePasswordService: changePasswordService,
  updateProfileService: updateProfileService,
  getIndustryPartnerService: getIndustryPartnerService,
  getIndustryPartnerDetailService: getIndustryPartnerDetailService,
  removeIndustryPartnerService: removeIndustryPartnerService,
  addUpdateIndustryPartnerService: addUpdateIndustryPartnerService,
  updateMultipleRecordsService: updateMultipleRecordsService,
  industryPartnerBulkUploadCheckStateCityService: industryPartnerBulkUploadCheckStateCityService,
  processIndustryPartnerCSVCronService: processIndustryPartnerCSVCronService,
  addCandidateDetailService: addCandidateDetailService,
  getHiringRequests: getHiringRequests,
  getHiringRequestDetailsForCandidateService: getHiringRequestDetailsForCandidateService,
  getIndustryPartnerSettingsService: getIndustryPartnerSettingsService,
  getIndustryPartnerDashboardDataService: getIndustryPartnerDashboardDataService,
  sendNewHiringRequestBulkEmail: sendNewHiringRequestBulkEmail,
}