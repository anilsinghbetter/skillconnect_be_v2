var tbl_UserMaster = "tbl_UserMaster";
var tbl_RoleMaster = "tbl_RoleMaster";
var tbl_UserType = "tbl_UserType";
var tbl_AccessToken = "tbl_AccessToken";
var tbl_transactionMaster = "tbl_transactionMaster";
var view_GetUserDetail = "view_GetUserDetail";
var tbl_IndustryPartners = "tbl_IndustryPartners";
var view_GetLocationDetail = "view_GetLocationDetail";
var tbl_SectorMaster = "tbl_SectorMaster";
var industrypartners_bulk_upload = "industrypartners_bulk_upload";
var tbl_CandidateDetail = "tbl_CandidateDetail";
var tbl_Candidates = "tbl_Candidates";
var tbl_TrainingPartners = "tbl_TrainingPartners";
var view_SectorJobRoleDetail = "view_SectorJobRoleDetail";
var tbl_HiringRequestsDetail = "tbl_HiringRequestsDetail";
var view_HiringDetail = "view_HiringDetail";
var tbl_FutureHiringRequests = "tbl_FutureHiringRequests";

var query = {
    createIndustryUserQuery: {
        table: tbl_IndustryPartners,
        insert: []
    },
    checkUserIsExistQuery: {
        table: tbl_IndustryPartners,
        select: [{
            field: 'pk_industrypartnerid',
            alias: 'industrypartnerid'
        }, {
            field: 'contact_email_address',
            alias: 'contact_email_address'
        }],
        filter: {
            and: [{
                field: 'pk_industrypartnerid',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    checkIndustryPartnerIsExistQuery: {
        table: tbl_IndustryPartners,
        select: [{
            field: 'pk_industrypartnerid',
            alias: 'industrypartnerid'
        }],
        filter: {
            and: [{
                field: 'is_deleted',
                operator: 'EQ',
                value: 0
            }, {
                or: [{
                    field: 'ipid',
                    operator: 'EQ',
                    value: ''
                }, {
                    field: 'contact_email_address',
                    operator: 'EQ',
                    value: ''
                }]
            }],
        }
    },
    getIndustryInfoQuery: {
        table: tbl_IndustryPartners,
        alias: 'IP',
        select: [{
            field: 'IP.pk_industrypartnerid',
            encloseField: false,
            alias: 'industrypartnerid'
        }, {
            field: 'IP.ipid',
            encloseField: false,
            alias: 'ipid'
        }, {
            field: 'IP.password',
            encloseField: false,
            alias: 'password'
        }, {
            field: 'IP.employertype',
            encloseField: false,
            alias: 'employertype'
        }, {
            field: 'IP.legal_entity_name',
            encloseField: false,
            alias: 'legal_entity_name'
        }, {
            field: 'IP.contact_person_name',
            encloseField: false,
            alias: 'contact_person_name'
        }, {
            field: 'IP.contact_email_address',
            encloseField: false,
            alias: 'contact_email_address'
        }, {
            field: 'IP.contact_mobile',
            encloseField: false,
            alias: 'contact_mobile'
        }, {
            field: 'IP.address',
            encloseField: false,
            alias: 'address'
        }, {
            field: 'IP.office_phone',
            encloseField: false,
            alias: 'office_phone'
        }, {
            field: 'IP.website',
            encloseField: false,
            alias: 'website'
        }, {
            field: 'IP.fk_sectorID',
            encloseField: false,
            alias: 'sector'
        }, {
            field: 'IP.created_date',
            encloseField: false,
            alias: 'created_date'
        }, {
            field: 'IP.status',
            encloseField: false,
            alias: 'status'
        }, {
            field: 'IP.pincode',
            encloseField: false,
            alias: 'pincode'
        }],
        filter: {
            and: [{
                field: 'IP.ipid',
                encloseField: false,
                operator: 'EQ',
                value: ''
            }, {
                field: 'IP.is_deleted',
                encloseField: false,
                operator: 'EQ',
                value: 0
            }]
        }
    },
    updateAccessTokenQuery: {
        table: tbl_AccessToken,
        update: [{
            field: 'isExpired',
            fValue: 1
        }],
        filter: {
            or: [{
                and: [{
                    field: 'fk_industrypartnerid',
                    operator: 'EQ',
                    value: ''
                }, {
                    field: 'deviceID',
                    operator: 'EQ',
                    value: ''
                }, {
                    field: 'requestHost',
                    operator: 'EQ',
                    value: ''
                }]
            }, {
                field: 'deviceID',
                operator: 'EQ',
                value: ''
            }, {
                field: 'requestHost',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    insertAccessTokenQuery: {
        table: tbl_AccessToken,
        insert: {
            field: ["fk_industrypartnerid", "token", "expiryDateTime", "deviceID", "requestHost"],
            fValue: []
        }
    },
    checkUserTransactionQuery: {
        table: tbl_transactionMaster,
        select: [{
            field: 'pk_transactionID',
            aggregation: 'count',
            alias: 'totalCount'
        }],
        filter: {
            and: [{
                field: 'deviceID',
                operator: 'EQ',
                value: ''
            }, {
                field: 'deviceType',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    updateUserTransactionQuery: {
        table: tbl_transactionMaster,
        update: [{
            field: 'isLogedIn',
            fValue: 1
        }],
        filter: {
            and: [{
                field: 'deviceID',
                operator: 'EQ',
                value: ''
            }, {
                field: 'deviceType',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    insertUserTransactionQuery: {
        table: tbl_transactionMaster,
        insert: {
            field: ["deviceID", "deviceType"],
            fValue: []
        }
    },
    validateUserQuery: {
        table: tbl_IndustryPartners,
        select: [{
            field: 'pk_industrypartnerid',
            alias: 'industrypartnerid'
        }, {
            field: 'ipid',
            alias: 'ipid'
        }, {
            field: 'password',
            alias: 'password'
        }, {
            field: 'status',
            alias: 'status'
        }],
        filter: {
            and: [{
                field: 'pk_industrypartnerid',
                operator: 'EQ',
                value: ''
            }, {
                field: 'is_deleted',
                operator: 'EQ',
                value: 0
            }]
        }
    },
    updateUserQuery: {
        table: tbl_IndustryPartners,
        update: [],
        filter: {
            and: [{
                field: 'pk_industrypartnerid',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    getIndustryPartnersCountQuery: {
        table: tbl_IndustryPartners,
        select: [{
            field: 'pk_industrypartnerid',
            aggregation: 'count',
            alias: 'totalCount'
        }],
        filter: {}
    },
    getIndustryPartnersQuery: {
        table: tbl_IndustryPartners,
        select: [{
            field: 'pk_industrypartnerid',
            alias: 'industrypartnerid'
        }, {
            field: 'ipid',
            alias: 'ipid'
        }, {
            field: 'employertype',
            alias: 'employertype'
        }, {
            field: 'legal_entity_name',
            alias: 'legal_entity_name'
        }, {
            field: 'contact_person_name',
            alias: 'contact_person_name'
        }, {
            field: 'contact_email_address',
            alias: 'contact_email_address'
        }, {
            field: 'contact_mobile',
            alias: 'contact_mobile'
        }, {
            field: 'address',
            alias: 'address'
        }, {
            field: 'status',
            alias: 'status'
        }, {
            field: 'is_email_sent',
            alias: 'is_email_sent'
        }, {
            field: 'office_phone',
            alias: 'office_phone'
        }, {
            field: 'website',
            alias: 'website'
        }, {
            field: 'fk_sectorID',
            alias: 'sectorid'
        }, {
            field: 'fk_stateID',
            alias: 'stateid'
        }, {
            field: 'fk_cityID',
            alias: 'cityid'
        }, {
            field: 'pincode',
            alias: 'pincode'
        }],
        filter: {
            and: [{
                field: 'is_deleted',
                operator: 'EQ',
                value: 0
            }]
        },
        sortby: [{
            field: 'pk_industrypartnerid',
            order: 'DESC'
        }]
    },
    getIndustryPartnerDetailQuery: {
        join: {
            table: tbl_IndustryPartners,
            alias: 'IP',
            joinwith: [{
                table: view_GetLocationDetail,
                alias: 'LD',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'LD',
                        field: 'stateid',
                        operator: 'eq',
                        value: {
                            table: 'IP',
                            field: 'fk_stateID'
                        }
                    }, {
                        table: 'LD',
                        field: 'cityid',
                        operator: 'eq',
                        value: {
                            table: 'IP',
                            field: 'fk_cityID'
                        }
                    }]
                }
            }, {
                table: tbl_SectorMaster,
                alias: 'SEC',
                joincondition: {
                    table: 'SEC',
                    field: 'pk_sectorID',
                    operator: 'find_in_set',
                    value: {
                        table: 'IP',
                        field: 'fk_sectorID'
                    }
                }
            }]
        },
        select: [{
            field: 'GROUP_CONCAT(SEC.sectorName)',
            encloseField: false,
            alias: 'sectorname'
        }, {
            field: 'LD.stateName',
            encloseField: false,
            alias: 'stateName'
        }, {
            field: 'LD.stateid',
            encloseField: false,
            alias: 'stateid'
        }, {
            field: 'LD.cityid',
            encloseField: false,
            alias: 'cityid'
        }, {
            field: 'LD.cityName',
            encloseField: false,
            alias: 'cityName'
        }, {
            field: 'IP.pk_industrypartnerid',
            encloseField: false,
            alias: 'industrypartnerid'
        }, {
            field: 'IP.ipid',
            encloseField: false,
            alias: 'ipid'
        }, {
            field: 'IP.employertype',
            encloseField: false,
            alias: 'employertype'
        }, {
            field: 'IP.legal_entity_name',
            encloseField: false,
            alias: 'legal_entity_name'
        }, {
            field: 'IP.contact_person_name',
            encloseField: false,
            alias: 'contact_person_name'
        }, {
            field: 'IP.contact_email_address',
            encloseField: false,
            alias: 'contact_email_address'
        }, {
            field: 'IP.contact_mobile',
            encloseField: false,
            alias: 'contact_mobile'
        }, {
            field: 'IP.address',
            encloseField: false,
            alias: 'address'
        }, {
            field: 'IP.office_phone',
            encloseField: false,
            alias: 'office_phone'
        }, {
            field: 'IP.website',
            encloseField: false,
            alias: 'website'
        }, {
            field: 'IP.created_date',
            encloseField: false,
            alias: 'created_date'
        }, {
            field: 'IP.status',
            encloseField: false,
            alias: 'status'
        }, {
            field: 'IP.pincode',
            encloseField: false,
            alias: 'pincode'
        }],
        filter: {
            and: [{
                field: 'IP.pk_industrypartnerid',
                encloseField: false,
                operator: 'EQ',
                value: ''
            }, {
                field: 'IP.is_deleted',
                encloseField: false,
                operator: 'EQ',
                value: 0
            },{
                field: 'SEC.status',
                encloseField: false,
                operator: 'EQ',
                value: 1
            }]
        }
    },
    getIndustryPartnerInfoQuery: {
        table: tbl_IndustryPartners,
        alias: 'IP',
        select: [{
            field: 'IP.pk_industrypartnerid',
            encloseField: false,
            alias: 'industrypartnerid'
        }, {
            field: 'IP.ipid',
            encloseField: false,
            alias: 'ipid'
        }, {
            field: 'IP.employertype',
            encloseField: false,
            alias: 'employertype'
        }, {
            field: 'IP.legal_entity_name',
            encloseField: false,
            alias: 'legal_entity_name'
        }, {
            field: 'IP.contact_person_name',
            encloseField: false,
            alias: 'contact_person_name'
        }, {
            field: 'IP.contact_email_address',
            encloseField: false,
            alias: 'contact_email_address'
        }],
        filter: {
            and: [{
                field: 'IP.pk_industrypartnerid',
                encloseField: false,
                operator: 'EQ',
                value: ''
            }, {
                field: 'IP.is_deleted',
                encloseField: false,
                operator: 'EQ',
                value: 0
            }]
        }
    },
    getIndustryPartnerStateCityFromBulkUplodQuery: {
        table: industrypartners_bulk_upload,
        select: [{
            field: 'id',
        }, {
            field: 'state',
        }, {
            field: 'district',
        }],
        filter: {
            and: [{
                field: 'is_processed',
                operator: 'EQ',
                value: 0
            }, {
                field: 'is_statecity_done',
                operator: 'EQ',
                value: 0
            }]
        },
        sortby: [{
            field: 'id',
            order: 'ASC'
        }]
    },
    removeIndustryPartnerQuery: {
        table: tbl_IndustryPartners,
        update: [],
        filter: {
            and: [{
                field: 'pk_industrypartnerid',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    updateIndustryPartnerBulkUploadQuery: {
        table: industrypartners_bulk_upload,
        update: [],
        filter: {
            and: [{
                field: 'id',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    getIndustryPartnerFromBulkUploadQuery: {
        table: industrypartners_bulk_upload,
        select: [{
            field: 'id'
        }, {
            field: 'employertype'
        }, {
            field: 'legal_entity_name'
        }, {
            field: 'contact_person_name'
        }, {
            field: 'contact_email_address'
        }, {
            field: 'contact_mobile'
        }, {
            field: 'address'
        }, {
            field: 'sector'
        }, {
            field: 'office_phone'
        }, {
            field: 'website'
        }, {
            field: 'state'
        }, {
            field: 'district'
        }, {
            field: 'pincode'
        }],
        filter: {
            and: [{
                field: 'is_processed',
                operator: 'EQ',
                value: 0
            }, {
                field: 'is_sector_done',
                operator: 'EQ',
                value: 1
            }, {
                field: 'is_statecity_done',
                operator: 'EQ',
                value: 1
            }]
        },
        sortby: [{
            field: 'id',
            order: 'ASC'
        }]
    },
    createHiringRequestDetailQuery: {
        table: tbl_CandidateDetail,
        insert: []
    },
    createHiringRequestQuery: {
        table: tbl_HiringRequestsDetail,
        insert: []
    },
    getHiringRequestsCountQuery: {
        table: view_HiringDetail,
        select: [{
            field: 'hiringrequestdetailid',
            aggregation: 'count',
            alias: 'totalCount'
        }],
        filter: {}
    },
    getHiringRequestsQuery: {
        table: view_HiringDetail,
        select: [{
            field: 'hiringrequestdetailid',
        }, {
            field: 'candidatedetailid',
        }, {
            field: 'industrypartnerid',
        }, {
            field: 'trainingpartnerid',
        }, {
            field: 'candidateid',
        }, {
            field: 'hiring_request_status',
        }, {
            field: 'internal_status',
        }, {
            field: 'cancel_reason',
        }, {
            field: 'stateid',
        }, {
            field: 'cityid',
        }, {
            field: 'DATE_FORMAT(interview_date,"%d %M %Y")',
            encloseField: false,
            alias: 'interview_date'
        }, {
            field: 'TIME_FORMAT(interview_time, "%h:%i %p")',
            encloseField: false,
            alias: 'interview_time'
        }, {
            field: 'salary',
        }, {
            field: 'is_pf',
        }, {
            field: 'is_esic',
        }, {
            field: 'is_food',
        }, {
            field: 'is_transportation',
        }, {
            field: 'is_accomodation',
        }, {
            field: 'other_perks',
        }, {
            field: 'contact_person_name',
        }, {
            field: 'contact_person_email',
        }, {
            field: 'contact_person_phone',
        }, {
            field: 'other_information',
        }, {
            field: 'cityname',
        }, {
            field: 'statename',
        }, {
            field: 'training_partner_name',
        }, {
            field: 'industry_partner_name',
        }, {
            field: 'candidate_name',
        }, {
            field: 'sectorname',
        }, {
            field: 'jobroleName',
        },
        ],
        filter: {},
        sortby: [{
            field: 'hiringrequestdetailid',
            order: 'DESC'
        }]
    },
    /* getHiringRequestsQuery1: {
        join: {
            table: tbl_CandidateDetail,
            alias: 'CDETAIL',
            joinwith: [{
                table: view_GetLocationDetail,
                alias: 'LD',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'LD',
                        field: 'stateid',
                        operator: 'eq',
                        value: {
                            table: 'CDETAIL',
                            field: 'fk_stateID'
                        }
                    }, {
                        table: 'LD',
                        field: 'cityid',
                        operator: 'eq',
                        value: {
                            table: 'CDETAIL',
                            field: 'fk_cityID'
                        }
                    }]
                }
            }, {
                table: tbl_Candidates,
                alias: 'CD',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'CD',
                        field: 'pk_candidateid',
                        operator: 'eq',
                        value: {
                            table: 'CDETAIL',
                            field: 'fk_candidateid'
                        }
                    }]
                }
            },{
                table: tbl_IndustryPartners,
                alias: 'IP',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'IP',
                        field: 'pk_industrypartnerid',
                        operator: 'eq',
                        value: {
                            table: 'CDETAIL',
                            field: 'fk_industrypartnerid'
                        }
                    }]
                }
            },{
                table: tbl_TrainingPartners,
                alias: 'TP',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'TP',
                        field: 'pk_trainingpartnerid',
                        operator: 'eq',
                        value: {
                            table: 'CDETAIL',
                            field: 'fk_trainingpartnerid'
                        }
                    }]
                }
            }, {
                table: view_SectorJobRoleDetail,
                alias: 'SEC',
                type: 'LEFT',
                joincondition: {
                    and: [{
                        table: 'SEC',
                        field: 'sectorid',
                        operator: 'find_in_set',
                        value: {
                            table: 'CD',
                            field: 'fk_sectorID'
                        }
                    }, {
                        table: 'SEC',
                        field: 'jobroleid',
                        operator: 'eq',
                        value: {
                            table: 'CD',
                            field: 'fk_jobroleID'
                        }
                    }]
                }
            }]
        },
        select: [{
            field: 'SEC.sectorid',
            encloseField: false,
            alias: 'sectorid'
        },{
            field: 'SEC.sectorName',
            encloseField: false,
            alias: 'sectorName'
        },{
            field: 'SEC.jobroleid',
            encloseField: false,
            alias: 'jobroleid'
        }, {
            field: 'SEC.jobroleName',
            encloseField: false,
            alias: 'jobroleName'
        }, {
            field: 'LD.stateid',
            encloseField: false,
            alias: 'stateid'
        },  {
            field: 'LD.stateName',
            encloseField: false,
            alias: 'stateName'
        }, {
            field: 'LD.cityid',
            encloseField: false,
            alias: 'cityid'
        }, {
            field: 'LD.cityName',
            encloseField: false,
            alias: 'cityName'
        }, {
            field: 'CD.candidate_name',
            encloseField: false,
            alias: 'candidate_name'
        }, {
            field: 'TP.training_partner_name',
            encloseField: false,
            alias: 'training_partner_name'
        }, {
            field: 'IP.legal_entity_name',
            encloseField: false,
            alias: 'industry_partner_name'
        }, {
            field: 'CDETAIL.pk_candidatedetailid',
            encloseField: false,
            alias: 'status'
        }, {
            field: 'CDETAIL.fk_candidateid',
            encloseField: false,
            alias: 'candidateid'
        }, {
            field: 'DATE_FORMAT(CDETAIL.interview_date,"%d %M %Y")',
            encloseField: false,
            alias: 'interview_date'
        }, {
            field: 'CDETAIL.interview_time',
            encloseField: false,
            alias: 'interview_time'
        }, {
            field: 'CDETAIL.salary',
            encloseField: false,
            alias: 'salary'
        }, {
            field: 'CDETAIL.is_pf',
            encloseField: false,
            alias: 'is_pf'
        }, {
            field: 'CDETAIL.is_esic',
            encloseField: false,
            alias: 'is_esic'
        }, {
            field: 'CDETAIL.is_food',
            encloseField: false,
            alias: 'is_food'
        }, {
            field: 'CDETAIL.is_transportation',
            encloseField: false,
            alias: 'is_transportation'
        }, {
            field: 'CDETAIL.is_accomodation',
            encloseField: false,
            alias: 'is_accomodation'
        }, {
            field: 'CDETAIL.other_perks',
            encloseField: false,
            alias: 'other_perks'
        }, {
            field: 'CDETAIL.contact_person_name',
            encloseField: false,
            alias: 'contact_person_name'
        }, {
            field: 'CDETAIL.contact_person_email',
            encloseField: false,
            alias: 'contact_person_email'
        }, {
            field: 'CDETAIL.contact_person_phone',
            encloseField: false,
            alias: 'contact_person_phone'
        }, {
            field: 'CDETAIL.other_information',
            encloseField: false,
            alias: 'other_information'
        }, {
            field: 'CDETAIL.hiring_request_status',
            encloseField: false,
            alias: 'hiring_request_status'
        }],
        filter: {},
        sortby: [{
            field: 'CDETAIL.pk_candidatedetailid',
            encloseField: false,
            order: 'DESC'
        }]
    } */
    getHiringRequestDetailsForCandidateQuery: {
        table: view_HiringDetail,
        select: [{
            field: 'hiringrequestdetailid',
        }, {
            field: 'candidatedetailid',
        }, {
            field: 'industrypartnerid',
        }, {
            field: 'trainingpartnerid',
        }, {
            field: 'candidateid',
        }, {
            field: 'hiring_request_status',
        }, {
            field: 'stateid',
        }, {
            field: 'cityid',
        }, {
            field: 'DATE_FORMAT(interview_date,"%Y-%m-%d")',
            encloseField: false,
            alias: 'interview_date'
        }, {
            field: 'interview_time',
        }, {
            field: 'salary',
        }, {
            field: 'is_pf',
        }, {
            field: 'is_esic',
        }, {
            field: 'is_food',
        }, {
            field: 'is_transportation',
        }, {
            field: 'is_accomodation',
        }, {
            field: 'other_perks',
        }, {
            field: 'contact_person_name',
        }, {
            field: 'contact_person_email',
        }, {
            field: 'contact_person_phone',
        }, {
            field: 'other_information',
        }, {
            field: 'cityname',
        }, {
            field: 'statename',
        }, {
            field: 'training_partner_name',
        }, {
            field: 'industry_partner_name',
        }, {
            field: 'candidate_name',
        }, {
            field: 'sectorname',
        }, {
            field: 'jobroleName',
        },
        ],
        filter: {
            and: [{
                field: 'candidatedetailid',
                encloseField: false,
                operator: 'EQ',
                value: ''
            }]
        }
    },
    checkNewEmailToBeSentQuery: {
        table: tbl_HiringRequestsDetail,
        select: [{
            field: 'fk_industrypartnerid',
            alias: 'fk_industrypartnerid'
        }, {
            field: 'fk_trainingpartnerid',
            encloseField: false,
            alias: 'fk_trainingpartnerid'
        }],
        filter: {
            and: [{
                field: 'is_email_sent_for_new_request',
                operator: 'EQ',
                value: 0
            }, {
                field: 'hiring_request_status',
                operator: 'EQ',
                value: 1
            }, {
                field: 'log_status',
                operator: 'EQ',
                value: 1
            }]
        },
        groupby: [{
            field: 'fk_trainingpartnerid'
        }, {
            field: 'fk_industrypartnerid'
        }]
    },
    checkCandidateDetailIDIsExistQuery: {
        table: tbl_CandidateDetail,
        select: [{
            field: 'pk_candidatedetailid',
            alias: 'candidatedetailid'
        }],
        filter: {
            and: [{
                field: 'pk_candidatedetailid',
                encloseField: false,
                operator: 'EQ',
                value: ''
            }]
        }
    },
    updateCandidateDetailByIdQuery: {
        table: tbl_CandidateDetail,
        update: [],
        filter: {
            and: [{
                field: 'pk_candidatedetailid',
                operator: 'EQ',
                value: ''
            }]
        }
    },
    getTotalNumberOfJoinedCandidatesQuery: {
        table: view_HiringDetail,
        select: [{
            field: 'hiringrequestdetailid',
            aggregation: 'count',
            alias: 'totalJoinedCandidatesCount'
        }],
        filter: {
            and: [{
                field: 'industrypartnerid',
                operator: 'EQ',
                value: ''
            }, {
                field: 'hiring_request_status',
                operator: 'EQ',
                value: 2
            }, {
                field: 'internal_status',
                operator: 'EQ',
                value: 5
            }, {
                field: 'log_status',
                operator: 'EQ',
                value: 1
            }, {
                field: 'internal_log_status',
                operator: 'EQ',
                value: 1
            }]
        }
    },
    getTotalNumberOfFutureRequestsQuery: {
        table: tbl_FutureHiringRequests,
        select: [{
            field: 'SUM(total)',
            encloseField: false,
            alias: 'totalFutureRequestsCount'
        }],
        filter: {
            and: [{
                field: 'fk_industrypartnerid',
                operator: 'EQ',
                value: ''
            }, {
                field: 'status',
                operator: 'EQ',
                value: 1
            }, {
                field: 'is_deleted',
                operator: 'EQ',
                value: 0
            }]
        }
    },
    getRecentPendingRequestsQuery: {
        table: view_HiringDetail,
        select: [{
            field: 'hiringrequestdetailid',
        }, {
            field: 'training_partner_name',
            alias: 'trainingpartnername'
        }, {
            field: 'candidate_name',
            alias: 'candidatename'
        }, {
            field: 'statename',
        }, {
            field: 'jobroleName',
            alias: 'jobrolename'
        },
        ],
        filter: {
            and: [{
                field: 'industrypartnerid',
                operator: 'EQ',
                value: ''
            }, {
                field: 'hiring_request_status',
                operator: 'EQ',
                value: 1
            }, {
                field: 'log_status',
                operator: 'EQ',
                value: 1
            }]
        },
        sortby: [{
            field: 'hiringrequestdetailid',
            order: 'DESC'
        }]
    },
    getRecentRecruitmentsQuery: {
        table: view_HiringDetail,
        select: [{
            field: 'training_partner_name',
            alias: 'trainingpartnername'
        }, {
            field: 'DATE_FORMAT(modified_date,"%d %b %Y")',
            encloseField: false,
            alias: 'date'
        }, {
            field: 'DISTINCT(candidateid)',
            encloseField: false,
            aggregation: 'count',
            alias: 'count'
        }],
        filter: {
            and: [{
                field: 'industrypartnerid',
                operator: 'EQ',
                value: ''
            }, {
                field: 'internal_status',
                operator: 'EQ',
                value: 5
            }, {
                field: 'internal_log_status',
                operator: 'EQ',
                value: 1
            }]
        },
        groupby: [{
            field: 'trainingpartnerid'
        }],
        sortby: [{
            field: 'modified_date',
            order: 'DESC'
        }]
    }
};

module.exports = query
