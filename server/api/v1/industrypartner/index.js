var express = require('express');
var services = require('./industrypartner.service');
var middleware = require('../../../middleware');

var router = express.Router();
module.exports = router;

router.post('/industrypartner-signup',services.signupService);
router.post('/industrypartner-signin',services.signinService);
router.post('/industrypartner-changePassword',middleware.logger, services.changePasswordService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/industrypartner-updateProfile', middleware.logger, services.updateProfileService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/get-industrypartner', middleware.checkAccessToken,middleware.userRightsByAPI,middleware.logger, services.getIndustryPartnerService);
router.get('/get-industrypartner-detail/:id', middleware.checkAccessToken,middleware.userRightsByAPI,middleware.logger, services.getIndustryPartnerDetailService);
router.post('/remove-industrypartner/:id', middleware.checkAccessToken,middleware.userRightsByAPI,middleware.logger, services.removeIndustryPartnerService);
router.post('/addupdate-industrypartner', middleware.checkAccessToken,middleware.userRightsByAPI,middleware.logger, services.addUpdateIndustryPartnerService);
router.post('/addupdate-industrypartner-multiplerecords', middleware.checkAccessToken,middleware.userRightsByAPI,middleware.logger, services.updateMultipleRecordsService);

router.post('/addupdateCandidateDetail/:type', middleware.logger, services.addCandidateDetailService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/getHiringRequests/:type', middleware.logger, services.getHiringRequests);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/getHiringRequestDetailsForCandidate/:id', middleware.logger, services.getHiringRequestDetailsForCandidateService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/getIndustryPartnerSettings/:id', middleware.logger, services.getIndustryPartnerSettingsService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/getIndustryPartnerDashboardData', middleware.logger,services.getIndustryPartnerDashboardDataService);

//cron urls
router.post('/industryPartnerBulkUploadCheckStateCityService', services.industryPartnerBulkUploadCheckStateCityService);
router.post('/processIndustryPartnerCSVCronService', services.processIndustryPartnerCSVCronService);
router.post('/sendNewHiringRequestBulkEmail', services.sendNewHiringRequestBulkEmail); //temp route for bulk mail trigger to sent new hiring request emails