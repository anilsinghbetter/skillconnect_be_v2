var debug = require('debug')('server:api:v1:industrypartner:DAL');
var d3 = require("d3");
var DateLibrary = require('date-management');
var common = require('../common');
var constant = require('../constant');
var query = require('./industrypartner.query');
var dbDateFormat = constant.appConfig.DB_DATE_FORMAT;

/**
 * checkUserIsExistQuery
 * 
 * @param {String} id industry partner autoincrement id
 * @returns {object} 
 */

var checkEditUserIsExist = async function (id) {
  debug("Industry.DAL -> checkEditUserIsExist");
  var checkEditUserIsExist = common.cloneObject(query.checkUserIsExistQuery);
  checkEditUserIsExist.filter.and[0].value = id;
  return await common.executeQuery(checkEditUserIsExist);
};

/**
 * checkIndustryPartnerIsExistQuery
 * 
 * @param {String} ipid IndustryId
 * @returns {object} 
 */

var checkUserIsExist = async function (ipid, email) {
  debug("Industry.DAL -> checkUserIsExist");
  var checkIndustryPartnerIsExistQuery = common.cloneObject(query.checkIndustryPartnerIsExistQuery);
  checkIndustryPartnerIsExistQuery.filter.and[1].or[0].value = ipid;
  checkIndustryPartnerIsExistQuery.filter.and[1].or[1].value = email;
  return await common.executeQuery(checkIndustryPartnerIsExistQuery);
};

/**
 * create new user
 * 
 * @param  {Array of object}   fieldValue [the array object of the request body]
 * @return {object}
 */
var createIndustryUser = async function (fieldValue) {
  debug("Industry.DAL -> createIndustryUser");
  var createIndustryUserQuery = common.cloneObject(query.createIndustryUserQuery);
  createIndustryUserQuery.insert = fieldValue;
  return await common.executeQuery(createIndustryUserQuery);
}

/**
 * 
 * signin using ipid
 * 
 * @param  {string}   ipid      [industry partner id]
 * @return {object}      
 */
var signin = async function (ipid) {
  debug("Industry.DAL -> IndustryLogin ", ipid);
  var getIndustryInfoQuery = common.cloneObject(query.getIndustryInfoQuery);
  getIndustryInfoQuery.filter.and[0].value = ipid;
  return await common.executeQuery(getIndustryInfoQuery);
}

/**
 * expire access token
 * 
 * @param  {string}   userId
 * @param  {string}   deviceId
 * @param  {string}   host
 * @return {object}
 */
var exprieAccessToken = async function (userId, deviceId, host) {

  debug("user.DAL -> exprieAccessToken");
  var updateAccessTokenQuery = common.cloneObject(query.updateAccessTokenQuery);
  if (userId === undefined) {
    updateAccessTokenQuery.filter.or[1].value = deviceId;
    updateAccessTokenQuery.filter.or[2].value = host;
  } else {
    updateAccessTokenQuery.filter.or[0].and[0].value = userId;
    updateAccessTokenQuery.filter.or[0].and[1].value = deviceId;
    updateAccessTokenQuery.filter.or[0].and[2].value = host;
  }
  return await common.executeQuery(updateAccessTokenQuery);
};

/**
 * create access token
 *
 * @param  {string}   userId
 * @param  {string}   token
 * @param  {string}   expiryDateTime
 * @param  {string}   deviceId
 * @param  {string}   host
 * @return {object}
 */
var createAccessToken = async function (userId, token, expiryDateTime, deviceId, host) {
  debug("user.DAL -> accessTokenGenerate");
  var insertAccessTokenQuery = common.cloneObject(query.insertAccessTokenQuery);
  var dbExpiryDateTime = d3.timeFormat(dbDateFormat)(new Date(expiryDateTime));
  insertAccessTokenQuery.insert.fValue = [userId, token, dbExpiryDateTime, deviceId, host];
  return await common.executeQuery(insertAccessTokenQuery);
};


/**
 * check users login log(transction)
 *
 * @param  {string}   deviceId
 * @param  {string}   deviceType
 * @return {object}
 */
var checkUserTransaction = async function (deviceId, deviceType) {
  debug("user.DAL -> checkUserTransaction");
  var checkUserTransactionQuery = common.cloneObject(query.checkUserTransactionQuery);
  checkUserTransactionQuery.filter.and[0].value = deviceId;
  checkUserTransactionQuery.filter.and[1].value = deviceType;
  return await common.executeQuery(checkUserTransactionQuery);
};

/**
 * Update user transaction
 *
 * @param  {string}   deviceId
 * @param  {string}   deviceType
 * @param  {Array of object}   fieldValue
 * @return {object}
 */
var updateUserTransaction = function (deviceId, deviceType, fieldValue) {
  debug("user.DAL -> updateUserTransaction");
  var updateUserTransactionQuery = common.cloneObject(query.updateUserTransactionQuery);
  updateUserTransactionQuery.filter.and[0].value = deviceId;
  updateUserTransactionQuery.filter.and[1].value = deviceType;
  updateUserTransactionQuery.update = fieldValue;
  return common.executeQuery(updateUserTransactionQuery);
};

/**
 * create users login log(transaction) 
 *
 * @param  {string}   deviceId
 * @param  {string}   deviceType
 * @return {object}
 */
var createUserTransaction = function (deviceId, deviceType) {
  debug("user.DAL -> createUserTransaction");
  var insertUserTransactionQuery = common.cloneObject(query.insertUserTransactionQuery);
  insertUserTransactionQuery.insert.fValue = [deviceId, deviceType];
  return common.executeQuery(insertUserTransactionQuery);
};


/**
 * validate user
 * 
 * @param  {int}   userId  
 * @return {object}
 */
var validateUser = async function (userId) {
  debug("industrypartner.DAL -> validateUser");
  var validateUserQuery = common.cloneObject(query.validateUserQuery);
  validateUserQuery.filter.and[0].value = userId;
  return await common.executeQuery(validateUserQuery);
};

/**
 * Update user information by id
 * 
 * @param  {int}   userId     
 * @param  {Array of object}   fieldValue
 * @return {object}  
 */
var updateUserInfoById = async function (userId, fieldValue) {
  debug("industrypartner.DAL -> updateUserInfoById");
  var updateUserQuery = common.cloneObject(query.updateUserQuery);
  updateUserQuery.filter.and[0].value = userId;
  updateUserQuery.update = fieldValue;
  return await common.executeQuery(updateUserQuery);
};

/**
 * Get Industry Partners count
 * 
 * @param {object} searchObj
 * @return {object} 
 */
var getIndustryPartnersCount = async function (searchObj) {
  debug("industrypartner.DAL -> getIndustryPartnersCount");
  var getIndustryPartnersCountQuery = common.cloneObject(query.getIndustryPartnersCountQuery);
  var industrypartnerFilter = { and: [] };
  if (Object.keys(searchObj).length !== 0) {
    common.setSearchParams(searchObj, industrypartnerFilter);
    getIndustryPartnersCountQuery.filter = industrypartnerFilter;
  } else {
    delete getIndustryPartnersCountQuery.filter;
  }
  return await common.executeQuery(getIndustryPartnersCountQuery);
}

/**
 * Get Industry Partners
 * 
 * @param {Array of object} searchinfo [optional]
 */
var getIndustryPartners = async function (limit, searchObj) {
  debug("industrypartner.DAL -> getIndustryPartners");
  var getIndustryPartnersQuery = common.cloneObject(query.getIndustryPartnersQuery);
  var industrypartnerFilter = { and: [] };
  /* if (Object.keys(searchObj).length !== 0) {
    common.setSearchParams(searchObj, industrypartnerFilter);
    getIndustryPartnersQuery.filter = industrypartnerFilter;
  } else {
    delete getIndustryPartnersQuery.filter;
  } 
    getIndustryPartnersQuery.limit = limit; */
  return await common.executeQuery(getIndustryPartnersQuery);
}

/**
 * Get industry partner detail by id
 * 
 * @param {int} industrypartnerid 
 * @return {object}
 */
var getIndustryPartnerDetail = async function (industrypartnerid) {
  debug("Industry.DAL -> getIndustryPartnerDetail");
  var getIndustryPartnerDetailQuery = common.cloneObject(query.getIndustryPartnerDetailQuery);
  getIndustryPartnerDetailQuery.filter.and[0].value = industrypartnerid;
  return await common.executeQuery(getIndustryPartnerDetailQuery);
}

/* Get industry partner Info by id for reset pwd mail
* 
* @param {int} industrypartnerid 
* @return {object}
*/
var getIndustryPartnerInfo = async function (industrypartnerid) {
 debug("Industry.DAL -> getIndustryPartnerInfo");
 var getIndustryPartnerInfoQuery = common.cloneObject(query.getIndustryPartnerInfoQuery);
 getIndustryPartnerInfoQuery.filter.and[0].value = industrypartnerid;
 return await common.executeQuery(getIndustryPartnerInfoQuery);
}

/**
 * Remove industry partner(soft delete) by id
 * 
 * @param {int} industrypartnerid
 * @param {array of object} fieldValue 
 * @return {object}
 */
var removeIndustryPartner = async function (industrypartnerid, fieldValue) {
  debug("industrypartner.DAL -> removeIndustryPartner");
  var removeIndustryPartnerQuery = common.cloneObject(query.removeIndustryPartnerQuery);
  removeIndustryPartnerQuery.filter.and[0].value = industrypartnerid;
  removeIndustryPartnerQuery.update = fieldValue;
  return await common.executeQuery(removeIndustryPartnerQuery);
}

var updateIndustryPartnerBulkUpload = async function (id, fieldValue) {
  debug("other.DAL -> updateIndustryPartnerBulkUpload");
  var updateIndustryPartnerBulkUpload = common.cloneObject(query.updateIndustryPartnerBulkUploadQuery);
  updateIndustryPartnerBulkUpload.filter.and[0].value = id;
  updateIndustryPartnerBulkUpload.update = fieldValue;
  return await common.executeQuery(updateIndustryPartnerBulkUpload);
}

var getIndustryPartnerStateCityFromBulkUplod = async function () {
  debug("industrypartner.DAL -> getIndustryPartnerStateCityFromBulkUplod");
  var getIndustryPartnerStateCityFromBulkUplod = common.cloneObject(query.getIndustryPartnerStateCityFromBulkUplodQuery);
  getIndustryPartnerStateCityFromBulkUplod.limit = constant.appConfig.CRON_INDUSTRYPARTNER_BULK_RECORDS_LIMIT;
  return await common.executeQuery(getIndustryPartnerStateCityFromBulkUplod);
}

var getIndustryPartnerFromBulkUpload = async function () {
  debug("industrypartner.DAL -> getTrainingPartnerFromBulkUplod");
  var getIndustryPartnerFromBulkUpload = common.cloneObject(query.getIndustryPartnerFromBulkUploadQuery);
  getIndustryPartnerFromBulkUpload.limit = constant.appConfig.CRON_INDUSTRYPARTNER_BULK_RECORDS_LIMIT;
  return await common.executeQuery(getIndustryPartnerFromBulkUpload);
}

var createHiringRequestDetail = async function (fieldValue) {
  debug("Industry.DAL -> createHiringRequestDetail");
  var createHiringRequestDetail = common.cloneObject(query.createHiringRequestDetailQuery);
  createHiringRequestDetail.insert = fieldValue;
  return await common.executeQuery(createHiringRequestDetail);
}

var createHiringRequest = async function (fieldValue) {
  debug("Industry.DAL -> createHiringRequest");
  var createHiringRequest = common.cloneObject(query.createHiringRequestQuery);
  createHiringRequest.insert = fieldValue;
  return await common.executeQuery(createHiringRequest);
}

/**
 * Get Hiring Requests count
 * 
 * @param {object} searchObj
 * @return {object} 
 */
var getHiringRequestsCount = async function (searchObj) {
  debug("industrypartner.DAL -> getHiringRequestsCount");
  var getHiringRequestsCount = common.cloneObject(query.getHiringRequestsCountQuery);
  var HiringRequestsFilter = { and: [] };
  if (Object.keys(searchObj).length !== 0) {
    common.setSearchParams(searchObj, HiringRequestsFilter, 'view_HiringDetail');
    getHiringRequestsCount.filter = HiringRequestsFilter;
  } else {
    delete getHiringRequestsCount.filter;
  }
  HiringRequestsFilter.and.push({
    field: 'log_status',
    operator: 'EQ',
    value: 1
  });
  getHiringRequestsCount.filter = HiringRequestsFilter;
  return await common.executeQuery(getHiringRequestsCount);
}

/**
 * Get Hiring Requests
 * 
 * @param {*} limit 
 * @param {*} searchObj 
 * @param {*} requestType admin / web
 */
var getHiringRequests = async function (limit, searchObj, requestType) {
  debug("industrypartner.DAL -> getHiringRequests");
  var getHiringRequestsQuery = common.cloneObject(query.getHiringRequestsQuery);
  var HiringRequestsFilter = { and: [] };
  if (Object.keys(searchObj).length !== 0) {
    common.setSearchParams(searchObj, HiringRequestsFilter, 'view_HiringDetail');
    getHiringRequestsQuery.filter = HiringRequestsFilter;
  } else {
    delete getHiringRequestsQuery.filter;
  }
  HiringRequestsFilter.and.push({
    field: 'log_status',
    operator: 'EQ',
    value: 1
  });
  HiringRequestsFilter.and.push({
    field: 'internal_log_status',
    operator: 'EQ',
    value: 1
  });
  getHiringRequestsQuery.filter = HiringRequestsFilter;
  getHiringRequestsQuery.limit = limit;
  return await common.executeQuery(getHiringRequestsQuery);
}

var getHiringRequestDetailsForCandidate = async function(hiringrequestdetailid){
  debug("industrypartner.DAL -> getHiringRequestDetailsForCandidate");
  var getHiringRequestDetailsForCandidate = common.cloneObject(query.getHiringRequestDetailsForCandidateQuery);
  getHiringRequestDetailsForCandidate.filter.and[0].value = hiringrequestdetailid;
  return await common.executeQuery(getHiringRequestDetailsForCandidate);
}

var checkCandidateDetailIDIsExist = async function(candidatedetailid){
  debug("industrypartner.DAL -> checkCandidateDetailIDIsExist");
  var checkCandidateDetailIDIsExist = common.cloneObject(query.checkCandidateDetailIDIsExistQuery);
  checkCandidateDetailIDIsExist.filter.and[0].value = candidatedetailid;
  return await common.executeQuery(checkCandidateDetailIDIsExist);
}

var checkNewEmailToBeSent = async function(){
  debug("industrypartner.DAL -> checkNewEmailToBeSent");
  var checkNewEmailToBeSent = common.cloneObject(query.checkNewEmailToBeSentQuery);
  return await common.executeQuery(checkNewEmailToBeSent);
}

var updateCandidateDetailById = async function(candidatedetailid,fieldValueUpdate){
  debug("industrypartner.DAL -> updateCandidateDetailById");
  var updateCandidateDetailById = common.cloneObject(query.updateCandidateDetailByIdQuery);
  updateCandidateDetailById.filter.and[0].value = candidatedetailid;
  updateCandidateDetailById.update = fieldValueUpdate;
  return await common.executeQuery(updateCandidateDetailById);
}

var getTotalNumberOfJoinedCandidates = async function(industrypartnerid){
  debug("industrypartner.DAL -> getTotalNumberOfJoinedCandidates");
  var getTotalNumberOfJoinedCandidates = common.cloneObject(query.getTotalNumberOfJoinedCandidatesQuery);
  getTotalNumberOfJoinedCandidates.filter.and[0].value = industrypartnerid;
  return await common.executeQuery(getTotalNumberOfJoinedCandidates);
}

var getTotalNumberOfFutureRequests = async function(industrypartnerid){
  debug("industrypartner.DAL -> getTotalNumberOfFutureRequests");
  var getTotalNumberOfFutureRequests = common.cloneObject(query.getTotalNumberOfFutureRequestsQuery);
  getTotalNumberOfFutureRequests.filter.and[0].value = industrypartnerid;
  return await common.executeQuery(getTotalNumberOfFutureRequests);
}

var getRecentPendingRequests = async function(industrypartnerid){
  debug("industrypartner.DAL -> getRecentPendingRequests");
  var getRecentPendingRequests = common.cloneObject(query.getRecentPendingRequestsQuery);
  getRecentPendingRequests.filter.and[0].value = industrypartnerid;
  getRecentPendingRequests.limit = constant.appConfig.EMP_DASHBOARD_DATA_RECENT_HIRING_REQUEST_LIMIT;
  return await common.executeQuery(getRecentPendingRequests);
}

var getRecentRecruitments = async function(industrypartnerid){
  debug("industrypartner.DAL -> getRecentRecruitments");
  var getRecentRecruitments = common.cloneObject(query.getRecentRecruitmentsQuery);
  getRecentRecruitments.filter.and[0].value = industrypartnerid;
  getRecentRecruitments.limit = constant.appConfig.EMP_DASHBOARD_DATA_RECENT_RECRUITMENT_LIMIT;
  return await common.executeQuery(getRecentRecruitments);
}

module.exports = {
  checkEditUserIsExist: checkEditUserIsExist,
  checkUserIsExist: checkUserIsExist,
  createIndustryUser: createIndustryUser,
  signin: signin,
  exprieAccessToken: exprieAccessToken,
  createAccessToken: createAccessToken,
  checkUserTransaction: checkUserTransaction,
  updateUserTransaction: updateUserTransaction,
  createUserTransaction: createUserTransaction,
  validateUser: validateUser,
  updateUserInfoById: updateUserInfoById,
  getIndustryPartnersCount: getIndustryPartnersCount,
  getIndustryPartners: getIndustryPartners,
  getIndustryPartnerDetail: getIndustryPartnerDetail,
  getIndustryPartnerInfo: getIndustryPartnerInfo,
  removeIndustryPartner: removeIndustryPartner,
  getIndustryPartnerStateCityFromBulkUplod: getIndustryPartnerStateCityFromBulkUplod,
  updateIndustryPartnerBulkUpload: updateIndustryPartnerBulkUpload,
  getIndustryPartnerFromBulkUpload: getIndustryPartnerFromBulkUpload,
  createHiringRequestDetail : createHiringRequestDetail,
  createHiringRequest: createHiringRequest,
  getHiringRequestsCount: getHiringRequestsCount,
  getHiringRequests: getHiringRequests,
  getHiringRequestDetailsForCandidate : getHiringRequestDetailsForCandidate,
  checkCandidateDetailIDIsExist : checkCandidateDetailIDIsExist,
  checkNewEmailToBeSent : checkNewEmailToBeSent,
  updateCandidateDetailById : updateCandidateDetailById,
  getTotalNumberOfJoinedCandidates : getTotalNumberOfJoinedCandidates,
  getTotalNumberOfFutureRequests : getTotalNumberOfFutureRequests,
  getRecentPendingRequests : getRecentPendingRequests,
  getRecentRecruitments : getRecentRecruitments
}
