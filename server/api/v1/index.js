var express = require('express');
var middleware = require('../../middleware');
var router = express.Router();
module.exports = router;

router.use('/trainingpartner', require('./trainingpartner'));
router.use('/industrypartner', require('./industrypartner'));
router.use('/trainingcenter', require('./trainingcenter'));
router.use('/candidate', require('./candidate'));
router.use('/futurehiringrequest', require('./futurehiringrequest'));
router.use('/user', require('./user'));
router.use('/role',  require('./role'));//middleware.checkRequestHeader,
router.use('/other',  require('./other'));
router.use('/sectorjobrole',  require('./sectorjobrole'));
router.use('/sector',  require('./sector'));
router.use('/jobrole',  require('./jobrole'));