var debug = require('debug')('server:api:v1:Other:DAL');
var DateLibrary = require('date-management');
var common = require('../common');
var constant = require('../constant');
var query = require('./other.query');
var dbDateFormat = constant.appConfig.DB_DATE_FORMAT;
var d3 = require("d3");

/**
 * check if the tpid or email is valid 
 * 
 * @param  {string} ipid [industrypartner id]
 * @param  {string} email  [email address]
 * @return {object}
 */
var checkUserIdIsValid = async function (user, partnerid, email) {
  debug("other.DAL -> checkUserIdIsValid");
  switch (user) {
    case 'EMP':
      var checkUserIdIsValidQuery = common.cloneObject(query.checkIpidIsValidQuery);
      break;
    case 'TP':
      var checkUserIdIsValidQuery = common.cloneObject(query.checkTpidIsValidQuery);
      break;
  }
  checkUserIdIsValidQuery.filter.and[1].value = partnerid;
  checkUserIdIsValidQuery.filter.and[2].value = email;
  return await common.executeQuery(checkUserIdIsValidQuery);
}

/**
 * Created By: CBT
 * Updated By: CBT
 * [setTableActive description]
 * @param {[string]}   tableName   [description]
 * @param {[int]}   pk_idField  [description]
 * @param {int}  pk_idValue  [description]
 * @param {Function} cb          [description]
 */
var setTableActive = async function (tableName, isActive, table_PK_IDField, pk_idValue) {
  debug("other.DAL -> setTableActivee");
  var updateTableQuery = common.cloneObject(query.updateTableQuery);
  updateTableQuery.table = tableName;
  updateTableQuery.update = [];
  if (tableName != 'tbl_UserMaster') {
    updateTableQuery.update.push({
      field: "status",
      fValue: isActive
    });
  } else {
    updateTableQuery.update.push({
      field: "isActive",
      fValue: isActive
    }, {
        field: "modifiedDate",
        fValue: d3.timeFormat(dbDateFormat)(new Date())
      });
  }

  updateTableQuery.filter.field = table_PK_IDField;
  updateTableQuery.filter.value = pk_idValue;
  return await common.executeQuery(updateTableQuery);
};

var getTrainingPartnerUsers = async function () {
  debug("other.DAL -> getTrainingPartnerUsers");
  var getTrainingUsersQuery = common.cloneObject(query.getTrainingUsersQuery);
  getTrainingUsersQuery.limit = constant.appConfig.CRON_EMAILCRONSERVICE_RECORDS_LIMIT;
  return await common.executeQuery(getTrainingUsersQuery);
}
var getIndustryPartnerUsers = async function () {
  debug("other.DAL -> getIndustryPartnerUsers");
  var getIndustryUsersQuery = common.cloneObject(query.getIndustryUsersQuery);
  getIndustryUsersQuery.limit = constant.appConfig.CRON_EMAILCRONSERVICE_RECORDS_LIMIT;
  return await common.executeQuery(getIndustryUsersQuery);
}
var updateIsEmailSent = async function (tableName, isSent, table_field, table_value) {
  debug("other.DAL -> updateIsEmailSent");
  var updateTableQuery = common.cloneObject(query.updateTableQuery);
  updateTableQuery.table = tableName;
  updateTableQuery.update = [];
  updateTableQuery.update.push({
    field: "is_email_sent",
    fValue: isSent
  }, {
      field: "modified_date",
      fValue: d3.timeFormat(dbDateFormat)(new Date())
    });

  updateTableQuery.filter.field = table_field;
  updateTableQuery.filter.value = table_value;
  return await common.executeQuery(updateTableQuery);
};

/**
 * Created By: CBT
 * Updated By: CBT
 * [getModule description]
 * @param  {Function} cb         [description]
 * @return {[type]}              [description]
 */
var getModule = async function () {
  debug("other.DAL -> getModule");
  var getModuleQuery = common.cloneObject(query.getModuleQuery);
  return await common.executeQuery(getModuleQuery);
}

/**
 * check city and state record in view
 * 
 * @param {string} statename 
 * @param {string} cityname 
 * @returns {object}
 */
var checkCityStateView = async function (statename, cityname) {
  debug("other.DAL -> checkCityStateView");
  var checkCityStateViewQuery = common.cloneObject(query.checkCityStateViewQuery);
  checkCityStateViewQuery.filter.and[0].value = statename;
  checkCityStateViewQuery.filter.and[1].value = cityname;
  return await common.executeQuery(checkCityStateViewQuery);
}

var checkSectorJobRoleView = async function (statename, cityname) {
  debug("other.DAL -> checkSectorJobRoleView");
  var checkSectorJobRoleViewQuery = common.cloneObject(query.checkSectorJobRoleViewQuery);
  checkSectorJobRoleViewQuery.filter.and[0].value = statename;
  checkSectorJobRoleViewQuery.filter.and[1].value = cityname;
  return await common.executeQuery(checkSectorJobRoleViewQuery);

}

var checkSchemeExistIntoMaster = async function (SchemeType) {
  debug("other.DAL -> checkSchemeExistIntoMaster");
  var checkSchemeExistIntoMasterQuery = common.cloneObject(query.checkSchemeExistIntoMasterQuery);
  checkSchemeExistIntoMasterQuery.filter.and[0].value = SchemeType;
  return await common.executeQuery(checkSchemeExistIntoMasterQuery);
}


var checkSector = async function (sectors) {
  debug("other.DAL -> checkSectorView");
  console.lo(sectors);
  return true;
  // var checkSectorQuery = common.cloneObject(query.checkSectorQuery);
  // // Here I am ......
  // checkSectorQuery.filter.and[0].value = statename;
  // checkSectorQuery.filter.and[1].value = cityname;
  // return await common.executeQuery(checkSectorQuery);g
}

var checkState = async function (statename) {
  debug("other.DAL -> checkState");
  var checkStateQuery = common.cloneObject(query.checkStateQuery);
  checkStateQuery.filter.and[0].value = statename;
  return await common.executeQuery(checkStateQuery);
}

var checkSectorExistIntoMaster = async function (sectorName) {
  debug("other.DAL -> checkSectorExistIntoMaster");
  var checkSectorExistIntoMasterQuery = common.cloneObject(query.checkSectorExistIntoMasterQuery);
  checkSectorExistIntoMasterQuery.filter.and[0].value = sectorName;
  return await common.executeQuery(checkSectorExistIntoMasterQuery);
}

var checkJobRoleExistIntoMaster = async function (sectorId, jobroleName, jobroleCode) {
  debug("other.DAL -> checkJobRoleExistIntoMaster");
  var checkJobRoleExistIntoMasterQuery = common.cloneObject(query.checkJobRoleExistIntoMasterQuery);
  checkJobRoleExistIntoMasterQuery.filter.and[0].value = sectorId;
  checkJobRoleExistIntoMasterQuery.filter.and[1].value = jobroleName;
  checkJobRoleExistIntoMasterQuery.filter.and[2].value = jobroleCode;
  return await common.executeQuery(checkJobRoleExistIntoMasterQuery);
}

var checkJobRoleNameExistIntoMaster = async function (sectorId, jobroleName) {
  debug("other.DAL -> checkJobRoleNameExistIntoMaster");
  var checkJobRoleNameExistIntoMaster = common.cloneObject(query.checkJobRoleNameExistIntoMasterQuery);
  checkJobRoleNameExistIntoMaster.filter.and[0].value = sectorId;
  checkJobRoleNameExistIntoMaster.filter.and[1].value = jobroleName;
  return await common.executeQuery(checkJobRoleNameExistIntoMaster);
}

var insertPasswordToken = async function (fieldValueUpdate) {
  debug("other.DAL -> insertPasswordToken");
  var insertPasswordTokenQuery = common.cloneObject(query.insertPasswordTokenQuery);
  insertPasswordTokenQuery.insert = fieldValueUpdate;
  return await common.executeQuery(insertPasswordTokenQuery);
}

var getUserPasswordToken = async function (token) {
  debug("other.DAL -> getUserPasswordToken");
  var getUserPasswordTokenQuery = common.cloneObject(query.getUserPasswordTokenQuery);
  getUserPasswordTokenQuery.filter.and[0].value = token;
  return await common.executeQuery(getUserPasswordTokenQuery);
}

var updatePasswordToken = async function (passwordtokenid, fieldValueUpdate) {
  debug("other.DAL -> updatePasswordToken");
  var updatePasswordTokenQuery = common.cloneObject(query.updatePasswordTokenQuery);
  updatePasswordTokenQuery.filter.and[0].value = passwordtokenid;
  updatePasswordTokenQuery.update = fieldValueUpdate;
  return await common.executeQuery(updatePasswordTokenQuery);
}

var checkRecordExist = async function (tableName, table_PK_IDField, pk_idValue) {
  debug("other.DAL -> checkRecordExist");
  var checkRecordExist = common.cloneObject(query.recordExistInTableQuery);
  checkRecordExist.table = tableName;
  checkRecordExist.select = [];
  checkRecordExist.select.push({
    field: table_PK_IDField,
  });
  checkRecordExist.filter.field = table_PK_IDField;
  checkRecordExist.filter.value = pk_idValue;
  return await common.executeQuery(checkRecordExist);
}

var removeRecord = async function (tableName, table_PK_IDField, pk_idValue, delete_type) {
  debug("other.DAL -> removeRecord");
  if (delete_type == 0) {
    var updateTableQuery = common.cloneObject(query.updateTableQuery);
    updateTableQuery.table = tableName;
    updateTableQuery.update = [];
    updateTableQuery.update.push({
      field: "is_deleted",
      fValue: 1
    });
  } else {
    var updateTableQuery = common.cloneObject(query.removeRecordQuery);
    updateTableQuery.table = tableName;
    updateTableQuery.delete = [];
  }
  updateTableQuery.filter.field = table_PK_IDField;
  updateTableQuery.filter.value = pk_idValue;
  return await common.executeQuery(updateTableQuery);
}

var checkScheme = async function (schemename) {
  debug("other.DAL -> checkScheme");
  var checkScheme = common.cloneObject(query.checkSchemeQuery);
  checkScheme.filter.and[0].value = schemename;
  return await common.executeQuery(checkScheme);
}


var getRecentActivities = async function (userid, usertype) {
  debug("other.DAL -> getRecentActivities");
  var getRecentActivities = common.cloneObject(query.getRecentActivitiesQuery);
  getRecentActivities.filter.and[0].value = userid;
  getRecentActivities.filter.and[1].value = usertype;
  return await common.executeQuery(getRecentActivities);
}

var getHiringRequestDetailForMail = async function (TP_ID, IP_ID) {
  debug("other.DAL -> getHiringRequestDetailForMail");
  var getHiringRequestDetailForMailQuery = common.cloneObject(query.getHiringRequestDetailForMailQuery);
  getHiringRequestDetailForMailQuery.filter.and[0].value = TP_ID;
  getHiringRequestDetailForMailQuery.filter.and[1].value = IP_ID;
  return await common.executeQuery(getHiringRequestDetailForMailQuery);
}

var getAllDetailsForHiringrequestStatusEmail = async function (hiringRequestId) {
  debug("other.DAL -> getAllDetailsForHiringrequestStatusEmail");
  var getAllDetailsForHiringrequestStatusEmailQuery = common.cloneObject(query.getAllDetailsForHiringrequestStatusEmailQuery);
  getAllDetailsForHiringrequestStatusEmailQuery.filter.and[0].value = hiringRequestId;
  return await common.executeQuery(getAllDetailsForHiringrequestStatusEmailQuery);
}



var getSectorJobroleMapping = async function(trainingpartnerid){
  debug("other.DAL -> getSectorJobroleMapping");
  // console.log(trainingpartnerid,"TP ID IN DAL");
  var getBulkUploadErrorStatusQuery = common.cloneObject(query.getBulkUploadErrorStatusQuery);
  getBulkUploadErrorStatusQuery.filter.and[0].value = trainingpartnerid;
  return await common.executeQuery(getBulkUploadErrorStatusQuery);
}

var getBulkUploadErrorStatusForFile = async function(trainingpartnerid,meta_id){
  debug("other.DAL -> getBulkUploadErrorStatusForFile");
  // console.log(trainingpartnerid,"TP ID IN DAL For File Creation");
  var getBulkUploadErrorStatusForFileQuery = common.cloneObject(query.getBulkUploadErrorStatusForFileQuery);
  getBulkUploadErrorStatusForFileQuery.filter.and[0].value = trainingpartnerid;
  getBulkUploadErrorStatusForFileQuery.filter.and[1].value = meta_id;
  return await common.executeQuery(getBulkUploadErrorStatusForFileQuery);
}

var getTpInfoForBulkUploadCompletionMail = async function(){
  debug("other.DAL -> getTpInfoForBulkUploadCompletionMail");
  var getTpInfoForBulkUploadCompletionMailQuery = common.cloneObject(query.getTpInfoForBulkUploadCompletionMailQuery);
  getTpInfoForBulkUploadCompletionMailQuery.limit = constant.appConfig.CRON_BULKUPLOADCOMPLETION_LIMIT;
  return await common.executeQuery(getTpInfoForBulkUploadCompletionMailQuery);
}

var updateMailFlagForMetadata = async function(id, meta_id,user_type){
  debug("other.DAL -> updateMailFlagForMetadata");
  if(user_type == 'TP'){
    var updateMailFlagForMetadataQuery = common.cloneObject(query.updateMailFlagForMetadataQuery);
  } else if(user_type == 'EMP'){
    var updateMailFlagForMetadataQuery = common.cloneObject(query.updateMailFlagForMetadataQueryEMP);
  } else if(user_type == 'Admin'){
    var updateMailFlagForMetadataQuery = common.cloneObject(query.updateMailFlagForMetadataQueryAdmin);
  }
  updateMailFlagForMetadataQuery.filter.and[0].value = id;
  updateMailFlagForMetadataQuery.filter.and[1].value = meta_id;
  return await common.executeQuery(updateMailFlagForMetadataQuery);
}

var updateContactMessage = async function (fieldValue) {
  debug("other.DAL -> updateContactMessage");
  var updateContactMessageQuery = common.cloneObject(query.updateContactMessageQuery);
  updateContactMessageQuery.insert = fieldValue;
  return await common.executeQuery(updateContactMessageQuery);
}

var updateFlagForEmail = async function (candidateDetailID) {
  debug("other.DAL -> updateFlagForEmail");
  var updateFlagForEmail = common.cloneObject(query.updateFlagForEmailQuery);
  updateFlagForEmail.filter.and[0].value = candidateDetailID;
  return await common.executeQuery(updateFlagForEmail);
}

var updateFlagForBulkEmail = async function (trainingPartnerID, industryPartnerID) {
  debug("other.DAL -> updateFlagForBulkEmail");
  var updateFlagForBulkEmail = common.cloneObject(query.updateFlagForBulkEmailQuery);
  updateFlagForBulkEmail.filter.and[0].value = trainingPartnerID;
  updateFlagForBulkEmail.filter.and[1].value = industryPartnerID;
  return await common.executeQuery(updateFlagForBulkEmail);
}

var updateContactEnquiryService = async function (row_id, email_address, reply) {
  debug("other.DAL -> updateContactEnquiryService");
  var updateContactEnquiryService = common.cloneObject(query.updateContactEnquiryServiceQuery);
  updateContactEnquiryService.update[0].fValue = reply;
  updateContactEnquiryService.filter.and[0].value = row_id;
  updateContactEnquiryService.filter.and[1].value = email_address;
  return await common.executeQuery(updateContactEnquiryService);
}

var getContactService = async function (fieldValue) {
  debug("other.DAL -> getContactService");
  var getContactServiceQuery = common.cloneObject(query.getContactServiceQuery);
  //getContactServiceQuery.insert = fieldValue;
  return await common.executeQuery(getContactServiceQuery);
}

var getContactDetailService = async function (partner_id, partner_type, row_id) {
  debug("other.DAL -> getContactDetailService");
  var getContactDetailServiceQuery;
  if (partner_type === 'TP') {
    getContactDetailServiceQuery = common.cloneObject(query.getContactDetailTPServiceQuery);
    getContactDetailServiceQuery.filter.and[0].value = row_id;
  } else {
    getContactDetailServiceQuery = common.cloneObject(query.getContactDetailIPServiceQuery);
    getContactDetailServiceQuery.filter.and[0].value = row_id;
  }
  return await common.executeQuery(getContactDetailServiceQuery);
}

var getDetailForContactRowService = async function (row_id) {
  debug("other.DAL -> getDetailForContactRowService");
  var getDetailForContactRowService = common.cloneObject(query.getDetailForContactRowServiceQuery);
  getDetailForContactRowService.filter.and[0].value = row_id;
  return await common.executeQuery(getDetailForContactRowService);
}

var getPageContent = async function (pagetype) {
  debug("other.DAL -> getPageContent");
  var getPageContent = common.cloneObject(query.getPageContentQuery);
  getPageContent.filter.and[0].value = pagetype;
  return await common.executeQuery(getPageContent);
}

var getBulkUploadErrorStatus = async function(id,usertype){
  debug("other.DAL -> getBulkUploadErrorStatus");
  if(usertype === 'TP'){
    var getBulkUploadErrorStatusQuery = common.cloneObject(query.getBulkUploadErrorStatusQuery);
  } else if(usertype === 'EMP') {
    var getBulkUploadErrorStatusQuery = common.cloneObject(query.getBulkUploadErrorStatusQueryEMP);
  }
  getBulkUploadErrorStatusQuery.filter.and[0].value = id;
  return await common.executeQuery(getBulkUploadErrorStatusQuery);
}

var getTotalNumberOfFutureRequests = async function () {
  debug("other.DAL -> getTotalNumberOfFutureRequests");
  var getTotalNumberOfFutureRequests = common.cloneObject(query.getTotalNumberOfFutureRequestsQuery);
  return await common.executeQuery(getTotalNumberOfFutureRequests);
}

var getTotalNumberOfCandidates = async function () {
  debug("other.DAL -> getTotalNumberOfCandidates");
  var getTotalNumberOfCandidates = common.cloneObject(query.getTotalNumberOfCandidatesQuery);
  return await common.executeQuery(getTotalNumberOfCandidates);
}

var getTotalNumberOfTrainingPartners = async function () {
  debug("other.DAL -> getTotalNumberOfTrainingPartners");
  var getTotalNumberOfTrainingPartners = common.cloneObject(query.getTotalNumberOfTrainingPartnersQuery);
  return await common.executeQuery(getTotalNumberOfTrainingPartners);
}

var getTotalNumberOfIndustryPartners = async function () {
  debug("other.DAL -> getTotalNumberOfIndustryPartners");
  var getTotalNumberOfIndustryPartners = common.cloneObject(query.getTotalNumberOfIndustryPartnersQuery);
  return await common.executeQuery(getTotalNumberOfIndustryPartners);
}

/**
 * Get Hiring Requests count
 * 
 * @param {object} searchObj
 * @return {object} 
 */
var getHiringRequestsCount = async function (searchObj) {
  debug("other.DAL -> getHiringRequestsCount");
  var getHiringRequestsCount = common.cloneObject(query.getHiringRequestsCountQuery);
  var HiringRequestsFilter = { and: [] };
  if (Object.keys(searchObj).length !== 0) {
    common.setSearchParams(searchObj, HiringRequestsFilter, 'view_HiringDetail');
    getHiringRequestsCount.filter = HiringRequestsFilter;
  } else {
    delete getHiringRequestsCount.filter;
  }
  HiringRequestsFilter.and.push({
    field: 'log_status',
    operator: 'EQ',
    value: 1
  });
  getHiringRequestsCount.filter = HiringRequestsFilter;
  return await common.executeQuery(getHiringRequestsCount);
}

var getBulkUploadMetaDetails  = async function(){
  debug("other.DAL -> getBulkUploadMetaDetails");
  // console.log(trainingpartnerid,"TP ID IN DAL");
  var getBulkUploadMetaDetailsQuery = common.cloneObject(query.getBulkUploadMetaDetailsQuery);
  // getBulkUploadMetaDetailsQuery.filter.and[0].value = trainingpartnerid;
  getBulkUploadMetaDetailsQuery.limit = constant.appConfig.CRON_BULKUPLOAD_FILE_CREATION_LIMIT;
  return await common.executeQuery(getBulkUploadMetaDetailsQuery);
}

var updatefileCreatedFlag = async function(meta_id){
  debug("other.DAL -> updatefileCreatedFlag");
  // console.log(trainingpartnerid,"TP ID IN DAL For File Creation");
  var updatefileCreatedFlagQuery = common.cloneObject(query.updatefileCreatedFlagQuery);
  updatefileCreatedFlagQuery.filter.and[0].value = meta_id;
  return await common.executeQuery(updatefileCreatedFlagQuery);
}

var getEmpInfoForBulkUploadCompletionMail = async function(){
  debug("other.DAL -> getEmpInfoForBulkUploadCompletionMail");
  var getEmpInfoForBulkUploadCompletionMailQuery = common.cloneObject(query.getEmpInfoForBulkUploadCompletionMailQuery);
  getEmpInfoForBulkUploadCompletionMailQuery.limit = constant.appConfig.CRON_BULKUPLOADCOMPLETION_LIMIT;
  return await common.executeQuery(getEmpInfoForBulkUploadCompletionMailQuery);
}


var getAdminInfoForBulkUploadCompletionMail = async function(){
  debug("other.DAL -> getAdminInfoForBulkUploadCompletionMail");
  var getAdminInfoForBulkUploadCompletionMail = common.cloneObject(query.getAdminInfoForBulkUploadCompletionMailQuery);
  getAdminInfoForBulkUploadCompletionMail.limit = constant.appConfig.CRON_BULKUPLOADCOMPLETION_LIMIT;
  return await common.executeQuery(getAdminInfoForBulkUploadCompletionMail);
}
var getApiKey  = async function(key_type){
  debug("other.DAL -> getApiKey");
  var getApiKeyQuery = common.cloneObject(query.getApiKeyQuery);
  getApiKeyQuery.filter.and[0].value = key_type;
  return await common.executeQuery(getApiKeyQuery);
}

var getErrorFileList = async function(){
  debug("other.DAL -> getErrorFileList");
  console.log("IN DAL");
  var getErrorFileListQuery = common.cloneObject(query.getErrorFileListQuery);
  getErrorFileListQuery.filter.and[0].value = 1; //static admin id
  // getErrorFileListQuery.filter.and[0].value = 1; //static admin id
  return await common.executeQuery(getErrorFileListQuery);
}

var getErrorFileNameList = async function(){
  debug("other.DAL -> getErrorFileNameList");
  // console.log("IN DAL");
  var getErrorFileNameListQuery = common.cloneObject(query.getErrorFileNameListQuery);
  getErrorFileNameListQuery.filter.and[0].value = 1; //static admin id
  getErrorFileNameListQuery.limit = 2; //limit to be put into constant
  return await common.executeQuery(getErrorFileNameListQuery);
}

var getErrorDataTPAdmin = async function(meta_id,module_type){
  debug("other.DAL -> getErrorDataTPAdmin");
  var getErrorDataTPAdminQuery = common.cloneObject(query.getErrorDataTPAdminQuery);
  getErrorDataTPAdminQuery.filter.and[0].value = meta_id; 
  getErrorDataTPAdminQuery.filter.and[1].value = module_type;
  return await common.executeQuery(getErrorDataTPAdminQuery);
} 

var getErrorDataEMPAdmin = async function(meta_id,module_type){
  debug("other.DAL -> getErrorDataEMPAdmin");
  var getErrorDataEMPAdminQuery = common.cloneObject(query.getErrorDataEMPAdminQuery);
  getErrorDataEMPAdminQuery.filter.and[0].value = meta_id; 
  getErrorDataEMPAdminQuery.filter.and[1].value = module_type;
  return await common.executeQuery(getErrorDataEMPAdminQuery);
}

module.exports = {
  setTableActive: setTableActive,
  getModule: getModule,
  updateIsEmailSent: updateIsEmailSent,
  checkCityStateView: checkCityStateView,
  checkSector,
  checkState: checkState,
  getTrainingPartnerUsers: getTrainingPartnerUsers,
  getIndustryPartnerUsers: getIndustryPartnerUsers,
  checkUserIdIsValid: checkUserIdIsValid,
  checkSectorExistIntoMaster: checkSectorExistIntoMaster,
  checkJobRoleExistIntoMaster: checkJobRoleExistIntoMaster,
  checkJobRoleNameExistIntoMaster: checkJobRoleNameExistIntoMaster,
  insertPasswordToken: insertPasswordToken,
  getUserPasswordToken: getUserPasswordToken,
  updatePasswordToken: updatePasswordToken,
  checkRecordExist: checkRecordExist,
  removeRecord: removeRecord,
  checkScheme: checkScheme,
  checkSectorJobRoleView: checkSectorJobRoleView,
  checkSchemeExistIntoMaster: checkSchemeExistIntoMaster,
  getRecentActivities: getRecentActivities,
  updateContactMessage: updateContactMessage,
  getContactService: getContactService,
  getContactDetailService: getContactDetailService,
  getPageContent: getPageContent,
  updateFlagForEmail: updateFlagForEmail,
  updateFlagForBulkEmail: updateFlagForBulkEmail,
  updateContactEnquiryService: updateContactEnquiryService,
  getDetailForContactRowService: getDetailForContactRowService,
  getHiringRequestDetailForMail: getHiringRequestDetailForMail,
  getAllDetailsForHiringrequestStatusEmail: getAllDetailsForHiringrequestStatusEmail,
  getBulkUploadErrorStatus: getBulkUploadErrorStatus,
  getSectorJobroleMapping: getSectorJobroleMapping,
  getBulkUploadErrorStatusForFile: getBulkUploadErrorStatusForFile,
  getTpInfoForBulkUploadCompletionMail: getTpInfoForBulkUploadCompletionMail,
  getTotalNumberOfFutureRequests: getTotalNumberOfFutureRequests,
  getTotalNumberOfCandidates: getTotalNumberOfCandidates,
  getTotalNumberOfTrainingPartners : getTotalNumberOfTrainingPartners,
  getTotalNumberOfIndustryPartners : getTotalNumberOfIndustryPartners,
  getHiringRequestsCount: getHiringRequestsCount,
  getBulkUploadMetaDetails: getBulkUploadMetaDetails,
  updateMailFlagForMetadata: updateMailFlagForMetadata,
  updatefileCreatedFlag : updatefileCreatedFlag,
  getEmpInfoForBulkUploadCompletionMail: getEmpInfoForBulkUploadCompletionMail,
  getAdminInfoForBulkUploadCompletionMail : getAdminInfoForBulkUploadCompletionMail,
  getApiKey: getApiKey,
  getErrorFileList: getErrorFileList,
  getErrorFileNameList: getErrorFileNameList,
  getErrorDataTPAdmin: getErrorDataTPAdmin,
  getErrorDataEMPAdmin: getErrorDataEMPAdmin
};
