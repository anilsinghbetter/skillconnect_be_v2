var debug = require('debug')('server:api:v1:Other:service');
var DateLibrary = require('date-management');
var common = require('../common');
var constant = require('../constant');
var otherDAL = require('./other.DAL');
var sectorDAL = require('../sector/sector.DAL');
var trainingPartnerDAL = require('../trainingpartner/trainingpartner.DAL');
var trainingCenterDAL = require('../trainingcenter/trainingcenter.DAL');
var industryPartnerDAL = require('../industrypartner/industrypartner.DAL');
var dbDateFormat = constant.appConfig.DB_DATE_FORMAT;
var forgotPasswordDateFormat = constant.appConfig.FP_DATE_FORMAT;
var d3 = require("d3");
var async = require("async");
var connection = require('../../../helper/connection');
var fileExtension = require('file-extension');
var randomstring = require("randomstring");
var sizeOf = require('image-size');
var fs = require('fs');
var mkdirp = require('mkdirp');
var bcrypt = require('bcrypt');
var randomize = require('randomatic');

/**
 * active inactive
 * 
 * @param  {object}   request
 * @param  {object}   response
 * @return {object}    
 */
var tableActiveService = async function (request, response) {
  debug("other.service -> tableActiveService");
  if (request.body.tbl_code == undefined || request.body.tbl_code === "" || request.body.is_active == undefined || request.body.is_active === "" || request.body.id == undefined || request.body.id === "") {
    return common.sendResponse(response, constant.otherMessage.ERR_INVALID_TABLE_ACTIVE_REQUEST, false);
  }

  var tableCode = request.body.tbl_code;
  var tableDetail = constant.tableCodeMapping.filter(function (d) {
    if (d.code == tableCode) {
      return d;
    }
  }).map(function (d) {
    return {
      tableName: d.tableName,
      pk_idField: d.pk_idField
    }
  })[0];

  var isActive = request.body.is_active;
  var tableName = tableDetail.tableName;
  var table_PK_IDField = tableDetail.pk_idField;
  var id = request.body.id;
  var result = await otherDAL.setTableActive(tableName, isActive, table_PK_IDField, id);
  if (result.status === false) {
    return common.sendResponse(response, constant.otherMessage.ERR_INVALID_TABLE_ACTIVE_REQUEST, false);
  } else {
    return common.sendResponse(response, constant.otherMessage.ACTIVE_SUCCESS, true);
  }
};


/**
 * Delete records [It performs both soft and hard delete just pass the delete_type param from request body]
 * delete_type 1= hard delete,0 = soft delete
 * 
 * While Deleting record(s) from any particular records please see the Dependencies
 * 
 * TrainingPartner delete ->  tbl_TrainingCenters,tbl_Candidates,tbl_CandidateDetail,tbl_HiringRequestsDetail,tbl_TrainingPartners
 * IndustryPartner delete ->  tbl_CandidateDetail,tbl_HiringRequestsDetail,tbl_FutureHiringRequests,tbl_IndustryPartners
 * TrainingCenter delete ->  tbl_Candidates,tbl_CandidateDetail,tbl_HiringRequestsDetail,tbl_TrainingCenters
 * Candidate delete ->  tbl_CandidateDetail,tbl_HiringRequestsDetail,tbl_Candidates
 * 
 * 
 * @param  {object}   request
 * @param  {object}   response
 * @return {object}    
 */
var recordsDeleteService = async function (request, response) {
  debug("other.service -> recordsDeleteService");
  //console.log('#delete', request.body);
  if (request.params.type == 'admin') {
    if (request.body.tbl_code == undefined || request.body.tbl_code === "" || request.body.id == undefined || request.body.id === "" || request.body.delete_type == undefined || request.body.delete_type === "") {
      return common.sendResponse(response, constant.otherMessage.ERR_INVALID_RECORD_DELETE_REQUEST, false);
    }
  } else {
    if (request.body.tablename == undefined || request.body.tablename === "" || request.body.id == undefined || request.body.id === "") {
      return common.sendResponse(response, constant.otherMessage.ERR_INVALID_RECORD_DELETE_REQUEST, false);
    }
  }

  if (request.params.type == 'admin') {
    var tableCode = request.body.tbl_code;
    var delete_type = request.body.delete_type;
    var tableDetail = constant.tableCodeMapping.filter(function (d) {
      if (d.code == tableCode) {
        return d;
      }
    }).map(function (d) {
      return {
        tableName: d.tableName,
        pk_idField: d.pk_idField
      }
    })[0];
  } else {
    var table = request.body.tablename;
    var delete_type = 0;
    var tableDetail = constant.tableCodeMapping.filter(function (d) {
      if (d.table == table) {
        return d;
      }
    }).map(function (d) {
      return {
        tableName: d.tableName,
        pk_idField: d.pk_idField
      }
    })[0];
  }

  var tableName = tableDetail.tableName;
  var table_PK_IDField = tableDetail.pk_idField;
  var id = request.body.id;

  var res_record_exist = await otherDAL.checkRecordExist(tableName, table_PK_IDField, id);
  if (res_record_exist.status === false) {
    return common.sendResponse(response, constant.userMessages.ERR_RECORD_NOT_EXIST, false);
  } else if (res_record_exist.status == true && res_record_exist.content !== undefined && res_record_exist.content.length > 0) {

    //Child Table Delete Entries
    if (tableName == 'tbl_Candidates') {
      await removeRecordsFromCandidatesChildTables(id, 'fk_candidateid', 1);
    } else if (tableName == 'tbl_TrainingCenters') {
      await removeRecordsFromTrainingCentersChildTables(id);
    } else if (tableName == 'tbl_IndustryPartners') {
      await removeRecordsFromCandidatesChildTables(id, 'fk_industrypartnerid', 1);
      var query = 'SELECT `pk_futurerequestID` FROM tbl_FutureHiringRequests WHERE `fk_industrypartnerid` = ' + id;
      var recordExist = await connection.executeRawQuery(query);
      if (recordExist.status == true && recordExist.content !== undefined && recordExist.content.length > 0) {
        for (let i = 0; i < recordExist.content.length; i++) {
          var result = await otherDAL.removeRecord('tbl_FutureHiringRequests', 'pk_futurerequestID', recordExist.content[i].pk_futurerequestID, 0);
        }
      }
    } else if (tableName == 'tbl_TrainingPartners') {
      await removeRecordsFromTrainingPartnersChildTables(id);
    }

    var result = await otherDAL.removeRecord(tableName, table_PK_IDField, id, delete_type);
    if (result.status === false) {
      return common.sendResponse(response, constant.otherMessage.ERR_INVALID_RECORD_DELETE_REQUEST, false);
    } else {
      if (request.params.type != 'admin') {
        return common.sendResponse(response, constant.otherMessage.DELETE_SUCCESS, true, request.body.paginationValue);
      } else {
        return common.sendResponse(response, constant.otherMessage.DELETE_SUCCESS, true);
      }
    }
  } else {
    return common.sendResponse(response, constant.userMessages.ERR_RECORD_NOT_EXIST, false);
  }
};

async function removeRecordsFromCandidatesChildTables(id, whereConditionOn, delete_type) {
  var query = 'SELECT `pk_hiringrequestdetailid`,`fk_candidatedetailid` FROM tbl_HiringRequestsDetail WHERE ' + whereConditionOn + ' = ' + id;
  var recordExist = await connection.executeRawQuery(query);
  if (recordExist.status == true && recordExist.content !== undefined && recordExist.content.length > 0) {
    for (let i = 0; i < recordExist.content.length; i++) {
      var result = await otherDAL.removeRecord('tbl_HiringRequestsDetail', 'pk_hiringrequestdetailid', recordExist.content[i].pk_hiringrequestdetailid, 1);
      if (result.status == true) {
        var result = await otherDAL.removeRecord('tbl_CandidateDetail', 'pk_candidatedetailid', recordExist.content[i].fk_candidatedetailid, 1);
      }
    }
  }
}
async function removeRecordsFromTrainingCentersChildTables(id) {
  var query = 'SELECT `pk_candidateid` FROM tbl_Candidates WHERE `fk_trainingcenterid` = ' + id;
  var recordExist = await connection.executeRawQuery(query);
  if (recordExist.status == true && recordExist.content !== undefined && recordExist.content.length > 0) {
    for (let i = 0; i < recordExist.content.length; i++) {
      //Find id from hiring request detail id
      await removeRecordsFromCandidatesChildTables(recordExist.content[i].pk_candidateid, 'fk_candidateid', 1);
      var result = await otherDAL.removeRecord('tbl_Candidates', 'pk_candidateid', recordExist.content[i].pk_candidateid, 0);
    }
  }
}
async function removeRecordsFromTrainingPartnersChildTables(id) {
  var query = 'SELECT `pk_trainingcenterid` FROM tbl_TrainingCenters WHERE `fk_trainingpartnerid` =' + id;
  var recordExist = await connection.executeRawQuery(query);
  if (recordExist.status == true && recordExist.content !== undefined && recordExist.content.length > 0) {
    for (let i = 0; i < recordExist.content.length; i++) {
      var result = await otherDAL.removeRecord('tbl_TrainingCenters', 'pk_trainingcenterid', recordExist.content[i].pk_trainingcenterid, 0);
    }
  }

  var query = 'SELECT `pk_candidateid` FROM tbl_Candidates WHERE `fk_trainingpartnerid` = ' + id;
  var recordExist = await connection.executeRawQuery(query);
  if (recordExist.status == true && recordExist.content !== undefined && recordExist.content.length > 0) {
    for (let i = 0; i < recordExist.content.length; i++) {
      //Find id from hiring request detail id
      await removeRecordsFromCandidatesChildTables(recordExist.content[i].pk_candidateid, 'fk_candidateid', 1);
      var result = await otherDAL.removeRecord('tbl_Candidates', 'pk_candidateid', recordExist.content[i].pk_candidateid, 0);
    }
  }
}
/**
 * 
 * getModuleService
 * @param  {Object}   request
 * @param  {Function} cb
 */
var getModuleService = async function (request, response) {
  debug("other.service -> getModuleService");
  var result = await otherDAL.getModule();
  if (result.status == false) {
    return common.sendResponse(response, result.error, false);
  }
  return common.sendResponse(response, result.content, true);
}

/**
 * upload csv service
 * 
 * @param  {object}   request
 * @return  {object}  response
 */
var uploadCSVService = async function (request, response) {
  debug("other.service -> uploadCSVService");
  var parse = require('csv-parse');
  //console.log(request.body[0].file);
  if (request.body.file == undefined || request.body.file == '' || request.body.file == 'undefined') {
    return common.sendResponse(response, constant.otherMessage.ERR_INVALID_MEDIA_UPLOAD_REQUEST, false);
  }
  var fileObject = request.body.file;

  /* console.log("file size: " + fileObject.size);
  console.log("file path: " + fileObject.path);
  console.log("file name: " + fileObject.name);
  console.log("file type: " + fileObject.type); 
  console.log("file date: " + fileObject.lastModifiedDate); 
  :
  */
  //  console.log('#FILES',fileObject);
  var filesize = fileObject.size;
  var filepath = fileObject.path;
  var filename = fileObject.name;
  var filetype = fileObject.type;
  var filedate = d3.timeFormat(dbDateFormat)(new Date(fileObject.lastModifiedDate));

  if (request.body.trainingpartnerid !== undefined) {
    var tpid = request.body.trainingpartnerid;
  } else if (request.body.industrypartnerid !== undefined) {
    var ipid = request.body.industrypartnerid;
  } else {
    var admin_id = 1;
  }

  //File type validation
  var extensions = ['text/csv', 'application/vnd.ms-excel'];
  if (!extensions.includes(fileObject.type.toLowerCase())) {
    return common.sendResponse(response, constant.otherMessage.INVALID_FILE_TYPE_CSV, false);
  }

  //File size validation
  var FileSize = fileObject.size / 1024 / 1024; // in MB
  var maxFileSizeLimit = 2;
  if (request.body.table == "candidate" || request.body.table == "FutureHiringRequest" || request.body.table == "TrainingPartners" || request.body.table == "IndustryPartners") {
    maxFileSizeLimit = 35;
  }
  if (FileSize > maxFileSizeLimit) {
    return common.sendResponse(response, constant.otherMessage.INVALID_FILE_SIZE_CSV, false);
  }

  //Check headers
  var checkHeaders = await checkCSVHeaders(fileObject.path, request.body.table);
  if (checkHeaders.status === false) {
    return common.sendResponse(response, constant.otherMessage.CSV_INVALID_HEADERS, false);
  }

  if (request.body.table == "candidate" || request.body.table == "FutureHiringRequest" || request.body.table == "TrainingPartners" || request.body.table == "IndustryPartners") {
    //Restrict user if file upload process is already going on...
    var metaCount = 0;
    var getStatusOfMetaDataQuery = 'SELECT COUNT(`status`) as totalcount FROM `tbl_candidates_metadata` WHERE `module` = "' + request.body.table + '"  AND  `status` = 2';
    console.log('#getStatusOfMetaDataQuery', getStatusOfMetaDataQuery);
    var resulStatusOfMetaData = await connection.executeRawQuery(getStatusOfMetaDataQuery);
    console.log('#getStatusOfMetaDataQuery result', resulStatusOfMetaData);
    if (resulStatusOfMetaData.status == true && resulStatusOfMetaData.content.length > 0) {
      console.log('#getStatusOfMetaDataQuery count', resulStatusOfMetaData.content[0].totalcount);
      metaCount = resulStatusOfMetaData.content[0].totalcount;
    }
    if (metaCount > 0) {
      return common.sendResponse(response, constant.otherMessage.PROCESS_ALREADY_IN_PROGRESS_CSV, false);
    }


    //Add file meta data into table..
    if (tpid !== undefined && tpid > 0) {
      var addMetaDataQuery = ' INSERT INTO `tbl_candidates_metadata` (`trainingpartner_id`,`filepath`, `filesize`, `filename`, `filetype`, `filedate`,`module`,`status`) VALUES ("' + tpid + '","' + filepath + '", "' + filesize + '", "' + filename + '", "' + filetype + '", "' + filedate + '", "' + request.body.table + '",2)';
    } else if (ipid !== undefined && ipid > 0) {
      var addMetaDataQuery = ' INSERT INTO `tbl_candidates_metadata` (`industrypartner_id`,`filepath`, `filesize`, `filename`, `filetype`, `filedate`,`module`,`status`) VALUES ("' + ipid + '","' + filepath + '", "' + filesize + '", "' + filename + '", "' + filetype + '", "' + filedate + '", "' + request.body.table + '",2)';
    } else {
      var addMetaDataQuery = ' INSERT INTO `tbl_candidates_metadata` (`admin_id`,`filepath`, `filesize`, `filename`, `filetype`, `filedate`,`module`,`status`) VALUES ("'+admin_id+'","' + filepath + '", "' + filesize + '", "' + filename + '", "' + filetype + '", "' + filedate + '", "' + request.body.table + '",2)';
    }

    console.log('#metaquery', addMetaDataQuery);
    var resulMetaData = await connection.executeRawQuery(addMetaDataQuery);
    console.log('#filemeta result', resulMetaData);
    console.log('#metadataID', resulMetaData.content.insertId);

    var metaDataID = resulMetaData.content.insertId;
  }
  /* 
    if (false) {// FileSize <= 0.08296012878417969 [500 records] IF filesize or Records are lesser than it will go directly to the respective table
      var chunks = [];
      fs.createReadStream(fileObject.path)
        .pipe(parse({ delimiter: ',' }))
        .on('data', async function (sql) {
          chunks.push(sql);
        })
        .on('end', async function (rows) {
          var totalRows = rows - 1;
          if (request.body.table == "jobRoles") {
            var res = await sectorJobRolesBulkUpload(chunks, request.body.table);
          } else if (request.body.table == "candidate") {
            var res = await candidateBulkUpload(chunks, request.body.table, request.body.trainingpartnerid, response);
          } else {
            var res = await trainingPartnerIndustryPartnerBulkUpload(chunks, request.body.table);
          }
          return response.send({ status: true, data: res });
        })
        .on('error', function (error) {
          console.error('transform error');
          console.error(error); //Handle error
          return common.sendResponse(response, error, false);
        }); */
  // } else {
  var csv2sql = require('csv2sql-stream');
  var chunks = [];
  var tableToUse = '';
  switch (request.body.table) {
    case 'IndustryPartners':
      tableToUse = 'industrypartners_bulk_upload';
      break;
    case 'TrainingPartners':
      tableToUse = 'trainingpartners_bulk_upload';
      break;
    case 'FutureHiringRequest':
      tableToUse = 'futurerequest_bulk_upload';
      break;
    case 'candidate':
      tableToUse = 'candidate_bulk_upload';
      break;
  }
  csv2sql.transform(tableToUse, fs.createReadStream(fileObject.path))
    .on('data', async function (sql) {
      chunks.push(sql);
    })
    .on('end', async function (rows) {
      var totalRows = rows - 1;
      if (tableToUse == 'candidate_bulk_upload') {
        if (metaDataID != undefined) {
          var res = await insertRowsForCandidateTable(chunks, metaDataID, tpid);
        }
      } else if (tableToUse == 'futurerequest_bulk_upload') {
        var res = await insertRowsForFutureHiringRequestTable(chunks, metaDataID, ipid, tableToUse);
      } else if (tableToUse == 'trainingpartners_bulk_upload') {
        var res = await insertRowsForTrainingPartnerTable(chunks, metaDataID, admin_id, tableToUse);
      } else if(tableToUse == 'industrypartners_bulk_upload'){
        var res = await insertRowsForIndustryPartnerTable(chunks, metaDataID, admin_id, tableToUse);
      } else {
        var res = await insertRowsToTable(chunks);
      }
      return response.send({ status: true, data: res });
    })
    .on('error', function (error) {
      console.error('transform error');
      console.error(error); //Handle error
      return common.sendResponse(response, error, false);
    });
  // }
};
async function insertRowsForIndustryPartnerTable(chunks, metaDataID, admin_id, tableToUse){
  var total_rows = chunks.length;
  for (var sql of chunks) {
    try {
      var res = await connection.executeRawQuery(sql);
      if (res.status === true && res.content !== undefined && res.content.insertId > 0) {

      } else {
        var str = sql.substring(sql.indexOf(" ('") + 2);
        var errorString = str.substr(0, str.length - 3);
        //errorString = res.status.message +' :=  '+errorString;
        errorData.error.push({
          errorString
        });
      }
    } catch (ex) {
      console.log(ex);
    }
  }
  
  if (admin_id > 0) {
    var updateQuery = 'UPDATE tbl_candidates_metadata SET total_records = ' + total_rows + ' WHERE admin_id = ' + admin_id + ' AND ' + 'id=' + metaDataID;
    console.log('#up', updateQuery);
    await connection.executeRawQuery(updateQuery);

    var ippasswddynamic = bcrypt.hashSync(constant.appConfig.SYSTEM_PASSWORD, 10);
    var updateIPBulkUploadQuery = 'UPDATE industrypartners_bulk_upload SET password = "' + ippasswddynamic + '" , meta_id=' + metaDataID;
    console.log('#up', updateIPBulkUploadQuery);
    await connection.executeRawQuery(updateIPBulkUploadQuery);

    var updateempidquery = "UPDATE industrypartners_bulk_upload SET ipid= CONCAT('EMP-',FLOOR(RAND() * 401) + 10000) WHERE meta_id="+ metaDataID;
    console.log('#up', updateempidquery);
    await connection.executeRawQuery(updateempidquery);
  }

  //First update all the actual fields like sector,jobrole etc
  var updateSectorNameQuery = ' UPDATE industrypartners_bulk_upload SET `employertype_name` = `employertype`,`sector_name` = `sector`,`state_name` = `state`,`district_name` = `district`  WHERE meta_id=' + metaDataID;
  var resultSector = await connection.executeRawQuery(updateSectorNameQuery);
  console.log('#FIrst query Sectors', resultSector);

  //REmove Space From ENUM fields..
  var enumarray = ['employertype'];
  for (let index = 0; index < enumarray.length; index++) {
    var setremoveSpaceForEnum = "REPLACE(`" + enumarray[index] + "`, ' ', '')";
    var removeSpaceForEnumQuery = ' update `industrypartners_bulk_upload` set ' + enumarray[index] + ' = ' + setremoveSpaceForEnum + ' WHERE meta_id=' + metaDataID;
    var resultremoveSpaceForEnum = await connection.executeRawQuery(removeSpaceForEnumQuery);
  }

  //After 1st Release we can make below queries into 1 function..

  var updateCityQuery = ' UPDATE industrypartners_bulk_upload  INNER JOIN view_GetLocationDetail  ON (industrypartners_bulk_upload.district = view_GetLocationDetail.cityName and industrypartners_bulk_upload.state = view_GetLocationDetail.stateName)  SET industrypartners_bulk_upload.district = view_GetLocationDetail.cityid WHERE meta_id=' + metaDataID;
  console.log('#updae city query', updateCityQuery);
  var resultCity = await connection.executeRawQuery(updateCityQuery);
  console.log('#FIrst query res city', resultCity);
  if (resultCity.content.affectedRows > 0) var cityflag = 1;

  var updateStateQuery = ' UPDATE industrypartners_bulk_upload INNER JOIN view_GetLocationDetail ON (industrypartners_bulk_upload.state = view_GetLocationDetail.stateName) SET industrypartners_bulk_upload.state = view_GetLocationDetail.stateid WHERE meta_id=' + metaDataID;
  var resultState = await connection.executeRawQuery(updateStateQuery);
  console.log('#FIrst query res state', resultState);
  if (resultState.content.affectedRows > 0) var stateflag = 1;
  

  //Replace @# with comma seperation
  var updateCommaQuery = "UPDATE industrypartners_bulk_upload SET sector = REPLACE(sector, '@#', ',') WHERE meta_id=" + metaDataID;
  var resultComma = await connection.executeRawQuery(updateCommaQuery);

  var updateSectorQuery = 'UPDATE industrypartners_bulk_upload AS t1 INNER JOIN (SELECT IP.id, GROUP_CONCAT(SEC.pk_sectorID) as secid FROM `industrypartners_bulk_upload` as IP LEFT JOIN `tbl_SectorMaster` as SEC ON  FIND_IN_SET (`SEC`.`sectorName`,IP.sector) GROUP BY IP.id) AS t2 ON t1.id = t2.id SET t1.sector = t2.secid WHERE meta_id=' + metaDataID;
  var resultSector = await connection.executeRawQuery(updateSectorQuery);
  console.log('#FIrst query Sectors', updateSectorQuery);
  console.log('#FIrst query Sectors', resultSector);
  if (resultSector.content.affectedRows > 0) { var sectorflag = 1; }
  

  var emptyFlagsQuery = ' INSERT IGNORE INTO `industrypartners_erros`(`meta_id`,`ipid`,`password`,`employertype`,`employertype_name`,`legal_entity_name`,`contact_person_name`,`contact_email_address`,`contact_mobile`,`address`,`sector`,`sector_name`,`office_phone`,`website`,`state`,`state_name`,`district`,`district_name`,`pincode`) SELECT `meta_id`,`ipid`,`password`,`employertype`,`employertype_name`,`legal_entity_name`,`contact_person_name`,`contact_email_address`,`contact_mobile`,`address`,`sector`,`sector_name`,`office_phone`,`website`,`state`,`state_name`,`district`,`district_name`,`pincode` FROM `industrypartners_bulk_upload` WHERE meta_id = ' + metaDataID;
  console.log('#Empty Flagquery', emptyFlagsQuery);
  var emptyflagres = await connection.executeRawQuery(emptyFlagsQuery);
  console.log('#AR', emptyflagres);



  try {
    //Insert into Validation Table...
    if (stateflag == 1 && cityflag == 1 && sectorflag == 1) {
      // Finally Insert into TP Table...

      var insertToMainTableQuery = ' INSERT IGNORE INTO `tbl_IndustryPartners`(`fk_meta_id`,`ipid`,`password`,`employertype`,`legal_entity_name`,`contact_person_name`,`contact_email_address`,`contact_mobile`,`address`,`fk_sectorID`,`office_phone`,`website`,`fk_stateID`,`fk_cityID`, `pincode`) SELECT `meta_id`,`ipid`,`password`,`employertype`,`legal_entity_name`,`contact_person_name`,`contact_email_address`,`contact_mobile`,`address`,`sector`,`office_phone`,`website`,`state`,`district`,`pincode` FROM `industrypartners_erros` WHERE employertype > 0 AND sector > 0 AND state > 0 and district > 0 AND meta_id = ' + metaDataID;
      console.log('#FINALQUERY', insertToMainTableQuery);
      var resultAfterInsertion = await connection.executeRawQuery(insertToMainTableQuery);
      console.log('#Completed Insertion into Main Candidate table For Candiates ', resultAfterInsertion);


      //Update to MetaData Status..
      var updateMetaDataStatus = ' UPDATE `tbl_candidates_metadata` SET `status` = 1 WHERE `module` = "IndustryPartners" AND `id` = ' + metaDataID + ' AND `admin_id` = ' + admin_id;
      console.log('#MetaDateStatusQueryFINALQUERY', updateMetaDataStatus);
      var metaDataStatusRes = await connection.executeRawQuery(updateMetaDataStatus);
      console.log('#File Status Updated In metadata table', metaDataStatusRes);

      //Removing data from Error table which are already moved to the main table..
      var deleteFromBulkUploadquery = ' DELETE FROM  `industrypartners_erros` WHERE employertype > 0 AND sector > 0 and state > 0 and district > 0 AND meta_id = ' + metaDataID;
      console.log('# query ', deleteFromBulkUploadquery);
      var removedBulkUPloadtbl = await connection.executeRawQuery(deleteFromBulkUploadquery);
      console.log('#Bulk Upload Table Removed', removedBulkUPloadtbl);

      //Removing data from Bulk upload Table..
      var deleteFromBulkUploadquery = ' DELETE FROM  `industrypartners_bulk_upload` WHERE meta_id = ' + metaDataID;
      var removedBulkUPloadtbl = await connection.executeRawQuery(deleteFromBulkUploadquery);
      console.log('#Bulk Upload Table Removed', removedBulkUPloadtbl);

      var updateFailedRowsQuery = 'UPDATE tbl_candidates_metadata SET failed_records = (SELECT COUNT(*) as `f` from industrypartners_erros where meta_id = ' + metaDataID + ' ) WHERE tbl_candidates_metadata.id = ' + metaDataID + ' AND  tbl_candidates_metadata.admin_id = ' + admin_id;
      //UPDATE tbl_candidates_metadata SET failed_records = (SELECT COUNT(*) as `f` from futurerequest_errors where meta_id = 52 AND 11 ) WHERE id = 52 AND industrypartner_id = 11
      console.log('#fialed query', updateFailedRowsQuery);
      await connection.executeRawQuery(updateFailedRowsQuery);
    }
  }
  catch (e) {
    console.log('#ERRORAR', e);
  }
}
async function insertRowsForTrainingPartnerTable(chunks, metaDataID, admin_id, tableToUse){
  var total_rows = chunks.length;
  for (var sql of chunks) {
    try {
      var res = await connection.executeRawQuery(sql);
      if (res.status === true && res.content !== undefined && res.content.insertId > 0) {

      } else {
        var str = sql.substring(sql.indexOf(" ('") + 2);
        var errorString = str.substr(0, str.length - 3);
        //errorString = res.status.message +' :=  '+errorString;
        errorData.error.push({
          errorString
        });
      }
    } catch (ex) {
      console.log(ex);
    }
  }
  
  if (admin_id > 0) {
    var updateQuery = 'UPDATE tbl_candidates_metadata SET total_records = ' + total_rows + ' WHERE admin_id = ' + admin_id + ' AND ' + 'id=' + metaDataID;
    console.log('#up', updateQuery);
    await connection.executeRawQuery(updateQuery);

    var tppasswddynamic = bcrypt.hashSync(constant.appConfig.SYSTEM_PASSWORD, 10);
    var updateTPBulkUploadQuery = 'UPDATE trainingpartners_bulk_upload SET password = "' + tppasswddynamic + '" , meta_id=' + metaDataID;
    console.log('#up', updateTPBulkUploadQuery);
    await connection.executeRawQuery(updateTPBulkUploadQuery);

    var updatetpidquery = "UPDATE trainingpartners_bulk_upload SET tpid= CONCAT('TP-',FLOOR(RAND() * 401) + 10000) WHERE meta_id="+ metaDataID;
    console.log('#up', updatetpidquery);
    await connection.executeRawQuery(updatetpidquery);
  }
  
  //First update all the actual fields like sector,jobrole etc
  var updateSectorNameQuery = ' UPDATE trainingpartners_bulk_upload SET `sector_name` = `sector`,`state_name` = `state`,`district_name` = `district`  WHERE meta_id=' + metaDataID;
  var resultSector = await connection.executeRawQuery(updateSectorNameQuery);
  console.log('#FIrst query Sectors', resultSector);

  //After 1st Release we can make below queries into 1 function..

  var updateCityQuery = ' UPDATE trainingpartners_bulk_upload  INNER JOIN view_GetLocationDetail  ON (trainingpartners_bulk_upload.district = view_GetLocationDetail.cityName and trainingpartners_bulk_upload.state = view_GetLocationDetail.stateName)  SET trainingpartners_bulk_upload.district = view_GetLocationDetail.cityid WHERE meta_id=' + metaDataID;
  console.log('#updae city query', updateCityQuery);
  var resultCity = await connection.executeRawQuery(updateCityQuery);
  console.log('#FIrst query res city', resultCity);
  if (resultCity.content.affectedRows > 0) var cityflag = 1;

  var updateStateQuery = ' UPDATE trainingpartners_bulk_upload INNER JOIN view_GetLocationDetail ON (trainingpartners_bulk_upload.state = view_GetLocationDetail.stateName) SET trainingpartners_bulk_upload.state = view_GetLocationDetail.stateid WHERE meta_id=' + metaDataID;
  var resultState = await connection.executeRawQuery(updateStateQuery);
  console.log('#FIrst query res state', resultState);
  if (resultState.content.affectedRows > 0) var stateflag = 1;
  

  //Replace @# with comma seperation
  var updateCommaQuery = "UPDATE trainingpartners_bulk_upload SET sector = REPLACE(sector, '@#', ',') WHERE meta_id=" + metaDataID;
  var resultComma = await connection.executeRawQuery(updateCommaQuery);

  var updateSectorQuery = 'UPDATE trainingpartners_bulk_upload AS t1 INNER JOIN (SELECT IP.id, GROUP_CONCAT(SEC.pk_sectorID) as secid FROM `trainingpartners_bulk_upload` as IP LEFT JOIN `tbl_SectorMaster` as SEC ON  FIND_IN_SET (`SEC`.`sectorName`,IP.sector) GROUP BY IP.id) AS t2 ON t1.id = t2.id SET t1.sector = t2.secid WHERE meta_id=' + metaDataID;
  var resultSector = await connection.executeRawQuery(updateSectorQuery);
  console.log('#FIrst query Sectors', updateSectorQuery);
  console.log('#FIrst query Sectors', resultSector);
  if (resultSector.content.affectedRows > 0) { var sectorflag = 1; }
  

  var emptyFlagsQuery = ' INSERT IGNORE INTO `trainingpartners_errors`(`meta_id`,`tpid`,`password`,`nsdc_ssc_registration_number`,`training_partner_name`,`legal_entity_name`,`contact_person_name`,`contact_email_address`,`contact_mobile`,`address`,`sector`,`sector_name`,`state`,`state_name`,`district`,`district_name`,`pincode`) SELECT `meta_id`,`tpid`,`password`,`nsdc_ssc_registration_number`,`training_partner_name`,`legal_entity_name`,`contact_person_name`,`contact_email_address`,`contact_mobile`,`address`,`sector`,`sector_name`,`state`,`state_name`,`district`,`district_name`,`pincode` FROM `trainingpartners_bulk_upload` WHERE meta_id = ' + metaDataID;
  console.log('#Empty Flagquery', emptyFlagsQuery);
  var emptyflagres = await connection.executeRawQuery(emptyFlagsQuery);
  console.log('#AR', emptyflagres);



  try {
    //Insert into Validation Table...
    if (stateflag == 1 && cityflag == 1 && sectorflag == 1) {
      // Finally Insert into TP Table...

      var insertToMainTableQuery = ' INSERT IGNORE INTO `tbl_TrainingPartners`(`fk_meta_id`,`tpid`,`password`,`nsdc_registration_number`,`training_partner_name`,`legal_entity_name`,`contact_person_name`,`contact_email_address`,`contact_mobile`,`address`,`fk_sectorID`,`fk_stateID`,`fk_cityID`, `pincode`) SELECT `meta_id`,`tpid`,`password`,`nsdc_ssc_registration_number`,`training_partner_name`,`legal_entity_name`,`contact_person_name`,`contact_email_address`,`contact_mobile`,`address`,`sector`,`state`,`district`,`pincode` FROM `trainingpartners_errors` WHERE sector > 0 AND state > 0 and district > 0 AND meta_id = ' + metaDataID;
      console.log('#FINALQUERY', insertToMainTableQuery);
      var resultAfterInsertion = await connection.executeRawQuery(insertToMainTableQuery);
      console.log('#Completed Insertion into Main Candidate table For Candiates ', resultAfterInsertion);


      //Update to MetaData Status..
      var updateMetaDataStatus = ' UPDATE `tbl_candidates_metadata` SET `status` = 1 WHERE `module` = "TrainingPartners" AND `id` = ' + metaDataID + ' AND `admin_id` = ' + admin_id;
      console.log('#MetaDateStatusQueryFINALQUERY', updateMetaDataStatus);
      var metaDataStatusRes = await connection.executeRawQuery(updateMetaDataStatus);
      console.log('#File Status Updated In metadata table', metaDataStatusRes);

      //Removing data from Error table which are already moved to the main table..
      var deleteFromBulkUploadquery = ' DELETE FROM  `trainingpartners_errors` WHERE sector > 0 and state > 0 and district > 0 AND meta_id = ' + metaDataID;
      console.log('# query ', deleteFromBulkUploadquery);
      var removedBulkUPloadtbl = await connection.executeRawQuery(deleteFromBulkUploadquery);
      console.log('#Bulk Upload Table Removed', removedBulkUPloadtbl);

      //Removing data from Bulk upload Table..
      var deleteFromBulkUploadquery = ' DELETE FROM  `trainingpartners_bulk_upload` WHERE meta_id = ' + metaDataID;
      var removedBulkUPloadtbl = await connection.executeRawQuery(deleteFromBulkUploadquery);
      console.log('#Bulk Upload Table Removed', removedBulkUPloadtbl);

      var updateFailedRowsQuery = 'UPDATE tbl_candidates_metadata SET failed_records = (SELECT COUNT(*) as `f` from trainingpartners_errors where meta_id = ' + metaDataID + ' ) WHERE tbl_candidates_metadata.id = ' + metaDataID + ' AND  tbl_candidates_metadata.admin_id = ' + admin_id;
      //UPDATE tbl_candidates_metadata SET failed_records = (SELECT COUNT(*) as `f` from futurerequest_errors where meta_id = 52 AND 11 ) WHERE id = 52 AND industrypartner_id = 11
      console.log('#fialed query', updateFailedRowsQuery);
      await connection.executeRawQuery(updateFailedRowsQuery);
    }
  }
  catch (e) {
    console.log('#ERRORAR', e);
  }
}
async function insertRowsForFutureHiringRequestTable(chunks, metaDataID, ipid, tableToUse) {
  var total_rows = chunks.length;
  for (var sql of chunks) {
    try {
      var res = await connection.executeRawQuery(sql);
      if (res.status === true && res.content !== undefined && res.content.insertId > 0) {

      } else {
        var str = sql.substring(sql.indexOf(" ('") + 2);
        var errorString = str.substr(0, str.length - 3);
        //errorString = res.status.message +' :=  '+errorString;
        errorData.error.push({
          errorString
        });
      }
    } catch (ex) {
      console.log(ex);
    }
  }

  if (ipid > 0) {
    var year = '';
    var multiYears = new Array();
    var currentDate = new Date();
    var currentYear = currentDate.getFullYear();

    // create year array
    for (var y = 0; y <= 1; y++) {
      var forwardYears = currentYear + y;
      multiYears.push(forwardYears);
    }
    year = multiYears.join('-');
    console.log('# year', year);
    var updateQuery = 'UPDATE tbl_candidates_metadata SET total_records = ' + total_rows + ' WHERE industrypartner_id = ' + ipid + ' AND ' + 'id=' + metaDataID;
    console.log('#up', updateQuery);
    await connection.executeRawQuery(updateQuery);

    var updateCandidateQuery = 'UPDATE futurerequest_bulk_upload SET year="' + year + '" ,fk_industrypartnerid = ' + ipid + ' , ' + 'meta_id=' + metaDataID;
    console.log('#up', updateCandidateQuery);

    await connection.executeRawQuery(updateCandidateQuery);
  }
  var updateAllNullToZeroQuery = 'UPDATE futurerequest_bulk_upload  SET currentmonth = IFNULL(currentmonth, 0),January = IFNULL(January, 0),February = IFNULL(February, 0),March = IFNULL(March, 0),April = IFNULL(April, 0),May = IFNULL(May, 0),June = IFNULL(June, 0),July = IFNULL(July, 0),August = IFNULL(August, 0),September = IFNULL(September, 0),October = IFNULL(October, 0),November = IFNULL(November, 0),December = IFNULL(December, 0) WHERE meta_id=' + metaDataID + ' AND fk_industrypartnerid = ' + ipid;
  var resultAfterupdateAllNullToZeroQuery = await connection.executeRawQuery(updateAllNullToZeroQuery);

  //First update all the actual fields like sector,jobrole etc
  var updateSectorNameQuery = ' UPDATE futurerequest_bulk_upload SET `sector_name` = `sector`,`jobrole_name` = `jobrole`,`state_name` = `state`,`district_name` = `district`  WHERE meta_id=' + metaDataID + ' AND fk_industrypartnerid = ' + ipid;
  var resultSector = await connection.executeRawQuery(updateSectorNameQuery);
  console.log('#FIrst query Sectors', resultSector);

  //After 1st Release we can make below queries into 1 function..

  var updateCityQuery = ' UPDATE futurerequest_bulk_upload  INNER JOIN view_GetLocationDetail  ON (futurerequest_bulk_upload.district = view_GetLocationDetail.cityName and futurerequest_bulk_upload.state = view_GetLocationDetail.stateName)  SET futurerequest_bulk_upload.district = view_GetLocationDetail.cityid WHERE meta_id=' + metaDataID + ' AND fk_industrypartnerid = ' + ipid;
  console.log('#updae city query', updateCityQuery);
  var resultCity = await connection.executeRawQuery(updateCityQuery);
  console.log('#FIrst query res city', resultCity);
  if (resultCity.content.affectedRows > 0) var cityflag = 1;

  var updateStateQuery = ' UPDATE futurerequest_bulk_upload INNER JOIN view_GetLocationDetail ON (futurerequest_bulk_upload.state = view_GetLocationDetail.stateName) SET futurerequest_bulk_upload.state = view_GetLocationDetail.stateid WHERE meta_id=' + metaDataID + ' AND fk_industrypartnerid = ' + ipid;
  var resultState = await connection.executeRawQuery(updateStateQuery);
  console.log('#FIrst query res state', resultState);
  if (resultState.content.affectedRows > 0) var stateflag = 1;

  var updateJobrolesQuery = ' UPDATE futurerequest_bulk_upload  INNER JOIN view_SectorJobRoleDetail ON (futurerequest_bulk_upload.jobrole = view_SectorJobRoleDetail.jobroleName and futurerequest_bulk_upload.sector = view_SectorJobRoleDetail.sectorName) SET futurerequest_bulk_upload.jobrole = view_SectorJobRoleDetail.jobroleid WHERE meta_id=' + metaDataID + ' AND fk_industrypartnerid = ' + ipid;
  var resultJobroles = await connection.executeRawQuery(updateJobrolesQuery);
  console.log('#FIrst query Jobroles', resultJobroles);
  if (resultJobroles.content.affectedRows > 0) var jobrolesflag = 1;

  var updateSectorQuery = ' UPDATE futurerequest_bulk_upload  INNER JOIN view_SectorJobRoleDetail ON (futurerequest_bulk_upload.sector = view_SectorJobRoleDetail.sectorName) SET futurerequest_bulk_upload.sector = view_SectorJobRoleDetail.sectorid WHERE meta_id=' + metaDataID + ' AND fk_industrypartnerid = ' + ipid;
  var resultSector = await connection.executeRawQuery(updateSectorQuery);
  console.log('#FIrst query Sectors', resultSector);
  if (resultSector.content.affectedRows > 0) { var sectorflag = 1; }


  var emptyFlagsQuery = ' INSERT IGNORE INTO `futurerequest_errors`(`fk_industrypartnerid`,`meta_id`,`sector`,`sector_name`,`jobrole`,`jobrole_name`,`state`,`state_name`,`district`,`district_name`,`year`,`currentmonth`,`January`,`February`,`March`,`April`,`May`,`June`,`July`,`August`,`September`,`October`,`November`,`December`) SELECT `fk_industrypartnerid`,`meta_id`,`sector`,`sector_name`,`jobrole`,`jobrole_name`,`state`,`state_name`,`district`,`district_name`,`year`,`currentmonth`,`January`,`February`,`March`,`April`,`May`,`June`,`July`,`August`,`September`,`October`,`November`,`December` FROM `futurerequest_bulk_upload` WHERE meta_id = ' + metaDataID + ' AND fk_industrypartnerid = ' + ipid;
  console.log('#Empty Flagquery', emptyFlagsQuery);
  var emptyflagres = await connection.executeRawQuery(emptyFlagsQuery);
  console.log('#AR', emptyflagres);


  var updateUniqueKeyQuery = 'UPDATE futurerequest_errors set unique_key = (SELECT  CONCAT(`fk_industrypartnerid`,`sector`,`jobrole`,`state`,`district`,`currentmonth`,`January`,`February`,`March`,`April`,`May`,`June`,`July`,`August`,`September`,`October`,`November`,`December`) AS FIRSTNAME)';
  var resultAfterupdateUniqueKeyQuery = await connection.executeRawQuery(updateUniqueKeyQuery);
  //Insert all the columns from the bulk_upload as new columns to errors table in error_column 
  /* var errorColumnQuery = "UPDATE `futurerequest_errors` SET `error_column` SELECT CONCAT(`sector`,'@@@@',`jobrole`,'@@@@',`state`,'@@@@',`district`,'@@@@',`currentmonth`,'@@@@',`January`,'@@@@',`February`,'@@@@',`March`,'@@@@',`April`,'@@@@',`May`,'@@@@',`June`,'@@@@',`July`,'@@@@',`August`,'@@@@',`September`,'@@@@',`October`,'@@@@',`November`,'@@@@',`December`) AS new_column FROM futurerequest_bulk_upload WHERE meta_id = " + metaDataID + " AND " + " fk_industrypartnerid = " + ipid;
  console.log('#errorColumnQuery', errorColumnQuery);
  var errorColumnQueryRes = await connection.executeRawQuery(errorColumnQuery);
  console.log('#CT', errorColumnQueryRes);*/



  try {
    //Insert into Validation Table...
    if (stateflag == 1 && cityflag == 1 && sectorflag == 1 && jobrolesflag == 1) {
      //Update Sector validation with industrypartner table into Validation Table
      var updateSectorValueQuery = ' UPDATE futurerequest_errors, tbl_IndustryPartners SET futurerequest_errors.is_sector_matched = (SELECT count(*) from tbl_IndustryPartners where `pk_industrypartnerid` = futurerequest_errors.fk_industrypartnerid AND FIND_IN_SET (futurerequest_errors.sector,fk_sectorID)) WHERE meta_id = ' + metaDataID + ' AND  fk_industrypartnerid = ' + ipid;
      console.log('# updateSectorValueQuery', updateSectorValueQuery);
      var resupdateStateVal = await connection.executeRawQuery(updateSectorValueQuery);
      console.log('#Updated is_sector_mathc flag successfully ', resupdateStateVal);


      // Finally Insert into Candidate Table...

      var insertToMainTableQuery = ' INSERT IGNORE INTO `tbl_FutureHiringRequests`(`fk_meta_id`,`fk_industrypartnerid`,`fk_sectorID`,`fk_jobroleID`,`fk_stateID`,`fk_cityID`, `year`, `currentmonth`, `January`,`February`,`March`,`April`,`May`,`June`,`July`,`August`,`September`,`October`,`November`,`December`,`unique_key`) SELECT `meta_id`,`fk_industrypartnerid`,`sector`,`jobrole`,`state`,`district`,`year`,`currentmonth`,`January`,`February`,`March`,`April`,`May`,`June`,`July`,`August`,`September`,`October`,`November`,`December`,`unique_key` FROM `futurerequest_errors` WHERE is_sector_matched = 1 AND fk_industrypartnerid > 0 AND sector > 0 AND jobrole > 0 and state > 0 and district > 0 AND meta_id = ' + metaDataID + ' AND fk_industrypartnerid = ' + ipid;
      console.log('#FINALQUERY', insertToMainTableQuery);
      var resultAfterInsertion = await connection.executeRawQuery(insertToMainTableQuery);
      console.log('#Completed Insertion into Main Candidate table For Candiates ', resultAfterInsertion);



      //Update to MetaData Status..
      var updateMetaDataStatus = ' UPDATE `tbl_candidates_metadata` SET `status` = 1 WHERE `module` = "FutureHiringRequest" AND `id` = ' + metaDataID + ' AND `industrypartner_id` = ' + ipid;
      console.log('#MetaDateStatusQueryFINALQUERY', updateMetaDataStatus);
      var metaDataStatusRes = await connection.executeRawQuery(updateMetaDataStatus);
      console.log('#File Status Updated In metadata table', metaDataStatusRes);

      var updateTotalQuery = 'UPDATE `tbl_FutureHiringRequests` SET `total`= (SELECT SUM(`currentmonth` + `January` + `February` + `March` + `April` + `May` + `June` + `July` + `August` + `September` + `October` + `November` + `December`) as `numberOfPositions`)';
      var updateTotalQueryRes = await connection.executeRawQuery(updateTotalQuery);

      //Removing data from Error table which are already moved to the main table..
      var deleteFromBulkUploadquery = ' DELETE FROM  `futurerequest_errors` WHERE is_sector_matched = 1 AND fk_industrypartnerid > 0 AND sector > 0 AND jobrole > 0 and state > 0 and district > 0 AND meta_id = ' + metaDataID + ' AND  fk_industrypartnerid = ' + ipid;
      console.log('# query ', deleteFromBulkUploadquery);
      var removedBulkUPloadtbl = await connection.executeRawQuery(deleteFromBulkUploadquery);
      console.log('#Bulk Upload Table Removed', removedBulkUPloadtbl);

      //Removing data from Bulk upload Table..
      var deleteFromBulkUploadquery = ' DELETE FROM  `futurerequest_bulk_upload` WHERE meta_id = ' + metaDataID + ' AND fk_industrypartnerid = ' + ipid;
      var removedBulkUPloadtbl = await connection.executeRawQuery(deleteFromBulkUploadquery);
      console.log('#Bulk Upload Table Removed', removedBulkUPloadtbl);

      var updateFailedRowsQuery = 'UPDATE tbl_candidates_metadata SET failed_records = (SELECT COUNT(*) as `f` from futurerequest_errors where meta_id = ' + metaDataID + ' AND fk_industrypartnerid = ' + ipid + ' ) WHERE tbl_candidates_metadata.id = ' + metaDataID + ' AND  tbl_candidates_metadata.industrypartner_id = ' + ipid;
      //UPDATE tbl_candidates_metadata SET failed_records = (SELECT COUNT(*) as `f` from futurerequest_errors where meta_id = 52 AND 11 ) WHERE id = 52 AND industrypartner_id = 11
      console.log('#fialed query', updateFailedRowsQuery);
      await connection.executeRawQuery(updateFailedRowsQuery);
    }
  }
  catch (e) {
    console.log('#ERRORAR', e);
  }
}
async function insertRowsForCandidateTable(chunks, metaDataID, tpid) {
  var errorData = { error: [] };
  var finalData = {};
  var successCounter = 0;
  var total_rows = chunks.length;

  for (var sql of chunks) {
    try {
      var res = await connection.executeRawQuery(sql);
      if (res.status === true && res.content !== undefined && res.content.insertId > 0) {
        successCounter++;
      } else {
        var str = sql.substring(sql.indexOf(" ('") + 2);
        var errorString = str.substr(0, str.length - 3);
        //errorString = res.status.message +' :=  '+errorString;
        errorData.error.push({
          errorString
        });
      }
    } catch (ex) {
      console.log(ex);
      var str = sql.substring(sql.indexOf(" ('") + 2);
      var errorString = str.substr(0, str.length - 3);
      errorString = ex.error.message + ' :=  ' + errorString;
      errorData.error.push({
        errorString
      });
    }
  }

  /*   var currenttime = new Date();
    var start_time = currenttime.getSeconds();
    console.log('#Start time',start_time);
   */

  if (tpid > 0) {

    var updateTotalCountQuery = 'UPDATE tbl_candidates_metadata SET total_records = ' + total_rows + '  WHERE id = ' + metaDataID;
    console.log('#totalquer', updateTotalCountQuery);
    // return
    await connection.executeRawQuery(updateTotalCountQuery);

    var updateCandidateQuery = 'UPDATE candidate_bulk_upload SET trainingpartner_id = ' + tpid + ' , ' + 'candidate_meta_id=' + metaDataID;
    console.log('#up', updateCandidateQuery);
    await connection.executeRawQuery(updateCandidateQuery);
  }
  //console.log('#BULK UPLOAD');
  //return;
  //Step 1 : Replace Aadharcard number with "".
/*
  var updatedVal = "Replace(`aadhaar_number`, '\"\','')";
  var updateAadharcardQuery = ' update `candidate_bulk_upload` set `aadhaar_number` = ' + updatedVal + ' WHERE trainingpartner_id = ' + tpid;
  //console.log('#upAadhar', updateAadharcardQuery);
  var resultAadharCard = await connection.executeRawQuery(updateAadharcardQuery);
*/

/*
  //Step 1 : Replace Age to DOb.
  var updatedVal = "CONCAT('1/1/',(SELECT DATE_FORMAT(NOW(), '%Y') - (SELECT val)))";
  var updateAadharcardQuery = ' update `candidate_bulk_upload` set `dob` = ' + updatedVal + ' WHERE trainingpartner_id = ' + tpid;
  var resultAadharCard = await connection.executeRawQuery(updateAadharcardQuery);

  */
  //Apply Trim..
     
  var applyTrim = "TRIM(REPLACE(REPLACE(REPLACE(`training_center`,'\n', ' '), '\r', ' '), '\t', ' '))";
  var removeSpaceQuery = ' update `candidate_bulk_upload` set `training_center` = ' + applyTrim + ' WHERE trainingpartner_id = ' + tpid;
  var resultofSpace = await connection.executeRawQuery(removeSpaceQuery);

  var applyTrimState = 'TRIM(REPLACE(`state`, " ", ""))';
  var removeSpaceStaeQuery = ' update `candidate_bulk_upload` set `state` = ' + applyTrimState + ' WHERE trainingpartner_id = ' + tpid;
  var resultofStateSpace = await connection.executeRawQuery(removeSpaceStaeQuery);
  console.log('#styate',removeSpaceStaeQuery);

  //REmove Space From ENUM fields..
  var enumarray = ['employment_type', 'placement_status', 'training_status', 'alternateidtype'];
  for (let index = 0; index < enumarray.length; index++) {
    var setremoveSpaceForEnum = "REPLACE(`" + enumarray[index] + "`, ' ', '')";
    var removeSpaceForEnumQuery = ' update `candidate_bulk_upload` set ' + enumarray[index] + ' = ' + setremoveSpaceForEnum + ' WHERE trainingpartner_id = ' + tpid + ' AND candidate_meta_id=' + metaDataID;
    var resultremoveSpaceForEnum = await connection.executeRawQuery(removeSpaceForEnumQuery);
  }
  //return;
  //Update all ALIAS names....

  var updateAliasQuery = ' UPDATE `candidate_bulk_upload` SET `traning_center_name` = `training_center` , `district_name` = `district`,`state_name` = `state`,`sector_name` = `sector`,`jobrole_name` = `jobrole`,`scheme_type_name` = `scheme_type`,`state_skill_name` = `state_skill_development_mission` where candidate_meta_id = ' + metaDataID + ' AND ' + ' trainingpartner_id = ' + tpid;
  console.log('#updae Alias query', updateAliasQuery);
  var resultAlias = await connection.executeRawQuery(updateAliasQuery);

  //End of updating alias..

  /*   //Need to add the clone into candidate_send_error_list for sending error list to users..
    var addCloneQuery = ' INSERT INTO candidate_send_error_list(`trainingpartner_id`,`candidate_meta_id`,`training_center`,`district`,`state`,`sector`,`jobrole`,`scheme_type`,`state_skill_development_mission`,`candidate_name`,`aadhaar_number`,`aadhaar_enrollment_id`,`alternateidtype`,`alternate_id_number`,`phone`,`email_address`,`dob`,`gender`,`address`,`pincode`,`experience_in_years`,`sdmsnsdc_enrollment_number`,`training_status`,`attendence_percentage`,`placement_status`,`employment_type`,`assessment_score`,`max_assessment_score`,`assessment_date`,`training_type`,`batch_id`,`batch_start_date`,`batch_end_date`,`willing_to_relocate`)SELECT `trainingpartner_id`,`candidate_meta_id`,`training_center`,`district`,`state`,`sector`,`jobrole`,`scheme_type`,`state_skill_development_mission`,`candidate_name`,`aadhaar_number`,`aadhaar_enrollment_id`,`alternateidtype`,`alternate_id_number`,`phone`,`email_address`,`dob`,`gender`,`address`,`pincode`,`experience_in_years`,`sdmsnsdc_enrollment_number`,`training_status`,`attendence_percentage`,`placement_status`,`employment_type`,`assessment_score`,`max_assessment_score`,`assessment_date`,`training_type`,`batch_id`,`batch_start_date`,`batch_end_date`,`willing_to_relocate` FROM `candidate_bulk_upload` ';
    // console.log('#Adding all rows into candidate_send_error_list table', addCloneQuery);
    var resultClone = await connection.executeRawQuery(addCloneQuery);
    console.log('#Result of clone query into  candidate_send_error_list table', resultClone);
   */
  // return

  //After 1st Release we can make below queries into 1 function..
 
  var updateCityQuery = ' UPDATE candidate_bulk_upload  INNER JOIN view_GetLocationDetail  ON (candidate_bulk_upload.district = view_GetLocationDetail.cityName and candidate_bulk_upload.state = view_GetLocationDetail.display_name)  SET candidate_bulk_upload.district = view_GetLocationDetail.cityid,trainingpartner_id = ' + tpid;
  console.log('#updae city query', updateCityQuery);
  var resultCity = await connection.executeRawQuery(updateCityQuery);
  console.log('#FIrst query res city', resultCity);
  if (resultCity.content.affectedRows > 0) var cityflag = 1;

  var updateStateQuery = ' UPDATE candidate_bulk_upload INNER JOIN tbl_StateMaster ON (candidate_bulk_upload.state = tbl_StateMaster.display_name) SET candidate_bulk_upload.state = tbl_StateMaster.pk_stateID,trainingpartner_id = ' + tpid;
  var resultState = await connection.executeRawQuery(updateStateQuery);
  console.log('#FIrst query res state', resultState);
  if (resultState.content.affectedRows > 0) var stateflag = 1;

  var updateJobrolesQuery = ' UPDATE candidate_bulk_upload  INNER JOIN view_SectorJobRoleDetail ON (candidate_bulk_upload.jobrole = view_SectorJobRoleDetail.jobroleName and candidate_bulk_upload.sector = view_SectorJobRoleDetail.sectorName) SET candidate_bulk_upload.jobrole = view_SectorJobRoleDetail.jobroleid,trainingpartner_id = ' + tpid;
  var resultJobroles = await connection.executeRawQuery(updateJobrolesQuery);
  console.log('#FIrst query Jobroles', resultJobroles);
  if (resultJobroles.content.affectedRows > 0) var jobrolesflag = 1;

  var updateSectorQuery = ' UPDATE candidate_bulk_upload  INNER JOIN view_SectorJobRoleDetail ON (candidate_bulk_upload.sector = view_SectorJobRoleDetail.sectorName) SET candidate_bulk_upload.sector = view_SectorJobRoleDetail.sectorid,trainingpartner_id = ' + tpid;
  var resultSector = await connection.executeRawQuery(updateSectorQuery);
  console.log('#FIrst query Sectors', resultSector);
  if (resultSector.content.affectedRows > 0) { var sectorflag = 1; }

  var updateSchemeTypesQuery = ' UPDATE candidate_bulk_upload  INNER JOIN tbl_SchemeMaster  ON (candidate_bulk_upload.scheme_type = tbl_SchemeMaster.schemeType) SET candidate_bulk_upload.scheme_type = tbl_SchemeMaster.pk_schemeID,trainingpartner_id = ' + tpid;
  var resultSchemeTypes = await connection.executeRawQuery(updateSchemeTypesQuery);
  console.log('#FIrst query SchmeTYpes', resultSchemeTypes);
  if (resultSchemeTypes.content.affectedRows > 0) var schemetypesflag = 1;

  var updateStateSchemeTypesQuery = ' UPDATE candidate_bulk_upload  INNER JOIN tbl_StateMaster ON (candidate_bulk_upload.state_skill_development_mission = tbl_StateMaster.stateName) SET candidate_bulk_upload.state_skill_development_mission = tbl_StateMaster.pk_stateID,trainingpartner_id = ' + tpid;
  var resultStateSchemeTypes = await connection.executeRawQuery(updateStateSchemeTypesQuery);
  console.log('#FIrst query StateSchmeTYpes', resultStateSchemeTypes);
  if (resultStateSchemeTypes.content.affectedRows > 0 || resultStateSchemeTypes.status == true) var stateschemeflag = 1;

  var updateTrainingCentersQuery = ' UPDATE candidate_bulk_upload INNER JOIN tbl_TrainingCenters  ON (candidate_bulk_upload.training_center = tbl_TrainingCenters.trainingcenter_name) SET candidate_bulk_upload.training_center = tbl_TrainingCenters.pk_trainingcenterid,trainingpartner_id = ' + tpid;
  var resultTrainingCenters = await connection.executeRawQuery(updateTrainingCentersQuery);
  console.log('#FIrst query Training Centers', resultTrainingCenters);
  if (resultTrainingCenters.content.affectedRows > 0) var trainingcenterflag = 1;

  var setofQuery = `(candidate_name IS NULL OR candidate_name = "") OR
  (phone IS NULL OR phone = "") OR
  (age IS NULL OR age = "") OR
  (gender IS NULL OR gender = "") OR
  (address IS NULL OR address = "") OR
  (pincode IS NULL OR pincode = "") OR
  (training_type IS NULL OR training_type = "") OR
  (sdmsnsdc_enrollment_number IS NULL OR sdmsnsdc_enrollment_number = "") OR
  (training_status IS  NULL OR training_status = "") OR
  (assessment_date IS NULL OR assessment_date = "") OR
  (batch_start_date IS NULL OR batch_start_date = "") OR
  (batch_id IS NULL OR batch_id = "") OR
  (batch_end_date IS NULL OR batch_end_date = "")   
  `;

  var blankRowsinsertToErrorTabelQuery = ' INSERT IGNORE INTO `tbl_Candidate_validations`(`trainingpartner_id`,`candidate_meta_id`,`training_center`,`traning_center_name`,`district`,`district_name`,`state`,`state_name`,`sector`,`sector_name`,`jobrole`,`jobrole_name`,`scheme_type`,`scheme_type_name`,`state_skill_development_mission`,`state_skill_name`,`candidate_name`,`candidate_id`,`aadhaar_number`,`aadhaar_enrollment_id`,`alternateidtype`,`alternate_id_number`,`phone`,`email_address`,`dob`,`age`,`gender`,`address`,`pincode`,`experience_in_years`,`sdmsnsdc_enrollment_number`,`training_status`,`attendence_percentage`,`placement_status`,`employment_type`,`assessment_score`,`max_assessment_score`,`assessment_date`,`training_type`,`batch_id`,`batch_start_date`,`batch_end_date`,`willing_to_relocate`) SELECT `trainingpartner_id`,`candidate_meta_id`,`training_center`,`traning_center_name`,`district`,`district_name`,`state`,`state_name`,`sector`,`sector_name`,`jobrole`,`jobrole_name`,`scheme_type`,`scheme_type_name`,`state_skill_development_mission`,`state_skill_name`,`candidate_name`,`candidate_id`,`aadhaar_number`,`aadhaar_enrollment_id`,`alternateidtype`,`alternate_id_number`,`phone`,`email_address`,`dob`,`age`,`gender`,`address`,`pincode`,`experience_in_years`,`sdmsnsdc_enrollment_number`,`training_status`,`attendence_percentage`,`placement_status`,`employment_type`,`assessment_score`,`max_assessment_score`,`assessment_date`,`training_type`,`batch_id`,`batch_start_date`,`batch_end_date`,`willing_to_relocate` FROM `candidate_bulk_upload` WHERE ' + setofQuery + ' AND candidate_meta_id = ' + metaDataID + ' AND ' + 'trainingpartner_id = ' + tpid;
  console.log('#Blank/Empty query', blankRowsinsertToErrorTabelQuery);
  var resBlankRows = await connection.executeRawQuery(blankRowsinsertToErrorTabelQuery);

//  console.log('#all flags', stateflag, cityflag, sectorflag, jobrolesflag);
 
  try {
    //Insert into Validation Table...
    if ((stateflag == 1 && cityflag == 1 && sectorflag == 1 && jobrolesflag == 1) )  {

      var insertToValidationTableQuery = ' INSERT IGNORE INTO `tbl_Candidate_validations`(`trainingpartner_id`,`candidate_meta_id`,`training_center`,`traning_center_name`,`district`,`district_name`,`state`,`state_name`,`sector`,`sector_name`,`jobrole`,`jobrole_name`,`scheme_type`,`scheme_type_name`,`state_skill_development_mission`,`state_skill_name`,`candidate_name`,`candidate_id`,`aadhaar_number`,`aadhaar_enrollment_id`,`alternateidtype`,`alternate_id_number`,`phone`,`email_address`,`dob`,`age`,`gender`,`address`,`pincode`,`experience_in_years`,`sdmsnsdc_enrollment_number`,`training_status`,`attendence_percentage`,`placement_status`,`employment_type`,`assessment_score`,`max_assessment_score`,`assessment_date`,`training_type`,`batch_id`,`batch_start_date`,`batch_end_date`,`willing_to_relocate`) SELECT `trainingpartner_id`,`candidate_meta_id`,`training_center`,`traning_center_name`,`district`,`district_name`,`state`,`state_name`,`sector`,`sector_name`,`jobrole`,`jobrole_name`,`scheme_type`,`scheme_type_name`,`state_skill_development_mission`,`state_skill_name`,`candidate_name`,`candidate_id`,`aadhaar_number`,`aadhaar_enrollment_id`,`alternateidtype`,`alternate_id_number`,`phone`,`email_address`,`dob`,`age`,`gender`,`address`,`pincode`,`experience_in_years`,`sdmsnsdc_enrollment_number`,`training_status`,`attendence_percentage`,`placement_status`,`employment_type`,`assessment_score`,`max_assessment_score`,`assessment_date`,`training_type`,`batch_id`,`batch_start_date`,`batch_end_date`,`willing_to_relocate` FROM `candidate_bulk_upload` WHERE candidate_meta_id = ' + metaDataID + ' AND ' + ' trainingpartner_id = ' + tpid;
      //var insertToValidationTableQuery = ' INSERT IGNORE INTO `tbl_Candidate_validations`(`trainingpartner_id`,`candidate_meta_id`,`training_center`,`training_center_name`,`district`,`district_name`,`state`,`state_name`,`sector`,`sector_name`,`jobrole`,`jobrole_name`,`scheme_type`,`scheme_type_name`,`state_skill_development_mission`,`state_skill_name`,`candidate_name`,`aadhaar_number`,`aadhaar_enrollment_id`,`alternateidtype`,`alternate_id_number`,`phone`,`email_address`,`dob`,`gender`,`address`,`pincode`,`experience_in_years`,`sdmsnsdc_enrollment_number`,`training_status`,`attendence_percentage`,`placement_status`,`employment_type`,`assessment_score`,`max_assessment_score`,`assessment_date`,`training_type`,`batch_id`,`batch_start_date`,`batch_end_date`,`willing_to_relocate`) SELECT `trainingpartner_id`,`candidate_meta_id`,`training_center`,`traning_center_name`,`district`,`district_name`,`state`,`state_name`,`sector`,`sector_name`,`jobrole`,`jobrole_name`,`scheme_type`,`scheme_type_name`,`state_skill_development_mission`,`state_skill_name`,`candidate_name`,`aadhaar_number`,`aadhaar_enrollment_id`,`alternateidtype`,`alternate_id_number`,`phone`,`email_address`,date_format(str_to_date(`dob`,"%m/%d/%Y"), "%Y-%m-%d"),`gender`,`address`,`pincode`,`experience_in_years`,`sdmsnsdc_enrollment_number`,`training_status`,`attendence_percentage`,`placement_status`,`employment_type`,`assessment_score`,`max_assessment_score`,date_format(str_to_date(`assessment_date`, "%m/%d/%Y"), "%Y-%m-%d"),`training_type`,`batch_id`,date_format(str_to_date(`batch_start_date`, "%m/%d/%Y"), "%Y-%m-%d"),date_format(str_to_date(`batch_end_date`, "%m/%d/%Y"), "%Y-%m-%d"),`willing_to_relocate` FROM `candidate_bulk_upload` WHERE candidate_meta_id = ' + metaDataID + ' AND ' + ' trainingpartner_id = ' + tpid;
      console.log('#inserttoValidatinTable', insertToValidationTableQuery);
      var resultAfterInsertionvalidation = await connection.executeRawQuery(insertToValidationTableQuery);
      console.log('#Added all records into Validation table For Candiates ', resultAfterInsertionvalidation);

      //Update Sector validation with trainingpartner table into Validation Table
      /* var updateSectorValueQuery = ' UPDATE tbl_Candidate_validations, tbl_TrainingPartners SET tbl_Candidate_validations.is_sector_matched = (SELECT count(*) from tbl_TrainingPartners where `pk_trainingpartnerid` = tbl_Candidate_validations.trainingpartner_id AND FIND_IN_SET(tbl_Candidate_validations.sector,`fk_sectorID`)) WHERE candidate_meta_id = ' + metaDataID + ' AND ' + ' trainingpartner_id = ' + tpid;
      var resupdateStateVal = await connection.executeRawQuery(updateSectorValueQuery);
      console.log('#Updated is_sector_mathc flag successfully ', resupdateStateVal);
 */
      // Finally Insert into Candidate Table...  //Error table..
      var insertToErrorTabelQuery = ' INSERT IGNORE INTO `tbl_Candidates_new`(`fk_trainingpartnerid`, `fk_trainingcenterid`, `fk_cityID`, `fk_stateID`, `fk_sectorID`, `fk_jobroleID`, `fk_schemeID`, `fk_ssdm`,`candidate_meta_id`, `candidate_name`,`candidate_id`, `aadhaar_number`, `aadhaar_enrollment_id`, `alternateidtype`, `alternate_id_number`, `phone`, `email_address`, `dob`,`age`,`gender`, `address`, `pincode`, `experience_in_years`, `sdmsnsdc_enrollment_number`, `training_status`, `attendence_percentage`, `placement_status`, `employment_type`, `assessment_score`, `max_assessment_score`, `assessment_date`, `training_type`, `batch_id`, `batch_start_date`, `batch_end_date`, `is_willing_to_relocate`) SELECT `trainingpartner_id`,`training_center`,`district`,`state`,`sector`,`jobrole`,`scheme_type`,`state_skill_development_mission`,`candidate_meta_id`,`candidate_name`,`candidate_id`,`aadhaar_number`,`aadhaar_enrollment_id`,`alternateidtype`,`alternate_id_number`,`phone`,`email_address`,date_format(str_to_date(`dob`,"%m/%d/%Y"), "%Y-%m-%d"),`age`,`gender`,`address`,`pincode`,`experience_in_years`,`sdmsnsdc_enrollment_number`,`training_status`,`attendence_percentage`,`placement_status`,`employment_type`,`assessment_score`,`max_assessment_score`,date_format(str_to_date(`assessment_date`,"%m/%d/%Y"), "%Y-%m-%d"),`training_type`,`batch_id`,date_format(str_to_date(`batch_start_date`,"%m/%d/%Y"), "%Y-%m-%d"),date_format(str_to_date(`batch_end_date`,"%m/%d/%Y"), "%Y-%m-%d"),`willing_to_relocate` FROM `tbl_Candidate_validations` WHERE (trainingpartner_id > 0 AND training_center > 0 AND district > 0 AND district > 0 AND state > 0 AND sector > 0 AND jobrole > 0 AND scheme_type  > 0 AND is_sector_matched = 1) AND candidate_meta_id = ' + metaDataID + ' AND ' + ' trainingpartner_id = ' + tpid;
      // console.log('#FINALINSERT QWUR',insertToErrorTabelQuery);
      var resultErrorTable = await connection.executeRawQuery(insertToErrorTabelQuery);
      console.log('#Completed Insertion into Main Candidate table For Candiates', resultErrorTable);

      //Delete error rows from Validation Table..
      var deleteFromValidationTabelQuery = ' DELETE FROM `tbl_Candidate_validations` WHERE (trainingpartner_id > 0 AND training_center > 0 AND district > 0 AND district > 0 AND state > 0 AND sector > 0 AND jobrole > 0 AND scheme_type  > 0 AND is_sector_matched = 1) AND candidate_meta_id = ' + metaDataID + ' AND ' + ' trainingpartner_id = ' + tpid;
      var resultdeleted = await connection.executeRawQuery(deleteFromValidationTabelQuery);
      console.log('#Deleted Error Rows from validation table', resultdeleted);

      //Get the Total Failed Rows..

      //'+ total_rows + '
      var updateFailedRowsQuery = 'UPDATE tbl_candidates_metadata SET failed_records = (SELECT COUNT(*) as `f` from tbl_Candidate_validations where candidate_meta_id = ' + metaDataID + ' AND ' + ' trainingpartner_id = ' + tpid + ' ) , success_records = (SELECT ' + total_rows + ' - COUNT(*) as `f` from tbl_Candidate_validations where candidate_meta_id = ' + metaDataID + ' AND ' + ' trainingpartner_id = ' + tpid + ') WHERE id = ' + metaDataID + ' AND ' + ' trainingpartner_id = ' + tpid;
      //UPDATE tbl_candidates_metadata SET failed_records = (SELECT COUNT(*) as `f` from futurerequest_errors where meta_id = 52 AND 11 ) WHERE id = 52 AND industrypartner_id = 11
      console.log('#fialed query',updateFailedRowsQuery);
      await connection.executeRawQuery(updateFailedRowsQuery);

      //Removing data from Bulk upload Table..
      var deleteFromBulkUploadquery = ' DELETE FROM  `candidate_bulk_upload` WHERE candidate_meta_id = ' + metaDataID + ' AND ' + ' trainingpartner_id = ' + tpid;
      var removedBulkUPloadtbl = await connection.executeRawQuery(deleteFromBulkUploadquery);
      console.log('#Bulk Upload Table Removed', removedBulkUPloadtbl);

      var updateMetaDataStatus = ' UPDATE `tbl_candidates_metadata` SET `status` = "1" WHERE id = ' + metaDataID + ' AND ' + ' trainingpartner_id = ' + tpid;
      //console.log('#MetaDateStatusQueryFINALQUERY', updateMetaDataStatus);
      var metaDataStatusRes = await connection.executeRawQuery(updateMetaDataStatus);
      console.log('#File Status Updated In metadata table', metaDataStatusRes);

    }
  }
  catch (e) {
    console.log('#ERROR', e);
  }
  // if (resultTrainingCenters.content.affectedRows > 0) var trainingcenterflag = 1;
  /*
    async.series([
      async function(callback) {
        var updateStateQuery = ' UPDATE candidate_bulk_upload INNER JOIN view_GetLocationDetail ON (candidate_bulk_upload.state = view_GetLocationDetail.stateName) SET candidate_bulk_upload.state = view_GetLocationDetail.stateid,trainingpartner_id = ' + tpid;
        var resultState= await connection.executeRawQuery(updateStateQuery);
        console.log('#FIrst query res state',resultState);
        callback(null,1);
      },
   
      async function(callback) {
        var updateCityQuery = ' UPDATE candidate_bulk_upload  INNER JOIN view_GetLocationDetail  ON (candidate_bulk_upload.district = view_GetLocationDetail.cityName)  SET candidate_bulk_upload.district = view_GetLocationDetail.cityid,trainingpartner_id = ' + tpid;
        var resultCity= await connection.executeRawQuery(updateCityQuery);
        console.log('#FIrst query res city',resultCity);
        callback(null,2);
      },
   
    ], function(error, results) {
      console.log(results);
    });*/

  /* finalData = {
    message: constant.otherMessage.CSV_IMPORT_SUCCESS.message,
    successrecords: successCounter++,
    errorrecords: errorData.error.length > 0 ? errorData : 0
  };
  return finalData; */
}
async function insertRowsToTable(chunks) {
  var errorData = { error: [] };
  var finalData = {};
  var successCounter = 0;
  for (var sql of chunks) {
    try {
      var res = await connection.executeRawQuery(sql);
      if (res.status === true && res.content !== undefined && res.content.insertId > 0) {
        successCounter++;
      } else {
        var str = sql.substring(sql.indexOf(" ('") + 2);
        var errorString = str.substr(0, str.length - 3);
        //errorString = res.status.message +' :=  '+errorString;
        errorData.error.push({
          errorString
        });
      }
    } catch (ex) {
      console.log(ex);
      var str = sql.substring(sql.indexOf(" ('") + 2);
      var errorString = str.substr(0, str.length - 3);
      errorString = ex.error.message + ' :=  ' + errorString;
      errorData.error.push({
        errorString
      });
    }
  }
  finalData = {
    message: constant.otherMessage.CSV_IMPORT_SUCCESS.message,
    successrecords: successCounter++,
    errorrecords: errorData.error.length > 0 ? errorData : 0
  };
  return finalData;
}

async function trainingPartnerIndustryPartnerBulkUpload(chunks, tableName) {
  var errorData = { error: [] };
  var finalData = {};
  var successCounter = 0;
  chunks.shift();
  for (var csvrecord of chunks) {
    //Add system generated id and password
    (tableName == 'IndustryPartners') ? csvrecord.push('EMP-' + randomize('0', 5)) : csvrecord.push('TP-' + randomize('0', 5));
    csvrecord.push(bcrypt.hashSync(constant.appConfig.SYSTEM_PASSWORD, 10));
    var actualSector = csvrecord[10];
    if (csvrecord[10] != '') {
      var sectors = csvrecord[10].split(constant.appConfig.CSV_SEPERATOR);
      if (sectors != '') {
        var commaSeperatedSectorIds = '';
        for (var sectorname of sectors) {
          var sectorResponse = await otherDAL.checkSectorExistIntoMaster(sectorname);
          if (sectorResponse.status == true && sectorResponse.content.length > 0 && sectorResponse.content[0].sectorid > 0) {
            commaSeperatedSectorIds += sectorResponse.content[0].sectorid + ',';
          }
        }
        if (commaSeperatedSectorIds != '') {
          commaSeperatedSectorIds = commaSeperatedSectorIds.substring(0, commaSeperatedSectorIds.lastIndexOf(","));
        }
        csvrecord[10] = (commaSeperatedSectorIds != '' ? commaSeperatedSectorIds : null);//sector(s)
      }
    } else {
      csvrecord[10] = null;
    }
    var actualState = csvrecord[7];
    var actualCity = csvrecord[8];

    var cityStateResponse = await checkCityState(csvrecord[7], csvrecord[8]);
    if (cityStateResponse.status == true && cityStateResponse.content.length > 0 && cityStateResponse.content[0].stateid > 0 && cityStateResponse.content[0].cityid > 0) {

      csvrecord[7] = cityStateResponse.content[0].stateid;//state
      csvrecord[8] = cityStateResponse.content[0].cityid;//city

      var sqlQuery = (tableName == 'IndustryPartners') ? constant.appConfig.INDUSTRY_PARTNERS_INSERT_QUERY : constant.appConfig.TRAINING_PARTNERS_INSERT_QUERY;
      sqlQuery = sqlQuery + '(' + JSON.stringify(csvrecord) + ')';
      sqlQuery = sqlQuery.replace(/[[\]]/g, '');

      try {
        var res = await connection.executeRawQuery(sqlQuery);
        if (res.status === true && res.content !== undefined && res.content.insertId > 0) {
          successCounter++;
        } else {
          errorStringAppend(csvrecord, errorData, actualState, actualCity, actualSector);
        }
      } catch (ex) {
        errorStringAppend(csvrecord, errorData, actualState, actualCity, actualSector);
      }
    } else {
      errorStringAppend(csvrecord, errorData, actualState, actualCity, actualSector);
    }
  }
  finalData = {
    message: constant.otherMessage.CSV_IMPORT_SUCCESS.message,
    successrecords: successCounter++,
    errorrecords: errorData.error.length > 0 ? errorData : 0
  };
  return finalData;
}

async function candidateBulkUpload(chunks, tableName, trainingpartnerid, response) {
  var errorData = { error: [] };
  var finalData = {};
  var successCounter = 0;
  chunks.shift();

  //Get sectors of TrainingPartner whos uploading bulk candiate to match the sectors of TrainingPartner and candidate csv
  var getSectorsResponse = await sectorDAL.getSectorsByTrainingPartnerID(trainingpartnerid);
  if (getSectorsResponse.status == true && getSectorsResponse.content.length > 0 && getSectorsResponse.content[0].sectorName != null) {
    var sectorsOfTrainingPartner = getSectorsResponse.content[0].sectorName.split(',');
    var sectorsIdsOfTrainingPartner = getSectorsResponse.content[0].sectorid.split(',');
    var arrayString = sectorsOfTrainingPartner.toString();
    var arrayLowerString = arrayString.toLowerCase();
    var newArray = arrayLowerString.split(",");
  } else { //error
    return common.sendResponse(response, constant.otherMessage.TRAINING_PARTNER_DONT_HAVE_SECTORS, false);
  }

  for (var csvrecord of chunks) {
    var sector = csvrecord[0].trim().toLowerCase();
    var jobrole = csvrecord[1].trim();
    var candidate_name = csvrecord[2].trim();
    var trainingcenter = csvrecord[3].trim();
    var aadhar_number = csvrecord[4].trim();
    aadhar_number = aadhar_number.replace(/[“”""]/g, '');  //Need to check with windows machine
    var aadhaar_enrollment_id = csvrecord[5].trim();
    var alternateidtype = csvrecord[6].trim().toLowerCase();
    var alternate_id_number = csvrecord[7].trim();
    var phone = csvrecord[8].trim();
    var email_address = csvrecord[9].trim();
    var dob = csvrecord[10] != '' ? d3.timeFormat(dbDateFormat)(new Date(csvrecord[10].trim())) : null;
    var gender = csvrecord[11].trim().toLowerCase();
    var address = csvrecord[12].trim();
    var state = csvrecord[13].trim();
    var district = csvrecord[14].trim();
    var pincode = csvrecord[15].trim();
    var experience_in_years = csvrecord[16] != '' ? parseFloat(csvrecord[16].trim()).toFixed(1) : 0;
    var sdmsnsdc_enrollment_number = csvrecord[17].trim();
    var training_status = csvrecord[18].trim().toLowerCase();
    var attendence_percentage = csvrecord[19] != '' ? parseFloat(csvrecord[19].trim()).toFixed(2) : 0;
    var placement_status = csvrecord[20].trim().toLowerCase();
    var employment_type = csvrecord[21].trim().toLowerCase();
    var assessment_score = csvrecord[22] != '' ? parseFloat(csvrecord[22].trim()).toFixed(2) : 0;
    var max_assessment_score = csvrecord[23] != '' ? parseFloat(csvrecord[23].trim()).toFixed(2) : 0;
    var assessment_date = csvrecord[24] != '' ? d3.timeFormat(dbDateFormat)(new Date(csvrecord[24].trim())) : null;
    var training_type = csvrecord[25].trim().toLowerCase();
    var batch_id = csvrecord[26].trim();
    var batch_start_date = csvrecord[27] != '' ? d3.timeFormat(dbDateFormat)(new Date(csvrecord[27].trim())) : null;
    var batch_end_date = csvrecord[28] != '' ? d3.timeFormat(dbDateFormat)(new Date(csvrecord[28].trim())) : null;
    var scheme_type = csvrecord[29].trim();
    var state_skill_development_mission = csvrecord[30].trim();
    var is_willing_to_relocate = csvrecord[31] != '' ? csvrecord[31].trim().toLowerCase() : 'no';
    var fk_trainingpartnerid = trainingpartnerid;

    //Check for mandatory fields are blank or not
    if (sector.length == 0 || jobrole.length == 0 || candidate_name.length == 0 || trainingcenter.length == 0 || aadhar_number.length == 0
      || phone.length == 0
      || dob.length == 0
      || gender.length == 0
      || address.length == 0
      || state.length == 0
      || district.length == 0
      || pincode.length == 0
      || sdmsnsdc_enrollment_number.length == 0
      || training_status.length == 0
      || placement_status.length == 0
      || employment_type.length == 0
      || training_type.length == 0
      || batch_id.length == 0
      || batch_start_date.length == 0
      || batch_end_date.length == 0
      || scheme_type.length == 0
    ) {
      errorStringAppend(csvrecord, errorData);
      continue;
    }

    //Sector Section
    console.log(newArray);
    console.log(sector);
    var checkSector = newArray.indexOf(sector);
    console.log(checkSector);
    if (checkSector < 0) {
      console.log('sector');
      errorStringAppend(csvrecord, errorData);
      continue;
    } else {
      sector = sectorsIdsOfTrainingPartner[checkSector]; //sector id
    }

    //Jobrole Section
    var jobRoleResponse = await checkJobRoles(sector, jobrole);
    if (jobRoleResponse.status == true && jobRoleResponse.content.length > 0 && jobRoleResponse.content[0].jobroleid > 0) {
      jobrole = jobRoleResponse.content[0].jobroleid;
    } else {
      console.log('jobrole');
      errorStringAppend(csvrecord, errorData);
      continue;
    }

    //Training Center Section
    var trainingCenterResponse = await checkTrainingCenter(trainingcenter, trainingpartnerid);
    if (trainingCenterResponse.status == true && trainingCenterResponse.content.length > 0 && trainingCenterResponse.content[0].trainingcenterid > 0) {
      trainingcenter = trainingCenterResponse.content[0].trainingcenterid;
    } else {
      console.log('trainingcenter');
      trainingcenter = 0;
      /* errorStringAppend(csvrecord, errorData);
      continue; */
    }

    //Check Aadhar Card Number length
    if (aadhar_number == null || aadhar_number == '' || aadhar_number.length != constant.appConfig.AADHAR_CARD_NUMBER_LENGTH) {
      console.log('validation');
      errorStringAppend(csvrecord, errorData);
      continue;
    }

    //state / city Section
    var cityStateResponse = await checkCityState(state, district);
    if (cityStateResponse.status == true && cityStateResponse.content.length > 0 && cityStateResponse.content[0].stateid > 0 && cityStateResponse.content[0].cityid > 0) {
      state = cityStateResponse.content[0].stateid;//state
      district = cityStateResponse.content[0].cityid;//city
    } else {
      console.log('district');
      errorStringAppend(csvrecord, errorData);
      continue;
    }

    //check scheme 
    var schemeResponse = await checkSchemeType(scheme_type);
    if (schemeResponse.status == true && schemeResponse.content.length > 0 && schemeResponse.content[0].schemeid > 0) {
      scheme_type = schemeResponse.content[0].schemeid;
    } else {
      console.log('scheme_type');
      errorStringAppend(csvrecord, errorData);
      continue;
    }

    //state skill dev. mission Section
    var stateSkillDevelopmentMissionResponse = await checkState(state_skill_development_mission);
    if (stateSkillDevelopmentMissionResponse.status == true && stateSkillDevelopmentMissionResponse.content.length > 0 && stateSkillDevelopmentMissionResponse.content[0].stateid > 0) {
      state_skill_development_mission = stateSkillDevelopmentMissionResponse.content[0].stateid;
    } else {
      console.log('scheme_type');
      state_skill_development_mission = 0;
    }

    //Remove spaces from Alternate ID Type 
    alternateidtype = alternateidtype != '' ? alternateidtype.replace(/\s+/g, '') : '';
    //Remove spaces from Gender 
    gender = gender.replace(/\s+/g, '');
    //Remove spaces from Training Status
    training_status = training_status.replace(/\s+/g, '');
    //Remove spaces from Placement Status
    placement_status = placement_status.replace(/\s+/g, '');
    //Remove spaces from Employment Type
    employment_type = employment_type.replace(/\s+/g, '');
    //Remove spaces from Training Type
    training_type = training_type.replace(/\s+/g, '');

    var sqlQuery = constant.appConfig.CANDIDATES_INSERT_QUERY;
    sqlQuery += '("' + sector + '","' + jobrole + '","' + candidate_name + '","' + aadhar_number + '","' + aadhaar_enrollment_id + '","' + alternateidtype + '","' + alternate_id_number + '","' + phone + '","' + email_address + '","' + dob + '","' + gender + '","' + address + '","' + state + '","' + district + '","' + pincode + '","' + experience_in_years + '","' + sdmsnsdc_enrollment_number + '","' + training_status + '","' + attendence_percentage + '","' + placement_status + '","' + employment_type + '","' + assessment_score + '","' + max_assessment_score + '","' + assessment_date + '","' + training_type + '","' + batch_id + '","' + batch_start_date + '","' + batch_end_date + '","' + scheme_type + '","' + state_skill_development_mission + '","' + fk_trainingpartnerid + '","' + trainingcenter + '","' + is_willing_to_relocate + '")';
    console.log(sqlQuery);
    try {
      var res = await connection.executeRawQuery(sqlQuery);
      if (res.status === true && res.content !== undefined && res.content.insertId > 0) {
        successCounter++;
      } else {
        errorStringAppend(csvrecord, errorData);
        continue;
      }
    } catch (ex) {
      errorStringAppend(csvrecord, errorData);
      continue;
    }
  }
  finalData = {
    message: constant.otherMessage.CSV_IMPORT_SUCCESS.message,
    successrecords: successCounter++,
    errorrecords: errorData.error.length > 0 ? errorData : 0
  };
  return finalData;
}

async function sectorJobRolesBulkUpload(chunks, tableName) {
  var errorData = { error: [] };
  var finalData = {};
  var successCounter = 0;
  chunks.shift();
  for (var csvrecord of chunks) {
    if (csvrecord[0].trim().length == 0 || csvrecord[1].trim().length == 0 || csvrecord[2].trim().length == 0 || csvrecord[3].trim().length == 0) {
      errorStringAppend(csvrecord, errorData);
      continue;
    }
    var sectorResponse = await checkSectorIntoMaster(csvrecord[0]);
    var sectorid = '';
    if (sectorResponse.status == true && sectorResponse.content.length == 0) {//No records exist
      //Insert new record
      var sectorNameTmp = csvrecord[0].trim();
      var sectorSkillCouncilTmp = csvrecord[1].trim();

      var sqlQuery = 'INSERT INTO `tbl_SectorMaster` (`sectorName`, `sectorSkillCouncil`) VALUES ("' + sectorNameTmp + '","' + sectorSkillCouncilTmp + '")';
      try {
        var res = await connection.executeRawQuery(sqlQuery);
        if (res.status == true && res.content.insertId > 0) {
          sectorid = res.content.insertId;
        } else {
          errorStringAppend(csvrecord, errorData);
        }
      } catch (ex) {
        errorStringAppend(csvrecord, errorData);
      }
    } else {//update id
      if (sectorResponse.status == true && sectorResponse.content.length > 0 && sectorResponse.content[0].sectorid > 0) {//records exist
        sectorid = sectorResponse.content[0].sectorid;
      }
    }

    var jobRoleResponse = await checkJobRoleIntoMaster(sectorid, csvrecord[2], csvrecord[3]);

    if (jobRoleResponse.status == true && jobRoleResponse.content.length == 0) {//No records exist
      var jobroleNameTmp = csvrecord[2].trim();
      var jobroleCodeTmp = csvrecord[3].trim();
      var sqlQuery = 'INSERT INTO `tbl_JobroleMaster` (`fk_sectorID`, `jobroleName`,`jobroleCode`) VALUES ("' + sectorid + '","' + jobroleNameTmp + '","' + jobroleCodeTmp + '")';
      try {
        var res = await connection.executeRawQuery(sqlQuery);
        if (res.status == true && res.content.insertId > 0) {
          successCounter++;
        } else {
          errorStringAppend(csvrecord, errorData);
        }
      } catch (ex) {
        errorStringAppend(csvrecord, errorData);
      }
    } else { // Already exist
      successCounter++;
    }
  }
  finalData = {
    message: constant.otherMessage.CSV_IMPORT_SUCCESS.message,
    successrecords: successCounter++,
    errorrecords: errorData.error.length > 0 ? errorData : 0
  };
  return finalData;
}

function errorStringAppend(csvrecord, errorData, actualState, actualCity, actualSector) {
  if (actualState !== undefined && actualState != '') {
    csvrecord.splice(-2); //removed last 2 rows i.e. system generated id and password
    csvrecord[7] = actualState;//state
  }
  if (actualCity !== undefined && actualCity != '') {
    csvrecord[8] = actualCity;//city
  }
  if (actualSector !== undefined && actualSector != '') {
    csvrecord[10] = actualSector;//sector
  }
  var errorString = csvrecord.toString();
  errorData.error.push({
    errorString
  });
}

function checkCSVHeaders(filepath, tableName) {
  return new Promise(function (resolve, reject) {
    var csvHeaders = require('csv-headers');

    // Various options are:
    // file      : Mandatory, Absolute path of file : specified in quotes
    // delimiter : Optional, Specify the delimiter between various fields, defaults to comma(,)
    // encoding  : Optional, Specify the file encoding, defaults to 'utf8'

    var options = {
      file: filepath,
      delimiter: ','
    };

    csvHeaders(options, function (err, headers) {
      if (!err)
        var csvHeaders = '';
      switch (tableName) {
        case 'IndustryPartners':
          csvHeaders = constant.appConfig.INDUSTRY_PARTNERS_CSV_HEADERS;
          break;
        case 'TrainingPartners':
          csvHeaders = constant.appConfig.TRAINING_PARTNERS_CSV_HEADERS;
          break;
        case 'jobRoles':
          csvHeaders = constant.appConfig.SECTOR_JOBROLE_CSV_HEADERS;
          break;
        case 'candidate':
          csvHeaders = constant.appConfig.CANDIDATES_CSV_HEADERS;
          break;
        case 'FutureHiringRequest':
          csvHeaders = constant.appConfig.FUTURE_HIRING_REQUEST_CSV_HEADERS;
          break;
      }
      Array.prototype.equals = function (array, strict) {
        if (tableName == 'FutureHiringRequest') {
          array.sort();
          headers.sort();
        }
        if (!array)
          return false;

        if (arguments.length == 1)
          strict = true;

        if (this.length != array.length)
          return false;

        for (var i = 0; i < this.length; i++) {
          if (this[i] instanceof Array && array[i] instanceof Array) {
            if (!this[i].equals(array[i], strict))
              return false;
          }
          else if (strict && this[i] != array[i]) {
            return false;
          }
          else if (!strict) {
            return this.sort().equals(array.sort(), true);
          }
        }
        return true;
      };

      //Check if the uploaded CSVs headers are proper and ordered as per the sample csv
      if (!headers.equals(csvHeaders)) {
        resolve({ status: false });
      } else {
        resolve({ status: true });
      }
    });
  });

}

var emailSendService = async function (request, response) {
  debug("other.service -> emailSendService");

  //Get the email template from DB
  var result = await common.getEmailTemplateFromDB(request.body.emailType);
  if (result.status === false) {
    return common.sendResponse(response, constant.userMessages.ERR_EMAIL_SENT, false);
  }
  var subject = result.content[0].subject;
  var html = result.content[0].content;
  var host = constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_WEB_HOST;
  if (request.body.tableName == 'tbl_IndustryPartners') {
    html = html.replace("#IPID#", request.body.ipid);
  } else {
    html = html.replace("#TPID#", request.body.tpid);
  }
  html = html.replace("#PASSWORD#", constant.appConfig.SYSTEM_PASSWORD);
  html = html.replace("#NAME#", request.body.contact_person_name);

  html = replaceAll(html, "#SITEURL#", host);
  html = replaceAll(html, "#PATHURL#", constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_HOST);
  var content = html;
  var res_email = await common.sendEmailToUser(request.body.contact_email_address, subject, content);
  if (res_email.status == false) {
    return common.sendResponse(response, constant.userMessages.ERR_EMAIL_SENT, false);
  } else {
    //Update the is_email_sent flag
    var tableName = request.body.tableName;
    var table_field = 'pk_' + request.body.filedToUpdate;
    var table_value = request.body.filedUpdateValue;

    var result = await otherDAL.updateIsEmailSent(tableName, 1, table_field, table_value);
    if (result.status === false) {
      return common.sendResponse(response, constant.otherMessage.ERR_INVALID_TABLE_ACTIVE_REQUEST, false);
    } else {
      return common.sendResponse(response, constant.userMessages.MSG_EMAIL_SENT_SUCCESSFULLY, true);
    }
  }
}

var emailSignUpSendService = async function (type, userData) {
  debug("other.service -> emailSignUpSendService");
  //Get the email template from DB 
  var result = await common.getEmailTemplateFromDB(type);

  if (result.status === false) {
    return common.sendResponse(response, constant.userMessages.ERR_EMAIL_SENT, false);
  }
  var subject = result.content[0].subject;
  var html = result.content[0].content;
  if (type === 'ip_registration') {
    html = html.replace("#TYPE#", 'Employer');
  } else {
    html = html.replace("#TYPE#", 'Training Partner');
  }

  html = html.replace("#NAME#", userData.contact_person_name);
  html = replaceAll(html, "#SITEURL#", constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_WEB_HOST);
  html = replaceAll(html, "#PATHURL#", constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_HOST);

  var content = html;
  var res_email = await common.sendEmailToUser(userData.contact_email_address, subject, content);
  if (res_email.status == false) {
    //console.log("Email Sending Failed");
  } else {
    //console.log("Email Sent Succesfully");
  }
  // if (res_email.status == false) {
  //   return common.sendResponse(response, constant.userMessages.ERR_EMAIL_SENT, false);
  // } else {
  //   //Update the is_email_sent flag
  //   var tableName = request.body.tableName;
  //   var table_field = 'pk_' + request.body.filedToUpdate;
  //   var table_value = request.body.filedUpdateValue;

  //   var result = await otherDAL.updateIsEmailSent(tableName, 1, table_field, table_value);
  //   if (result.status === false) {
  //     return common.sendResponse(response, constant.otherMessage.ERR_INVALID_TABLE_ACTIVE_REQUEST, false);
  //   } else {
  //     return common.sendResponse(response, constant.userMessages.MSG_EMAIL_SENT_SUCCESSFULLY, true);
  //   }
  // }
}

var emailCronService = async function (userType) {
  //Query:SELECT * FROM `tbl_TrainingPartners` WHERE `is_email_sent` = 0 AND `status`=1 AND `is_deleted` = 0
  //Step 1 :  Get the inactivated users 
  //Step 2 : Send an email with their templates
  //Step 3 : Update email sent flag to 1.
  debug("other.service -> emailCronService");
  switch (userType) {
    case 'trainingpartner':
      var getUsers = await otherDAL.getTrainingPartnerUsers();
      var emailType = 'tp_onboarding';
      var tableName = 'tbl_TrainingPartners';
      var tableFiled = 'pk_trainingpartnerid';
      var stringReplace = "#TPID#";
      break;

    case 'industrypartner':
      var getUsers = await otherDAL.getIndustryPartnerUsers();
      var emailType = 'ip_onboarding';
      var tableName = 'tbl_IndustryPartners';
      var tableFiled = 'pk_industrypartnerid';
      var stringReplace = "#IPID#";
      break;
  }
  //Get the email template from DB
  if (getUsers.content.length > 0) {

    var host = constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_WEB_HOST;

    var result = await common.getEmailTemplateFromDB(emailType);
    if (result.status === false) {
      //return common.sendResponse(response, constant.userMessages.ERR_EMAIL_SENT, false);
      debug("err : ", constant.userMessages.ERR_EMAIL_SENT);
    }
    var subject = result.content[0].subject;

    let toBeUpdateIds = [];
    for (i = 0; i < getUsers.content.length; i++) {
      var html = result.content[0].content;
      var contact_email_address = getUsers.content[i].email;
      var partnerid = getUsers.content[i].partnerid;//ipid or tpid 
      var filedToUpdate = getUsers.content[i].id;

      html = html.replace(stringReplace, partnerid);
      html = html.replace("#PASSWORD#", constant.appConfig.SYSTEM_PASSWORD);
      html = html.replace("#NAME#", getUsers.content[i].contact_person_name);

      html = replaceAll(html, "#SITEURL#", host);
      html = replaceAll(html, "#PATHURL#", constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_HOST);

      var content = html;
      var res_email = await common.sendEmailToUser(contact_email_address, subject, content);

      if (res_email.status == false) {
        //return common.sendResponse(response, constant.userMessages.ERR_EMAIL_SENT, false);
        debug("err : ", constant.userMessages.ERR_EMAIL_SENT);
      } else {
        toBeUpdateIds.push(filedToUpdate);
      }
    }//end of for loop...
    if (toBeUpdateIds.length > 0) {
      await updateAllIdsForCron(tableName, tableFiled, toBeUpdateIds);
    }
  }
  else {
    //console.log("Sorry, No emails to send");
    debug("err : Sorry, No emails to send");
  }
}

async function updateAllIdsForCron(tableName, tableFiled, toBeUpdateIds) {
  //Update the is_email_sent flag
  var sqlQuery = 'UPDATE ' + tableName + '  SET `is_email_sent`=1 WHERE ' + tableFiled + ' IN(' + toBeUpdateIds + ')';
  var res = await connection.executeRawQuery(sqlQuery);
}

async function checkSectorIntoMaster(sectorName) {
  sectorName = (sectorName !== undefined && sectorName != null && sectorName != '') ? sectorName.trim().toLowerCase() : '';
  var result = await otherDAL.checkSectorExistIntoMaster(sectorName);
  return result;
}

async function checkJobRoleIntoMaster(sectorId, jobroleName, jobroleCode) {
  jobroleName = (jobroleName !== undefined && jobroleName != null && jobroleName != '') ? jobroleName.trim().toLowerCase() : '';
  jobroleCode = (jobroleCode !== undefined && jobroleCode != null && jobroleCode != '') ? jobroleCode.trim().toLowerCase() : '';
  var result = await otherDAL.checkJobRoleExistIntoMaster(sectorId, jobroleName, jobroleCode);
  return result;
}

async function checkJobRoles(sectorId, jobroleName) {
  jobroleName = (jobroleName !== undefined && jobroleName != null && jobroleName != '') ? jobroleName.trim().toLowerCase() : '';
  var result = await otherDAL.checkJobRoleNameExistIntoMaster(sectorId, jobroleName);
  return result;
}

async function checkTrainingCenter(trainingcenter, trainingpartnerid) {
  trainingcentername = trainingcenter.trim().toLowerCase();
  var result = await trainingCenterDAL.checkTrainingCenterName(trainingcenter, trainingpartnerid);
  return result;
}

async function checkCityState(statename, cityname) {
  statename = statename.trim().toLowerCase();
  cityname = cityname.trim().toLowerCase();
  var result = await otherDAL.checkCityStateView(statename, cityname);
  return result;
}

async function checkState(statename) {
  statename = statename.trim().toLowerCase();
  var result = await otherDAL.checkState(statename);
  return result;
}

async function checkSchemeType(schemename) {
  schemename = schemename.trim().toLowerCase();
  var result = await otherDAL.checkScheme(schemename);
  return result;
}

var getSchemeService = async function (request, response) {
  debug("other.service -> getSchemeService");
  var result = await common.getSchemeTypes();
  if (result.status === false) {
    //return common.sendResponse(response, constant.otherMessage.ERR_INVALID_GET_STATE_REQUEST, false); Need to check and update error response

  } else {
    return common.sendResponse(response, result.content, true);
  }
}

var getTrainingCenterMasterService = async function (request, response) {
  debug("other.service -> getSchemeService");
  var isValid = common.validateParams([request.params.id]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_GET_DETAIL_REQUEST, false);
  }
  var result = await trainingCenterDAL.getTrainingCentersByTrainingPartnerID(request.params.id);
  if (result.status === false) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_GET_DETAIL_REQUEST, false);
  } else {
    return common.sendResponse(response, result.content, true);
  }
}

var getStateService = async function (request, response) {
  debug("other.service -> getStateService");
  var result = await common.getStateFromView();
  if (result.status === false) {
    return common.sendResponse(response, constant.otherMessage.ERR_INVALID_GET_STATE_REQUEST, false);
  } else {
    return common.sendResponse(response, result.content, true);
  }
}

var getCityService = async function (request, response) {
  debug("other.service -> getCityService");
  //console.log(request.params.id);
  var isValid = common.validateParams([request.params.id]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_GET_CITY_REQUEST, false);
  }
  var result = await common.getCityFromView(request.params.id);
  if (result.status === false) {
    return common.sendResponse(response, constant.otherMessage.ERR_INVALID_GET_CITY_REQUEST, false);
  } else {
    return common.sendResponse(response, result.content, true);
  }
}

var getJobRolesService = async function (request, response) {
  debug("other.service -> getJobRolesService");
  var isValid = common.validateParams([request.params.id]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_GET_JOBROLE_REQUEST, false);
  }
  var result = await common.getJobroleFromView(request.params.id);
  //console.log('getJobroleFromView', result);
  if (result.status === false) {
    return common.sendResponse(response, constant.otherMessage.ERR_INVALID_GET_JOBROLE_REQUEST, false);
  } else {
    return common.sendResponse(response, result.content, true);
  }
}

/* Define function for escaping user input to be treated as 
 a literal string within a regular expression */
var escapeRegExp = function (string) {
  return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
};

/* Define functin to find and replace specified term with replacement string */
var replaceAll = function (str, term, replacement) {
  return str.replace(new RegExp(escapeRegExp(term), 'g'), replacement);
};

/**
 *
 * forgotPassword send link user register email id
 *
 * @param  {object}   request
 * @param  {object} response
 * @return {object}
 */
var forgotPasswordService = async function (request, response) {
  debug("other.service -> forgotPasswordService");
  var isValid = common.validateParams([request.body.tpid, request.body.email_address]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_FORGOT_PASSWORD_REQUEST, false);
  }
  var splitValue = request.body.tpid.split('-');
  if (splitValue.length > 0) {
    var user = '';
    switch (splitValue[0]) {
      case 'EMP':
        user = 'EMP';
        break;
      case 'TP':
        user = 'TP';
        break;
      default:
        return common.sendResponse(response, constant.userMessages.FORGOT_PASSWORD_USER_NOT_EXIST, false);
        break;
    }
  }

  var result = await otherDAL.checkUserIdIsValid(user, request.body.tpid, request.body.email_address);
  if (result.status === false) {
    return common.sendResponse(response, result);
  } else if (result.content.length === 0) {
    return common.sendResponse(response, constant.userMessages.FORGOT_PASSWORD_USER_NOT_EXIST, false);
  } else {
    var emailAddress = result.content[0].contact_email_address;//email_address
    //Send Email to user for reset password link
    var id = (user == 'EMP') ? result.content[0].industrypartnerid : result.content[0].trainingpartnerid;
    var token = randomize('A0', 16);
    var expiryDateTime = DateLibrary.getRelativeDate(new Date(), {
      operationType: "Absolute_DateTime",
      granularityType: "days",
      value: constant.appConfig.MAX_PASSWORD_TOKEN_EXPIRY_DAYS
    });
    //update this token into DB
    var fieldValueUpdate = [];
    if (user == 'EMP') {
      fieldValueUpdate.push({
        field: "fk_industrypartnerid",
        fValue: id
      });
    } else {
      fieldValueUpdate.push({
        field: "fk_trainingpartnerid",
        fValue: id
      });
    }
    fieldValueUpdate.push({
      field: "token",
      fValue: token
    });
    fieldValueUpdate.push({
      field: "expire_datetime",
      fValue: d3.timeFormat(dbDateFormat)(new Date(expiryDateTime))
    });

    var updateTokenResponse = await otherDAL.insertPasswordToken(fieldValueUpdate);
    if (updateTokenResponse.status == false) {
      return common.sendResponse(response, constant.userMessages.ERR_FORGOT_PASSWORD_TOKEN_UPDATE, false);
    }
    var userData = result;
    var resetpasswordLink = constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_WEB_HOST + 'reset-password/' + token;
    var result = await common.getEmailTemplateFromDB('forgot_password');
    if (result.status === false) {
      return common.sendResponse(response, constant.userMessages.ERR_EMAIL_SENT, false);
    }
    var subject = result.content[0].subject;
    var html = result.content[0].content;
    var host = constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_WEB_HOST;

    html = replaceAll(html, "#SITEURL#", constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_WEB_HOST);
    html = replaceAll(html, "#PATHURL#", constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_HOST);
    html = html.replace("#NAME#", userData.content[0].contact_person_name);
    html = html.replace("#LINK#", resetpasswordLink);


    var content = html;
    // var res_email = await common.sendEmailToUser(userData.contact_email_address, subject, content);
    // if(res_email.status == false) {
    //   console.log("Email Sending Failed");
    // }else{
    //   console.log("Email Sent Succesfully");
    // }

    //var content = 'Hello,<br/> Dear ' + result.content[0].contact_person_name + '- As requested, please click <a href="' + resetpasswordLink + '">here</a> to reset your password.<br/><br/>Skillconnect Team';
    var res_email = await common.sendEmailToUser(emailAddress, subject, content);
    if (res_email.status == false) {
      return common.sendResponse(response, constant.userMessages.ERR_EMAIL_SENT, false);
    }
    return common.sendResponse(response, constant.userMessages.MSG_EMAIL_SENT_SUCCESSFULLY, true);
  }
};

var resetPasswordService = async function (request, response) {
  debug("other.service -> resetPasswordService");
  var isValid = common.validateParams([request.body.token, request.body.password]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_RESET_PASSWORD_REQUEST, false);
  }
  common.sanitizeAll(request.body);
  //console.log(request.body, "BODY");
  var passwordLength = request.body.password.length;

  //Password should be minimum of 8 and maximum of 50 characters.
  if (passwordLength < constant.appConfig.RESET_PASSWORD_MINIMUM_CHARACTERS_LIMIT) {
    return common.sendResponse(response, constant.requestMessages.ERR_RESET_PASSWORD_MIN_LENGTH, false);
  }
  if (passwordLength > constant.appConfig.RESET_PASSWORD_MAXIMUM_CHARACTERS_LIMIT) {
    return common.sendResponse(response, constant.requestMessages.ERR_RESET_PASSWORD_MAX_LENGTH, false);
  }

  var getUserToken = await otherDAL.getUserPasswordToken(request.body.token);
  if (getUserToken.status == false) {
    //console.log(getUserToken, "Response 0");
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_RESET_PASSWORD_REQUEST, false);
  } else if (getUserToken.status == true) {
    if (getUserToken.content.length == 0) {
      //console.log(getUserToken, "Response length 0");
      return common.sendResponse(response, constant.requestMessages.ERR_INVALID_RESET_PASSWORD_REQUEST, false);
    } else {
      var responseData = getUserToken.content[0];
      var currentDateTime = d3.timeFormat(forgotPasswordDateFormat)(new Date());
      if (responseData.expire_datetime !== undefined && responseData.expire_datetime != '') {
        responseData.expire_datetime = d3.timeFormat(forgotPasswordDateFormat)(responseData.expire_datetime);
      }

      var res = common.compareDateString(responseData.expire_datetime, currentDateTime);

      if (res == false) {
        //set status to 0
        var fieldValueUpdate = [];
        fieldValueUpdate.push({
          field: "status",
          fValue: 0
        });
        var resultUpdatePasswordToken = await otherDAL.updatePasswordToken(responseData.passwordtokenid, fieldValueUpdate);
        return common.sendResponse(response, constant.requestMessages.ERR_TOKEN_RESET_PASSWORD_EXPIRED, false);
      }
      var idToUpdate = '';
      var user = '';
      if (responseData.trainingpartnerid !== undefined && responseData.trainingpartnerid != '' && responseData.trainingpartnerid != null) {
        idToUpdate = responseData.trainingpartnerid;
        user = 'TP';
      } else if (responseData.industrypartnerid !== undefined && responseData.industrypartnerid != '' && responseData.industrypartnerid != null) {
        idToUpdate = responseData.industrypartnerid;
        user = 'EMP';
      }
    }
  }
  var newPassword = bcrypt.hashSync(request.body.password, 10);
  if (user == 'TP') {
    var result = await trainingPartnerDAL.validateUser(idToUpdate);
  } else if (user == 'EMP') {
    var result = await industryPartnerDAL.validateUser(idToUpdate);
  } else {
    //console.log(response, "Response 1");
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_RESET_PASSWORD_REQUEST, false);
  }

  if (result.status === false) {
    return common.sendResponse(response, result);
  } else if (result.content.length == 0) {
    //console.log(response, "Response 2");
    return common.sendResponse(response, constant.userMessages.ERR_INVALID_RESET_PASSWORD_REQUEST, false);
  } else if (result.content.length > 0) {
    if (result.content[0].status == 0) {
      return common.sendResponse(response, constant.userMessages.ERR_USER_IS_NOT_ACTIVE, false);
    }
  }
  var fieldValueUpdate = [];
  fieldValueUpdate.push({
    field: "password",
    fValue: newPassword
  });
  if (user == 'TP') {
    var result = await trainingPartnerDAL.updateUserInfoById(idToUpdate, fieldValueUpdate);
    var TPinfo = await trainingPartnerDAL.getTrainingPartnerInfo(idToUpdate);
    if (TPinfo.status === true) {
      sendPwdResetEmailToUser(TPinfo.content[0].contact_person_name, TPinfo.content[0].contact_email_address);
    }
  } else {
    var result = await industryPartnerDAL.updateUserInfoById(idToUpdate, fieldValueUpdate);
    var EMPinfo = await industryPartnerDAL.getIndustryPartnerInfo(idToUpdate);
    if (EMPinfo.status === true) {
      sendPwdResetEmailToUser(EMPinfo.content[0].contact_person_name, EMPinfo.content[0].contact_email_address);
    }
  }
  if (result.status === false) {
    return common.sendResponse(response, result);
  } else {
    var fieldValueUpdate = [];
    fieldValueUpdate.push({
      field: "status",
      fValue: 0
    });
    var resultUpdatePasswordToken = await otherDAL.updatePasswordToken(responseData.passwordtokenid, fieldValueUpdate);
    return common.sendResponse(response, constant.userMessages.MSG_PASSWORD_CHANGE_SUCCESSFULLY, true);
  }
}

/* send Password Reset E-mail to both TP and IP */
var sendPwdResetEmailToUser = async function (contact_person_name, contact_email_address) {
  debug("other.service -> sendPwdResetEmailToUser");
  //Get the email template from DB 
  var result = await common.getEmailTemplateFromDB('reset_password');
  if (result.status === false) {
    return common.sendResponse(response, constant.userMessages.ERR_EMAIL_SENT, false);
  }
  var subject = result.content[0].subject;
  var html = result.content[0].content;
  var host = constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_WEB_HOST;

  html = html.replace("#NAME#", contact_person_name);
  html = replaceAll(html, "#SITEURL#", host);
  html = replaceAll(html, "#PATHURL#", constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_HOST);

  var content = html;
  var res_email = await common.sendEmailToUser(contact_email_address, subject, content);
  if (res_email.status == false) {
    //console.log("Email Sending Failed");
  } else {
    //console.log("Email Sent Succesfully");
  }
  return;
}

var sendNewHiringRequestEmailToTP = async function (BulkMailFlag, hiring_details) {
  debug("other.service -> sendNewHiringRequestEmailToTP");
  //Get the email template from DB 
  var result_hiring_request = await otherDAL.getHiringRequestDetailForMail(hiring_details.fk_trainingpartnerid, hiring_details.fk_industrypartnerid);
  var result_TP_details = await trainingPartnerDAL.getTrainingPartnerInfo(hiring_details.fk_trainingpartnerid);
  var result = await common.getEmailTemplateFromDB('new_hiring_request');
  if (result.status === false || result_hiring_request === false || result_TP_details === false) {
    return common.sendResponse(response, constant.userMessages.ERR_EMAIL_SENT, false);
  }
  var subject = result.content[0].subject;
  var html = result.content[0].content;
  var host = constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_WEB_HOST;
  html = html.replace("#NUM_OF_HIRING_REQUEST#", result_hiring_request.content.length);
  html = html.replace("#EMPLOYER_NAME#", result_hiring_request.content[0].industry_partner_name);
  html = html.replace("#NAME#", result_TP_details.content[0].contact_person_name);
  html = html.replace("#DYNAMIC_NEW_HIRING_REQUEST#", host + 'TrainingPartner-hiring-requests/1');
  html = replaceAll(html, "#SITEURL#", host);
  html = replaceAll(html, "#PATHURL#", constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_HOST);
  var content = html;
  var res_email = await common.sendEmailToUser(result_TP_details.content[0].contact_email_address, subject, content);


  if (res_email.status == false) {
    //console.log("Email Sending Failed");
  } else {
    // console.log("Email Sent Succesfully");
    if (BulkMailFlag) {
      var res_mail_sent = await otherDAL.updateFlagForBulkEmail(hiring_details.fk_trainingpartnerid, hiring_details.fk_industrypartnerid);
    } else {
      var res_mail_sent = await otherDAL.updateFlagForEmail(hiring_details.fk_candidatedetailid);
    }
    if (res_mail_sent.status === true) {
      console.log("Email Sent", res_mail_sent.status);
    } else {
      console.log("Email Sent", res_mail_sent.status);
    }
  }
  return;
}

var sendHiringRequestStatusEmailToTP = async function (hiring_request_details) {
  debug("other.service -> sendHiringRequestStatusEmailToTP");
  var result_ALL_details = await otherDAL.getAllDetailsForHiringrequestStatusEmail(hiring_request_details.hiringrequestdetailid);
  var result, html, mailTo;
  //Get the email template from DB
  var host = constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_WEB_HOST;
  if (hiring_request_details.hiring_request_status === 5) {
    result = await common.getEmailTemplateFromDB('hiring_request_cancelled');
    html = result.content[0].content;
    html = html.replace("#TP_CONTACT_PERSON_NAME#", result_ALL_details.content[0].tp_contact_person_name);
    html = html.replace("#INDUSTRY_PARTNER_NAME#", result_ALL_details.content[0].ip_legal_entity_name);
    html = html.replace("#CANDIDATE_NAME#", result_ALL_details.content[0].candidate_name);
    html = html.replace("#JOBROLE_NAME#", result_ALL_detailIsBulkMailSends.content[0].jobroleName);
    html = html.replace("#CANCEL_REASONE#", hiring_request_IsBulkMailSenddetails.cancel_reason);
    html = html.replace("#DYNAMIC_CANCEL_HIRING_REQUEST#", host + 'TrainingPartner-hiring-requests/5');
    mailTo = result_ALL_details.content[0].tp_contact_email_address;
  }
  else if (hiring_request_details.hiring_request_status === 2) {
    result = await common.getEmailTemplateFromDB('hiring_request_accepted');
    html = result.content[0].content;
    html = html.replace("#IP_CONTACT_PERSON_NAME#", result_ALL_details.content[0].ip_contact_person_name);
    html = html.replace("#TRAINING_PARTNER_NAME#", result_ALL_details.content[0].tp_legal_entity_name);
    html = html.replace("#CANDIDATE_NAME#", result_ALL_details.content[0].candidate_name);
    html = html.replace("#JOBROLE_NAME#", result_ALL_details.content[0].jobroleName);
    html = html.replace("#DYNAMIC_ACCEPT_HIRING_REQUEST#", host + 'Employer-hiring-requests/2');
    mailTo = result_ALL_details.content[0].ip_contact_email_address;
  }
  else if (hiring_request_details.hiring_request_status === 4) {
    result = await common.getEmailTemplateFromDB('hiring_request_declined');
    html = result.content[0].content;
    html = html.replace("#IP_CONTACT_PERSON_NAME#", result_ALL_details.content[0].ip_contact_person_name);
    html = html.replace("#TRAINING_PARTNER_NAME#", result_ALL_details.content[0].tp_legal_entity_name);
    html = html.replace("#CANDIDATE_NAME#", result_ALL_details.content[0].candidate_name);
    html = html.replace("#JOBROLE_NAME#", result_ALL_details.content[0].jobroleName);
    html = html.replace("#DYNAMIC_DECLINE_HIRING_REQUEST#", host + 'Employer-hiring-requests/4');
    mailTo = result_ALL_details.content[0].ip_contact_email_address;
  }
  else {
    return common.sendResponse(response, constant.userMessages.ERR_EMAIL_SENT, false);
  }

  if (result.status === false || result_ALL_details === false) {
    return common.sendResponse(response, constant.userMessages.ERR_EMAIL_SENT, false);
  }
  var subject = result.content[0].subject;
  html = replaceAll(html, "#SITEURL#", host);
  html = replaceAll(html, "#PATHURL#", constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_HOST);

  var content = html;
  var res_email = await common.sendEmailToUser(mailTo, subject, content);
  if (res_email.status == false) {
    //console.log("Email Sending Failed");
  } else {
    //console.log("Email Sent Succesfully");
  }
  return;
}

var getSchemeCentralMinistryService = async function (request, response) {
  debug("other.service -> getSchemeCentralMinistryService");
  var isValid = common.validateParams([request.params.id]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_GET_CENTRAL_MINISTRY_REQUEST, false);
  }
  var result = await common.getSchemeCentralMinistryMapping(request.params.id);

  if (result.status === false) {
    return common.sendResponse(response, constant.otherMessage.ERR_INVALID_GET_CENTRAL_MINISTRY_REQUEST, false);
  } else if (result.content.length > 0 && result.content[0].centralministryid > 0) {
    var result = await common.getSchemeCentralMinistry(result.content[0].centralministryid);
    if (result.status === false) {
      return common.sendResponse(response, constant.otherMessage.ERR_INVALID_GET_CENTRAL_MINISTRY_REQUEST, false);
    }
    return common.sendResponse(response, result.content, true);
  } else {
    return common.sendResponse(response, result.content, true);
  }
}

var createBulkUploadErrorFileService = async function (request, response) {
  debug("other.service -> createBulkUploadErrorFileService");
  try {
    let FilesToBeCreated = await otherDAL.getBulkUploadMetaDetails();
    if (FilesToBeCreated.status === true && FilesToBeCreated.content.length > 0) {
      // console.log(FilesToBeCreated.content);

      for (file of FilesToBeCreated.content) {
        var trainingpartnerid = file.trainingpartner_id;
        var meta_id = file.candidate_meta_id;
        // console.log(trainingpartnerid,"TP_ID",meta_id,"META ID");
        var errorFileName = 'error_' + trainingpartnerid + '_' + meta_id + '.csv';

        fs.writeFileSync('routes/bulk_error_files/' + errorFileName, 'sector,jobrole,candidate_id,candidate_name,training_center,aadhaar_number,aadhaar_enrollment_id,alternateidtype,alternate_id_number,phone,email_address,dob,gender,age,address,state,district,pincode,experience_in_years,sdmsnsdc_enrollment_number,training_status,attendence_percentage,placement_status,employment_type,assessment_score,max_assessment_score,assessment_date,training_type,batch_id,batch_start_date,batch_end_date,scheme_type,state_skill_development_mission,willing_to_relocate\r\n');


        let result = await otherDAL.getBulkUploadErrorStatusForFile(trainingpartnerid, meta_id);
        if (result.status == true && result.content.length > 0) {
          // console.log(result.content,"RESULT CONTENT");
          // return;
          for (var errorFile of result.content) {
            var errorFileName = 'error_' + errorFile.trainingpartner_id + '_' + errorFile.candidate_meta_id + '.csv';
            // // replace(/(\r\n|\n|\r|,)/gm,"");
            for (let ErrorField in errorFile) {
              if (errorFile[ErrorField] === null) {
                errorFile[ErrorField] = '';
              }
            }
            try {

              var training_center = errorFile.training_center !== null ? errorFile.training_center.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.training_center + ',';
              var district = errorFile.district !== null ? errorFile.district.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.district + ',';
              var state = errorFile.state !== null ? errorFile.state.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.state + ',';
              var sector = errorFile.sector !== null ? errorFile.sector.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.sector + ',';
              var jobrole = errorFile.jobrole !== null ? errorFile.jobrole.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.jobrole + ',';
              var scheme_type = errorFile.scheme_type !== null ? errorFile.scheme_type.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.scheme_type + ',';
              var state_skill_development_mission = errorFile.state_skill_development_mission !== null ? errorFile.state_skill_development_mission.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.state_skill_development_mission + ',';
              var candidate_name = errorFile.candidate_name !== null ? errorFile.candidate_name.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.candidate_name + ',';
              var candidate_id = errorFile.candidate_id !== null ? errorFile.candidate_id.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.candidate_id + ',';
              var aadhaar_number = errorFile.aadhaar_number !== null ? errorFile.aadhaar_number.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.aadhaar_number + ',';
              var aadhaar_enrollment_id = errorFile.aadhaar_enrollment_id !== null ? errorFile.aadhaar_enrollment_id.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.aadhaar_enrollment_id + ',';
              var alternateidtype = errorFile.alternateidtype !== null ? errorFile.alternateidtype.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.alternateidtype + ',';
              var alternate_id_number = errorFile.alternate_id_number !== null ? errorFile.alternate_id_number.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.alternate_id_number + ',';
              var phone = errorFile.phone !== null ? errorFile.phone.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.phone + ',';
              var email_address = errorFile.email_address !== null ? errorFile.email_address.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.email_address + ',';
              var dob = errorFile.dob + ',';
              var gender = errorFile.gender !== null ? errorFile.gender.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.gender + ',';
              var age = errorFile.age + ',';
              var address = errorFile.address !== null ? errorFile.address.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.address + ',';
              var pincode = errorFile.pincode + ',';
              var experience_in_years = errorFile.experience_in_years + ',';
              var sdmsnsdc_enrollment_number = errorFile.sdmsnsdc_enrollment_number !== null ? errorFile.sdmsnsdc_enrollment_number.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.sdmsnsdc_enrollment_number + ',';
              var training_status = errorFile.training_status !== null ? errorFile.training_status.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.training_status + ',';
              var attendence_percentage = errorFile.attendence_percentage + ',';
              var placement_status = errorFile.placement_status !== null ? errorFile.placement_status.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.placement_status + ',';
              var employment_type = errorFile.employment_type !== null ? errorFile.employment_type.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.employment_type + ',';
              var assessment_score = errorFile.assessment_score + ',';
              var max_assessment_score = errorFile.max_assessment_score + ',';
              var assessment_date = errorFile.assessment_date + ',';
              var training_type = errorFile.training_type !== null ? errorFile.training_type.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.training_type + ',';
              var batch_id = errorFile.batch_id !== null ? errorFile.batch_id.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.batch_id + ',';
              var batch_start_date = errorFile.batch_start_date + ',';
              var batch_end_date = errorFile.batch_end_date + ',';
              var willing_to_relocate = errorFile.willing_to_relocate !== null ? errorFile.willing_to_relocate.replace(/(\r\n|\n|\r|,)/gm, "") : errorFile.willing_to_relocate + ',';


              fs.appendFileSync('routes/bulk_error_files/' + errorFileName, sector + jobrole + candidate_id + candidate_name + training_center + aadhaar_number + aadhaar_enrollment_id + alternateidtype + alternate_id_number + phone + email_address + dob + gender + age + address + state + district + pincode + experience_in_years + sdmsnsdc_enrollment_number + training_status + attendence_percentage + placement_status + employment_type + assessment_score + max_assessment_score + assessment_date + training_type + batch_id + batch_start_date + batch_end_date + scheme_type + state_skill_development_mission + willing_to_relocate + '\r\n');


              //UPDATE FLAG OF FILE IN DB AS CREATED.....

              // console.log('The "data to append" was appended to file!');
            } catch (err) {
              // console.log(err,'Error writing DATA');
            }
          }

          let flag_update_result = await otherDAL.updatefileCreatedFlag(meta_id);
          if (flag_update_result.status === true) {
            // console.log("flag updated successfully..");
          }
          else {
            // console.log("could not update flaag....");
          }

        }

      }
      return common.sendResponse(response, FilesToBeCreated.content, true);
    } else {
      // console.log("Either ERROR or file already created");
      return common.sendResponse(response, constant.userMessages.ERR_NO_BULKUPLOADERROR_FOUND, false);
    }
  }
  catch (ex) {
    // console.log(ex,"ERROR");
    return common.sendResponse(response, constant.userMessages.ERR_NO_BULKUPLOADERROR_FOUND, false);
  }
}

/* send Bulk Upload Completion E-mail to TP */
var sendBulkUploadCompletionEmailToTP = async function (request, response) {
  debug("other.service -> sendBulkUploadCompletionEmailToTP");
  let TPInfo_result = await otherDAL.getTpInfoForBulkUploadCompletionMail();
  if (TPInfo_result.status == true && TPInfo_result.content.length > 0) {
    // Get the email template from DB 
    var result = await common.getEmailTemplateFromDB('bulk_upload_completion');
    if (result.status === false) {
      return common.sendResponse(response, constant.userMessages.ERR_EMAIL_SENT, false);
    }
    var subject = result.content[0].subject;
    subject = subject.replace("#MODULE#", TPInfo_result.content[0].module);

    var host = constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_WEB_HOST;
    var html, record_id,module_name, contact_person_name, contact_email_address, filename, total_records, success_records, failed_records, viewMoreLink, counter = 0;
    for (file_statastics of TPInfo_result.content) {

      html = result.content[0].content;

      trainingpartnerid = file_statastics.tpid;
      record_id = file_statastics.id;
      module_name = file_statastics.module;
      contact_person_name = file_statastics.contact_person_name;
      contact_email_address = file_statastics.contact_email_address;
      filename = file_statastics.filename;
      total_records = file_statastics.total_records ? file_statastics.total_records : 0;
      success_records = file_statastics.success_records ? file_statastics.success_records : 0;
      failed_records = file_statastics.failed_records ? file_statastics.failed_records : 0;
      viewMoreLink = constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_WEB_HOST + 'Bulk-Upload-Status';

      html = html.replace("#NAME#", contact_person_name);
      html = html.replace("#MODULE#", module_name);
      html = html.replace("#TOTAL_RECORDS#", total_records);
      html = html.replace("#SUCCESS_RECORDS#", success_records);
      html = html.replace("#FAILED_RECORDS#", failed_records);
      html = html.replace("#FILE_NAME#", filename);
      html = html.replace("#LINK#", viewMoreLink);

      html = replaceAll(html, "#SITEURL#", host);
      html = replaceAll(html, "#PATHURL#", constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_HOST);

      var content = html;
      // console.log(contact_email_address, subject, total_records, success_records, failed_records);

      var res_email = await common.sendEmailToUser(contact_email_address, subject, content);
      if (res_email.status == false) {
        // console.log("Email Sending Failed");
      } else {
        // console.log("Email Sent Succesfully");
        counter += 1;
        var flag_update_result = await otherDAL.updateMailFlagForMetadata(trainingpartnerid, record_id,'TP');
        if (flag_update_result.status === true) {
          // console.log("EMAIL FLAG UPDATED SUCCESSFULLY");
        } else {
          // console.log("EMAIL FLAG UPDATE FAILED");
        }
      }
    }
  } else {
    return common.sendResponse(response, constant.userMessages.ERR_NO_BULKUPLOADERROR_FOUND, false);
  }
}

var sendBulkUploadCompletionEmailToEmp = async function (request, response) {
  debug("other.service -> sendBulkUploadCompletionEmailToEmp");
  let EMPInfo_result = await otherDAL.getEmpInfoForBulkUploadCompletionMail();
  //console.log('# info',EMPInfo_result);
  if (EMPInfo_result.status == true && EMPInfo_result.content.length > 0) {
    // Get the email template from DB 
    var result = await common.getEmailTemplateFromDB('bulk_upload_completion');
    if (result.status === false) {
      return common.sendResponse(response, constant.userMessages.ERR_EMAIL_SENT, false);
    }
    var subject = result.content[0].subject;
    subject = subject.replace("#MODULE#", EMPInfo_result.content[0].module);

    var host = constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_WEB_HOST;
    var html, record_id,module_name, contact_person_name, contact_email_address, filename, total_records, success_records, failed_records, viewMoreLink, counter = 0;
    for (file_statastics of EMPInfo_result.content) {

      html = result.content[0].content;

      industrypartnerid = file_statastics.ipid;
      record_id = file_statastics.id;
      module_name = file_statastics.module;
      contact_person_name = file_statastics.contact_person_name;
      contact_email_address = file_statastics.contact_email_address;
      filename = file_statastics.filename;
      total_records = file_statastics.total_records ? file_statastics.total_records : 0;
      success_records = file_statastics.success_records ? file_statastics.success_records : 0;
      failed_records = file_statastics.failed_records ? file_statastics.failed_records : 0;
      viewMoreLink = constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_WEB_HOST + 'Bulk-Upload-Status';

      html = html.replace("#NAME#", contact_person_name);
      html = html.replace("#MODULE#", module_name);
      html = html.replace("#TOTAL_RECORDS#", total_records);
      html = html.replace("#SUCCESS_RECORDS#", success_records);
      html = html.replace("#FAILED_RECORDS#", total_records- success_records);
      html = html.replace("#FILE_NAME#", filename);
      html = html.replace("#LINK#", viewMoreLink);

      html = replaceAll(html, "#SITEURL#", host);
      html = replaceAll(html, "#PATHURL#", constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_HOST);

      var content = html;
      // console.log(contact_email_address, subject, total_records, success_records, failed_records);

      var res_email = await common.sendEmailToUser(contact_email_address, subject, content);
      if (res_email.status == false) {
        // console.log("Email Sending Failed");
      } else {
        // console.log("Email Sent Succesfully");
        counter += 1;
        var flag_update_result = await otherDAL.updateMailFlagForMetadata(industrypartnerid, record_id,'EMP');
        if (flag_update_result.status === true) {
          // console.log("EMAIL FLAG UPDATED SUCCESSFULLY");
        } else {
          // console.log("EMAIL FLAG UPDATE FAILED");
        }
      }
    }
  } else {
    console.log('No File Upload Record found.');
    return;
    //return common.sendResponse(response, constant.userMessages.ERR_NO_BULKUPLOADERROR_FOUND, false);
  }
}

var getTrainingCenterService = async function (request, response) {
  debug("other.service -> getTrainingCenterService");
  try {
    let result = await trainingCenterDAL.getTrainingCenterDropdownForSearch();
    if (result.status == true && result.content.length > 0) {
      return common.sendResponse(response, result.content, true);
    } else {
      return common.sendResponse(response, constant.userMessages.ERR_NO_TRAININGCENTER_FOUND, false);
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.ERR_NO_TRAININGCENTER_FOUND, false);
  }
}

var updateContactMessage = async function (request, response) {
  debug("other.service -> updateContactMessageService");
  var isValid = common.validateParams([request.body.partner_id, request.body.partner_type, request.body.firstname, request.body.message, request.body.subject, request.body.email]);
  if (!isValid) {
    return common.sendResponse(response, constant.otherMessage.ERR_INVALID_UPDATE_REQUEST, false);
  }
  common.sanitizeAll(request.body);

  var userinfo = {};
  userinfo.user_id = request.body.partner_id;
  userinfo.user_type = request.body.partner_type;
  userinfo.first_name = request.body.firstname;
  userinfo.last_name = request.body.lastname;
  userinfo.email_address = request.body.email;
  userinfo.mobile_number = request.body.mobile;
  userinfo.subject = request.body.subject;
  userinfo.message = request.body.message;

  var userKeys = Object.keys(userinfo);

  var userfieldValueInsert = [];
  userKeys.forEach(function (userKeys) {
    if (userinfo[userKeys] !== undefined) {
      var fieldValueObj = {};
      fieldValueObj = {
        field: userKeys,
        fValue: userinfo[userKeys]
      }
      userfieldValueInsert.push(fieldValueObj);
    }
  });
  try {
    let result = await otherDAL.updateContactMessage(userfieldValueInsert);
    if (result.status == true) {
      return common.sendResponse(response, constant.otherMessage.CONTACT_MESSAGE_UPDATE_SUCCESS, true);
    } else {
      return common.sendResponse(response, constant.otherMessage.ERR_INVALID_CONTACT_UPDATE_REQUEST, false);
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.otherMessage.ERR_INVALID_CONTACT_UPDATE_REQUEST, false);
  }
}

var updateContactEnquiryService = async function (request, response) {
  debug("other.sevice -> updateContactEnquiryService");
  common.sanitizeAll(request.body);
  row_id = request.body.row_id;
  email_address = request.body.eMail;
  reply = request.body.comment;
  try {
    let rescontact = await otherDAL.getDetailForContactRowService(row_id);
    if (rescontact.status == true && rescontact.content.length > 0) {
      var name = rescontact.content[0].first_name + " " + rescontact.content[0].last_name;
      //Get the email template from DB
      var result = await common.getEmailTemplateFromDB('contact_inquiry_reply');
      if (result.status === false) {
        return common.sendResponse(response, constant.userMessages.ERR_EMAIL_SENT, false);
      }
      var subject = result.content[0].subject;
      var html = result.content[0].content;
      var emailTo = rescontact.content[0].email_address;
      var inquiry = rescontact.content[0].message;
      var host = constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_WEB_HOST;
      html = html.replace("#CONTACT_REPLY#", request.body.comment);
      html = html.replace("#NAME#", name);
      html = html.replace("#INQUIRY#", inquiry);

      html = replaceAll(html, "#SITEURL#", host);
      html = replaceAll(html, "#PATHURL#", constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_HOST);
      var content = html;

      var res_email = await common.sendEmailToUser(emailTo, subject, content);
      if (res_email.status == false) {
        return common.sendResponse(response, constant.userMessages.ERR_EMAIL_SENT, false);
      } else {
        //Update the is_email_sent flag = 1 and response column
        let res = await otherDAL.updateContactEnquiryService(row_id, email_address, reply);
        if (res.status == true) {
          return common.sendResponse(response, constant.otherMessage.CONTACT_MESSAGE_UPDATE_SUCCESS, true);
        } else {
          return common.sendResponse(response, constant.otherMessage.ERR_INVALID_CONTACT_UPDATE_REQUEST, false);
        }
      }
      // Code over
    } else {
      return common.sendResponse(response, constant.otherMessage.ERR_INVALID_CONTACT_UPDATE_REQUEST, false);
    }
  } catch (ex) {
    return common.sendResponse(response, constant.userMessages.ERR_NO_CONTACT_QUERY_FOUND, false);
  }
}

var getContactService = async function (request, response) {
  debug("other.service -> getContactService");
  try {
    let result = await otherDAL.getContactService();
    return common.sendResponse(response, result.content, true);
    // return common.sendResponse(response, constant.userMessages.ERR_NO_CONTACT_QUERY_FOUND, false);
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.ERR_NO_CONTACT_QUERY_FOUND, false);
  }
}

var getContactDetailService = async function (request, response) {
  debug("other.service -> getContactDetailService");
  // console.log(request.params.id,request.params.type,request.params.row_id,"ID TYPE");
  try {
    let result = await otherDAL.getContactDetailService(request.params.id, request.params.type, request.params.row_id);
    // console.log(result.content,"RESPONSE");
    if (result.status === false) {
      return common.sendResponse(response, result);
    } else {
      response.render("partials/contactenquirydetail.ejs", { data: result.content[0] }, function (err, html) {
        response.send(html);
      });
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.ERR_NO_CONTACT_QUERY_FOUND, false);
  }
}
var getPageContentService = async function (request, response) {
  debug("other.service -> getPageContentService");
  var isValid = common.validateParams([request.body.pagetype]);
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_GET_PAGE_CONTENT_REQUEST, false);
  }
  try {
    let result = await otherDAL.getPageContent(request.body.pagetype);
    //console.log('#', result);
    if (result.status == true && result.content.length > 0) {
      return common.sendResponse(response, result.content, true);
    } else {
      return common.sendResponse(response, constant.userMessages.ERR_NO_PAGECONTENT_FOUND, false);
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}
//Lists Error File names in TP 
var getBulkUploadErrorStatusService = async function (request, response) {
  debug("other.service -> getBulkUploadErrorStatusService");
  var id = 0;
  var user_type = 'TP';
  if (request.body.trainingpartnerid !== undefined) {
    id = request.body.trainingpartnerid;
  } else if (request.body.industrypartnerid !== undefined) {
    id = request.body.industrypartnerid;
    user_type = 'EMP';
  }
  try {
    let result = await otherDAL.getBulkUploadErrorStatus(id, user_type);
    if (result.status == true && result.content.length > 0) {
      return common.sendResponse(response, result.content, true);
    } else {
      return common.sendResponse(response, constant.userMessages.ERR_NO_BULKUPLOADERROR_FOUND, false);
    }
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

var getHomePageStatistics = async function (request, response) {
  debug("other.service -> getHomePageStatistics");
  try {
    var result = { counts: [] };
    var totalFutureRequests = 0;
    var totalCandidates = 0;
    var totalHiringRequests = 0;
    var totalTrainingPartners = 0;
    var totalEmployers = 0;
    var totalPendingHiringRequests = 0;
    var totalCompletedHiringRequests = 0;
    var totalDeclinedHiringRequests = 0;
    var totalCancelledHiringRequests = 0;

    //Get Total Number of Future requests
    let totalfuturereqRes = await otherDAL.getTotalNumberOfFutureRequests();
    if (totalfuturereqRes.status === true && totalfuturereqRes.content.length > 0) {
      totalFutureRequests = totalfuturereqRes.content[0].TotalFutureHiringRequests !== null ? totalfuturereqRes.content[0].TotalFutureHiringRequests : 0;
    }

    //Get Total Number of Candidates
    let totalcandidatesRes = await otherDAL.getTotalNumberOfCandidates();
    if (totalcandidatesRes.status === true && totalcandidatesRes.content.length > 0) {
      totalCandidates = totalcandidatesRes.content[0].totalCount;
    }

    //Get Total Number of Pending Hiring Requests
    var filterObj = {};
    filterObj.hiring_request_status = 1;//Pending
    let totalpendingrequestRes = await otherDAL.getHiringRequestsCount(filterObj);
    if (totalpendingrequestRes.status === true && totalpendingrequestRes.content.length > 0) {
      totalPendingHiringRequests = totalpendingrequestRes.content[0].totalCount;
    }

    //Get Total Number of Completed Hiring Requests
    var filterObj = {};
    filterObj.hiring_request_status = 3;//Completed
    let totalcompletedrequestRes = await otherDAL.getHiringRequestsCount(filterObj);
    if (totalcompletedrequestRes.status === true && totalcompletedrequestRes.content.length > 0) {
      totalCompletedHiringRequests = totalcompletedrequestRes.content[0].totalCount;
    }

    //Get Total Number of Declined Hiring Requests posted by loggedin EMP
    var filterObj = {};
    filterObj.hiring_request_status = 4;//Declined
    let totaldeclinedrequestRes = await otherDAL.getHiringRequestsCount(filterObj);
    if (totaldeclinedrequestRes.status === true && totaldeclinedrequestRes.content.length > 0) {
      totalDeclinedHiringRequests = totaldeclinedrequestRes.content[0].totalCount;
    }

    //Get Total Number of Cancelled Hiring Requests
    var filterObj = {};
    filterObj.hiring_request_status = 5;//Cancelled
    let totalcancelledrequestRes = await otherDAL.getHiringRequestsCount(filterObj);
    if (totalcancelledrequestRes.status === true && totalcancelledrequestRes.content.length > 0) {
      totalCancelledHiringRequests = totalcancelledrequestRes.content[0].totalCount;
    }

    //Get Total Number of Training Partners
    let totaltrainingpartnersRes = await otherDAL.getTotalNumberOfTrainingPartners();
    if (totaltrainingpartnersRes.status === true && totaltrainingpartnersRes.content.length > 0) {
      totalTrainingPartners = totaltrainingpartnersRes.content[0].totalCount;
    }

    //Get Total Number of Industry Partners
    let totalindustrypartnersRes = await otherDAL.getTotalNumberOfIndustryPartners();
    if (totalindustrypartnersRes.status === true && totalindustrypartnersRes.content.length > 0) {
      totalEmployers = totalindustrypartnersRes.content[0].totalCount;
    }

    var countObj = {};
    countObj.totalFutureRequests = totalFutureRequests;
    countObj.totalCandidates = totalCandidates;
    countObj.totalTrainingPartners = totalTrainingPartners;
    countObj.totalEmployers = totalEmployers;
    countObj.totalHiringRequests = totalPendingHiringRequests + totalCompletedHiringRequests + totalDeclinedHiringRequests + totalCancelledHiringRequests; //Total HiringRequests = [pending+completed+declined+cancelled]
    result.counts.push(countObj);

    return common.sendResponse(response, result, true);
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

var getSectorJobroleMappingService = async function (request, response) {
  debug("other.service -> getSectorJobroleMappingService");
  var partnerid = request.body.trainingpartnerid;
  var partnerType = request.body.partnerType;
  if (partnerType === 'EMP') {
    var FileName = partnerid + '_sample_sector_jobrole.csv';
    if (fs.existsSync('routes/SectorJobroleEMP/' + FileName) !== true) {
      try {
        let result_sector = await sectorDAL.getSectorJobroleByIndustryPartnerID(partnerid);

        if (result_sector.status == true && result_sector.content.length > 0) {
          // console.log(result_sector,"sectors");
          var fileData = result_sector.content;
          try {
            fs.writeFileSync('routes/SectorJobroleEMP/' + FileName, 'Sector,Jobrole\r\n');
            for (jobrole of fileData) {
              for (fields in jobrole) {
                if (typeof (jobrole[fields]) === 'string') {
                  jobrole[fields] = jobrole[fields].replace(/(\r\n|\n|\r|,)/gm, "-");
                }
              }
              fs.appendFileSync('routes/SectorJobroleEMP/' + FileName, jobrole.sectorName + ',' + jobrole.jobroleName + '\r\n');
            }
            // console.log("Appended successfully..");
          } catch (e) {
            // console.log(e,"file creation failed");
          }
          // var sectors = result_sector.content[0].sectorid.split(",");
          // console.log(sectors,"sectors");
          // let result = await sectorDAL.getSectorsByTrainingPartnerID(trainingpartnerid);
          return common.sendResponse(response, result_sector.content, true);
        } else {
          // console.log(" 1 sectors");
          return common.sendResponse(response, constant.userMessages.ERR_NO_SECTOR_JOBROLE_FOUND, false);
        }
      }
      catch (ex) {
        // console.log(ex," err sectors");
        return common.sendResponse(response, constant.userMessages.ERR_NO_SECTOR_JOBROLE_FOUND, false);
      }
    } else {
      // console.log("Sample already Exists");
    }
  } else {
    var FileName = partnerid + '_sample_sector_jobrole.csv';
    if (fs.existsSync('routes/SectorJobrole/' + FileName) !== true) {
      try {
        let result_sector = await sectorDAL.getSectorJobroleByTrainingPartnerID(partnerid);

        if (result_sector.status == true && result_sector.content.length > 0) {
          // console.log(result_sector,"sectors");
          var fileData = result_sector.content;
          try {
            fs.writeFileSync('routes/SectorJobrole/' + FileName, 'Sector,Jobrole\r\n');
            for (jobrole of fileData) {
              for (fields in jobrole) {
                if (typeof (jobrole[fields]) === 'string') {
                  jobrole[fields] = jobrole[fields].replace(/(\r\n|\n|\r|,)/gm, "-");
                }
              }
              fs.appendFileSync('routes/SectorJobrole/' + FileName, jobrole.sectorName + ',' + jobrole.jobroleName + '\r\n');
            }
            // console.log("Appended successfully..");
          } catch (e) {
            // console.log(e,"file creation failed");
          }
          // var sectors = result_sector.content[0].sectorid.split(",");
          // console.log(sectors,"sectors");
          // let result = await sectorDAL.getSectorsByTrainingPartnerID(trainingpartnerid);
          return common.sendResponse(response, result_sector.content, true);
        } else {
          // console.log(" 1 sectors");
          return common.sendResponse(response, constant.userMessages.ERR_NO_SECTOR_JOBROLE_FOUND, false);
        }
      }
      catch (ex) {
        // console.log(ex," err sectors");
        return common.sendResponse(response, constant.userMessages.ERR_NO_SECTOR_JOBROLE_FOUND, false);
      }
    } else {
      // console.log("Sample already Exists");
    }
  }
}


var sendBulkUploadCompletionEmailToAdmin = async function () {
  debug("other.service -> sendBulkUploadCompletionEmailToAdmin");
  let AdminInfo_result = await otherDAL.getAdminInfoForBulkUploadCompletionMail();
  
  if (AdminInfo_result.status == true && AdminInfo_result.content.length > 0) {
    // Get the email template from DB 
    var result = await common.getEmailTemplateFromDB('bulk_upload_completion_admin');
    if (result.status === false) {
      console.log('No email template found');
      return;
      //return common.sendResponse(response, constant.userMessages.ERR_EMAIL_SENT, false);
    }
    var subject = result.content[0].subject;

    var host = constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_WEB_HOST;
    var html, record_id,module_name, contact_person_name, contact_email_address, filename, total_records, success_records, failed_records, viewMoreLink, counter = 0;
    for (file_statastics of AdminInfo_result.content) {

      html = result.content[0].content;
      
      adminid = file_statastics.adminid;
      record_id = file_statastics.id;
      module_name = file_statastics.module;
      contact_person_name = file_statastics.name;
      contact_email_address = file_statastics.email;
      filename = file_statastics.filename;
      total_records = file_statastics.total_records ? file_statastics.total_records : 0;
      success_records = file_statastics.success_records ? file_statastics.success_records : 0;
      failed_records = file_statastics.failed_records ? file_statastics.failed_records : 0;
      

      html = html.replace("#NAME#", contact_person_name);
      html = html.replace("#MODULE#", module_name);
      html = html.replace("#TOTAL_RECORDS#", total_records);
      html = html.replace("#SUCCESS_RECORDS#", success_records);
      html = html.replace("#FAILED_RECORDS#", total_records- success_records);
      html = html.replace("#FILE_NAME#", filename);
      

      html = replaceAll(html, "#SITEURL#", host);
      html = replaceAll(html, "#PATHURL#", constant.appConfig.CRON_EMAILCRONSERVICE_EMAIL_HOST);

      var content = html;
      // console.log(contact_email_address, subject, total_records, success_records, failed_records);
      var res_email = await common.sendEmailToUser(contact_email_address, subject, content);
      if (res_email.status == false) {
        // console.log("Email Sending Failed");
      } else {
        //console.log("Email Sent Succesfully");
        counter += 1;
        var flag_update_result = await otherDAL.updateMailFlagForMetadata(adminid, record_id,'Admin');
        if (flag_update_result.status === true) {
          // console.log("EMAIL FLAG UPDATED SUCCESSFULLY");
        } else {
          // console.log("EMAIL FLAG UPDATE FAILED");
        }
      }
    }
  } else {
    console.log('No File Upload Record found.');
    return;
    //return common.sendResponse(response, constant.userMessages.ERR_NO_BULKUPLOADERROR_FOUND, false);
  }
}

var getErrorFileList = async function (request, response) {
  debug("other.service -> getErrorFileList");
  try {
    let result = await otherDAL.getErrorFileList();
    for(let file of result.content){
      // console.log(file.module, "ONE");
      if(file.module === 'TrainingPartners'){
        file.error_file_name = 'Error_Admin_TP_'+file.admin_id+'_'+file.id;
      }else{
        file.error_file_name = 'Error_Admin_EMP_'+file.admin_id+'_'+file.id;
      }
    }
    return common.sendResponse(response, result.content, true);
    // return common.sendResponse(response, constant.userMessages.ERR_NO_CONTACT_QUERY_FOUND, false);
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.ERR_NO_ERROR_FILE_FOUND, false);
  }
}

var createErrorFilesAdminService = async function (request, response) {
  debug("other.service -> createErrorFilesAdminService");
  try {
      let FilesToBeCreated = await otherDAL.getErrorFileNameList();
      if (FilesToBeCreated.status === true && FilesToBeCreated.content.length > 0) {
        var errorFileName;
        for (file of FilesToBeCreated.content) {
          var meta_id = file.id;
          var module_type = file.module;
          if(file.module === 'TrainingPartners'){
            errorFileName = 'Error_Admin_TP_'+file.admin_id+'_'+file.id+'.csv';
            fs.writeFileSync('routes/Error_files_admin/' + errorFileName, 'nsdc_ssc_registration_number,training_partner_name,legal_entity_name,contact_person_name,contact_email_address,contact_mobile,address,state,district,pincode,sector\r\n');

            let dataToBeInserted = await otherDAL.getErrorDataTPAdmin(meta_id,module_type);
            if (dataToBeInserted.status === true && dataToBeInserted.content.length > 0) {
            
              for (var errorFile of dataToBeInserted.content) {
                // var errorFileName = 'error_' + errorFile.trainingpartner_id + '_' + errorFile.candidate_meta_id + '.csv';
                // // replace(/(\r\n|\n|\r|,)/gm,"");
                for (let ErrorField in errorFile) {
                  if (errorFile[ErrorField] === null) {
                    errorFile[ErrorField] = '';
                  }
                }

                try {
                  var nsdc_ssc_registration_number = typeof(errorFile.nsdc_ssc_registration_number) === 'string' ? errorFile.nsdc_ssc_registration_number.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.nsdc_ssc_registration_number + ',';
                  var training_partner_name = typeof(errorFile.training_partner_name) === 'string' ? errorFile.training_partner_name.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.training_partner_name + ',';
                  var legal_entity_name = typeof(errorFile.legal_entity_name) === 'string' ? errorFile.legal_entity_name.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.legal_entity_name + ',';
                  var contact_person_name = typeof(errorFile.contact_person_name) === 'string' ? errorFile.contact_person_name.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.contact_person_name + ',';
                  var contact_email_address = typeof(errorFile.contact_email_address) === 'string' ? errorFile.contact_email_address.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.contact_email_address + ',';
                  var contact_mobile = typeof(errorFile.contact_mobile) === 'string' ? errorFile.contact_mobile.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.contact_mobile + ',';
                  var address = typeof(errorFile.address) === 'string' ? errorFile.address.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.address + ',';
                  var state = typeof(errorFile.state) === 'string' ? errorFile.state.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.state + ',';
                  var district = typeof(errorFile.district) === 'string' ? errorFile.district.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.district + ',';
                  var pincode = typeof(errorFile.pincode) === 'string' ? errorFile.pincode.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.pincode + ',';
                  var sector = typeof(errorFile.sector) === 'string' ? errorFile.sector.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.sector + ',';

                  fs.appendFileSync('routes/Error_files_admin/'  + errorFileName, nsdc_ssc_registration_number + training_partner_name + legal_entity_name + contact_person_name + contact_email_address + contact_mobile + address + state + district + pincode + sector + '\r\n');
                  
                } catch (err) {
                  //  console.log(err,'Error writing DATA IN TP');
                }

              }
            }
          }else{
            errorFileName = 'Error_Admin_EMP_'+file.admin_id+'_'+file.id+'.csv';
            fs.writeFileSync('routes/Error_files_admin/' + errorFileName, 'employertype,legal_entity_name,contact_person_name,contact_email_address,contact_mobile,address,office_phone,state,district,pincode,sector,website\r\n');  

            let dataToBeInserted = await otherDAL.getErrorDataEMPAdmin(meta_id,module_type);
            if (dataToBeInserted.status === true && dataToBeInserted.content.length > 0) {
              for (var errorFile of dataToBeInserted.content) {
                // var errorFileName = 'error_' + errorFile.trainingpartner_id + '_' + errorFile.candidate_meta_id + '.csv';
                // // replace(/(\r\n|\n|\r|,)/gm,"");
                for (let ErrorField in errorFile) {
                  if (errorFile[ErrorField] === null) {
                    errorFile[ErrorField] = '';
                  }
                }
                try {
                  var employertype = typeof(errorFile.employertype) === 'string' ? errorFile.employertype.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.employertype + ',';
                  var legal_entity_name = typeof(errorFile.legal_entity_name) === 'string' ? errorFile.legal_entity_name.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.legal_entity_name + ',';
                  var contact_person_name = typeof(errorFile.contact_person_name) === 'string' ? errorFile.contact_person_name.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.contact_person_name + ',';
                  var contact_email_address = typeof(errorFile.contact_email_address) === 'string' ? errorFile.contact_email_address.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.contact_email_address + ',';
                  var contact_mobile = typeof(errorFile.contact_mobile) === 'string' ? errorFile.contact_mobile.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.contact_mobile + ',';
                  var address = typeof(errorFile.address) === 'string' ? errorFile.address.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.address + ',';
                  var office_phone = typeof(errorFile.office_phone) === 'string' ? errorFile.office_phone.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.office_phone + ',';
                  var state = typeof(errorFile.state) === 'string' ? errorFile.state.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.state + ',';
                  var district = typeof(errorFile.district) === 'string' ? errorFile.district.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.district + ',';
                  var pincode = typeof(errorFile.pincode) === 'string' ? errorFile.pincode.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.pincode + ',';
                  var sector = typeof(errorFile.sector) === 'string' ? errorFile.sector.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.sector + ',';
                  var website = typeof(errorFile.website) === 'string' ? errorFile.website.replace(/(\r\n|\n|\r|,)/gm, "") + ',' : errorFile.website + ',';
                  
                  fs.appendFileSync('routes/Error_files_admin/'  + errorFileName, employertype + legal_entity_name + contact_person_name + contact_email_address + contact_mobile + address + office_phone + state + district + pincode + sector + website + '\r\n');
                } catch (err) {
                  //  console.log(err,'Error writing DATA IN EMP');
                }

              }

            }
          }

          let flag_update_result = await otherDAL.updatefileCreatedFlag(meta_id);
          if (flag_update_result.status === true) {
            // console.log("flag updated successfully..");
          }
          else {
            // console.log("could not update flag....");
          }
        }
      }
    }
  catch (ex) {
    // console.log(ex,"ERROR");` `
    return common.sendResponse(response, constant.userMessages.ERR_NO_ERROR_FILE_FOUND, false);
  }
}


module.exports = {
  tableActiveService: tableActiveService,
  recordsDeleteService: recordsDeleteService,
  getModuleService: getModuleService,
  uploadCSVService: uploadCSVService,
  emailSendService: emailSendService,
  emailSignUpSendService: emailSignUpSendService,
  getStateService: getStateService,
  getSchemeService: getSchemeService,
  getTrainingCenterMasterService: getTrainingCenterMasterService,
  getCityService: getCityService,
  getJobRolesService: getJobRolesService,
  emailCronService: emailCronService,
  forgotPasswordService: forgotPasswordService,
  resetPasswordService: resetPasswordService,
  sendNewHiringRequestEmailToTP: sendNewHiringRequestEmailToTP,
  sendHiringRequestStatusEmailToTP: sendHiringRequestStatusEmailToTP,
  getSchemeCentralMinistryService: getSchemeCentralMinistryService,
  getSectorJobroleMappingService: getSectorJobroleMappingService,
  getTrainingCenterService: getTrainingCenterService,
  updateContactMessage: updateContactMessage,
  getContactService: getContactService,
  getContactDetailService: getContactDetailService,
  getPageContentService: getPageContentService,
  updateContactEnquiryService: updateContactEnquiryService,
  createBulkUploadErrorFileService: createBulkUploadErrorFileService,
  sendBulkUploadCompletionEmailToTP: sendBulkUploadCompletionEmailToTP,
  getBulkUploadErrorStatusService: getBulkUploadErrorStatusService,
  getHomePageStatistics: getHomePageStatistics,
  sendBulkUploadCompletionEmailToEmp : sendBulkUploadCompletionEmailToEmp,
  sendBulkUploadCompletionEmailToAdmin : sendBulkUploadCompletionEmailToAdmin,
  getErrorFileList: getErrorFileList,
  createErrorFilesAdminService: createErrorFilesAdminService,
};