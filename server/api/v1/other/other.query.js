var tbl_ModuleMaster = "tbl_ModuleMaster";
var view_GetLocationDetail = "view_GetLocationDetail";
var tbl_TrainingPartners = "tbl_TrainingPartners";
var tbl_IndustryPartners = "tbl_IndustryPartners";
var tbl_SectorMaster = "tbl_SectorMaster";
var tbl_JobroleMaster = "tbl_JobroleMaster";
var tbl_PasswordToken = "tbl_PasswordToken";
var tbl_StateMaster = "tbl_StateMaster";
var tbl_SchemeMaster = "tbl_SchemeMaster";
var trainingpartners_bulk_upload = "trainingpartners_bulk_upload";
var view_SectorJobRoleDetail = "view_SectorJobRoleDetail";
var tbl_ActivityLog = 'tbl_ActivityLog';
var tbl_ContactUS = "tbl_ContactUS";
var tbl_PageContents = 'tbl_PageContents';
var view_HiringDetail = 'view_HiringDetail';
var tbl_HiringRequestsDetail = 'tbl_HiringRequestsDetail';
var tbl_Candidate_validations = 'tbl_Candidate_validations';
var tbl_candidates_metadata = 'tbl_candidates_metadata';
var futurerequest_errors = 'futurerequest_errors';
var tbl_Candidates = "tbl_Candidates";
var tbl_FutureHiringRequests = "tbl_FutureHiringRequests";
var tbl_UserMaster = 'tbl_UserMaster';
var tbl_settings = "tbl_settings";


var query = {
  updateTableQuery: {
    table: '',
    update: [],
    filter: {
      field: '',
      operator: 'EQ',
      value: ''
    }
  },
  removeRecordQuery: {
    table: '',
    delete: [],
    filter: {
      field: '',
      operator: 'EQ',
      value: ''
    }
  },
  checkIpidIsValidQuery: {
    table: tbl_IndustryPartners,
    select: [{
      field: 'contact_email_address',
      alias: 'contact_email_address'
    }, {
      field: 'contact_person_name',
      alias: 'contact_person_name'
    }, {
      field: 'pk_industrypartnerid',
      alias: 'industrypartnerid'
    }],
    filter: {
      and: [{
        field: 'status',
        operator: 'EQ',
        value: 1
      }, {
        field: 'ipid',
        operator: 'EQ',
        value: ''
      }, {
        field: 'contact_email_address',
        operator: 'EQ',
        value: ''
      }, {
        field: 'is_deleted',
        operator: 'EQ',
        value: 0
      }]
    }
  },
  checkTpidIsValidQuery: {
    table: tbl_TrainingPartners,
    select: [{
      field: 'contact_email_address',
      alias: 'contact_email_address'
    }, {
      field: 'contact_person_name',
      alias: 'contact_person_name'
    }, {
      field: 'pk_trainingpartnerid',
      alias: 'trainingpartnerid'
    }],
    filter: {
      and: [{
        field: 'status',
        operator: 'EQ',
        value: 1
      }, {
        field: 'tpid',
        operator: 'EQ',
        value: ''
      }, {
        field: 'contact_email_address',
        operator: 'EQ',
        value: ''
      }, {
        field: 'is_deleted',
        operator: 'EQ',
        value: 0
      }]
    }
  },
  getModuleQuery: {
    table: tbl_ModuleMaster,
    select: [{
      field: 'pk_moduleID',
      alias: 'moduleid'
    },
    {
      field: 'moduleName',
      alias: 'modulename'
    },
    {
      field: 'adminCreated',
      alias: 'admincreated'
    }]
  },
  checkCityStateViewQuery: {
    table: view_GetLocationDetail,
    select: [{
      field: 'stateid',
    }, {
      field: 'cityid',
    }],
    filter: {
      and: [{
        field: 'stateName',
        operator: 'EQ',
        value: ''
      }, {
        field: 'cityName',
        operator: 'EQ',
        value: ''
      }]
    }
  },
  checkSectorJobRoleViewQuery: {
    table: view_SectorJobRoleDetail,
    select: [{
      field: 'sectorid',
    }, {
      field: 'jobroleid',
    }],
    filter: {
      and: [{
        field: 'sectorName',
        operator: 'EQ',
        value: ''
      }, {
        field: 'jobroleName',
        operator: 'EQ',
        value: ''
      }]
    }
  },
  checkSchemeExistIntoMasterQuery: {
    table: tbl_SchemeMaster,
    select: [{
      field: 'pk_schemeID',
    }],
    filter: {
      and: [{
        field: 'schemeType',
        operator: 'EQ',
        value: ''
      }]
    }
  },
  checkSectorQuery: {
    table: tbl_SectorMaster,
    select: [{
      field: 'pk_sectorID',
    }],
    filter: {
      and: [{
        field: 'sectorName',
        operator: 'EQ',
        value: ''
      }]
    }
  },
  checkStateQuery: {
    table: tbl_StateMaster,
    select: [{
      field: 'pk_stateID',
      alias: 'stateid'
    }],
    filter: {
      and: [{
        field: 'stateName',
        operator: 'EQ',
        value: ''
      }]
    }
  },
  checkSectorExistIntoMasterQuery: {
    table: tbl_SectorMaster,
    select: [{
      field: 'pk_sectorID',
      alias: 'sectorid'
    }],
    filter: {
      and: [{
        field: 'sectorName',
        operator: 'EQ',
        value: ''
      }, {
        field: 'status',
        operator: 'EQ',
        value: 1
      }]
    }
  },
  checkJobRoleExistIntoMasterQuery: {
    table: tbl_JobroleMaster,
    select: [{
      field: 'pk_jobroleID',
      alias: 'jobroleid'
    }],
    filter: {
      and: [{
        field: 'fk_sectorID',
        operator: 'EQ',
        value: ''
      }, {
        field: 'jobroleName',
        operator: 'EQ',
        value: ''
      }, {
        field: 'jobroleCode',
        operator: 'EQ',
        value: ''
      }, {
        field: 'status',
        operator: 'EQ',
        value: 1
      }]
    }
  },
  checkJobRoleNameExistIntoMasterQuery: {
    table: tbl_JobroleMaster,
    select: [{
      field: 'pk_jobroleID',
      alias: 'jobroleid'
    }],
    filter: {
      and: [{
        field: 'fk_sectorID',
        operator: 'EQ',
        value: ''
      }, {
        field: 'jobroleName',
        operator: 'EQ',
        value: ''
      }, {
        field: 'status',
        operator: 'EQ',
        value: 1
      }]
    }
  },
  getTrainingUsersQuery: {
    table: tbl_TrainingPartners,
    select: [{
      field: 'pk_trainingpartnerid',
      alias: 'id'
    },
    {
      field: 'tpid',
      alias: 'partnerid'
    },
    {
      field: 'contact_email_address',
      alias: 'email'
    },
    {
      field: 'contact_person_name',
      alias: 'contact_person_name'
    },
    {
      field: 'is_email_sent',
      alias: 'is_email_sent'
    },
    {
      field: 'status',
      alias: 'status'
    }],
    filter: {
      and: [{
        field: 'is_email_sent',
        operator: 'EQ',
        value: 0
      }, {
        field: 'status',
        operator: 'EQ',
        value: 1
      }, {
        field: 'is_deleted',
        operator: 'EQ',
        value: 0
      }]
    }
  },
  getIndustryUsersQuery: {
    table: tbl_IndustryPartners,
    select: [{
      field: 'pk_industrypartnerid',
      alias: 'id'
    },
    {
      field: 'ipid',
      alias: 'partnerid'
    },
    {
      field: 'contact_email_address',
      alias: 'email'
    },
    {
      field: 'contact_person_name',
      alias: 'contact_person_name'
    },
    {
      field: 'is_email_sent',
      alias: 'is_email_sent'
    },
    {
      field: 'status',
      alias: 'status'
    }],
    filter: {
      and: [{
        field: 'is_email_sent',
        operator: 'EQ',
        value: 0
      }, {
        field: 'status',
        operator: 'EQ',
        value: 1
      }, {
        field: 'is_deleted',
        operator: 'EQ',
        value: 0
      }]
    }
  },
  insertPasswordTokenQuery: {
    table: tbl_PasswordToken,
    insert: [],
  },
  getUserPasswordTokenQuery: {
    table: tbl_PasswordToken,
    select: [{
      field: 'pk_passwordTokenID',
      alias: 'passwordtokenid'
    },
    {
      field: 'fk_trainingpartnerid',
      alias: 'trainingpartnerid'
    },
    {
      field: 'fk_industrypartnerid',
      alias: 'industrypartnerid'
    },
    {
      field: 'token',
      alias: 'token'
    }, {
      field: 'expire_datetime',
      alias: 'expire_datetime'
    }, {
      field: 'status',
      alias: 'status'
    }],
    filter: {
      and: [{
        field: 'token',
        operator: 'EQ',
        value: ''
      }, {
        field: 'status',
        operator: 'EQ',
        value: 1
      }]
    }
  },
  updatePasswordTokenQuery: {
    table: tbl_PasswordToken,
    update: [],
    filter: {
      and: [{
        field: 'pk_passwordTokenID',
        operator: 'EQ',
        value: ''
      }, {
        field: 'status',
        operator: 'EQ',
        value: 1
      }]
    }
  },
  recordExistInTableQuery: {
    table: '',
    select: [],
    filter: {
      field: '',
      operator: 'EQ',
      value: ''
    }
  },
  checkSchemeQuery: {
    table: tbl_SchemeMaster,
    select: [{
      field: 'pk_schemeID',
      alias: 'schemeid'
    }],
    filter: {
      and: [{
        field: 'schemeType',
        operator: 'EQ',
        value: ''
      }]
    }
  },
  getRecentActivitiesQuery: {
    table: tbl_ActivityLog,
    select: [{
      field: 'activity',
      alias: 'activityname'
    }, {
      field: 'DATE_FORMAT(created_date,"%d %b %Y")',
      encloseField: false,
      alias: 'activitytytime'
    }],
    filter: {
      and: [{
        field: 'userid',
        operator: 'EQ',
        value: ''
      }, {
        field: 'user_type',
        operator: 'EQ',
        value: ''
      }]
    },
    sortby: [{
      field: 'created_date',
      order: 'DESC'
    }]
  },
  getHiringRequestDetailForMailQuery: {
    table: view_HiringDetail,
    select: [{
      field: 'industry_partner_name',
      encloseField: false,
    }],
    filter: {
      and: [{
        field: 'trainingpartnerid',
        operator: 'EQ',
        value: ''
      }, {
        field: 'industrypartnerid',
        operator: 'EQ',
        value: ''
      },{
        field: 'hiring_request_status',
        operator: 'EQ',
        value: 1
      }, {
        field: 'log_status',
        operator: 'EQ',
        value: 1
      }
    ]
    },
  },
  getAllDetailsForHiringrequestStatusEmailQuery : {
    join: {
      table: tbl_HiringRequestsDetail,
      alias: 'HRD',
      joinwith: [{
          table: 'tbl_TrainingPartners',
          alias: 'TP',
          type: 'LEFT',
          joincondition: {
              and: [{
                  table: 'TP',
                  field: 'pk_trainingpartnerid',
                  operator: 'eq',
                  value: {
                      table: 'HRD',
                      field: 'fk_trainingpartnerid'
                  }
              }]
          }
      },{
        table: 'tbl_IndustryPartners',
          alias: 'IP',
          type: 'LEFT',
          joincondition: {
              and: [{
                  table: 'IP',
                  field: 'pk_industrypartnerid',
                  operator: 'eq',
                  value: {
                      table: 'HRD',
                      field: 'fk_industrypartnerid'
                  }
              }]
          }
      },{
        table: 'tbl_Candidates',
            alias: 'CD',
            type: 'LEFT',
            joincondition: {
                and: [{
                    table: 'CD',
                    field: 'pk_candidateid',
                    operator: 'eq',
                    value: {
                        table: 'HRD',
                        field: 'fk_candidateid'
                    }
                }]
            }
      },{
        table: 'tbl_JobroleMaster',
            alias: 'JRM',
            type: 'LEFT',
            joincondition: {
                and: [{
                    table: 'JRM',
                    field: 'pk_jobroleID',
                    operator: 'eq',
                    value: {
                        table: 'CD',
                        field: 'fk_jobroleID'
                    }
                }]
            }
      }]
  },
  select: [{
      field: 'TP.contact_person_name',
      encloseField: false,
      alias: 'tp_contact_person_name'
  },{
    field: 'TP.legal_entity_name',
    encloseField: false,
    alias: 'tp_legal_entity_name'
  },{
    field: 'TP.contact_email_address',
    encloseField: false,
    alias: 'tp_contact_email_address'
  },{
    field: 'IP.contact_person_name',
    encloseField: false,
    alias: 'ip_contact_person_name'
  },{
    field: 'IP.legal_entity_name',
    encloseField: false,
    alias: 'ip_legal_entity_name'
  },{
    field: 'IP.contact_email_address',
    encloseField: false,
    alias: 'ip_contact_email_address'
  },{
    field: 'CD.candidate_name',
    encloseField: false,
    alias: 'candidate_name'
  },{
    field: 'JRM.jobroleName',
    encloseField: false,
    alias: 'jobroleName',
  },],
  filter: {
      and: [{
          field: 'HRD.pk_hiringrequestdetailid',
          encloseField: false,
          operator: 'EQ',
          value: ''
      }] 
  }},

  getBulkUploadErrorStatusQuery:{
    join: {
      table: tbl_Candidate_validations,
      alias: 'ERLIST',
      joinwith: [{
          table: tbl_candidates_metadata,
          alias: 'MTDATA',
          type: 'LEFT',
          joincondition: {
              and: [{
                  table: 'ERLIST',
                  field: 'trainingpartner_id',
                  operator: 'eq',
                  value: {
                      table: 'MTDATA',
                      field: 'trainingpartner_id'
                  }
              }]
          }
      }]
    },
    select: [{
      field: 'ERLIST.trainingpartner_id',
      encloseField: false,
    },{
      field: 'ERLIST.candidate_meta_id',
      encloseField: false,
    },{
      field: 'MTDATA.filename',
      encloseField: false,
    },{
      field: 'DATE_FORMAT(MTDATA.created,"%d %b %Y")',
      encloseField: false,
      alias : 'created'
    },{
      field: 'MTDATA.failed_records',
      encloseField: false,
      alias : 'error_count'
    }],
    filter: {
      and: [{
        field: 'ERLIST.trainingpartner_id',
        encloseField: false,
        operator: 'EQ',
        value: ''
      },{
        field: 'ERLIST.candidate_meta_id',
        encloseField: false,
        operator: 'EQ',
        value: {
          table: 'MTDATA',
          field: 'id'
          }
      },{
        field: 'MTDATA.is_error_file_created',
        encloseField: false,
        operator: 'EQ',
        value: 1
      }]
    },
    groupby: [{
      field: 'ERLIST.trainingpartner_id',
      encloseField: false,
    }, {
      field: 'ERLIST.candidate_meta_id',
      encloseField: false,
    }]
  },

  getBulkUploadMetaDetailsQuery:{
    join: {
      table: tbl_Candidate_validations,
      alias: 'ERLIST',
      joinwith: [{
          table: tbl_candidates_metadata,
          alias: 'MTDATA',
          type: 'LEFT',
          joincondition: {
              and: [{
                  table: 'ERLIST',
                  field: 'trainingpartner_id',
                  operator: 'eq',
                  value: {
                      table: 'MTDATA',
                      field: 'trainingpartner_id'
                  }
              }]
          }
      }]
    },
    select: [{
      field: 'ERLIST.trainingpartner_id',
      encloseField: false,
    },{
      field: 'ERLIST.candidate_meta_id',
      encloseField: false,
    },{
      field: 'MTDATA.filename',
      encloseField: false,
    },{
      field: 'DATE_FORMAT(MTDATA.created,"%d %b %Y")',
      encloseField: false,
      alias : 'created'
    },{
      field: 'MTDATA.failed_records',
      encloseField: false,
      alias : 'error_count'
    }],
    filter: {
      and: [{
        field: 'MTDATA.is_error_file_created',
        encloseField: false,
        operator: 'EQ',
        value: 0
      },{
        field: 'ERLIST.candidate_meta_id',
        encloseField: false,
        operator: 'EQ',
        value: {
          table: 'MTDATA',
          field: 'id'
      }
      }]
    },
    groupby: [{
      field: 'ERLIST.trainingpartner_id',
      encloseField: false,
    }, {
      field: 'ERLIST.candidate_meta_id',
      encloseField: false,
    }],
    orderby: [{
      field: 'ERLIST.candidate_meta_id',
      encloseField: false,
    }]
  },

  getBulkUploadErrorStatusForFileQuery: {
    table: tbl_Candidate_validations,
    alias: 'ERLIST',
    select: [{
      field: 'ERLIST.trainingpartner_id',
      encloseField: false,
    },{
      field: 'ERLIST.candidate_meta_id',
      encloseField: false,
    },{
      field: 'ERLIST.traning_center_name',
      encloseField: false,
      alias: 'training_center',      
    },{
      field: 'ERLIST.district_name',
      encloseField: false,
      alias: 'district',
    },{
      field: 'ERLIST.state_name',
      encloseField: false,
      alias: 'state',
    },{
      field: 'ERLIST.age',
      encloseField: false,
    },{
      field: 'ERLIST.candidate_id',
      encloseField: false,
    },{
      field: 'ERLIST.sector_name',
      encloseField: false,
      alias: 'sector',
    },{
      field: 'ERLIST.jobrole_name',
      encloseField: false,
      alias: 'jobrole',
    },{
      field: 'ERLIST.scheme_type_name',
      encloseField: false,
      alias: 'scheme_type',
    },{
      field: 'ERLIST.state_skill_name',
      encloseField: false,
      alias: 'state_skill_development_mission',
    },{
      field: 'ERLIST.candidate_name',
      encloseField: false,
    },{
      field: 'ERLIST.aadhaar_number',
      encloseField: false,
    },{
      field: 'ERLIST.aadhaar_enrollment_id',
      encloseField: false,
    },{
      field: 'ERLIST.alternateidtype',
      encloseField: false,
    },{
      field: 'ERLIST.alternate_id_number',
      encloseField: false,
    },{
      field: 'ERLIST.phone',
      encloseField: false,
    },{
      field: 'ERLIST.email_address',
      encloseField: false,
    },{
      field: 'ERLIST.dob',
      encloseField: false,
    },{
      field: 'ERLIST.gender',
      encloseField: false,
    },{
      field: 'ERLIST.address',
      encloseField: false,
    },{
      field: 'ERLIST.pincode',
      encloseField: false,
    },{
      field: 'ERLIST.experience_in_years',
      encloseField: false,
    },{
      field: 'ERLIST.sdmsnsdc_enrollment_number',
      encloseField: false,
    },{
      field: 'ERLIST.training_status',
      encloseField: false,
    },{
      field: 'ERLIST.attendence_percentage',
      encloseField: false,
    },{
      field: 'ERLIST.placement_status',
      encloseField: false,
    },{
      field: 'ERLIST.employment_type',
      encloseField: false,
    },{
      field: 'ERLIST.assessment_score',
      encloseField: false,
    },{
      field: 'ERLIST.max_assessment_score',
      encloseField: false,
    },{
      field: 'ERLIST.assessment_date',
      encloseField: false,
    },{
      field: 'ERLIST.training_type',
      encloseField: false,
    },{
      field: 'ERLIST.batch_id',
      encloseField: false,
    },{
      field: 'ERLIST.batch_start_date',
      encloseField: false,
    },{
      field: 'ERLIST.batch_end_date',
      encloseField: false,
    },{
      field: 'ERLIST.willing_to_relocate',
      encloseField: false,
  }],
    filter: {
      and: [{
        field: 'ERLIST.trainingpartner_id',
        encloseField: false,
        operator: 'EQ',
        value: ''
      },{
        field: 'ERLIST.candidate_meta_id',
        encloseField: false,
        operator: 'EQ',
        value: ''
      }]
    },
    // groupby: [{
    //   field: 'ERLIST.trainingpartner_id',
    //   encloseField: false,
    // }, {
    //   field: 'ERLIST.candidate_meta_id',
    //   encloseField: false,
    // }]
    orderby: [{
      field: 'ERLIST.candidate_meta_id',
      encloseField: false,
    }]
  },

  
  getTpInfoForBulkUploadCompletionMailQuery:{
    join: {
      table: tbl_TrainingPartners,
      alias: 'TP',
      joinwith: [{
          table: tbl_candidates_metadata,
          alias: 'MTDATA',
          type: 'RIGHT',
          joincondition: {
              and: [{
                  table: 'TP',
                  field: 'pk_trainingpartnerid',
                  operator: 'eq',
                  value: {
                      table: 'MTDATA',
                      field: 'trainingpartner_id'
                  }
              }]
          }
      }]
    },
    select: [{
      field: 'TP.pk_trainingpartnerid',
      encloseField: false,
      alias: 'tpid',
    },{
      field: 'MTDATA.id',
      encloseField: false,
    },{
      field: 'MTDATA.filename',
      encloseField: false,
    },{
      field: 'DATE_FORMAT(MTDATA.created,"%d %b %Y")',
      encloseField: false,
      alias : 'created'
    },{
      field: 'MTDATA.total_records',
      encloseField: false,
    },{
      field: 'MTDATA.success_records',
      encloseField: false,
    },{
      field: 'MTDATA.failed_records',
      encloseField: false,
    },{
      field: 'MTDATA.module',
      encloseField: false,
    },{
      field: 'TP.contact_person_name',
      encloseField: false,
      alias : 'contact_person_name'
    },{
      field: 'TP.contact_email_address',
      encloseField: false,
      alias : 'contact_email_address'
    }],
    filter: {
      and: [{
        field: 'MTDATA.status',
        encloseField: false,
        operator: 'EQ',
        value: 1
      },{
        field: 'MTDATA.is_mail_sent',
        encloseField: false,
        operator: 'EQ',
        value: 0
      },{
        field: 'MTDATA.is_error_file_created',
        encloseField: false,
        operator: 'EQ',
        value: 1
      }]
    },
  },

  updateMailFlagForMetadataQuery: {
    table: tbl_candidates_metadata,
    update: [{
      field: 'is_mail_sent',
      fValue: 1
    }],
    filter: {
      and:[ {
        field: 'trainingpartner_id',
        operator: 'EQ',
        value: ''
      },
      {
        field: 'id',
        operator: 'EQ',
        value: ''
      },{
        field: 'is_mail_sent',
        operator: 'EQ',
        value: 0
      }]
    }
  },
  updateMailFlagForMetadataQueryEMP: {
    table: tbl_candidates_metadata,
    update: [{
      field: 'is_mail_sent',
      fValue: 1
    }],
    filter: {
      and:[ {
        field: 'industrypartner_id',
        operator: 'EQ',
        value: ''
      },
      {
        field: 'id',
        operator: 'EQ',
        value: ''
      },{
        field: 'is_mail_sent',
        operator: 'EQ',
        value: 0
      }]
    }
  },
  updateMailFlagForMetadataQueryAdmin: {
    table: tbl_candidates_metadata,
    update: [{
      field: 'is_mail_sent',
      fValue: 1
    }],
    filter: {
      and:[ {
        field: 'admin_id',
        operator: 'EQ',
        value: ''
      },
      {
        field: 'id',
        operator: 'EQ',
        value: ''
      },{
        field: 'is_mail_sent',
        operator: 'EQ',
        value: 0
      }]
    }
  },
  updatefileCreatedFlagQuery: {
    table: tbl_candidates_metadata,
    update: [{
      field: 'is_error_file_created',
      fValue: 1
    }],
    filter: {
      and:[{
        field: 'id',
        operator: 'EQ',
        value: ''
      },{
        field: 'is_error_file_created',
        operator: 'EQ',
        value: 0
      }]
    }
  },

  getPageContentQuery: {
    table: tbl_PageContents,
    select: [{
      field: 'id',
    }, {
      field: 'page_content'
    }],
    filter: {
      and: [{
        field: 'page_type',
        operator: 'EQ',
        value: ''
      }, {
        field: 'status',
        operator: 'EQ',
        value: 1
      }]
    }
  },
  updateContactMessageQuery : {
    table: tbl_ContactUS,
    insert:[]
  },
  updateContactEnquiryServiceQuery: {
    table: tbl_ContactUS,
    update:[{
      field: 'response',
      fValue: ''
    },{
      field: 'is_mail_sent',
      fValue: 1
    }],
    filter:{
      and: [
        {
          field: 'id',
          operator: 'EQ',
          value: ''
        },
        {
          field: 'email_address',
          operator: 'EQ',
          value: ''
        }
      ]
    }
  },
  updateFlagForEmailQuery: {
    table: tbl_HiringRequestsDetail,
    update:[{
      field: 'is_email_sent_for_new_request',
      fValue: 1
    }],
    filter:{
      and: [
        {
          field: 'fk_candidatedetailid',
          operator: 'EQ',
          value: ''
        },{
          field: 'hiring_request_status',
          operator: 'EQ',
          value: 1
        },{
          field: 'log_status',
          operator: 'EQ',
          value: 1
        }
      ]
    }
  },
  updateFlagForBulkEmailQuery: {
    table: tbl_HiringRequestsDetail,
    update:[{
      field: 'is_email_sent_for_new_request',
      fValue: 1
    }],
    filter:{
      and: [
        {
          field: 'fk_trainingpartnerid',
          operator: 'EQ',
          value: ''
        },{
          field: 'fk_industrypartnerid',
          operator: 'EQ',
          value: ''
        },{
          field: 'hiring_request_status',
          operator: 'EQ',
          value: 1
        },{
          field: 'log_status',
          operator: 'EQ',
          value: 1
        }
      ]
    }
  },
  getContactServiceQuery : {
    table: tbl_ContactUS,
    select: [{
      field: 'id',
      alias: 'row_id'
    },{
      field: 'first_name',
      alias: 'firstName'
    },{
      field: 'last_name',
      alias: 'lastName'
    },{
      field: 'subject',
      alias: 'subject',
    },{
      field: 'message',
      alias: 'message',
    },{
      field: 'email_address',
      alias: 'eMail',
    },{
      field: 'user_id',
      alias: 'partner_id'
    },{
      field: 'user_type',
      alias: 'partner_type',
    },{
      field: 'mobile_number',
      alias: 'mobile',
    },{
      field: 'is_mail_sent',
      alias: 'is_mail_sent',
    }]
    // filter: {
    //   and: []
    // }
  },
  
  getContactDetailTPServiceQuery : {
    join: {
      table: tbl_ContactUS,
      alias: 'CUS',
      joinwith: [{
          table: 'tbl_TrainingPartners',
          alias: 'TP',
          type: 'LEFT',
          joincondition: {
              and: [{
                  table: 'TP',
                  field: 'pk_trainingpartnerid',
                  operator: 'eq',
                  value: {
                      table: 'CUS',
                      field: 'user_id'
                  }
              }]
          }
      }]
  },
  select: [{
      field: 'TP.legal_entity_name',
      encloseField: false,
      alias: 'partner_name'
  },{
    field: 'CUS.first_name',
    encloseField: false,
    alias: 'firstName'
  },{
    field: 'CUS.last_name',
    encloseField: false,
    alias: 'lastName'
  },{
    field: 'CUS.subject',
    encloseField: false,
    alias: 'subject',
  },{
    field: 'CUS.message',
    encloseField: false,
    alias: 'message',
  },{
    field: 'CUS.email_address',
    encloseField: false,
    alias: 'eMail',
  },{
    field: 'CUS.user_id',
    encloseField: false,
    alias: 'partner_id'
  },{
    field: 'CUS.user_type',
    encloseField: false,
    alias: 'partner_type',
  },{
    field: 'CUS.mobile_number',
    encloseField: false,
    alias: 'mobile',
  }],
  filter: {
      and: [{
          field: 'CUS.id',
          encloseField: false,
          operator: 'EQ',
          value: ''
      }] 
  }},
  getDetailForContactRowServiceQuery:{
    table: tbl_ContactUS,
    select: [{
      field: 'first_name',
      alias: 'first_name'
    },
    {
      field: 'last_name',
      alias: 'last_name'
    },
    {
      field: 'email_address',
      alias: 'email_address'
    },
    {
      field: 'message',
      alias: 'message'
    }],
    filter: {
      and: [{
        field: 'id',
        operator: 'EQ',
        value: ''
      }, {
        field: 'is_mail_sent',
        operator: 'EQ',
        value: 0
      }, {
        field: 'status',
        operator: 'EQ',
        value: 1
      }]
    }
  },
  getContactDetailIPServiceQuery : {
    join: {
      table: tbl_ContactUS,
      alias: 'CUS',
      joinwith: [{
          table: 'tbl_IndustryPartners',
          alias: 'IP',
          type: 'LEFT',
          joincondition: {
              and: [{
                  table: 'IP',
                  field: 'pk_industrypartnerid',
                  operator: 'eq',
                  value: {
                      table: 'CUS',
                      field: 'user_id'
                  }
              }]
          }
      }]
  },
  select: [{
      field: 'IP.legal_entity_name',
      encloseField: false,
      alias: 'partner_name'
  },{
    field: 'CUS.first_name',
    encloseField: false,
    alias: 'firstName'
  },{
    field: 'CUS.last_name',
    encloseField: false,
    alias: 'lastName'
  },{
    field: 'CUS.subject',
    encloseField: false,
    alias: 'subject',
  },{
    field: 'CUS.message',
    encloseField: false,
    alias: 'message',
  },{
    field: 'CUS.email_address',
    encloseField: false,
    alias: 'eMail',
  },{
    field: 'CUS.user_id',
    encloseField: false,
    alias: 'partner_id'
  },{
    field: 'CUS.user_type',
    encloseField: false,
    alias: 'partner_type',
  },{
    field: 'CUS.mobile_number',
    encloseField: false,
    alias: 'mobile',
  }],
  filter: {
      and: [{
          field: 'CUS.id',
          encloseField: false,
          operator: 'EQ',
          value: ''
      }] 
  }},
  getBulkUploadErrorStatusQueryEMP: {
    join: {
      table: futurerequest_errors,
      alias: 'ERLIST',
      joinwith: [{
        table: tbl_candidates_metadata,
        alias: 'MTDATA',
        type: 'LEFT',
        joincondition: {
          and: [{
            table: 'ERLIST',
            field: 'fk_industrypartnerid',
            operator: 'eq',
            value: {
              table: 'MTDATA',
              field: 'industrypartner_id'
            }
          }]
        }
      }]
    },
    select: [{
      field: 'ERLIST.fk_industrypartnerid',
      encloseField: false,
    }, {
      field: 'ERLIST.meta_id',
      encloseField: false,
    }, {
      field: 'MTDATA.filename',
      encloseField: false,
    }, {
      field: 'MTDATA.failed_records',
      encloseField: false,
    }, {
      field: 'DATE_FORMAT(MTDATA.created,"%d %b %Y")',
      encloseField: false,
      alias: 'created'
    }],
    filter: {
      and: [{
        field: 'ERLIST.fk_industrypartnerid',
        encloseField: false,
        operator: 'EQ',
        value: ''
      }, {
        field: 'ERLIST.meta_id',
        encloseField: false,
        operator: 'EQ',
        value: {
          table: 'MTDATA',
          field: 'id'
        }
      },{
        field: 'MTDATA.is_error_file_created',
        encloseField: false,
        operator: 'EQ',
        value: 1
      }]
    },
    groupby: [{
      field: 'ERLIST.fk_industrypartnerid',
      encloseField: false,
    }, {
      field: 'ERLIST.meta_id',
      encloseField: false,
    }]
  },
  getTotalNumberOfFutureRequestsQuery: {
    table: tbl_FutureHiringRequests,
    select: [{
      field: 'SUM(total)',
      encloseField: false,
      alias: 'TotalFutureHiringRequests'
    }],
    filter: {
      and: [{
        field: 'status',
        operator: 'EQ',
        value: 1
      }, {
        field: 'is_deleted',
        operator: 'EQ',
        value: 0
      }]
    }
  },
  getTotalNumberOfCandidatesQuery: {
    table: tbl_Candidates,
    select: [{
      field: 'pk_candidateid',
      aggregation: 'count',
      alias: 'totalCount'
    }],
    filter: {
      and: [{
        field: 'status',
        operator: 'EQ',
        value: 1
      }, {
        field: 'is_deleted',
        operator: 'EQ',
        value: 0
      }]
    }
  },
  getTotalNumberOfTrainingPartnersQuery: {
    table: tbl_TrainingPartners,
    select: [{
      field: 'pk_trainingpartnerid',
      aggregation: 'count',
      alias: 'totalCount'
    }],
    filter: {
      and: [{
        field: 'status',
        operator: 'EQ',
        value: 1
      }, {
        field: 'is_deleted',
        operator: 'EQ',
        value: 0
      }]
    }
  },
  getTotalNumberOfIndustryPartnersQuery: {
    table: tbl_IndustryPartners,
    select: [{
      field: 'pk_industrypartnerid',
      aggregation: 'count',
      alias: 'totalCount'
    }],
    filter: {
      and: [{
        field: 'status',
        operator: 'EQ',
        value: 1
      }, {
        field: 'is_deleted',
        operator: 'EQ',
        value: 0
      }]
    }
  },
  getHiringRequestsCountQuery: {
    table: view_HiringDetail,
    select: [{
      field: 'hiringrequestdetailid',
      aggregation: 'count',
      alias: 'totalCount'
    }],
    filter: {}
  },
  getEmpInfoForBulkUploadCompletionMailQuery:{
    join: {
      table: tbl_IndustryPartners,
      alias: 'EMP',
      joinwith: [{
          table: tbl_candidates_metadata,
          alias: 'MTDATA',
          type: 'RIGHT',
          joincondition: {
              and: [{
                  table: 'EMP',
                  field: 'pk_industrypartnerid',
                  operator: 'eq',
                  value: {
                      table: 'MTDATA',
                      field: 'industrypartner_id'
                  }
              }]
          }
      }]
    },
    select: [{
      field: 'EMP.pk_industrypartnerid',
      encloseField: false,
      alias: 'ipid',
    },{
      field: 'MTDATA.id',
      encloseField: false,
    },{
      field: 'MTDATA.filename',
      encloseField: false,
    },{
      field: 'DATE_FORMAT(MTDATA.created,"%d %b %Y")',
      encloseField: false,
      alias : 'created'
    },{
      field: 'MTDATA.total_records',
      encloseField: false,
    },{
      field: 'MTDATA.success_records',
      encloseField: false,
    },{
      field: 'MTDATA.failed_records',
      encloseField: false,
    },{
      field: 'MTDATA.module',
      encloseField: false,
    },{
      field: 'EMP.contact_person_name',
      encloseField: false,
      alias : 'contact_person_name'
    },{
      field: 'EMP.contact_email_address',
      encloseField: false,
      alias : 'contact_email_address'
    }],
    filter: {
      and: [{
        field: 'MTDATA.status',
        encloseField: false,
        operator: 'EQ',
        value: 1
      },{
        field: 'MTDATA.is_mail_sent',
        encloseField: false,
        operator: 'EQ',
        value: 0
      },{
        field: 'MTDATA.is_error_file_created',
        encloseField: false,
        operator: 'EQ',
        value: 1
      }]
    },
    sortby: [{
      field: 'id',
      order: 'ASC'
    }]
  },
  getAdminInfoForBulkUploadCompletionMailQuery : {
    join: {
      table: tbl_UserMaster,
      alias: 'USER',
      joinwith: [{
          table: tbl_candidates_metadata,
          alias: 'MTDATA',
          type: 'RIGHT',
          joincondition: {
              and: [{
                  table: 'USER',
                  field: 'pk_userID',
                  operator: 'eq',
                  value: {
                      table: 'MTDATA',
                      field: 'admin_id'
                  }
              }]
          }
      }]
    },
    select: [{
      field: 'USER.pk_userID',
      encloseField: false,
      alias: 'adminid',
    },{
      field: 'MTDATA.id',
      encloseField: false,
    },{
      field: 'MTDATA.filename',
      encloseField: false,
    },{
      field: 'DATE_FORMAT(MTDATA.created,"%d %b %Y")',
      encloseField: false,
      alias : 'created'
    },{
      field: 'MTDATA.total_records',
      encloseField: false,
    },{
      field: 'MTDATA.success_records',
      encloseField: false,
    },{
      field: 'MTDATA.failed_records',
      encloseField: false,
    },{
      field: 'MTDATA.module',
      encloseField: false,
    },{
      field: 'USER.name',
      encloseField: false,
      alias : 'name'
    },{
      field: 'USER.alternate_email_address',
      encloseField: false,
      alias : 'email'
    }],
    filter: {
      and: [{
        field: 'MTDATA.status',
        encloseField: false,
        operator: 'EQ',
        value: 1
      },{
        field: 'MTDATA.is_mail_sent',
        encloseField: false,
        operator: 'EQ',
        value: 0
      },{
        field: 'MTDATA.is_error_file_created',
        encloseField: false,
        operator: 'EQ',
        value: 1
      }]
    },
    sortby: [{
      field: 'id',
      order: 'ASC'
    }]
  },
  getApiKeyQuery: {
    table: tbl_settings,
    select: [{
      field: 'api_key',
      encloseField: false,
      alias: 'api_key',
    }],
    filter: {
      and: [{
        field: 'type',
        operator: 'EQ',
        value: ''
      }]
    }
  },
  getErrorFileListQuery: {
    table: tbl_candidates_metadata,
    select: [{
      field: 'id',
      encloseField: false,
    },
    {
      field: 'admin_id',
      encloseField: false,
    },
    {
      field: 'filename',
      encloseField: false,
    },
    {
      field: 'module',
      encloseField: false,
    },
    {
      field: '(total_records- success_records)',
      alias: 'remaining',
      encloseField: false,
    },
    {
      field: 'failed_records',
      encloseField: false,
    },
    {
      field: 'is_error_file_created',
      encloseField: false,
    },
    {
      field: 'DATE_FORMAT(created,"%d %b %Y")',
      encloseField: false,
      alias: 'created',
    }],
    filter: {
      and: [{
        field: 'admin_id',
        operator: 'EQ',
        value: ''
      },{
        field: 'module',
        operator: 'EQ',
        value: ['IndustryPartners','TrainingPartners']
      },{
        field: 'is_error_file_created',
        operator: 'EQ',
        value: 1
      }]
    }
  },
  getErrorFileNameListQuery: {
    table: tbl_candidates_metadata,
    select: [{
      field: 'id',
      encloseField: false,
    },
    {
      field: 'admin_id',
      encloseField: false,
    },
    {
      field: 'filename',
      encloseField: false,
    },
    {
      field: 'module',
      encloseField: false,
    },
    {
      field: '(total_records- success_records)',
      alias: 'remaining',
      encloseField: false,
    },
    {
      field: 'failed_records',
      encloseField: false,
    },
    {
      field: 'is_error_file_created',
      encloseField: false,
    },
    {
      field: 'DATE_FORMAT(created,"%d %b %Y")',
      encloseField: false,
      alias: 'created',
    }],
    filter: {
      and: [{
        field: 'admin_id',
        operator: 'EQ',
        value: ''
      },{
        field: 'module',
        operator: 'EQ',
        value: ['IndustryPartners','TrainingPartners']
      },{
        field: 'is_error_file_created',
        operator: 'EQ',
        value: 0
      }]
    }
  },
  getErrorDataEMPAdminQuery:  {
    join: {
      table: tbl_candidates_metadata,
      alias: 'MTDATA',
      joinwith: [{
        table: 'industrypartners_erros',
        alias: 'IPER',
        type: 'LEFT',
        joincondition: {
            and: [{
                table: 'MTDATA',
                field: 'id',
                operator: 'eq',
                value: {
                    table: 'IPER',
                    field: 'meta_id'
                }
            }]
        }
    }]
    },
    select: [{
      field: 'MTDATA.id',
      encloseField: false,
    },{
      field: 'IPER.ipid',
      encloseField: false,
    },{
      field: 'IPER.employertype', //need to be changed to employertype_name
      encloseField: false,
      alias: 'employertype'
    },{
      field: 'IPER.legal_entity_name',
      encloseField: false,
    },{
      field: 'IPER.contact_person_name',
      encloseField: false,
    },{
      field: 'IPER.contact_email_address',
      encloseField: false,
    },{
      field: 'IPER.contact_mobile',
      encloseField: false,
    },{
      field: 'IPER.address',
      encloseField: false,
    },{
      field: 'IPER.sector_name',
      encloseField: false,
      alias: 'sector'
    },{
      field: 'IPER.office_phone',
      encloseField: false,
    },{
      field: 'IPER.website',
      encloseField: false,
    },{
      field: 'IPER.state_name',
      encloseField: false,
      alias: 'state'
    },{
      field: 'IPER.district_name',
      encloseField: false,
      alias: 'district'
    },{
      field: 'IPER.pincode',
      encloseField: false,
    }],
    filter: {
      and: [{
        field: 'MTDATA.id',
        encloseField: false,
        operator: 'EQ',
        value: ''
      },{
        field: 'MTDATA.module',
        encloseField: false,
        operator: 'EQ',
        value: ''
      },{
        field: 'MTDATA.is_error_file_created',
        encloseField: false,
        operator: 'EQ',
        value: 0
      }]
    },
  },

  getErrorDataTPAdminQuery:  {
    join: {
      table: tbl_candidates_metadata,
      alias: 'MTDATA',
      joinwith: [{
          table: 'trainingpartners_errors',
          alias: 'TPER',
          type: 'LEFT',
          joincondition: {
              and: [{
                  table: 'MTDATA',
                  field: 'id',
                  operator: 'eq',
                  value: {
                      table: 'TPER',
                      field: 'meta_id'
                  }
              }]
          }
      }]
    },
    select: [{
      field: 'MTDATA.id',
      encloseField: false,
    },{
      field: 'TPER.tpid',
      encloseField: false,
    },{
      field: 'TPER.nsdc_ssc_registration_number',
      encloseField: false,
    },{
      field: 'TPER.training_partner_name',
      encloseField: false,
    },{
      field: 'TPER.legal_entity_name',
      encloseField: false,
    },{
      field: 'TPER.contact_person_name',
      encloseField: false,
    },{
      field: 'TPER.contact_email_address',
      encloseField: false,
    },{
      field: 'TPER.contact_mobile',
      encloseField: false,
    },{
      field: 'TPER.address',
      encloseField: false,
    },{
      field: 'TPER.sector_name',
      encloseField: false,
      alias: 'sector'
    },{
      field: 'TPER.state_name',
      encloseField: false,
      alias: 'state'
    },{
      field: 'TPER.district_name',
      encloseField: false,
      alias: 'district'
    },{
      field: 'TPER.pincode',
      encloseField: false,
    }],
    filter: {
      and: [{
        field: 'MTDATA.id',
        encloseField: false,
        operator: 'EQ',
        value: ''
      },{
        field: 'MTDATA.module',
        encloseField: false,
        operator: 'EQ',
        value: ''
      },{
        field: 'MTDATA.is_error_file_created',
        encloseField: false,
        operator: 'EQ',
        value: 0
      }]
    },
  }
}

module.exports = query;