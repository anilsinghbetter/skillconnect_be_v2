var express = require('express');
var services = require('./other.service');
var middleware = require('../../../middleware');
var formidable = require('express-formidable');
var constant = require('../constant');

var router = express.Router();
module.exports = router;

 router.use(formidable.parse({
  keepExtensions: true,
  //uploadDir: constant.appConfig.MEDIA_UPLOAD_DIR
}));


//table Active API
router.post('/active', middleware.checkAccessToken,middleware.userRightsByAPI, middleware.logger, services.tableActiveService);
router.post('/delete/:type', middleware.logger, services.recordsDeleteService);//middleware.checkAccessToken,middleware.userRightsByAPI, 
router.get("/getModule", middleware.checkAccessToken,middleware.userRightsByAPI, middleware.logger, services.getModuleService);
router.post('/upload',   middleware.logger, services.uploadCSVService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/getState',   middleware.logger, services.getStateService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/getScheme',   middleware.logger, services.getSchemeService);
router.post('/getTrainingCenterMaster/:id',   middleware.logger, services.getTrainingCenterMasterService);
router.post('/getCity/:id',  middleware.logger, services.getCityService);//middleware.checkAccessToken,middleware.userRightsByAPI, 
router.post('/getJobRoles/:id',  middleware.logger, services.getJobRolesService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/getSchemeCentralMinistry/:id', middleware.logger, services.getSchemeCentralMinistryService);//middleware.checkAccessToken,middleware.userRightsByAPI,

router.post('/getSectorJobroleMapping', middleware.logger, services.getSectorJobroleMappingService);
router.post('/getTrainingCenters/:type', middleware.logger, services.getTrainingCenterService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/emailSend', middleware.logger,services.emailSendService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post("/forgotPassword",middleware.logger,services.forgotPasswordService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post("/resetPassword",middleware.logger,services.resetPasswordService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/updateContactEnquiry', middleware.logger, services.updateContactEnquiryService);

router.post("/updateContactMessage",middleware.logger,services.updateContactMessage);
router.post("/getContact",middleware.logger,services.getContactService);
router.get("/getContactDetail/:id/:type/:row_id",middleware.logger,services.getContactDetailService);
router.post("/getPageContent",middleware.logger,services.getPageContentService);//middleware.checkAccessToken,middleware.userRightsByAPI,



//Temp Route to check file generation
router.post("/createBulkUploadErrorFiles",middleware.logger,services.createBulkUploadErrorFileService);
router.post("/sendBulkUploadCompletionEmailToTP",middleware.logger,services.sendBulkUploadCompletionEmailToTP);
router.post("/sendBulkUploadCompletionEmailToEMP",middleware.logger,services.sendBulkUploadCompletionEmailToEmp);

router.post('/getBulkUploadErrorStatus', middleware.logger, services.getBulkUploadErrorStatusService);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/getHomePageStatistics', middleware.logger, services.getHomePageStatistics);//middleware.checkAccessToken,middleware.userRightsByAPI,
router.post('/sendemailtoadminbulkupload', middleware.logger, services.sendBulkUploadCompletionEmailToAdmin);//middleware.checkAccessToken,middleware.userRightsByAPI,

router.post("/errorFileList",middleware.logger,services.getErrorFileList);//middleware.checkAccessToken,middleware.userRightsByAPI,
//temp and to be deleted  
router.post("/createErrorFiles",middleware.logger,services.createErrorFilesAdminService);//middleware.checkAccessToken,middleware.userRightsByAPI,