var debug = require('debug')('server:api:v1:user:service');
var d3 = require("d3");
var bcrypt = require('bcrypt');
var DateLibrary = require('date-management');
var uuid = require('uuid');
var common = require('../common');
var constant = require('../constant');
var userDAL = require('./user.DAL');
var dbDateFormat = constant.appConfig.DB_DATE_FORMAT;
var config = require('../../../../config');
var connection = require('../../../helper/connection');

/**
 * Admin signin service
 * 
 * @param  {object}  request 
 * @param  {object}  response
 * @return {object}  
 */
var signinServiceAdmin = async function (request, response) {
  debug("user.service -> signin service admin");
  let isValid = common.validateParams([request.body.email, request.body.password])
  if (!isValid) {
    return common.sendResponse(response, constant.requestMessages.ERR_INVALID_SIGNIN_REQUEST, false);
  }
  try {
    var email = request.body.email;
    var password = request.body.password;

    let result = await userDAL.userLoginAdmin(email);
    if (result.status === false) {
      return common.sendResponse(response, result);
    } else if (result.content.length === 0) {
      return common.sendResponse(response, constant.userMessages.ERR_INVALID_EMAIL_AND_PASSWORD, false);
    }
    bcrypt.compare(password, result.content[0].password, async function (err, ress) {//First argurment is the input string to be compared
      if (ress) {
        let res_access_token = await checkAndCreateAccessToken(request, result.content);
        if (res_access_token.status === true) {
          var session = request.session;
          session.userInfo = {
            userId: res_access_token.data[0].user_id,
            name: res_access_token.data[0].name,
            email: res_access_token.data[0].email,
            mobile: res_access_token.data[0].mobile,
            role: res_access_token.data[0].role,
            userTypeID: res_access_token.data[0].user_type_id
          };
          var userRights = [];
          for (var i = 0; i < res_access_token.data.length; i++) {
            var objRights = {};
            objRights.moduleName = res_access_token.data[i].module_name;
            objRights.displayName = res_access_token.data[i].displayName;
            objRights.canView = res_access_token.data[i].can_view;
            objRights.canAdd = res_access_token.data[i].can_add;
            objRights.canEdit = res_access_token.data[i].can_edit;
            objRights.canDelete = res_access_token.data[i].can_delete;
            objRights.adminCreated = res_access_token.data[i].admin_created;
            userRights.push(objRights);
          }
          request.session.userInfo.userRights = userRights;
          return common.sendResponse(response, res_access_token, true);
        } else {
          return common.sendResponse(response, res_access_token, true);
        }
      } else {
        return common.sendResponse(response, constant.userMessages.ERR_INVALID_EMAIL_AND_PASSWORD, false);
      }
    });
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

/**
 * check and create access token
 * 
 * @param  {Object}   request  [description]
 * @param  {Object}   userInfo [description]
 * @return {object}            [description]
 */
async function checkAndCreateAccessToken(request, userInfo) {
  try {
    var userId = userInfo[0].user_id;
    var token = uuid.v1();
    var deviceId = request.headers["udid"];
    var deviceType = (request.headers["device-type"]).toLowerCase();
    var expiryDateTime = DateLibrary.getRelativeDate(new Date(), {
      operationType: "Absolute_DateTime",
      granularityType: "hours",
      value: constant.appConfig.MAX_ACCESS_TOKEN_EXPIRY_HOURS
    });
    var host = request.hostname;
    let result = await userDAL.exprieAccessToken(userId, deviceId, host);
    
    let res_access_token = await userDAL.createAccessToken(userId, token, expiryDateTime, deviceId, host);
    
    let res_user_tran = await userDAL.checkUserTransaction(deviceId, deviceType);

    if (res_user_tran.content[0].totalCount > 0) {
      var fieldValueUpdate = [];
      fieldValueUpdate.push({
        field: "isLogedIn",
        fValue: 1
      });
      fieldValueUpdate.push({
        field: "lastLoginDatetime",
        fValue: d3.timeFormat(dbDateFormat)(new Date())
      });
      let res_transaction = userDAL.updateUserTransaction(deviceId, deviceType, fieldValueUpdate);
    }
    else {
      let res_transaction = userDAL.createUserTransaction(deviceId, deviceType);
    }
    return { status: true, "access_token": token, data: userInfo };
  }
  catch (ex) {
    console.log(" error in checkAndCreateAccessToken");
    throw ex;
  }
};

/**
 *
 * signout user and expire access-token and session
 *
 * @param  {object}   request
 * @param  {object} response
 * @return {object}
 */
var signoutService = async function (request, response) {
  debug("user.service -> signoutService");
  var deviceId = request.headers["udid"];
  var userId = request.session.userInfo.userId;
  var deviceType = request.headers["device-type"];
  var host = request.hostname;
  var result = await userDAL.exprieAccessToken(userId, deviceId, host);
  request.session.destroy();
  if (result.status === false) {
    return common.sendResponse(response, constant.userMessages.ERR_SIGNOUT_IS_NOT_PROPER, false);
  } else {
    var fieldValueUpdate = [];
    fieldValueUpdate.push({
      field: "isLogedIn",
      fValue: "0"
    });
    var result = await userDAL.updateUserTransaction(deviceId, deviceType, fieldValueUpdate);
    if (result.status === false) {
      return common.sendResponse(response, constant.userMessages.ERR_SIGNOUT_IS_NOT_PROPER, false);
    } else {
      return common.sendResponse(response, constant.userMessages.MSG_SIGNOUT_SUCCESSFULLY, true);
    }
  }
};

/**
 * get admin service
 * 
 * @param  {object}   request
 * @param  {object} response
 * @returns {object}
 */
var getAdminService = async function (request, response) {
  debug("user.service -> getAdminService");
  var result = await userDAL.getUserByUserType(constant.userType.admin);
  if (result.status == false) {
    return common.sendResponse(response, result.error, false);
  }
  return common.sendResponse(response, result.content, true);
}

/**
 * get role service
 * 
 * @param  {object}   request
 * @param  {object}   response 
 * @return {object}
 */
var getRoleService = async function (request, response) {
  debug("user.service -> getRoleService");
  if (request.params.role_id === undefined || request.params.user_type_id === undefined || request.params.role_id === 0 || request.params.user_type_id === 0) {
    return common.sendResponse(response, constant.userMessages.ERR_INVALID_GET_ROLE_REQUEST, false);
  } else {
    var roleID = request.params.role_id;
    var userTypeID = request.params.user_type_id;
    var result = await userDAL.getRole(roleID, userTypeID);
    if (result.status === false) {
      return common.sendResponse(response, result, false);
    }
    return common.sendResponse(response, result.content, true);
  }
}

/**
 * get user type service
 * 
 * @param  {object}  request 
 * @param  {object}  response
 * @return {object}  
 */
var getUserTypeService = async function (request, response) {
  debug("user.service -> getUserTypeService");
  var result = await userDAL.getUserType();
  if (result.status === false) {
    return common.sendResponse(response, result, false);
  }
  return common.sendResponse(response, result.content, true);
}
var addUpdateAdminService = async function(request, response) {
  debug("user.service -> addUpdateAdminService");

  var isValidObject = common.validateObject([request.body]);
  var isValid = common.validateParams([request.body.user_id,request.body.name,request.body.email,request.body.country_code,request.body.mobile,request.body.password,request.body.role_id]);
  if(!isValidObject){
    return common.sendResponse(response,constant.requestMessages.ERR_INVALID_SIGNUP_REQUEST,false);
  }
  else if(!isValid){
     return common.sendResponse(response,constant.requestMessages.ERR_INVALID_ADD_UPDATE_REQUEST_OF_ADMIN_USER,false);
  }

  var userID = request.body.user_id;
  var userinfo = {};
  userinfo.fk_roleID = request.body.role_id;
  userinfo.name = request.body.name;
  userinfo.email = request.body.email;
  userinfo.countryCode = request.body.country_code;
  userinfo.mobile = request.body.mobile;
  if (request.body.password != undefined && request.body.password != "") {
    userinfo.password = md5(request.body.password);
  }
  userinfo.isVerified = 1;

  var userKeys = Object.keys(userinfo);
  var userfieldValueInsert = [];
  userKeys.forEach(function(userKeys) {
    if (userinfo[userKeys] !== undefined) {
      var fieldValueObj = {};
      fieldValueObj = {
        field: userKeys,
        fValue: userinfo[userKeys]
      }
      userfieldValueInsert.push(fieldValueObj);
    }
  });

  try{
    var result = await userDAL.checkUserIsExist(userinfo.countryCode, userinfo.mobile,userinfo.email);
    if (userID == -1 && result.content.length > 0) {
      return common.sendResponse(response,constant.userMessages.ERR_USER_IS_ALREADY_EXIST,false);
    }
    if (userID == -1){
      var res_create_user = await userDAL.createUser(userfieldValueInsert)
      return common.sendResponse(response,constant.userMessages.MSG_ADMIN_SUCESSFULLY_CREATED,true);
    }
    else{
      if(result.content.length == 0){
        return common.sendResponse(response,constant.userMessages.ERR_USER_NOT_EXIST,false);
      }
      else if(result.content.length > 0  && result.content[0].user_id == userID){
        var res_update_user =  await userDAL.updateUserInfoById(userID, userfieldValueInsert)
        return common.sendResponse(response,constant.userMessages.MSG_ADMIN_SUCESSFULLY_UPDATED,true);
      }
      else{
        return common.sendResponse(response,constant.userMessages.ERR_USER_IS_ALREADY_EXIST,false);
      }
    }
  }
  catch(ex){
    return common.sendResponse(response,constant.userMessages.MSG_ERROR_IN_QUERY,false);
  }

}
module.exports = {
  signinServiceAdmin: signinServiceAdmin,
  signoutService: signoutService,
  getAdminService: getAdminService,
  getRoleService: getRoleService,
  getUserTypeService: getUserTypeService,
  addUpdateAdminService : addUpdateAdminService
}
