var express = require('express');
var services = require('./sectorjobrole.service');
var middleware = require('../../../middleware');

var router = express.Router();
module.exports = router;

router.post('/get-sectorjobrole', middleware.checkAccessToken,middleware.userRightsByAPI,middleware.logger, services.getSectorJobRoleMappingService);
router.delete('/remove-sectorjobrole/:id',middleware.checkAccessToken,middleware.userRightsByAPI, middleware.logger, services.removeSectorJobRoleMappingService);