var debug = require('debug')('server:api:v1:sectorjobrole:service');
var common = require('../common');
var constant = require('../constant');
var sectorjobroleDAL = require('./sectorjobrole.DAL');
var config = require('../../../../config');
var connection = require('../../../helper/connection');

/**
 * get Sector/Job Role Mapping Service
 * 
 * @param {object} request 
 * @param {object} response
 * @return {object} 
 */
var getSectorJobRoleMappingService = async function (request, response) {
  debug("sectorjobrole.service -> getSectorJobRoleMappingService");
  try {
    let result = await sectorjobroleDAL.getSectorJobRoleMapping();
    return common.sendResponse(response, result.content, true);
  }
  catch (ex) {
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

/**
 * remove Sector/Job Role Service
 * 
 * @param {object} request 
 * @param {object} response
 * @return {object} 
 */
var removeSectorJobRoleMappingService = async function (request, response) {
  debug("sectorjobrole.service -> removeSectorJobRoleMappingService");
  var isValid = common.validateParams([request.params.id]);
  if (!isValid)
    return common.sendResponse(response, constant.userMessages.ERR_INVALID_ROLE_DELETE_REQUEST, false);

  var id = request.params.id;
  console.log(id);
  return;
  try {
    let resultRemove = await sectorjobroleDAL.removeSectorJobRole(id);
    return common.sendResponse(response, constant.userMessages.MSG_SECTORJOBROLE_SUCESSFULLY_DELETED, true);
  }
  catch (ex) {
    debug(ex);
    return common.sendResponse(response, constant.userMessages.MSG_ERROR_IN_QUERY, false);
  }
}

module.exports = {
  getSectorJobRoleMappingService: getSectorJobRoleMappingService,
  removeSectorJobRoleMappingService : removeSectorJobRoleMappingService
}