var tbl_UserMaster = "tbl_UserMaster";
var tbl_RoleMaster = "tbl_RoleMaster";
var tbl_UserType = "tbl_UserType";
var tbl_AccessToken = "tbl_AccessToken";
var tbl_transactionMaster = "tbl_transactionMaster";
var view_GetUserDetail = "view_GetUserDetail";
var tbl_IndustryPartners = "tbl_IndustryPartners";
var view_GetLocationDetail = "view_GetLocationDetail";
var view_SectorJobRoleDetail = "view_SectorJobRoleDetail";

var query = {
    getSectorJobRoleMappingQuery: {
        table: view_SectorJobRoleDetail,
        select: [{
            field: 'sectorid',
        }, {
            field: 'sectorName'
        }, 
        /* {
            field: 'sectorSkillCouncil'
        },  */
        {
            field: 'jobroleid'
        }, {
            field: 'jobroleName'
        }, 
        /* {
            field: 'jobroleCode'
        }, */ 
        {
            field: 'isSectorActive' 
        }, {
            field: 'isJobRoleActive' 
        }],
         filter: {
            and: [{
                field: 'isSectorActive',
                operator: 'EQ',
                value: 1
            },{
                field: 'isJobRoleActive',
                operator: 'EQ',
                value: 1
            }]
        },
        sortby: [{
            field: 'jobroleid',
            order: 'DESC'
        }]
    }
};

module.exports = query
