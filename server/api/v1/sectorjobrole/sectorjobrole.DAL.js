var debug = require('debug')('server:api:v1:sectorjobrole:DAL');
var common = require('../common');
var constant = require('../constant');
var query = require('./sectorjobrole.query');


/**
 * Get Job role mapping
 * 
 */
var getSectorJobRoleMapping = async function () {
  debug("sectorjobrole.DAL -> getSectorJobRoleMapping");
  var getSectorJobRoleMappingQuery = common.cloneObject(query.getSectorJobRoleMappingQuery);
  return await common.executeQuery(getSectorJobRoleMappingQuery);
}

/**
 * Remove role mapping
 * 
 */
var removeSectorJobRole = async function (id) {
  debug("sectorjobrole.DAL -> removeSectorJobRole");
  var removeSectorJobRoleQuery = common.cloneObject(query.removeSectorJobRoleQuery);
  removeSectorJobRoleQuery.filter.value = id;
  return await common.executeQuery(removeSectorJobRoleQuery);
}

module.exports = {
  getSectorJobRoleMapping : getSectorJobRoleMapping,
  removeSectorJobRole : removeSectorJobRole
}
