var config = require('../../../config');

var applicationConfiguration = {
  "CRON_EMAILCRONSERVICE_RECORDS_LIMIT": 50,
  "CRON_TRAININGPARTNER_BULK_RECORDS_LIMIT": 100,
  "CRON_INDUSTRYPARTNER_BULK_RECORDS_LIMIT": 100,
  "CRON_CANDIDATE_BULK_RECORDS_LIMIT": 100,
  "CRON_BULKUPLOADCOMPLETION_LIMIT": 10,
  "CRON_BULKUPLOAD_FILE_CREATION_LIMIT": 2,
  "TP_DASHBOARD_DATA_RECENT_HIRING_REQUEST_LIMIT": 10,
  "TP_DASHBOARD_DATA_RECENT_RECRUITMENT_LIMIT": 10,
  "EMP_DASHBOARD_DATA_RECENT_RECRUITMENT_LIMIT": 10,
  "EMP_DASHBOARD_DATA_RECENT_HIRING_REQUEST_LIMIT": 10,
  "CRON_EMAILCRONSERVICE_EMAIL_WEB_HOST": config.baseURLWeb,//Front end
  "CRON_EMAILCRONSERVICE_EMAIL_HOST": config.baseURLBackend,//back end
  "MAX_OTP_SEND_LIMIT": 3,
  "MAX_OTP_EXPIRY_SECONDS": 300, // 5 minute
  "OTP_SETTINGS": {
    "length": 5, // max length 10
    "charset": 'numeric'
  },
  "AADHAR_CARD_NUMBER_LENGTH": 12,
  "MOBILE_NUMBER_LENGTH": 10,
  "CSV_SEPERATOR": "@#",
  "RESET_PASSWORD_MINIMUM_CHARACTERS_LIMIT": 8,
  "RESET_PASSWORD_MAXIMUM_CHARACTERS_LIMIT": 50,
  "APPLICATION_API_KEY": "1", // 2.16 server
  "MAX_ACCESS_TOKEN_EXPIRY_HOURS": 720, // 30 days
  "MAX_PASSWORD_TOKEN_EXPIRY_DAYS": 1, // 30 days
  "PAGE_SIZE": 200,
  "EMP_HIRING_REQUEST_RECORDS_LIMIT": 200,
  "TRAININGCENTER_RECORDS_LIMIT": 200,
  "CANDIDATES_RECORDS_LIMIT": 200, //Not in used
  "API_START_PATH": '/api/',
  "API_VERSION": 'v1',
  "DB_DATE_FORMAT": '%Y-%m-%d %H:%M:%S',
  "FP_DATE_FORMAT": '%Y-%m-%d',
  "MEDIA_GET_STATIC_URL": '/api/v1/other/get-media/',
  "UPLOAD_DIR": 'D:/gitlab/tmp_Images/',//'E:/easypay/javascript/public/uploads/',
  "MEDIA_ACTUAL_DIR": 'D:/gitlab/server/images/Upload_Images/',//'/root/easypay/public/images/Upload_Images/',
  "MEDIA_UPLOAD_DIR": 'uploads/',//'/root/public/images/tmp_Images/',
  "MEDIA_UPLOAD_FILE_NAME_SETTINGS": {
    "length": 12,
  },
  "MEDIA_UPLOAD_SUBFOLDERS_NAME": {
    "CATEGORY": "categoryAndSubCategory/",
    "Thumb": "Thumb/"
  },
  "IMAGE_GET_STATIC_URL": '/images/Upload_Images/',
  "MEDIA_DEFAULT_IMAGES_PATH": 'D:/gitlab/public/images/',//'/server/images/', // path must be same level of app.js
  "MEDIA_DEFAULT_IMAGES_NAME": {
    "USER_PROFILE": 'user_profile.png',
    "TEAM_LOGO": 'teamlogo.png',
    "MATCH_MEDIA": 'images.png',
  },
  "MEDIA_MOVING_PATH": {
    "CATEGORY": "category"
  },
  "VALID_ACTIVE_STATUS_PARAM": ["0", "1", "-1"],
  "ACTIVITY_NAMES": [
    { 'CANDIDATE_ADD': '#TP# has added Candidate' },
    { 'CANDIDATE_EDIT': '#TP# has edited Candidate' },
    { 'CANDIDATE_DELETE': '#TP# has deleted Candidate' },
    { 'TRAINING_CENTER_ADD': '#TP# has added Training Center' },
    { 'TRAINING_CENTER_EDIT': '#TP# has edited Training Center' },
    { 'TRAINING_CENTER_DELETE': '#TP# has deleted Training Center' },
    { 'HIRING_REQUEST_ACCEPT': '#TP# has accepted Hiring Request' },
    { 'HIRING_REQUEST_DECLINE': '#TP# has declined Hiring Request' },
  ],
  "ALTERNATE_ID_TYPE": [
    { 'optionKey': 'Pan', 'optionValue': 'pan' },
    { 'optionKey': 'Voter ID', 'optionValue': 'voterid' },
    { 'optionKey': 'Driving License', 'optionValue': 'drivinglicense' }
  ],
  "GENDER_TYPE": [
    { 'optionKey': 'Male', 'optionValue': 'male' },
    { 'optionKey': 'Female', 'optionValue': 'female' },
    { 'optionKey': 'Other', 'optionValue': 'other' }
  ],
  "TRAINING_STATUS_TYPE": [
    { 'optionKey': 'Completed', 'optionValue': 'completed' },
    { 'optionKey': 'Ongoing', 'optionValue': 'ongoing' },
    { 'optionKey': 'Dropout', 'optionValue': 'dropout' },
    { 'optionKey': 'Enrolled', 'optionValue': 'enrolled' },
    { 'optionKey': 'Duplicate Enrollment', 'optionValue': 'duplicateenrollment' }
  ],
  "PLACEMENT_STATUS_TYPE": [
    { 'optionKey': 'Placed', 'optionValue': 'placed' },
    { 'optionKey': 'Not Placed', 'optionValue': 'notplaced' },
    { 'optionKey': 'Not Interested', 'optionValue': 'notinterested' },
    { 'optionKey': 'Self Employment', 'optionValue': 'selfemployment' }
  ],
  "EMPLOYMENT_STATUS_TYPE": [
    { 'optionKey': 'Wage Employment', 'optionValue': 'wageemployment' },
    { 'optionKey': 'Self Employed', 'optionValue': 'selfemployed' },
    { 'optionKey': 'Upskilled', 'optionValue': 'upskilled' },
    { 'optionKey': 'Higher Studies', 'optionValue': 'higherstudies' },
    { 'optionKey': 'Apprenticeship', 'optionValue': 'apprenticeship' },
  ],
  "TRAINING_TYPE": [
    { 'optionKey': 'Fresh Skilling', 'optionValue': 'freshskilling' },
    { 'optionKey': 'RPL', 'optionValue': 'RPL' },
  ],
  "SYSTEM_PASSWORD": "betterplace@123",
  "TRAINING_PARTNERS_CSV_HEADERS": ['nsdc_ssc_registration_number', 'training_partner_name', 'legal_entity_name', 'contact_person_name', 'contact_email_address', 'contact_mobile', 'address', 'state', 'district', 'pincode', 'sector'],
  "TRAINING_PARTNERS_INSERT_QUERY": 'INSERT INTO `tbl_TrainingPartners` (`nsdc_registration_number`, `training_partner_name`, `legal_entity_name`, `contact_person_name`, `contact_email_address`, `contact_mobile`, `address`,`fk_stateID`, `fk_cityID`, `pincode`,`fk_sectorID`,`tpid`,`password`) VALUES ',
  "INDUSTRY_PARTNERS_CSV_HEADERS": ['employertype', 'legal_entity_name', 'contact_person_name', 'contact_email_address', 'contact_mobile', 'address', 'office_phone', 'state', 'district', 'pincode', 'sector', 'website'],
  "INDUSTRY_PARTNERS_INSERT_QUERY": 'INSERT INTO `tbl_IndustryPartners` (`employertype`, `legal_entity_name`, `contact_person_name`, `contact_email_address`, `contact_mobile`, `address`,`office_phone`, `fk_stateID`, `fk_cityID`, `pincode`,`fk_sectorID`, `website`,`ipid`,`password`) VALUES ',
  "SECTOR_JOBROLE_CSV_HEADERS": ['Sector', 'SectorSkilCouncil', 'Jobrole', 'QPCode'],
  "CANDIDATES_CSV_HEADERS": ['sector', 'jobrole','candidate_id', 'candidate_name', 'training_center', 'aadhaar_number', 'aadhaar_enrollment_id', 'alternateidtype', 'alternate_id_number', 'phone', 'email_address', 'dob','gender','age','address', 'state', 'district', 'pincode', 'experience_in_years', 'sdmsnsdc_enrollment_number', 'training_status', 'attendence_percentage', 'placement_status', 'employment_type', 'assessment_score', 'max_assessment_score', 'assessment_date', 'training_type', 'batch_id', 'batch_start_date', 'batch_end_date', 'scheme_type', 'state_skill_development_mission', 'willing_to_relocate'],
  "CANDIDATES_INSERT_QUERY": 'INSERT INTO `tbl_Candidates` (`fk_sectorID`, `fk_jobroleID`, `candidate_name`, `aadhaar_number`, `aadhaar_enrollment_id`,`alternateidtype`,`alternate_id_number`,`phone`,`email_address`,`dob`,`gender`,`address`,`fk_stateID`,`fk_cityID`,`pincode`, `experience_in_years`, `sdmsnsdc_enrollment_number`, `training_status`, `attendence_percentage`, `placement_status`, `employment_type`, `assessment_score`, `max_assessment_score`, `assessment_date`, `training_type`, `batch_id`, `batch_start_date`, `batch_end_date`, `fk_schemeID`, `fk_ssdm`, `fk_trainingpartnerid`,`fk_trainingcenterid`,`is_willing_to_relocate`) VALUES ',
  "FUTURE_HIRING_REQUEST_CSV_HEADERS": ['sector', 'jobrole', 'state', 'district', 'currentmonth', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
};


var requestMessages = {
  'ERR_API_KEY_NOT_FOUND': {
    code: 2001,
    message: 'api-key not found'
  },
  'ERR_INVALID_API_KEY': {
    code: 2002,
    message: 'Invalid api-key'
  },
  'ERR_UDID_NOT_FOUND': {
    code: 2003,
    message: 'UDID not found'
  },
  'ERR_DEVICE_TYPE_NOT_FOUND': {
    code: 2004,
    message: 'device-type not found'
  },
  'ERR_INVALID_SIGNUP_REQUEST': {
    code: 2005,
    message: 'Invalid SignUp Request.'
  },
  'ERR_INVALID_SEND_OTP_REQUEST': {
    code: 2006,
    message: 'Invalid send otp request'
  },
  'ERR_INVALID_VERIFY_OTP_REQUEST': {
    code: 2007,
    message: 'Invalid verify otp request'
  },
  'ERR_INVALID_USER_PROFILE_UPDATE_REQUEST': {
    code: 2008,
    message: 'Invalid user profile update request'
  },
  'ERR_INVALID_PROFILE_UPDATE_REQUEST': {
    code: 2008,
    message: 'Invalid update profile request'
  },
  'ERR_INVALID_GET_CITY_REQUEST': {
    code: 2008,
    message: 'Invalid get city request'
  },
  'ERR_INVALID_GET_JOBROLE_REQUEST': {
    code: 2008,
    message: 'Invalid get jobrole request'
  },
  'ERR_INVALID_GET_CENTRAL_MINISTRY_REQUEST': {
    code: 2008,
    message: 'Invalid get central ministry request'
  },
  'ERR_INVALID_GET_PAGE_CONTENT_REQUEST': {
    code: 2008,
    message: 'Invalid get page content request'
  },
  'ERR_INVALID_SIGNIN_REQUEST': {
    code: 2009,
    message: 'Invalid SignIn request'
  },
  'ERR_INVALID_FORGOT_PASSWORD_REQUEST': {
    code: 2010,
    message: 'Invalid forgot password request'
  },
  'ERR_INVALID_GET_SECTOR_REQUEST': {
    code: 2010,
    message: 'Invalid get sector request'
  },
  'ERR_INVALID_DELETE_SECTOR_REQUEST': {
    code: 2010,
    message: 'Invalid delete sector request'
  },
  'ERR_INVALID_DELETE_JOBROLE_REQUEST': {
    code: 2010,
    message: 'Invalid delete jobrole request'
  },
  'ERR_INVALID_RESET_PASSWORD_REQUEST': {
    code: 2011,
    message: 'Invalid reset password request'
  },
  'ERR_TOKEN_RESET_PASSWORD_EXPIRED': {
    code: 2011,
    message: 'Token expired for the reset password link.'
  },
  'ERR_RESET_PASSWORD_MIN_LENGTH': {
    code: 2011,
    message: 'Password should be minimum of ' + applicationConfiguration.RESET_PASSWORD_MINIMUM_CHARACTERS_LIMIT + ' characters'
  },
  'ERR_RESET_PASSWORD_MAX_LENGTH': {
    code: 2011,
    message: 'Password should be maximum of ' + applicationConfiguration.RESET_PASSWORD_MAXIMUM_CHARACTERS_LIMIT + ' characters'
  },
  'ERR_INVALID_CHANGE_PASSWORD_REQUEST': {
    code: 2012,
    message: 'Invalid change password request'
  },
  'ERR_INVALID_GET_MY_PROFILE_REQUEST': {
    code: 2013,
    message: 'Invalid get my profile request'
  },
  'ERR_INVALID_TRAININGCENTER_ADD_REQUEST': {
    code: 2014,
    message: 'Invalid Training Center add request. Please enter all the required field(s).'
  },
  'ERR_INVALID_ADD_CANDIDATE_DETIAL_REQUEST': {
    code: 2014,
    message: 'Invalid Candidate Detail add request'
  },
  'ERR_INVALID_UPDATE_CANDIDATE_DETIAL_REQUEST': {
    code: 2014,
    message: 'Invalid Candidate Detail update request'
  },
  'ERR_INVALID_CATEGORY_GET_REQUEST': {
    code: 2015,
    message: 'Invalid category get request'
  },
  'ERR_INVALID_CATEGORY_UPDATE_REQUEST': {
    code: 2016,
    message: 'Invalid category update request'
  },
  'ERR_INVALID_CATEGORY_GET_REQUEST': {
    code: 2017,
    message: 'Invalid category GET request'
  },
  'ERR_INVALID_CATEGORY_DELETE_REQUEST': {
    code: 2018,
    message: 'Invalid category DELETE request'
  },
  'ERR_INVALID_GET_ADMIN_USER_REQUEST': {
    code: 2030,
    message: 'Unable to get admin user'
  },
  'ERR_INVALID_ADD_UPDATE_REQUEST_OF_ADMIN_USER': {
    code: 2031,
    message: 'Invalid request to add update admin user'
  },
  'ERR_INVALID_REQUEST_GET_USER_LIST': {
    code: 2044,
    message: 'Invalid request to get users'
  },
  'ERR_UNABLE_TO_GET_USER_LIST': {
    code: 2045,
    message: "unable to get users"
  },
  'ERR_INVALID_REQUEST_GET_MODULE_LIST': {
    code: 2046,
    message: 'Invalid request to get module'
  },
  'ERR_INVALID_GET_USERLIST_REQUEST': {
    code: 2047,
    message: 'Invalid get user list Request.'
  },
  'ERR_INVALID_ADD_CANDIDATE_REQUEST': {
    code: 2048,
    message: 'Invalid add candidate Request. Please enter all the required field(s).'
  },
  'ERR_AADHAR_CARD_LENGTH': {
    code: 2048,
    message: 'Aadhaar number should be of ' + applicationConfiguration.AADHAR_CARD_NUMBER_LENGTH + ' characters.'
  },
  'ERR_MOBILE_NUMBER_LENGTH': {
    code: 2048,
    message: 'Mobile number should be of ' + applicationConfiguration.MOBILE_NUMBER_LENGTH + ' digits.'
  },
  'ERR_SELECT_JOBROLE': {
    code: 2048,
    message: 'Please select sector and jobrole.'
  },
  'ERR_SELECT_DISTRICT': {
    code: 2048,
    message: 'Please select district.'
  },
  'ERR_SELECT_STATEDISTRICT': {
    code: 2048,
    message: 'Please select state and district.'
  },
  'ERR_COMBINATION_JOBROLE': {
    code: 2048,
    message: 'Invalid selection of sector and jobrole. Please select respective jobrole for the given sector.'
  },
  'ERR_COMBINATION_DISTRICT': {
    code: 2048,
    message: 'Invalid selection of state and district. Please select respective district for the given state.'
  },
  'ERR_STATE_SCHEME_NAME': {
    code: 2048,
    message: 'Please select state scheme.'
  },
  'ERR_INVALID_GET_DETAIL_REQUEST': {
    code: 2048,
    message: 'Invalid get detail Request.'
  }
};


var userRole = {
  'admin': 1,
};

var userType = {
  'admin': 1,
};

var userMessages = {
  'ERR_USER_IS_ALREADY_EXIST': {
    code: 17001,
    message: 'User already exists in the database.'
  },
  'ERR_USER_IS_NOT_ACTIVE': {
    code: 17002,
    message: 'User isn\'t active.'
  },
  'MSG_OTP_SENT_SUCCEFULLY': {
    code: 17003,
    message: 'One Time Password (OTP) has been sent to your mobile {{mobile}}, please enter the same here to verify.'
  },
  'ERR_OTP_LIMIT_EXCEEDED': {
    code: 17004,
    message: 'You have exceeded the maximum number of attempts at this time. Please wait 24 hours and try again later.'
  },
  'ERR_OTP_INVALID': {
    code: 17005,
    message: 'Invalid one time password (OTP) entered.'
  },
  'ERR_RECORD_NOT_EXIST': {
    code: 17006,
    message: 'Record does not exist in database.'
  },
  'ERR_USER_NOT_EXIST': {
    code: 17006,
    message: 'User does not exist in the database. Please enter valid email address or ID.'
  },
  'ERR_SECTOR_NOT_EXIST': {
    code: 17006,
    message: 'Sector does not exist in the database'
  },
  'ERR_JOBROLE_NOT_EXIST': {
    code: 17006,
    message: 'Jobrole does not exist in the database'
  },
  'FORGOT_PASSWORD_USER_NOT_EXIST': {
    message: 'It seems that you have entered invalid ID or email address. Please check and enter valid ID and email address'
  },
  'ERR_FORGOT_PASSWORD_TOKEN_UPDATE': {
    message: 'Error while generating token.Please try again'
  },
  'ERR_OTP_IS_EXPIRED': {
    code: 17007,
    message: 'One Time Password (OTP) was expired.'
  },
  'ERR_INVALID_MOBILE_AND_PASSWORD': {
    code: 17008,
    message: 'Please enter valid mobile/email and password.'
  },
  'MSG_PASSWORD_CHANGE_SUCCESSFULLY': {
    code: 17009,
    message: 'Password changed successfully'
  },
  'ERR_OLD_PASSWORD_NOT_MATCH': {
    code: 17010,
    message: 'Old password does not match.'
  },
  'MSG_PASSWORD_RESET_SUCCESSFULLY': {
    code: 17011,
    message: 'Password reset successfully'
  },
  'MSG_SIGNOUT_SUCCESSFULLY': {
    code: 17012,
    message: 'Signout successfully.'
  },
  'ERR_SIGNOUT_IS_NOT_PROPER': {
    //code: 17013,
    message: 'Signout is not proper.'
  },
  'MSG_SIGNUP_SUCCESSFULLY': {
    code: 200,
    message: 'Thank you for registration. You will receive an email once your account has been verified and approved by the Administrator.'
  },
  'ERR_INVALID_EMAIL_AND_PASSWORD': {
    code: 17015,
    message: 'Please enter valid ID or password.'
  },
  'ERR_INVALID_USERINFO': {
    code: 17016,
    message: 'User Info is undefined.'
  },
  'MSG_ADMIN_SUCESSFULLY_CREATED': {
    code: 17017,
    message: 'Admin user sucessfully created.'
  },
  'ERR_INVALID_USER_DELETE_REQUEST': {
    code: 17018,
    message: 'Invalid request to delete user.'
  },
  'MSG_USER_SUCESSFULLY_DELETED': {
    code: 17019,
    message: 'User sucessfully deleted.'
  },
  'ERR_INVALID_GET_ROLE_REQUEST': {
    code: 17020,
    message: 'Invalid request to get role'
  },
  'MSG_ADMIN_SUCESSFULLY_UPDATED': {
    code: 17021,
    message: 'Admin User sucessfully updated'
  },
  'ERR_PASSWORD_NOT_MATCH': {
    code: 17022,
    message: 'Password Not Match'
  },
  'ERR_INVALID_GET_USER_TYPE_REQUEST': {
    code: 17023,
    message: 'Invalid request to get user type'
  },
  'ERR_INVALID_GET_ROLE_MODULE_MAPPING_REQUEST': {
    code: 17024,
    message: 'Invalid request to get role module mapping'
  },
  'ERR_INVALID_ROLE_DELETE_REQUEST': {
    code: 17025,
    message: 'Invalid request to remove role.'
  },
  'ERR_INVALID_SECTORJOBROLE_DELETE_REQUEST': {
    code: 17025,
    message: 'Invalid request to remove sector/job role.'
  },
  'ERR_CANNOT_REMOVE_ROLE_NOW': {
    code: 17026,
    message: 'You cannot remove role now.'
  },
  'MSG_ROLE_SUCESSFULLY_DELETED': {
    code: 17027,
    message: 'Role sucessfully deleted.'
  },
  'MSG_SECTORJOBROLE_SUCESSFULLY_DELETED': {
    code: 17027,
    message: 'sector/job role sucessfully deleted.'
  },
  'MSG_ERROR_IN_QUERY': {
    code: 17028,
    message: 'Error in execute query.'
  },
  'MSG_EMAIL_SENT_SUCCESSFULLY': {
    code: 17029,
    message: 'Reset password link has been sent to your registered email address successfully.'
  },
  'ERR_EMAIL_SENT': {
    code: 17030,
    message: 'Error while sending the email. Please try again.'
  },
  'UPDATE_USER_PROFILE_SUCCESSFULLY': {
    code: 17031,
    message: 'User profile updated successfully.'
  },
  'UPDATE_PROFILE_SUCCESSFULLY': {
    code: 17031,
    message: 'Profile updated successfully.'
  },
  'HIRING_REQUEST_STATUS_CHANGED_SUCCESSFULLY': {
    code: 17031,
    message: 'Hiring request changed successfully.'
  },
  'ERR_TRAININGCENTER_IS_ALREADY_EXIST': {
    code: 17032,
    message: 'Training center already exist.'
  },
  'ERR_TRAININGCENTER_NOT_EXIST': {
    code: 17032,
    message: 'Training center does not exist.'
  },
  'MSG_TRAININGCENTER_UPDATE_SUCCESSFULLY': {
    code: 17033,
    message: 'Training center updated successfully.'
  },
  'MSG_CANDIDATE_UPDATE_SUCCESSFULLY': {
    code: 17033,
    message: 'Candidate(s) updated successfully.'
  },
  'MSG_FUTRUE_HIRING_UPDATE_SUCCESSFULLY': {
    code: 17033,
    message: 'Future Hiring Request(s) updated successfully.'
  },
  'MSG_TRAININGCENTER_ADD_SUCCESSFULLY': {
    code: 17033,
    message: 'Training center added successfully.'
  },
  'MSG_CANDIDATE_DETAIL_ADD_SUCCESSFULLY': {
    code: 17033,
    message: 'Candidate details added successfully.'
  },
  'MSG_CANDIDATE_DETAIL_UPDATE_SUCCESSFULLY': {
    code: 17033,
    message: 'Candidate details updated successfully.'
  },
  'ERR_CANDIDATE_DETAIL_ADD': {
    code: 17033,
    message: 'Error while adding candidate details.'
  },
  'ERR_INTERNAL_STATUS_CHANGE': {
    code: 17033,
    message: 'Error while updating the internal hiring request status.'
  },
  'ERR_HIRING_STATUS_CHANGE': {
    code: 17033,
    message: 'Error while updating the hiring request status.'
  },
  'ERR_CANDIDATE_IS_ALREADY_EXIST': {
    code: 17034,
    message: 'Candidate with same SDMS/NSDC Enrollment No already exist.'
  },
  'ERR_SECTOR_IS_ALREADY_EXIST': {
    code: 17034,
    message: 'Sector with same name or council already exist.'
  },
  'ERR_JOBROLE_IS_ALREADY_EXIST': {
    code: 17034,
    message: 'Jobrole with same name or code already exist.'
  },
  'ERR_FETCH_STATEWISE_FUTURREQUEST': {
    code: 17034,
    message: 'Error while fetching state wise future hiring request data.'
  },
  'ERR_NO_CANDIDATES_FOUND': {
    code: 17035,
    message: 'No Candidate(s) found.'
  },
  'ERR_NO_ADD_CANDIDATE_FORM_DATA_FOUND': {
    code: 17035,
    message: 'No sectors found for the given training partner id.'
  },
  'MSG_CANDIDATE_ADD_SUCCESSFULLY': {
    code: 17036,
    message: 'Candidate added successfully.'
  },
  'ERR_NO_TRAININGCENTER_FOUND': {
    code: 17037,
    message: 'No training center found.'
  },
  'ERR_NO_BULKUPLOADERROR_FOUND': {
    code: 17037,
    message: 'No File Upload Record found.'
  },
  'ERR_NO_SECTOR_JOBROLE_FOUND': {
    code: 17037,
    message: 'Error Fetching Sector Jobrole.'
  },
  'ERR_NO_PAGECONTENT_FOUND': {
    code: 17037,
    message: 'No content found for the requested page.'
  },
  'ERR_NO_HIRING_REQUEST_DETAILS_FOUND': {
    code: 17037,
    message: 'No Hiring request detail found.'
  },
  'ERR_NO_CANDIDATE_FOUND': {
    code: 17037,
    message: 'No candidate found.'
  },
  'ERR_NO_TRAININGPARTNER_FOUND': {
    code: 17038,
    message: 'No training partner found.'
  },
  'ERR_NO_INDUSTRYPARTNER_FOUND': {
    code: 17038,
    message: 'No industry partner found.'
  },
  'ERR_INVALID_REMOVE_TP_REQUEST': {
    code: 17038,
    message: 'Invalid request to remove training partner.'
  },
  'TP_DELETED_SUCCESSFULLY': {
    code: 17038,
    message: 'Training partner deleted successfully.'
  },
  'IP_DELETED_SUCCESSFULLY': {
    code: 17038,
    message: 'Industry partner deleted successfully.'
  },
  'SECTOR_DELETED_SUCCESSFULLY': {
    code: 17038,
    message: 'Sector deleted successfully.'
  },
  'JOBROLE_DELETED_SUCCESSFULLY': {
    code: 17038,
    message: 'Jobrole deleted successfully.'
  },
  'INVALID_ADD_UPDATE_TP_REQUEST': {
    code: 17038,
    message: 'Invalid request to add/update training partner.'
  },
  'INVALID_UPDATE_HIRING_REQUEST': {
    code: 17038,
    message: 'Invalid request to update hiring status.'
  },
  'INVALID_GET_TP_DASHBOARD_DATA_REQUEST': {
    code: 17038,
    message: 'Invalid request to get the trainingpartner dashboard data.'
  },
  'INVALID_GET_EMP_DASHBOARD_DATA_REQUEST': {
    code: 17038,
    message: 'Invalid request to get the industrypartner dashboard data.'
  },
  'ERR_NO_HIRING_REQUEST_RECORD_FOUND': {
    code: 17038,
    message: 'No Hiring Request Found for the given id.'
  },
  'INVALID_ADD_UPDATE_CANDIDATE_REQUEST': {
    code: 17038,
    message: 'Invalid request to add/update candidate.'
  },
  'INVALID_ADD_UPDATE_FUTURE_HIRING_REQUEST': {
    code: 17038,
    message: 'Invalid request to add future hiring request.'
  },
  'INVALID_ADD_UPDATE_SECTOR_REQUEST': {
    code: 17038,
    message: 'Invalid request to add/update sector.'
  },
  'INVALID_ADD_UPDATE_JOBROLE_REQUEST': {
    code: 17038,
    message: 'Invalid request to add/update jobrole.'
  },
  'MSG_TP_ADD_SUCCESSFULLY': {
    code: 17038,
    message: 'Training partner added successfully.'
  },
  'MSG_TP_UPDATE_SUCCESSFULLY': {
    code: 17038,
    message: 'Training partner updated successfully.'
  },
  'MSG_SECTOR_UPDATE_SUCCESSFULLY': {
    code: 17038,
    message: 'sector updated successfully.'
  },
  'MSG_JOBROLE_UPDATE_SUCCESSFULLY': {
    code: 17038,
    message: 'jobrole updated successfully.'
  },
  'INVALID_ADD_UPDATE_IP_REQUEST': {
    code: 17038,
    message: 'Invalid request to add/update industry partner.'
  },
  'MSG_IP_UPDATE_SUCCESSFULLY': {
    code: 17038,
    message: 'Industry partner updated successfully.'
  },
  'MSG_IP_ADD_SUCCESSFULLY': {
    code: 17038,
    message: 'Industry partner added successfully.'
  },
  'INVALID_ADD_UPDATE_FUTURE_HIRING_REQUEST': {
    code: 17038,
    message: 'Invalid request to add/update future hiring request.'
  },
  'MSG_FUTURE_HIRING_REQUEST_ADD_SUCCESSFULLY': {
    code: 17038,
    message: 'Future hiring request added successfully.'
  },
  'ERR_FUTURE_HIRING_REQUEST_ADD': {
    code: 17038,
    message: 'Error while adding future hiring request.'
  },
  'ERR_NO_CONTACT_QUERY_FOUND': {
    code: 17039,
    message: 'No Contact Enquiry Found.'
  }
};

var userRightsByApiMessage = {
  'API_URL_NOT_IN_USER_RIGHTS_BY_API': {
    code: 11001,
    message: 'This API have not any user Rights.'
  },
  'ERR_API_URL': {
    code: 11002,
    message: 'API URL Is Not Valid.'
  },
  'ERR_ACCESSTOKEN': {
    code: 11003,
    message: 'accessToken is undefined.'
  },
  'ERR_UDID': {
    code: 11004,
    message: 'uDID is undefined.'
  },
  'ERR_USER_TYPE': {
    code: 11005,
    message: 'User Type is not valid.'
  },
  'VALID_USER_RIGHTS': {
    code: 11006,
    message: 'User Rights Is Valid.'
  },
  'ERR_USER_RIGHTS': {
    code: 11007,
    message: 'User Rights Is Not Match.'
  },
  'ERR_PERMISSION_MODULE': {
    code: 11007,
    message: 'You do not have permission to access this module.'
  },
  'ERR_USER_INFO': {
    code: 11008,
    message: 'User Info is undefined.'
  },
}

var otherMessage = {
  'ERR_INVALID_IMAGEUPLOAD_ADD_REQUEST': {
    code: 12009,
    message: 'Invalid Image Upload Request.'
  },
  'ERR_INVALID_MEDIA_UPLOAD_REQUEST': {
    code: 12010,
    message: 'Invalid file upload request.'
  },
  'ERR_ACTIVE_PATH_NOT_FOUND': {
    code: 12011,
    message: 'Active path not found.'
  },
  'ERR_INVALID_GET_IMAGE_REQUEST': {
    code: 12012,
    message: 'Invalid get Image request'
  },
  'ERR_IMAGE_NOT_UPLOADED': {
    code: 12013,
    message: "Image not uploaded."
  },
  'ERR_INVALID_ACTIVE_BODY_REQUEST': {
    code: 12014,
    message: "Invalid Active or In-Active api Request."
  },
  'ERR_INVALID_TABLE_ACTIVE_REQUEST': {
    code: 12015,
    message: "Invalid Table Active or In-Active Request"
  },
  'ERR_INVALID_GET_STATE_REQUEST': {
    code: 12015,
    message: "Invalid get state Request"
  },
  'ERR_INVALID_RECORD_DELETE_REQUEST': {
    code: 12015,
    message: "Invalid Delete Request"
  },
  'ACTIVE_SUCCESS': {
    code: 12017,
    message: "Status changed Successfully."
  },
  'DELETE_SUCCESS': {
    code: 12017,
    message: "Record(s) deleted successfully."
  },
  'INVALID_ACTIVE_PARAM': {
    code: 12018,
    message: "Invalid active status."
  },
  'ERR_INVALID_ADD_ROLE_REQUEST': {
    code: 12019,
    message: "Invalid get role request."
  },
  'ROLE_CREATED_SUCCESSFUL': {
    code: 12020,
    message: "Role created successfully."
  },
  'ROLE_UPDATED_SUCCESSFUL': {
    code: 12021,
    message: "Role updated successfully."
  },
  'ERR_CREATE_AVTIVE_PATH_FOLDER': {
    code: 12022,
    message: 'Error while creating active path folder for moving image'
  },
  'ERR_CREATE_THUMB_PATH_FOLDER': {
    code: 12023,
    message: 'Error while creating thumb path folder for moving image'
  },
  'ERR_INVALID_GET_MEDIA_REQUEST': {
    code: 12024,
    message: "Invalid get media request."
  },
  'CSV_INVALID_HEADERS': {
    code: 12025,
    message: "Headers are not properly provided in csv. Please upload valid csv file as per the sample csv file."
  },
  'INVALID_FILE_TYPE_CSV': {
    code: 12026,
    message: "Invalid file type. Please upload csv file only."
  },
  'TRAINING_PARTNER_DONT_HAVE_SECTORS': {
    code: 12026,
    message: "This csv file can not be uploaded as no sectors were selected by Training Partner while registration."
  },
  'INVALID_FILE_SIZE_CSV': {
    code: 12027,
    message: "File size exceeds. Please upload valid size file."
  },
  'PROCESS_ALREADY_IN_PROGRESS_CSV': {
    code: 12027,
    message: "Please wait bulk upload process is going on.."
  },
  'CSV_IMPORT_SUCCESS': {
    code: 12027,
    message: "File Imported Successfully."
  },
  'ERR_INVALID_UPDATE_REQUEST': {
    code: 12028,
    message: "Please fill all required fields."
  },
  'ERR_INVALID_CONTACT_UPDATE_REQUEST': {
    code: 12029,
    message: "Some error occured, please try again after sometime."
  },
  'CONTACT_MESSAGE_UPDATE_SUCCESS': {
    code: 12030,
    message: "Email has been sent successfully with reply."
  }
};

var categoryMessages = {
  'ERR_CATEGORY_ALREADY_EXIST': {
    code: 18001,
    message: 'Category is already exist'
  },
  'ERR_CATEGORY_CODE_EXIST': {
    code: 18002,
    message: 'Category code is already exist'
  },
  'ERR_CATEGORY_NAME_EXIST': {
    code: 18003,
    message: 'Category name is already exist'
  },
  'CATEGORY_ADD_SUCCESS': {
    code: 18004,
    message: 'Category added successfully'
  },
  'ERR_NO_CATEGORY_FOUND': {
    code: 18005,
    message: 'No category found'
  },
  'ERR_INVALID_SEARCH_CATEGORY_REQUEST': {
    code: 18006,
    message: 'Invalid search category request'
  },
  'CATEGORY_UPDATE_SUCCESS': {
    code: 18007,
    message: 'Category updated successfully'
  },
  'ERR_REQUESTED_USER_NO_PERMISSION_OF_CATEGORY_REMOVE': {
    code: 18008,
    message: 'This CategoryID is not Valid.'
  },
  'MSG_CATEGORY_REMOVE_SUCCESSFULLY': {
    code: 18009,
    message: 'Category Remove successfully.'
  },
  'ERR_REQUESTED_USER_NO_PERMISSION_OF_CATEGORY_UPDATE': {
    code: 18010,
    message: 'This CategoryID is not Valid.'
  },
  'ERR_CATEGORY_EXIST': {
    code: 18022,
    message: 'Category is Exist.'
  },
  'ERR_NO_ERROR_FILE_FOUND': {
    code: 18023,
    message: 'No Error File Found.'
  }

};

var tableCodeMapping = [{
  code: "1",
  table: "user",
  tableName: "tbl_UserMaster",
  pk_idField: "pk_userID"
}, {
  code: "2",
  table: "trainingpartner",
  tableName: "tbl_TrainingPartners",
  pk_idField: "pk_trainingpartnerid"
}, {
  code: "3",
  table: "industrypartner",
  tableName: "tbl_IndustryPartners",
  pk_idField: "pk_industrypartnerid"
}, {
  code: "6",
  table: "trainingcenter",
  tableName: "tbl_TrainingCenters",
  pk_idField: "pk_trainingcenterid"
}, {
  code: "7",
  table: "candidate",
  tableName: "tbl_Candidates",
  pk_idField: "pk_candidateid"
}, {
  code: "9",
  table: "sector",
  tableName: "tbl_SectorMaster",
  pk_idField: "pk_sectorID"
}, {
  code: "10",
  table: "jobrole",
  tableName: "tbl_JobroleMaster",
  pk_idField: "pk_jobroleID"
}, {
  code: "11",
  table: "futurehiringrequest",
  tableName: "tbl_FutureHiringRequests",
  pk_idField: "pk_futurerequestID"
}];
module.exports = {
  requestMessages: requestMessages,
  appConfig: applicationConfiguration,
  userRole: userRole,
  userMessages: userMessages,
  userRightsByApiMessage: userRightsByApiMessage,
  userType: userType,
  otherMessage: otherMessage,
  tableCodeMapping: tableCodeMapping,
  categoryMessages: categoryMessages,
}
