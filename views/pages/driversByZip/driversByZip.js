// This example displays a marker at the center of Australia.
// When the user clicks the marker, an info window opens.

function initMap(searchedData) {
  //console.log('# searchedData',searchedData);
  var uluru = { lat: 21.7679, lng: 78.8718 };
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 4,
    center: uluru,
  });

  /* var contentString = '<div id="content">' +
      '<div id="siteNotice">' +
      '</div>' +
      '<h1 id="firstHeading" class="firstHeading">Uluru</h1>' +
      '<div id="bodyContent">' +
      '<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large ' +
      'sandstone rock formation in the southern part of the ' +
      'Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) ' +
      'south west of the nearest large town, Alice Springs; 450&#160;km ' +
      '(280&#160;mi) by road. Kata Tjuta and Uluru are the two major ' +
      'features of the Uluru - Kata Tjuta National Park. Uluru is ' +
      'sacred to the Pitjantjatjara and Yankunytjatjara, the ' +
      'Aboriginal people of the area. It has many springs, waterholes, ' +
      'rock caves and ancient paintings. Uluru is listed as a World ' +
      'Heritage Site.</p>' +
      '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">' +
      'https://en.wikipedia.org/w/index.php?title=Uluru</a> ' +
      '(last visited June 22, 2009).</p>' +
      '</div>' +
      '</div>';
 
  var infowindow = new google.maps.InfoWindow({
      content: contentString
  }); */

  if (typeof searchedData !== 'undefined') {
    //console.log('# sadsdsa',searchedData);
    var chartsMaindata = searchedData;
  } else {
    var chartsMaindata = JSON.parse(statedata);

  }

  //console.log('# here 1212', chartsMaindata);
  var totalHiringCount = 0;
  // console.log(chartsMaindata,"CHART MAIN DATA");
  for (var key in chartsMaindata) {

    var jobrole = chartsMaindata[key].jobrolename;
    var statename = chartsMaindata[key].statename;
    totalHiringCount += chartsMaindata[key].total;
    //console.log('#latlng', totalHiringCount);
    
    var marker = new google.maps.Marker({
      position: {
        lat: parseFloat(chartsMaindata[key].lat),
        lng: parseFloat(chartsMaindata[key].lng)
      },
      map: map,
       title: "State: " + statename + "\nDrivers: " + String(chartsMaindata[key].total),
       //title: "State: " + statename + "\nJobrole: " + jobrole + "\nNo.of Positions: " + String(chartsMaindata[key].total)
    });
    /* marker.addListener('click', function () {
        infowindow.open(map, marker);
    }); */
  }
  /*
 for (var i = 0; i < results.features.length; i++) {
            var coords = results.features[i].geometry.coordinates;
            var latLng = new google.maps.LatLng(coords[1],coords[0]);
            var marker = new google.maps.Marker({
              position: latLng,
              map: map
            });
          }
   */
  $(".customCount").text('Total Demand - ' + totalHiringCount);
}
//Validate all fields and submit the form...
$("#mapsearch").validate({
  onkeyup: false,
  errorClass: 'has-error-text',
  validClass: 'valid',
  errorElement: 'span',
  /* rules: {
    "nsdc_registration_number": {
      required: true,
    },
    "training_partner_name": {
      required: true,
    },
  },
  messages: {
    "nsdc_registration_number": {
      required: 'Please enter NSDC/SSC registration number',
    },
    "training_partner_name": {
      required: 'Please enter training partner name',
    },
  }, */
  highlight: function (element) {
    $(element).closest('div').addClass("has-error");
  },
  unhighlight: function (element) {
    $(element).closest('div').removeClass("has-error");
  },
  errorPlacement: function (error, element) {
    $(element).closest('div').append(error);
  },
  submitHandler: function (form) {
    $(".has-error-text").hide();
    $(".has-error-text").closest('div').removeClass("has-error");

    $(".loading").removeClass('hidden');
    var data = {};
    data = common.getFormValues($("#mapsearch"));
    //console.log('# data',data);
    //var newarray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    var months = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
    var start = data.monthfrom;//monthfrom
    var end = data.monthto;//monthto
    var res = [];
    //console.log('# start',start);
    //console.log('# end',end);
    while (parseInt(start) <= parseInt(end)) {
      var tmp = start++;
      res.push(months[tmp]);
    }
    //console.log(res);
    data.fromtomonths = res;
    console.log('# final data',data);

    var headers = {
      Authorization: $.cookie(Constants.User.authToken)
    };

    Api.post(Constants.Api.getStateWiseFutureRequestSearchResult, headers, data, function (error, res) {
      $(".loading").addClass('hidden');

      if (res != undefined && res.status == true) {
        //common.showMessage(res.data.message, false);
        //console.log('# res',res.data);
        initMap(res.data);
      } else if (res != undefined && res.status == false) {
        common.showMessage(res.error.message, true);
        //trainingPartner.clearValues(false);
      } else if (error != undefined && error.status == false) {
        common.showMessage(error.error.message, true);
        //trainingPartner.clearValues(false);
      }
    });
  }
});
   /* console.log("ARCHIT");
   google.charts.load('current', {
     'packages':['geochart'],
     // Note: you will need to get a mapsApiKey for your project.
     // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
     'mapsApiKey': 'AIzaSyB5VVX6Ozm60R-rR7R2GQrdbG0QJPLwr1Q'
   });

   google.charts.setOnLoadCallback(drawRegionsMap);

   function drawRegionsMap() {
     var data = google.visualization.arrayToDataTable([
 ['State', 'Future Hiring Requests'],
 ['Uttar Pradesh', 199581477],
 ['Maharashtra', 112372972],
 ['Bihar', 103804637],
 ['West Bengal', 91347736],
 ['Madhya Pradesh', 72597565],
 ['Tamil Nadu', 72138958],
 ['Rajasthan', 68621012],
 ['Karnataka', 61130704],
 ['Gujarat', 60383628],
 ['Andhra Pradesh', 49386799],
 ['Odisha', 41947358],
 ['Telangana', 35286757],
 ['Kerala', 33387677],
 ['Jharkhand', 32966238],
 ['Assam', 31169272],
 ['Punjab', 27704236],
 ['Chhattisgarh', 25540196],
 ['Haryana', 25353081],
 ['Jammu and Kashmir', 12548926],
 ['Uttarakhand', 10116752],
 ['Himachal Pradesh', 6856509],
 ['Tripura', 3671032],
 ['Meghalaya', 2964007],
 ['Manipur', 2721756],
 ['Nagaland', 1980602],
 ['Goa', 1457723],
 ['Arunachal Pradesh', 1382611],
 ['Mizoram', 1091014],
 ['Sikkim', 607688],
 ['Delhi', 16753235],
 ['Puducherry', 1244464],
 ['Chandigarh', 1054686],
 ['Andaman and Nicobar Islands', 379944],
 ['Dadra and Nagar Haveli', 342853],
 ['Daman and Diu', 242911],
 ['Lakshadweep', 64429]
])

     var options = {region: 'IN',
 displayMode: 'regions',
 resolution: 'provinces',
 width: 640, 
 height: 480};

     var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

     chart.draw(data, options);
   } */
// This example displays a marker at the center of Australia.
// When the user clicks the marker, an info window opens.
/* 
function initMap() {
  var uluru = {lat: -25.363, lng: 131.044};
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 4,
    center: uluru
  });

  var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h1 id="firstHeading" class="firstHeading">Uluru</h1>'+
      '<div id="bodyContent">'+
      '<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large ' +
      'sandstone rock formation in the southern part of the '+
      'Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) '+
      'south west of the nearest large town, Alice Springs; 450&#160;km '+
      '(280&#160;mi) by road. Kata Tjuta and Uluru are the two major '+
      'features of the Uluru - Kata Tjuta National Park. Uluru is '+
      'sacred to the Pitjantjatjara and Yankunytjatjara, the '+
      'Aboriginal people of the area. It has many springs, waterholes, '+
      'rock caves and ancient paintings. Uluru is listed as a World '+
      'Heritage Site.</p>'+
      '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">'+
      'https://en.wikipedia.org/w/index.php?title=Uluru</a> '+
      '(last visited June 22, 2009).</p>'+
      '</div>'+
      '</div>';

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });

  var marker = new google.maps.Marker({
    position: uluru,
    map: map,
    title: 'Uluru (Ayers Rock)'
  });
  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });
} */
/* var locations = [
['Bondi Beach', -33.890542, 151.274856, 4],
['Coogee Beach', -33.923036, 151.259052, 5],
['Cronulla Beach', -34.028249, 151.157507, 3],
['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
['Maroubra Beach', -33.950198, 151.259302, 1]
];
var map;
var markers = [];

function init(){
map = new google.maps.Map(document.getElementById('map_canvas'), {
  zoom: 10,
  center: new google.maps.LatLng(-33.92, 151.25),
  mapTypeId: google.maps.MapTypeId.ROADMAP
});

var num_markers = locations.length;
for (var i = 0; i < num_markers; i++) {  
  markers[i] = new google.maps.Marker({
    position: {lat:locations[i][1], lng:locations[i][2]},
    map: map,
    html: locations[i][0],
    id: i,
  });
    
  google.maps.event.addListener(markers[i], 'click', function(){
    var infowindow = new google.maps.InfoWindow({
      id: this.id,
      content:this.html,
      position:this.getPosition()
    });
    google.maps.event.addListenerOnce(infowindow, 'closeclick', function(){
      markers[this.id].setVisible(true);
    });
    this.setVisible(false);
    infowindow.open(map);
  });
}
}

init(); */
