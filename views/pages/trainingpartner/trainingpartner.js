$(document).ready(function () {
  trainingPartner.getData();
  $("#customCheckBox0").change(function () {
    $('.cb-element').prop('checked', this.checked);
  });
  $(".select-tags").select2({
    tags: false,
    theme: "bootstrap",
  });
});
//Validate all fields and submit the form...
$("#frmTrainingPartner").validate({
  onkeyup: false,
  errorClass: 'has-error-text',
  validClass: 'valid',
  errorElement: 'span',
  rules: {
    "nsdc_registration_number": {
      required: true,
    },
    "training_partner_name": {
      required: true,
    },
    "sectorid": {
      required: true,
    },
    "legal_entity_name": {
      required: true,
    },
    "contact_person_name": {
      required: true,
    },
    "contact_email_address": {
      required: true,
      email: true
    },
    "contact_mobile": {
      number: true
    },
    "pincode": {
      required: true,
    }
  },
  messages: {
    "nsdc_registration_number": {
      required: 'Please enter NSDC/SSC registration number',
    },
    "training_partner_name": {
      required: 'Please enter training partner name',
    },
    "sectorid": {
      required: 'Please select sector(s)',
    },
    "legal_entity_name": {
      required: 'Please enter legal entity name',
    },
    "contact_person_name": {
      required: 'Please enter contact person name',
    },
    "contact_email_address": {
      required: 'Please enter email address',
      email: 'Please enter valid email address'
    },
    "contact_mobile": {
      number: 'Please enter valid mobile number'
    },
    "pincode": {
      required: 'Please enter pincode',
    }
  },
  highlight: function (element) {
    $(element).closest('div').addClass("has-error");
  },
  unhighlight: function (element) {
    $(element).closest('div').removeClass("has-error");
  },
  errorPlacement: function (error, element) {
    $(element).closest('div').append(error);
  },
  submitHandler: function (form) {
    $(".has-error-text").hide();
    $(".has-error-text").closest('div').removeClass("has-error");

    $(".loading").removeClass('hidden');
    var data = {};
    data = common.getFormValues($("#frmTrainingPartner"));

    data.trainingpartnerid = trainingPartner.trainingpartnerid;
    var headers = {
      Authorization: $.cookie(Constants.User.authToken)
    };

    Api.post(Constants.Api.addUpdatetrainingPartner, headers, data, function (error, res) {
      $(".loading").addClass('hidden');
      trainingPartner.saveDisabled = false;
      if (res != undefined && res.status == true) {
        common.showMessage(res.data.message, false);
        trainingPartner.clearValues(true);
      } else if (res != undefined && res.status == false) {
        common.showMessage(res.error.message, true);
        //trainingPartner.clearValues(false);
      } else if (error != undefined && error.status == false) {
        common.showMessage(error.error.message, true);
        //trainingPartner.clearValues(false);
      }
      /* $('html, body').animate({
        scrollTop: $('#agency').offset().top - 400
      }, 800); */
    });
  }
});

trainingPartner = {
  trainingpartners: [],
  trainingpartnerid: -1,
  trainingPartnerTable: {},
  formMode: "",
  saveDisabled: false,
  /**
   * this function will be called on page load
   * @return {[type]} [description]
   */
  /* init: function () {
    trainingPartner.trainingpartners.push({
      isActionButton: true,
      targets: 0,
      orderable: false,
      searchable: false,
      isVisible: true,
      buttons: [{
        checkboxObj: 1,
        dataRowField: "trainingpartnerid"
      }]
    })
    trainingPartner.trainingpartners.push({
      name: "nsdc_registration_number",
      targets: 1
    })
    trainingPartner.trainingpartners.push({
      name: "tpid",
      targets: 2
    })
    trainingPartner.trainingpartners.push({
      name: "training_partner_name",
      targets: 3
    })
    trainingPartner.trainingpartners.push({
      name: "contact_person_name",
      targets: 4
    })
    trainingPartner.trainingpartners.push({
      name: "contact_email_address",
      targets: 5
    })
    trainingPartner.trainingpartners.push({
      name: "contact_mobile",
      targets: 6
    })
    if (canEdit > 0) {
      var buttons = [];
      if (canDelete > 0) {
        buttons.push({
          buttonObj: Constants.staticHtml.editButton,
          onClickEvent: trainingPartner.edit
        }, {
            buttonObj: Constants.staticHtml.viewButton,
            onClickEvent: trainingPartner.view
          }, {
            buttonObj: Constants.staticHtml.deleteButton,
            onClickEvent: trainingPartner.delete
          });
      } else {
        buttons.push({
          buttonObj: Constants.staticHtml.editButton,
          onClickEvent: trainingPartner.edit
        });
        buttons.push({
          buttonObj: Constants.staticHtml.viewButton,
          onClickEvent: trainingPartner.view
        });
      }

      trainingPartner.trainingpartners.push({
        isActionButton: true,
        targets: 7,
        orderable: false,
        searchable: false,
        isVisible: true,
        buttons: [{
          buttonObj: Constants.staticHtml.approveButton,
          onClickEvent: trainingPartner.onActiveClick,
          dataRowField: "status",
          compareValue: 1
        }, {
          buttonObj: Constants.staticHtml.rejectButton,
          onClickEvent: trainingPartner.onActiveClick,
          dataRowField: "status",
          compareValue: 0
        }]
      });

      trainingPartner.trainingpartners.push({
        isActionButton: true,
        targets: 8,
        orderable: false,
        searchable: false,
        isVisible: true,
        buttons: buttons
      })
    }
    trainingPartner.getData();
  } *
  /**
   * this function get user data and show into grid
   * @return {[type]} [description]
   */
  getData: function () {
    var gettrainingPartnerUrl = APIBASEURL + "" + Constants.Api.getTrainingPartner
    var headers = {
      "api-key": Constants.Api.apiKey,
      "UDID": common.getUDID(),
      "device-type": common.getDeviceType(),
      "Authorization": $.cookie(Constants.User.authToken)
    };
    trainingPartner.trainingPartnerTable = common.bindTrainingDatatable(headers, gettrainingPartnerUrl, "gridtrainingPartner");
    /*
    Api.post(gettrainingPartnerUrl, headers, '', function (error, res) {
      $(".loading").addClass('hidden');
      console.log('#QUERY DATA USNG API',res.data);
      if (res != undefined && res.status == true) {
        trainingPartner.trainingPartnerTable = common.bindCommonDatatable(res.data, trainingPartner.trainingpartners, "gridtrainingPartner", objModules.trainingPartner);
      } else if (res != undefined && res.status == false) {
        common.showMessage(res.error.message, true);
      } else if (error != undefined && error.status == false) {
        common.showMessage(error.error.message, true);
      }
    });*/
  },
  /**
   * this function will be called when user click on edit from user grid and show currentRow into edit mode
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  edit: function (currentRow) {
    trainingPartner.formMode = "edit";
    //event.preventDefault();
    var tableId = 'gridtrainingPartner';
    var data = $('#' + tableId).DataTable().row(currentRow).data();
    var currentRowData = data;
    $(".addeditCustom").text('Edit');
    if (currentRowData != undefined && currentRowData.pk_trainingpartnerid > 0) {
      common.getSector(function (res) {
        if (res != undefined && res.status == true) {
          $('#drpSector').empty();
          trainingPartner.bindSelect("#drpSector", res.data, "sectorid", "sectorName");

          common.getState(function (res) {
            if (res != undefined && res.status == true) {
              $('#drpState').empty();
              trainingPartner.bindSelect("#drpState", res.data, "stateid", "stateName");

              common.getCity(currentRowData.stateid, function (res) {
                if (res != undefined && res.status == true) {
                  $('#drpCity').empty();
                  trainingPartner.bindSelect("#drpCity", res.data, "cityid", "cityName");
                  trainingPartner.trainingpartnerid = currentRowData.pk_trainingpartnerid;
                  common.showHideDiv(false, objModules.trainingPartner);
                  common.fillFormValues($("#frmTrainingPartner"), currentRowData);
                }
              });
            } else {
              common.showMessage(res.errorMsg, true);
            }
          });
        }
      });
    }
  },
  /**
   * this function will be called when user changes the state from dropdown to render the city dropdown
   */
  changeState: function () {
    $(".loading").removeClass('hidden');
    common.getCity($("#drpState").val(), function (res) {
      if (res != undefined && res.status == true) {
        $(".loading").addClass('hidden');
        $('#drpCity').empty();
        trainingPartner.bindSelect("#drpCity", res.data, "cityid", "cityName");
      }
    });
  },
  /**
   * this function will be called when user click on view from user grid and show currentRows details
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  view: function (currentRow) {
    $(".loading").removeClass('hidden');
    var tableId = 'gridtrainingPartner';
    var data = $('#' + tableId).DataTable().row(currentRow).data();
    var currentRowData = data;
    if (currentRowData != undefined && currentRowData.pk_trainingpartnerid > 0) {
      var gettrainingPartnerDetailUrl = Constants.Api.getTrainingPartnerDetail + currentRowData.pk_trainingpartnerid;
      var headers = {
        Authorization: $.cookie(Constants.User.authToken)
      };
      Api.get(gettrainingPartnerDetailUrl, headers, function (error, res) {
        $(".loading").addClass('hidden');
        if (res != undefined) {
          $("#replaceHTML").html(res);
          $('#training-partner-dialog').modal('show');
        } else if (res != undefined && res.status == false) {
          common.showMessage(res.error.message, true);
        } else if (error != undefined && error.status == false) {
          common.showMessage(error.error.message, true);
        }
      });
    }
  },
  /**
   * this function will be called when user click on delete button from grid and delete current row record
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  delete: function (currentRow) {
    var tableId = 'gridtrainingPartner';
    var data = $('#' + tableId).DataTable().row(currentRow).data();
    var currentRowData = data;
    common.deleteRecordsApi(currentRowData.pk_trainingpartnerid, objModules.trainingPartner.tableId, 0, function (res) {
      if (res != undefined && res.status == true) {
        $('#' + tableId).DataTable().row(currentRow).remove().draw(false);
        //common.showMessage(res.message, false);
      } else {
        //common.showMessage(res.errorMsg, true);
      }
    });
  },
  /**
   * this function clear addRole form values,reset role grid data
   * @param  {[type]} isshowGridDiv [description]
   * @return {[type]}               [description]
   */
  clearValues: function (isshowGridDiv) {
    if (isshowGridDiv == true) {
      common.showHideDiv(true, objModules.trainingPartner);
      trainingPartner.trainingPartnerTable.destroy();
      trainingPartner.getData();
    }
    trainingPartner.trainingpartnerid = -1;
    common.clearValues($("#frmTrainingPartner"));
    trainingPartner.saveDisabled = false;
    $(".has-error-text").remove();
  },
  /**
   * called when click on add icon to show add role form
   */
  add: function () {
    common.showHideDiv(false, objModules.trainingPartner);
    trainingPartner.clearValues(false);
    trainingPartner.formMode = "add";
    $(".addeditCustom").text('Add');
    $("#drpSector").val('').trigger('change');
    common.getSector(function (res) {
      if (res != undefined && res.status == true) {
        $('#drpSector').empty();
        trainingPartner.bindSelect("#drpSector", res.data, "sectorid", "sectorName");
      }
    });
    common.getState(function (res) {
      if (res != undefined && res.status == true) {
        $('#drpState').empty();
        trainingPartner.bindSelect("#drpState", res.data, "stateid", "stateName");
        trainingPartner.changeState();
      }
    });
  },
  /**
   * called when user wants to import csv, it renders the view for bulk upload
   */
  import: function () {

    $('#divImport' + objModules.trainingPartner.displayName).show();
    $('#import' + objModules.trainingPartner.displayName).hide();
    $('#add' + objModules.trainingPartner.displayName).hide();
    $('#div' + objModules.trainingPartner.displayName).hide();
    $(".customerrorsuccess").addClass('hidden');
    const file = document.querySelector('#fileupload');
    file.value = '';
  },
  /* save: function (event) {
    if (trainingPartner.saveDisabled == false) {
      trainingPartner.saveDisabled = true;
    }
  }, */
  /**
   * called when cancel add or edit role to cancel add/edit
   * @return {[type]} [description]
   */
  cancel: function () {
   /*  trainingPartner.clearValues(true) */;
    window.location.reload(true);
  },
  /**
   * this function is used to save bulk upload csv data
   * @param  {[type]} event [description]
   * @return {[type]}       [description]
   */
  importCSV: function (event) {
    $(".loading").removeClass('hidden');
    var files = document.getElementById('fileupload').files[0];
    common.uploadMedia(files, "spreadSheet", 'TrainingPartners', function (res) {
      if (res != undefined && res.status == false) {
        $(".loading").addClass('hidden');
        common.showMessage(res.errorMsg, true);
      } else {
        setTimeout(function () {
          $(".loading").addClass('hidden');
          $(".customErrorRecords").html('Training Partner bulk upload process is in progress. You will be notified via email on your provided email address once upload process is completed.');
          $(".customerrorsuccess").removeClass('hidden');
          //common.showMessage('Training Partner bulk upload process is in progress. You will be notified via email on your provided email address once upload process is completed.', false);
        }, 1000);
        setTimeout(function () {
          $(".customerrorsuccess").addClass('hidden');
        }, 12000);
      }
      /* if (res != undefined && res.status == true) {
        $(".loading").addClass('hidden');
        common.showMessage(res.content.data.message, false);

        if (res.content.data.errorrecords != 0 && res.content.data.errorrecords.error.length > 0) {
          var errorrecords = res.content.data.errorrecords.error;
          var p = "";
          for (var i = 0; i < errorrecords.length; i++) {
            p += '[' + errorrecords[i].errorString + ']';
            p += '<br/>';
          }
          $(".customErrorRecords").html(p);
          $(".customerrorsuccess").removeClass('hidden');
        }

        //window.location.href = '/trainingpartner';
      } else if (res != undefined && res.status == false) {
        $(".loading").addClass('hidden');
        common.showMessage(res.errorMsg, true);
      } else if (error != undefined && error.status == false) {
        $(".loading").addClass('hidden');
        common.showMessage(error.error.message, true);
      } */
    });
  },
  /**
   * this function fill values into select(dropdown) component
   * @param  {[type]} selectId     [description]
   * @param  {[type]} dataSet      [description]
   * @param  {[type]} valField     [description]
   * @param  {[type]} dispValField [description]
   * @return {[type]}              [description]
   */
  bindSelect: function (selectId, dataSet, valField, dispValField) {
    try {
      var selectOptions = "";
      for (var i = 0; i < dataSet.length; i++) {
        selectOptions += '<option value="' + dataSet[i][valField] + '">' + dataSet[i][dispValField] + '</option>'
      }
      $(selectId).append(selectOptions);
    } catch (e) {
      throw (e);
    }
  },
  /**
   * this function active or deactive user, when click on active/deactive button from grid
   * @param  {[type]} rowObj [description]
   * @return {[type]}        [description]
   */
  onActiveClick: function (rowObj, tableId) {
    $(".loading").removeClass('hidden');
    var tableId = 'gridtrainingPartner';
    //var data = rowObj.data();
    var data = $('#' + tableId).DataTable().row(rowObj).data();
    //console.log('#',data);
    var status = "0";
    if (data.status == "0") {
      status = "1";
    }
    if (status == "1" && data.is_email_sent == 0) {
      var emailSendURL = Constants.Api.emailSend
      var headers = {
        Authorization: $.cookie(Constants.User.authToken)
      };
      data.tableName = 'tbl_TrainingPartners';
      data.filedToUpdate = 'trainingpartnerid';
      data.filedUpdateValue = data.pk_trainingpartnerid;
      data.emailType = 'tp_onboarding';
      data.contact_person_name = data.contact_person_name;
      Api.post(emailSendURL, headers, data, function (error, res) {
        $(".loading").addClass('hidden');
        if (res != undefined && res.status == true) {
          trainingPartner.updateActiveStatus(data, status);
        }
      });
    } else {
      trainingPartner.updateActiveStatus(data, status);
    }
  },
  /**
   * called when user clicks on active / inactive, triggers the api call 
   */
  updateActiveStatus: function (data, status) {
    common.updateActiveStatus(data.pk_trainingpartnerid, objModules.trainingPartner.tableId, status, function (res) {
      $(".loading").addClass('hidden');
      if (res != null && res.status == true) {
        trainingPartner.clearValues(true);
      }
    });
  },
  /**
   * calls when user selects one of the options from select action dropdown, i.e. select all and delete
   */
  mutipleaction: function (action) {
    var checkboxValues = [];
    $('input[name="row[]"]:checked').map(function () {
      checkboxValues.push($(this).val());
    });
    if (checkboxValues.length == 0) {
      common.showMessage('Please select at least one checkbox to do this action', true);
      return false;
    }
    var confirmation = 1;
    if (action == 'delete') {
      if (!confirm("Are you sure you want to delete multiple records ?")) {
        confirmation = 0;
      }
    }
    if (confirmation == 1) {
      var data = {};
      data.ids = checkboxValues;
      data.action = action;
      var headers = {
        Authorization: $.cookie(Constants.User.authToken)
      };

      Api.post(Constants.Api.trainingPartnerUpdateMultipleRecords, headers, data, function (error, res) {
        //$(".loading").addClass('hidden');
        trainingPartner.saveDisabled = false;
        if (res != undefined && res.status == true) {
          common.showMessage(res.data.message, false);
          trainingPartner.clearValues(true);
        } else if (res != undefined && res.status == false) {
          common.showMessage(res.error.message, true);
          //trainingPartner.clearValues(false);
        } else if (error != undefined && error.status == false) {
          common.showMessage(error.error.message, true);
          //trainingPartner.clearValues(false);
        }
        //clear the head checkbox value...
        $("#customCheckBox0").prop("checked", false);
      });
    }
  }
}
