var gettrainingPartnerUrl= "";

var playerTable = $('#gridtrainingPartner').DataTable({
    serverSide: true,
    processing: true,
    stateSave: true,
    deferLoading: 0,
    "ajax": {
      "url": gettrainingPartnerUrl,
      "type": "POST",
      "headers":headers,
      'beforeSend': function (request) {
         },
      "data": function (d) {
        d.is_Active = $('.chkIsActive:visible').is(":checked");
      }
    },
    responsive: {
      details: {
        type: 'column',
        target: 'tr'
      }
    },

    "columns": [{
      "data": "pk_trainingpartnerid",
      "width": "5%"
    }, {
      name: "nsdc_registration_number",
      "data": "nsdc_registration_number",
      "width": "25%"
    }, {
      name: "training_partner_name",
      "data": "training_partner_name",
      "width": "10%"
    }, {
      name: "tpid",
      "data": "tpid",
      "width": "5%"
    }],
    columnDefs: [{
      orderable: false,
      className: 'select-checkbox',
      targets: 0,
      'render': function (data, type, full, meta) {
        return "";
      }
    }, {
      orderable: true,
      targets: 1
    }, {
      orderable: false,
      targets: 2
    },
    {
      orderable: false,
      targets: 3
    },
    {
      orderable: false,
      targets: 4
    }
    ],
    //For checkall..
    select: {
      style: 'multi',
      selector: 'td:first-child'
    },
    //order: [1, 'asc'],
    "language": {
      sEmptyTable: "Please search data from the filter box",
      "info": " _START_ - _END_ of _TOTAL_ ",
      processing: "<img src='../assets/images/cricheroes-loader-new.gif'> Loading...",
      "sLengthMenu": "<span class='custom-select-title'>Rows per page:</span> <span class='custom-select'> _MENU_ </span>",
      "sSearch": "",
      "sSearchPlaceholder": "Search",
      "paginate": {
        "sNext": " ",
        "sPrevious": " "
      },
    },
    dom: "<'pmd-card-title pmd-card-title-custome'<'data-table-inverse'><'pmd-textfield'f>>" +
    "<'custom-select-info'<'custom-select-item'><'custom-select-action'>>" +
    "<'row'<'col-sm-12'tr>>" +
    "<'pmd-card-footer' <'pmd-datatable-pagination' l i p>>"
  });
