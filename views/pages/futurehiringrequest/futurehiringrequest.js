$(document).ready(function () {
  hiringRequest.getData();
  $("#customCheckBox0").change(function () {
    $('.cb-element').prop('checked', this.checked);
  });
});

hiringRequest = {
  hiringRequests: [],
  hiringrequestid: -1,
  hiringrequestTable: {},
  formMode: "",
  saveDisabled: false,
  tableid: "gridHiringRequest",
  /**
   * this function get user data and show into grid
   * @return {[type]} [description]
   */
  getData: function () {
    var getFutureHiringRequestUrl = APIBASEURL + "" + Constants.Api.getHiringRequest
    var headers = {
      "api-key": Constants.Api.apiKey,
      "UDID": common.getUDID(),
      "device-type": common.getDeviceType(),
      "Authorization": $.cookie(Constants.User.authToken)
    };
    hiringRequest.hiringRequestTable = hiringRequest.bindFutureHiringRequestDatatable(headers, getFutureHiringRequestUrl, hiringRequest.tableid);
  },

  /**
   * this function will be called when user click on view from user grid and show currentRows details
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  view: function (currentRow) {
    $(".loading").removeClass('hidden');

    var multiYears = [];
    var currentDate = new Date();
    var currentYear = currentDate.getFullYear();

    for (var y = 0; y <= 1; y++) {
      var forwardYears = currentYear + y;
      multiYears.push(forwardYears);
    }


    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    var htmlOptions = [];
    var i;
    var currentDate = new Date();
    for (var m = 0; m < multiYears.length; m++) {
      var multiYear = multiYears[m];
      var startMonth = 1;

      if (multiYear == currentDate.getYear() + 1900) {
        startMonth = currentDate.getMonth() + 1;
      }

      var uptoMonths = 12;
      if (multiYear == currentYear + 1) {
        uptoMonths = currentDate.getMonth() + 1;
      }
      for (var i = startMonth; i <= uptoMonths; i++) {
        htmlOptions.push(monthNames[i - 1] + "-" + multiYear);
      }
    }

    var currentRowData = $('#' + hiringRequest.tableid).DataTable().row(currentRow).data();
    if (currentRowData != undefined && currentRowData.request_id > 0) {
      var getHiringRequestDetailUrl = Constants.Api.getHiringRequestDetail + currentRowData.request_id;
      var headers = {
        Authorization: $.cookie(Constants.User.authToken)
      };
      Api.get(getHiringRequestDetailUrl, headers, function (error, res) {
        $(".loading").addClass('hidden');
        if (res != undefined) {
          $("#replaceHTML").html(res);
          $('#hiring-request-dialog').modal('show');
        } else if (res != undefined && res.status == false) {
          common.showMessage(res.error.message, true);
        } else if (error != undefined && error.status == false) {
          common.showMessage(error.error.message, true);
        }
      });
    }
  },
  /**
   * this function will be called when user click on delete button from grid and delete current row record
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  delete: function (currentRow) {
    var data = $('#' + hiringRequest.tableid).DataTable().row(currentRow).data();
    common.deleteRecordsApi(data.request_id, objModules.hiringrequest.tableId, 0, function (res) {
      if (res != undefined && res.status == true) {
        $('#' + hiringRequest.tableid).DataTable().row(currentRow).remove().draw(false);
        //currentRow.remove().draw(false);
        //common.showMessage(res.message, false);
      } else {
        //common.showMessage(res.errorMsg, true);
      }
    });
  },
  /**
   * this function clear addRole form values,reset role grid data
   * @param  {[type]} isshowGridDiv [description]
   * @return {[type]}               [description]
   */
  clearValues: function (isshowGridDiv) {
    if (isshowGridDiv == true) {
      common.showHideDiv(true, objModules.hiringrequest);
      hiringRequest.hiringRequestTable.destroy();
      hiringRequest.getData();
    }
    hiringRequest.hiringrequestid = -1;
    // common.clearValues($("#frmTrainingPartner"));
    hiringRequest.saveDisabled = false;
    $(".has-error-text").remove();
  },
  /**
   * calls when user selects one of the options from select action dropdown, i.e. select all and delete
   */
  mutipleaction: function (action) {
    var checkboxValues = [];
    $('input[name="row[]"]:checked').map(function () {
      checkboxValues.push($(this).val());
    });
    if (checkboxValues.length == 0) {
      common.showMessage('Please select at least one checkbox to do this action', true);
      return false;
    }
    var confirmation = 1;
    if (action == 'delete') {
      if (!confirm("Are you sure you want to delete multiple records ?")) {
        confirmation = 0;
      }
    }
    if (confirmation == 1) {
      var data = {};
      data.ids = checkboxValues;
      data.action = action;

      var headers = {
        Authorization: $.cookie(Constants.User.authToken)
      };

      Api.post(Constants.Api.futureHiringRequestsUpdateMultipleRecords, headers, data, function (error, res) {
        $(".loading").addClass('hidden');
        hiringRequest.saveDisabled = false;
        if (res != undefined && res.status == true) {
          common.showMessage(res.data.message, false);
          hiringRequest.clearValues(true);
        } else if (res != undefined && res.status == false) {
          common.showMessage(res.error.message, true);
        } else if (error != undefined && error.status == false) {
          common.showMessage(error.error.message, true);
        }
        //clear the head checkbox value...
        $("#customCheckBox0").prop("checked", false);
      });
    }
  },
  /**
   * Bind datatable of futurehiringrequest
   */
  bindFutureHiringRequestDatatable: function (headers, getFutureHiringRequestUrl, datatableID) {
    var gridFutureHiringRequestObj = $('#' + datatableID).DataTable({
      serverSide: true,
      processing: true,
      bFilter: true,
      bLengthChange: true,
      stateSave: true,
      order: [0, 'desc'],

      // "searching": true,
      "bSearchable": true,
      "ajax": {
        "url": getFutureHiringRequestUrl,
        "type": "POST",
        "headers": headers,
        'beforeSend': function (request) {

        },
        "data": function (d) {
          //d.status = $('#customCheckBox0').is(":checked");
          d.employerName = d.search.value;
          d.sectorName = d.search.value;
          d.jobroleName = d.search.value;
          d.stateName = d.search.value;
          d.cityName = d.search.value;
          d.year = d.search.value;
          d.gender = d.search.value;
          d.total = d.search.value;
          /* d.ipid = d.search.value;
          d.employertype = d.search.value;
          d.contact_person_name = d.search.value; */
        }
      },
      responsive: {
        details: {
          type: 'column',
          target: 'tr'
        }
      },
      "columns": [{
        neme: "pk_futurerequestID",
        "data": "request_id",
        "width": "5%"
      }, {
        name: "employerName",
        "data": "employerName",
        "width": "10%"
      }, {
        name: "sectorName",
        "data": "sectorName",
        "width": "20%"
      }, {
        name: "jobroleName",
        "data": "jobroleName",
        "width": "20%"
      },
      {
        name: "numberOfPositions",
        "data": "numberOfPositions",
        "width": "15%"
      },
      {
        name: "year",
        "data": "year",
        "width": "10%"
      },
      {
        "data": "Actions",
        "width": "10%"
      }
      ],
      columnDefs: [{
        orderable: false,
        //  className: 'select-checkbox',
        targets: 0,
        'render': function (data, type, full, meta) {
          return buttons = '<div class="custom-control custom-checkbox pmd-checkbox"><input class="custom-control-input cb-element" type="checkbox" name="row[]" value="' + full.request_id + '" id="customCheckBox' + full.request_id + '"><label class="custom-control-label" for="customCheckBox' + full.request_id + '"></label></div>';
        }
      }, {
        orderable: true,
        targets: 1
      }, {
        orderable: true,
        targets: 2
      },
      {
        orderable: true,
        targets: 3
      },
      {
        orderable: true,
        targets: 4
      },
      {
        orderable: false,
        targets: 5
      },
      {
        orderable: false,
        targets: 6,
        'render': function (data, type, full, meta) {
          var buttonHTML = '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary btnView" type="button" onclick="hiringRequest.view(' + meta.row + ');" value="' + data + '" title="Detail"><i class="material-icons pmd-sm">remove_red_eye</i></button>';
          buttonHTML += '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-danger btnDelete"  type="button" onclick="hiringRequest.delete(' + meta.row + ');" value="' + data + '" title="Delete"><i class="material-icons pmd-sm">delete</i></button>';
          return buttonHTML;
        }
      }
      ]
      ,
      //For checkall..
      select: {
        style: 'multi',
        selector: 'td:first-child'
      },
      "language": {
        "info": " _START_ - _END_ of _TOTAL_ ",
        "sLengthMenu": "<span class='custom-select-title hidden'>Rows per page:</span> <span class='pmd-custom-select hidden'> _MENU_ </span>",
        processing: "<span class='loading'></span>",
        "sSearch": "",
        "sSearchPlaceholder": "Search",
        "paginate": {
          "sNext": " ",
          "sPrevious": " "
        },
      },
      dom:
        "<'card-header d-flex flex-row'<'data-table-title-responsive'><'search-paper pmd-textfield ml-auto'f>>" +
        "<'row'<'col-sm-12 datatableCustom'tr>>" +
        "<'card-footer' <'pmd-datatable-pagination' l i p>>",
    });

    // Apply the filter
    $('.custom-select-info').hide();

    //$('#' + datatableID + '_wrapper').find("div.data-table-title-responsive").html();

    $('#' + datatableID + ' tbody').unbind("click");
    $(".custom-select-action").html('<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary" type="button"><i class="material-icons pmd-sm">delete</i></button><button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary" type="button"><i class="material-icons pmd-sm">more_vert</i></button>');

    $(".cb-element").change(function () {
      if ($(".cb-element").length == $(".cb-element:checked").length)
        $("#customCheckBox0").prop('checked', true);
      else
        $("#customCheckBox0").prop('checked', false);
    });

    $('#' + datatableID).on('page.dt', function () {
      //var info = tableObj.page.info();
      $("#customCheckBox0").prop('checked', false);
    });

    var btnClear = $('<a class="btnClearDataTableFilter cursor-pointer hidden" style="position: absolute;right: 20px;z-index: 9999;top: 25px;"><i class="material-icons md-dark pmd-xs">cancel</i></a>');
    btnClear.appendTo($('#' + datatableID).parents('.dataTables_wrapper').find('.dataTables_filter'));

    $('#' + datatableID + '_wrapper .btnClearDataTableFilter').click(function () {
      $('#' + datatableID).dataTable().fnFilter('');
      $('.btnClearDataTableFilter').addClass('hidden');
    });
    if (gridFutureHiringRequestObj.data().any()) {
      $('#' + datatableID + "_filter").hide();
      $('#' + datatableID + "_info").hide();
      $('#' + datatableID + "_paginate").hide();
      $('.customSelectAll').hide();
      $('#selectAction').hide();
    } else {
      $('#' + datatableID + "_filter").show();
      $('#' + datatableID + "_info").show();
      $('#' + datatableID + "_paginate").show();
      $('.customSelectAll').show();
      $('#selectAction').show();
    }
    function clickHandler(btnObj) {
      $('#' + datatableID + ' tbody').on('click', btnObj.buttonObj.targetClass, function () {
        btnObj.onClickEvent(tableObj.row($(this).parents('tr')));
      });
    }
    $("div.dataTables_filter input").keyup(function (e) {
      if ($(this).val() != '') {
        $('.btnClearDataTableFilter').removeClass('hidden');
      } else {
        $('.btnClearDataTableFilter').addClass('hidden');
      }
    });

    return gridFutureHiringRequestObj;
  },
}
