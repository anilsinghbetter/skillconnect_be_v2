$(document).ready(function () {
  industryPartner.getData();
  $("#customCheckBox0").change(function () {
    $('.cb-element').prop('checked', this.checked);
  });
  $(".select-tags").select2({
    tags: false,
    theme: "bootstrap",
  });
});

$("#frmIndustryPartner").validate({
  onkeyup: false,
  errorClass: 'has-error-text',
  validClass: 'valid',
  errorElement: 'span',
  rules: {
    "employertype": {
      required: true,
    },
    "legal_entity_name": {
      required: true,
    },
    "sectorid": {
      required: true,
    },
    "contact_person_name": {
      required: true,
    },
    "contact_email_address": {
      required: true,
      email: true
    },
    "contact_mobile": {
      number: true
    },
    "pincode": {
      required: true,
    }
  },
  messages: {
    "employertype": {
      required: 'Please select employer type',
    },
    "legal_entity_name": {
      required: 'Please enter legal entity name',
    },
    "sectorid": {
      required: 'Please select sector(s)',
    },
    "contact_person_name": {
      required: 'Please enter contact person name',
    },
    "contact_email_address": {
      required: 'Please enter email address',
      email: 'Please enter valid email address'
    },
    "contact_mobile": {
      number: 'Please enter valid mobile number'
    },
    "pincode": {
      required: 'Please enter pincode',
    }
  },
  highlight: function (element) {
    $(element).closest('div').addClass("has-error");
  },
  unhighlight: function (element) {
    $(element).closest('div').removeClass("has-error");
  },
  errorPlacement: function (error, element) {
    $(element).closest('div').append(error);
  },
  submitHandler: function (form) {
    $(".has-error-text").hide();
    $(".has-error-text").closest('div').removeClass("has-error");

    $(".loading").removeClass('hidden');
    var data = {};
    data = common.getFormValues($("#frmIndustryPartner"));

    data.industrypartnerid = industryPartner.industrypartnerid;

    var headers = {
      Authorization: $.cookie(Constants.User.authToken)
    };

    Api.post(Constants.Api.addUpdateindustryPartner, headers, data, function (error, res) {
      $(".loading").addClass('hidden');
      industryPartner.saveDisabled = false;
      if (res != undefined && res.status == true) {
        common.showMessage(res.data.message, false);
        industryPartner.clearValues(true);
      } else if (res != undefined && res.status == false) {
        common.showMessage(res.error.message, true);
        //industryPartner.clearValues(false);
      } else if (error != undefined && error.status == false) {
        common.showMessage(error.error.message, true);
        //industryPartner.clearValues(false);
      }
      /* $('html, body').animate({
        scrollTop: $('#agency').offset().top - 400
      }, 800); */
    });
  }
});

industryPartner = {
  industryPartners: [],
  industrypartnerid: -1,
  industryPartnerTable: {},
  formMode: "",
  saveDisabled: false,
  tableid: "gridindustryPartner",

  /**
   * this function get user data and show into grid
   * @return {[type]} [description]
   */
  getData: function () {
    var getindustryPartnerUrl = APIBASEURL + "" + Constants.Api.getIndustryPartner
    var headers = {
      "api-key": Constants.Api.apiKey,
      "UDID": common.getUDID(),
      "device-type": common.getDeviceType(),
      "Authorization": $.cookie(Constants.User.authToken)
    };
    industryPartner.industryPartnerTable = industryPartner.bindIndustryPartnerDatatable(headers, getindustryPartnerUrl, industryPartner.tableid);
  },
  /**
   * this function will be called when user click on edit from user grid and show currentRow into edit mode
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  edit: function (currentRow) {
    industryPartner.formMode = "edit";
    //event.preventDefault();
    var currentRowData = $('#' + industryPartner.tableid).DataTable().row(currentRow).data();
    $(".addeditCustom").text('Edit');
    if (currentRowData != undefined && currentRowData.industrypartnerid > 0) {
      common.getSector(function (res) {
        if (res != undefined && res.status == true) {
          $('#drpSector').empty();
          industryPartner.bindSelect("#drpSector", res.data, "sectorid", "sectorName");

          common.getState(function (res) {
            if (res != undefined && res.status == true) {
              $('#drpState').empty();
              industryPartner.bindSelect("#drpState", res.data, "stateid", "stateName");

              common.getCity(currentRowData.stateid, function (res) {
                if (res != undefined && res.status == true) {
                  $('#drpCity').empty();
                  industryPartner.bindSelect("#drpCity", res.data, "cityid", "cityName");

                  industryPartner.industrypartnerid = currentRowData.industrypartnerid;
                  common.showHideDiv(false, objModules.industryPartner);
                  common.fillFormValues($("#frmIndustryPartner"), currentRowData);
                }
              });
            } else {
              common.showMessage(res.errorMsg, true);
            }
          });
        }
      });
    }
  },
  /**
   * this function will be called when user changes the state from dropdown to render the city dropdown
   */
  changeState: function () {
    $(".loading").removeClass('hidden');
    common.getCity($("#drpState").val(), function (res) {
      if (res != undefined && res.status == true) {
        $(".loading").addClass('hidden');
        $('#drpCity').empty();
        industryPartner.bindSelect("#drpCity", res.data, "cityid", "cityName");
      }
    });
  },
  /**
   * this function will be called when user click on view from user grid and show currentRows details
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  view: function (currentRow) {
    $(".loading").removeClass('hidden');
    var currentRowData = $('#' + industryPartner.tableid).DataTable().row(currentRow).data();
    if (currentRowData != undefined && currentRowData.industrypartnerid > 0) {
      var getindustryPartnerDetailUrl = Constants.Api.getIndustryPartnerDetail + currentRowData.industrypartnerid;
      var headers = {
        Authorization: $.cookie(Constants.User.authToken)
      };
      Api.get(getindustryPartnerDetailUrl, headers, function (error, res) {
        $(".loading").addClass('hidden');
        if (res != undefined) {
          $("#replaceHTML").html(res);
          $('#industry-partner-dialog').modal('show');
        } else if (res != undefined && res.status == false) {
          common.showMessage(res.error.message, true);
        } else if (error != undefined && error.status == false) {
          common.showMessage(error.error.message, true);
        }
      });
    }
  },
  /**
   * this function will be called when user click on delete button from grid and delete current row record
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  delete: function (currentRow) {
    var data = $('#' + industryPartner.tableid).DataTable().row(currentRow).data();
    common.deleteRecordsApi(data.industrypartnerid, objModules.industryPartner.tableId, 0, function (res) {
      if (res != undefined && res.status == true) {
        $('#' + industryPartner.tableid).DataTable().row(currentRow).remove().draw(false);
        //common.showMessage(res.message, false);
      } else {
        //common.showMessage(res.errorMsg, true);
      }
    });
  },

  /**
   * this function clear addRole form values,reset role grid data
   * @param  {[type]} isshowGridDiv [description]
   * @return {[type]}               [description]
   */
  clearValues: function (isshowGridDiv) {
    if (isshowGridDiv == true) {
      common.showHideDiv(true, objModules.industryPartner);
      industryPartner.industryPartnerTable.destroy();
      industryPartner.getData();
    }
    industryPartner.industrypartnerid = -1;
    common.clearValues($("#frmIndustryPartner"));
    industryPartner.saveDisabled = false;
    $(".has-error-text").remove();
  },
  /**
   * called when click on add icon to show add role form
   */
  add: function () {
    common.showHideDiv(false, objModules.industryPartner);
    industryPartner.clearValues(false);
    industryPartner.formMode = "add";
    $(".addeditCustom").text('Add');
    $("#drpSector").val('').trigger('change');
    common.getSector(function (res) {
      if (res != undefined && res.status == true) {
        $('#drpSector').empty();
        industryPartner.bindSelect("#drpSector", res.data, "sectorid", "sectorName");
      }
    });
    common.getState(function (res) {
      if (res != undefined && res.status == true) {
        $('#drpState').empty();
        industryPartner.bindSelect("#drpState", res.data, "stateid", "stateName");
        industryPartner.changeState();
      }
    });
  },
  /**
   * called when user wants to import csv, it renders the view for bulk upload
   */
  import: function () {
    $('#divImport' + objModules.industryPartner.displayName).show();
    $('#import' + objModules.industryPartner.displayName).hide();
    $('#add' + objModules.industryPartner.displayName).hide();
    $('#div' + objModules.industryPartner.displayName).hide();
    $(".customerrorsuccess").addClass('hidden');
    const file = document.querySelector('#fileupload');
    file.value = '';
  },
  /**
   * called when cancel add or edit role to cancel add/edit
   * @return {[type]} [description]
   */
  cancel: function () {
    /*  trainingPartner.clearValues(true) */;
    window.location.reload(true);
  },
  /**
   * this function is used to save bulk upload csv data
   * @param  {[type]} event [description]
   * @return {[type]}       [description]
   */
  importCSV: function (event) {
    $(".loading").removeClass('hidden');
    var files = document.getElementById('fileupload').files[0];
    common.uploadMedia(files, "spreadSheet", 'IndustryPartners', function (res) {
      if (res != undefined && res.status == false) {
        $(".loading").addClass('hidden');
        common.showMessage(res.errorMsg, true);
      } else {
        setTimeout(function () {
          $(".loading").addClass('hidden');
          $(".customErrorRecords").html('Industry Partner bulk upload process is in progress. You will be notified via email on your provided email address once upload process is completed.');
          $(".customerrorsuccess").removeClass('hidden');
          //common.showMessage('Industry Partner bulk upload process is in progress. You will be notified via email on your provided email address once upload process is completed.', false);
        }, 1000);
        setTimeout(function () {
          $(".customerrorsuccess").addClass('hidden');
        }, 12000);
      }
      /* if (res != undefined && res.status == true) {
        $(".loading").addClass('hidden');
        common.showMessage(res.content.data.message, false);
        if (res.content.data.errorrecords != 0 && res.content.data.errorrecords.error.length > 0) {
          var errorrecords = res.content.data.errorrecords.error;
          var p = "";
          for (var i = 0; i < errorrecords.length; i++) {
            p += '[' + errorrecords[i].errorString + ']';
            p += '<br/>';
          }
          $(".customerrorsuccess").removeClass('hidden');
          $(".customErrorRecords").html(p);
        }

        //window.location.href = '/industrypartner';
      } else if (res != undefined && res.status == false) {
        $(".loading").addClass('hidden');
        common.showMessage(res.errorMsg, true);
      } else if (error != undefined && error.status == false) {
        $(".loading").addClass('hidden');
        common.showMessage(error.error.message, true);
      } */
    });
  },
  /**
   * this function fill values into select(dropdown) component
   * @param  {[type]} selectId     [description]
   * @param  {[type]} dataSet      [description]
   * @param  {[type]} valField     [description]
   * @param  {[type]} dispValField [description]
   * @return {[type]}              [description]
   */
  bindSelect: function (selectId, dataSet, valField, dispValField) {
    try {
      var selectOptions = "";
      for (var i = 0; i < dataSet.length; i++) {
        selectOptions += '<option value="' + dataSet[i][valField] + '">' + dataSet[i][dispValField] + '</option>'
      }
      $(selectId).append(selectOptions);
    } catch (e) {
      throw (e);
    }
  },
  /**
   * this function active or deactive user, when click on active/deactive button from grid
   * @param  {[type]} rowObj [description]
   * @return {[type]}        [description]
   */
  onActiveClick: function (rowObj) {
    $(".loading").removeClass('hidden');
    var data = $('#' + industryPartner.tableid).DataTable().row(rowObj).data();
    var status = "0";
    if (data.status == "0") {
      status = "1";
    }
    if (status == "1" && data.is_email_sent == 0) {
      var emailSendURL = Constants.Api.emailSend
      var headers = {
        Authorization: $.cookie(Constants.User.authToken)
      };
      data.tableName = 'tbl_IndustryPartners';
      data.filedToUpdate = 'industrypartnerid';
      data.filedUpdateValue = data.industrypartnerid;
      data.emailType = 'ip_onboarding';
      data.contact_person_name = data.contact_person_name;
      Api.post(emailSendURL, headers, data, function (error, res) {
        $(".loading").addClass('hidden');
        if (res != undefined && res.status == true) {
          industryPartner.updateActiveStatus(data, status);
        }
      });
    } else {
      industryPartner.updateActiveStatus(data, status);
    }

  },

  /**
   * called when user clicks on active / inactive, triggers the api call 
   */
  updateActiveStatus: function (data, status) {
    common.updateActiveStatus(data.industrypartnerid, objModules.industryPartner.tableId, status, function (res) {
      $(".loading").addClass('hidden');
      if (res != null && res.status == true) {
        industryPartner.clearValues(true);
      }
    });
  },
  /**
   * calls when user selects one of the options from select action dropdown, i.e. select all and delete
   */
  mutipleaction: function (action) {
    var checkboxValues = [];
    $('input[name="row[]"]:checked').map(function () {
      checkboxValues.push($(this).val());
    });
    if (checkboxValues.length == 0) {
      common.showMessage('Please select at least one checkbox to do this action', true);
      return false;
    }
    var confirmation = 1;
    if (action == 'delete') {
      if (!confirm("Are you sure you want to delete multiple records ?")) {
        confirmation = 0;
      }
    }
    if (confirmation == 1) {
      var data = {};
      data.ids = checkboxValues;
      data.action = action;

      var headers = {
        Authorization: $.cookie(Constants.User.authToken)
      };

      Api.post(Constants.Api.industryPartnerUpdateMultipleRecords, headers, data, function (error, res) {
        $(".loading").addClass('hidden');
        industryPartner.saveDisabled = false;
        if (res != undefined && res.status == true) {
          common.showMessage(res.data.message, false);
          industryPartner.clearValues(true);
        } else if (res != undefined && res.status == false) {
          common.showMessage(res.error.message, true);
          //industryPartner.clearValues(false);
        } else if (error != undefined && error.status == false) {
          common.showMessage(error.error.message, true);
          //industryPartner.clearValues(false);
        }
        //clear the head checkbox value...
        $("#customCheckBox0").prop("checked", false);
      });
    }
  },
  /**
   * Bind datatable of Industrypartner
   */
  bindIndustryPartnerDatatable: function (headers, getIndustryPartnerUrl, datatableID) {
    var gridIndustryPartnerObj = $('#' + datatableID).DataTable({
      serverSide: true,
      processing: true,
      bFilter: true,
      bLengthChange: true,
      stateSave: true,
      order: [0, 'desc'],

      // "searching": true,
      "bSearchable": true,
      "ajax": {
        "url": getIndustryPartnerUrl,
        "type": "POST",
        "headers": headers,
        'beforeSend': function (request) {

        },
        "data": function (d) {
          d.status = $('#customCheckBox0').is(":checked");
          d.contact_mobile = d.search.value;
          d.contact_email_address = d.search.value;
          d.legal_entity_name = d.search.value;
          d.ipid = d.search.value;
          d.employertype = d.search.value;
          d.contact_person_name = d.search.value;
        }
      },
      responsive: {
        details: {
          type: 'column',
          target: 'tr'
        }
      },
      "columns": [{
        name: "pk_industrypartnerid",
        "data": "industrypartnerid",
        "width": "5%"
      }, {
        name: "ipid",
        "data": "ipid",
        "width": "10%"
      }, {
        name: "employertype",
        "data": "employertype",
        "width": "20%"
      },
      {
        name: "legal_entity_name",
        "data": "legal_entity_name",
        "width": "15%"
      },
      {
        name: "contact_email_address",
        "data": "contact_email_address",
        "width": "10%"
      },
      {
        name: "status",
        "data": "status",
        "width": "10%"
      },
      {
        "data": "Actions",
        "width": "10%"
      }
      ],
      columnDefs: [{
        orderable: false,
        //  className: 'select-checkbox',
        targets: 0,
        'render': function (data, type, full, meta) {
          return buttons = '<div class="custom-control custom-checkbox pmd-checkbox"><input class="custom-control-input cb-element" type="checkbox" name="row[]" value="' + full.industrypartnerid + '" id="customCheckBox' + full.industrypartnerid + '"><label class="custom-control-label" for="customCheckBox' + full.industrypartnerid + '"></label></div>';
        }
      }, {
        orderable: true,
        targets: 1
      }, {
        orderable: true,
        targets: 2
      },
      {
        orderable: true,
        targets: 3
      },
      {
        orderable: true,
        targets: 4
      },
      {
        orderable: false,
        targets: 5,
        'render': function (data, type, full, meta) {
          if (data == 1) var buttonHTML = '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary btn-success btnApprove" title="Status" type="button" onclick="industryPartner.onActiveClick(' + meta.row + ');" value="' + data + '"><i class="material-icons pmd-sm">check</i></button>';
          if (data == 0) var buttonHTML = '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary btn-danger btnReject" title="Status" type="button" onclick="industryPartner.onActiveClick(' + meta.row + ');" value="' + data + '"><i class="material-icons pmd-sm">clear</i></button>';
          return buttonHTML;
        }
      },
      {
        orderable: false,
        targets: 6,
        'render': function (data, type, full, meta) {
          var buttonHTML = '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary btnEdit" onclick="industryPartner.edit(' + meta.row + ');" type="button" title="Edit"><i class="material-icons pmd-sm">edit</i></button> &nbsp;';
          buttonHTML += '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary btnView" type="button" onclick="industryPartner.view(' + meta.row + ');" value="' + data + '" title="Detail"><i class="material-icons pmd-sm">remove_red_eye</i></button>';
          buttonHTML += '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-danger btnDelete"  type="button" onclick="industryPartner.delete(' + meta.row + ');" value="' + data + '" title="Delete"><i class="material-icons pmd-sm">delete</i></button>';
          return buttonHTML;
        }
      }
      ]
      ,
      //For checkall..
      select: {
        style: 'multi',
        selector: 'td:first-child'
      },
      "language": {
        "info": " _START_ - _END_ of _TOTAL_ ",
        "sLengthMenu": "<span class='custom-select-title hidden'>Rows per page:</span> <span class='pmd-custom-select hidden'> _MENU_ </span>",
        processing: "<span class='loading'></span>",
        "sSearch": "",
        "sSearchPlaceholder": "Search",
        "paginate": {
          "sNext": " ",
          "sPrevious": " "
        },
      },
      dom:
        "<'card-header d-flex flex-row'<'data-table-title-responsive'><'search-paper pmd-textfield ml-auto'f>>" +
        "<'row'<'col-sm-12 datatableCustom'tr>>" +
        "<'card-footer' <'pmd-datatable-pagination' l i p>>",
    });

    // Apply the filter
    $('.custom-select-info').hide();

    //$('#' + datatableID + '_wrapper').find("div.data-table-title-responsive").html();

    $('#' + datatableID + ' tbody').unbind("click");
    $(".custom-select-action").html('<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary" type="button"><i class="material-icons pmd-sm">delete</i></button><button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary" type="button"><i class="material-icons pmd-sm">more_vert</i></button>');

    $(".cb-element").change(function () {
      if ($(".cb-element").length == $(".cb-element:checked").length)
        $("#customCheckBox0").prop('checked', true);
      else
        $("#customCheckBox0").prop('checked', false);
    });

    $('#' + datatableID).on('page.dt', function () {
      //var info = tableObj.page.info();
      $("#customCheckBox0").prop('checked', false);
    });

    var btnClear = $('<a class="btnClearDataTableFilter cursor-pointer hidden" style="position: absolute;right: 20px;z-index: 9999;top: 25px;"><i class="material-icons md-dark pmd-xs">cancel</i></a>');
    btnClear.appendTo($('#' + datatableID).parents('.dataTables_wrapper').find('.dataTables_filter'));

    $('#' + datatableID + '_wrapper .btnClearDataTableFilter').click(function () {
      $('#' + datatableID).dataTable().fnFilter('');
      $('.btnClearDataTableFilter').addClass('hidden');
    });
    if (gridIndustryPartnerObj.data().any()) {
      $('#' + datatableID + "_filter").hide();
      $('#' + datatableID + "_info").hide();
      $('#' + datatableID + "_paginate").hide();
      $('.customSelectAll').hide();
      $('#selectAction').hide();
    } else {
      $('#' + datatableID + "_filter").show();
      $('#' + datatableID + "_info").show();
      $('#' + datatableID + "_paginate").show();
      $('.customSelectAll').show();
      $('#selectAction').show();
    }
    function clickHandler(btnObj) {
      $('#' + datatableID + ' tbody').on('click', btnObj.buttonObj.targetClass, function () {
        btnObj.onClickEvent(tableObj.row($(this).parents('tr')));
      });
    }
    $("div.dataTables_filter input").keyup(function (e) {
      if ($(this).val() != '') {
        $('.btnClearDataTableFilter').removeClass('hidden');
      } else {
        $('.btnClearDataTableFilter').addClass('hidden');
      }
    });

    return gridIndustryPartnerObj;
  },
}
