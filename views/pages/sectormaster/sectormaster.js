$(document).ready(function () {
  sector.init();
  $("#customCheckBox0").change(function () {
    $('.cb-element').prop('checked', this.checked);
  });
});
$("#frmSector").validate({
  onkeyup: false,
  errorClass: 'has-error-text',
  validClass: 'valid',
  errorElement: 'span',
  rules: {
    "sectorName": {
      required: true,
    },
    "sectorSkillCouncil": {
      required: true,
    }
  },
  messages: {
    "sectorName": {
      required: 'Please enter sector name',
    },
    "sectorSkillCouncil": {
      required: 'Please enter sector skill council',
    }
  },
  highlight: function (element) {
    $(element).closest('div').addClass("has-error");
  },
  unhighlight: function (element) {
    $(element).closest('div').removeClass("has-error");
  },
  errorPlacement: function (error, element) {
    $(element).closest('div').append(error);
  },
  submitHandler: function (form) {
    $(".has-error-text").hide();
    $(".has-error-text").closest('div').removeClass("has-error");

    $(".loading").removeClass('hidden');
    var data = {};
    data = common.getFormValues($("#frmSector"));

    data.sectorid = sector.sectorid;

    var headers = {
      Authorization: $.cookie(Constants.User.authToken)
    };

    Api.post(Constants.Api.addUpdateSector, headers, data, function (error, res) {
      $(".loading").addClass('hidden');
      sector.saveDisabled = false;
      if (res != undefined && res.status == true) {
        common.showMessage(res.data.message, false);
        sector.clearValues(true);
      } else if (res != undefined && res.status == false) {
        common.showMessage(res.error.message, true);
      } else if (error != undefined && error.status == false) {
        common.showMessage(error.error.message, true);
      }
    });
  }
});
sector = {
  sectors: [],
  sectorid: -1,
  sectorTable: {},
  formMode: "",
  saveDisabled: false,
  /**
   * this function will be called on page load
   * @return {[type]} [description]
   */
  init: function () {
    sector.sectors.push({
      isActionButton: true,
      targets: 0,
      orderable: false,
      searchable: false,
      isVisible: true,
      buttons: [{
        checkboxObj: 1,
        dataRowField: "sectorid"
      }]
    })
    sector.sectors.push({
      name: "sectorName",
      targets: 1
    })
    /* sector.sectors.push({
      name: "sectorSkillCouncil",
      targets: 2
    }) */
    if (canEdit > 0) {
      var buttons = [];
      if (canDelete > 0) {
        buttons.push({
          buttonObj: Constants.staticHtml.editButton,
          onClickEvent: sector.edit
        }, {
            buttonObj: Constants.staticHtml.deleteButton,
            onClickEvent: sector.delete
          });
      } else {
        buttons.push({
          buttonObj: Constants.staticHtml.editButton,
          onClickEvent: sector.edit
        });
      }

      sector.sectors.push({
        isActionButton: true,
        targets: 2,
        orderable: false,
        searchable: false,
        isVisible: true,
        buttons: [{
          buttonObj: Constants.staticHtml.approveButton,
          onClickEvent: sector.onActiveClick,
          dataRowField: "status",
          compareValue: 1
        }, {
          buttonObj: Constants.staticHtml.rejectButton,
          onClickEvent: sector.onActiveClick,
          dataRowField: "status",
          compareValue: 0
        }]
      });

      sector.sectors.push({
        isActionButton: true,
        targets: 3,
        orderable: false,
        searchable: false,
        isVisible: true,
        buttons: buttons
      })
    }
    sector.getData();
  },
  /**
   * this function get user data and show into grid
   * @return {[type]} [description]
   */
  getData: function () {
    $(".loading").removeClass('hidden');
    var getSectorMappingUrl = Constants.Api.getSector + '/admin'
    var headers = {
      Authorization: $.cookie(Constants.User.authToken)
    };
    Api.post(getSectorMappingUrl, headers, '', function (error, res) {
      $(".loading").addClass('hidden');
      if (res != undefined && res.status == true) {
        sector.sectorTable = common.bindCommonDatatable(res.data, sector.sectors, "gridSector", objModules.sector);//, sector.edit, sector.delete);
      } else if (res != undefined && res.status == false) {
        common.showMessage(res.error.message, true);
      } else if (error != undefined && error.status == false) {
        common.showMessage(error.error.message, true);
      }
    });
  },
  /**
   * this function will be called when user click on edit from user grid and show currentRow into edit mode
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  edit: function (currentRow) {
    sector.formMode = "edit";
    //event.preventDefault();
    var currentRowData = currentRow.data();
    $(".addeditCustom").text('Edit');
    if (currentRowData != undefined && currentRowData.sectorid > 0) {
      sector.sectorid = currentRowData.sectorid;
      common.showHideDiv(false, objModules.sector);
      common.fillFormValues($("#frmSector"), currentRowData);
    }
  },
  /**
   * this function will be called when user changes the state from dropdown to render the city dropdown
   */
  changeState: function () {
    $(".loading").removeClass('hidden');
    common.getCity($("#drpState").val(), function (res) {
      if (res != undefined && res.status == true) {
        $(".loading").addClass('hidden');
        $('#drpCity').empty();
        sector.bindSelect("#drpCity", res.data, "cityid", "cityName");
      }
    });
  },
  /**
   * this function will be called when user click on view from user grid and show currentRows details
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  view: function (currentRow) {
    $(".loading").removeClass('hidden');
    //event.preventDefault();
    var currentRowData = currentRow.data();
    if (currentRowData != undefined && currentRowData.sectorid > 0) {
      var getsectorDetailUrl = Constants.Api.getsectorDetail + currentRowData.sectorid;
      var headers = {
        Authorization: $.cookie(Constants.User.authToken)
      };
      Api.get(getsectorDetailUrl, headers, function (error, res) {
        $(".loading").addClass('hidden');
        if (res != undefined) {
          $("#replaceHTML").html(res);
          $('#training-partner-dialog').modal('show');
        } else if (res != undefined && res.status == false) {
          common.showMessage(res.error.message, true);
        } else if (error != undefined && error.status == false) {
          common.showMessage(error.error.message, true);
        }
      });
    }
  },
  /**
   * this function will be called when user click on delete button from grid and delete current row record
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  delete: function (currentRow) {
    common.deleteRecordsApi(currentRow.data().sectorid, objModules.sector.tableId, 0, function (res) {
      if (res != undefined && res.status == true) {
        currentRow.remove().draw(false);
        //common.showMessage(res.message, false);
      } else {
        //common.showMessage(res.errorMsg, true);
      }
    });
    /* common.deleteData(objModules.sector, Constants.Api.removeSector, currentRow.data().sectorid, function (res) {
      if (res != undefined && res.status == true) {
        currentRow.remove().draw(false);
        common.showMessage(res.message, false);
      } else {
        common.showMessage(res.errorMsg, true);
      }
    }); */
  },
  /**
   * this function clear addRole form values,reset role grid data
   * @param  {[type]} isshowGridDiv [description]
   * @return {[type]}               [description]
   */
  clearValues: function (isshowGridDiv) {
    if (isshowGridDiv == true) {
      common.showHideDiv(true, objModules.sector);
      sector.sectorTable.destroy();
      sector.getData();
    }
    sector.sectorid = -1;
    common.clearValues($("#frmSector"));
    sector.saveDisabled = false;
    $(".has-error-text").remove();
  },
  /**
   * called when click on add icon to show add role form
   */
  add: function () {
    common.showHideDiv(false, objModules.sector);
    sector.clearValues(false);
    sector.formMode = "add";
    $(".addeditCustom").text('Add');
    common.getState(function (res) {
      if (res != undefined && res.status == true) {
        $('#drpState').empty();
        sector.bindSelect("#drpState", res.data, "stateid", "stateName");
        sector.changeState();
      }
    });
  },
  /**
   * called when user wants to import csv, it renders the view for bulk upload
   */
  import: function () {
    $('#divImport' + objModules.sector.displayName).show();
    $('#import' + objModules.sector.displayName).hide();
    $('#add' + objModules.sector.displayName).hide();
    $('#div' + objModules.sector.displayName).hide();
    $(".customerrorsuccess").addClass('hidden');
  },
  /* save: function (event) {
    if (sector.saveDisabled == false) {
      sector.saveDisabled = true;
    }
  }, */
  /**
   * called when cancel add or edit role to cancel add/edit
   * @return {[type]} [description]
   */
  cancel: function () {
    sector.clearValues(true);
  },
  /**
   * this function is used to save bulk upload csv data
   * @param  {[type]} event [description]
   * @return {[type]}       [description]
   */
  importCSV: function (event) {
    $(".loading").removeClass('hidden');
    var files = document.getElementById('fileupload').files[0];
    common.uploadMedia(files, "spreadSheet", 'sectors', function (res) {
      if (res != undefined && res.status == true) {
        $(".loading").addClass('hidden');
        common.showMessage(res.content.data.message, false);

        if (res.content.data.errorrecords.error.length > 0) {
          var errorrecords = res.content.data.errorrecords.error;
          var p = "";
          for (var i = 0; i < errorrecords.length; i++) {
            p += '[' + errorrecords[i].errorString + ']';
            p += '<br/>';
          }
          $(".customErrorRecords").html(p);
          $(".customerrorsuccess").removeClass('hidden');
        }

        //window.location.href = '/sector';
      } else if (res != undefined && res.status == false) {
        $(".loading").addClass('hidden');
        common.showMessage(res.errorMsg, true);
      } else if (error != undefined && error.status == false) {
        $(".loading").addClass('hidden');
        common.showMessage(error.error.message, true);
      }
    });
  },
  /**
   * this function fill values into select(dropdown) component
   * @param  {[type]} selectId     [description]
   * @param  {[type]} dataSet      [description]
   * @param  {[type]} valField     [description]
   * @param  {[type]} dispValField [description]
   * @return {[type]}              [description]
   */
  bindSelect: function (selectId, dataSet, valField, dispValField) {
    try {
      var selectOptions = "";
      for (var i = 0; i < dataSet.length; i++) {
        selectOptions += '<option value="' + dataSet[i][valField] + '">' + dataSet[i][dispValField] + '</option>'
      }
      $(selectId).append(selectOptions);
    } catch (e) {
      throw (e);
    }
  },
  /**
   * this function active or deactive user, when click on active/deactive button from grid
   * @param  {[type]} rowObj [description]
   * @return {[type]}        [description]
   */
  onActiveClick: function (rowObj) {
    $(".loading").removeClass('hidden');
    var data = rowObj.data();
    var status = "0";
    if (data.status == "0") {
      status = "1";
    }
    common.updateActiveStatus(data.sectorid, objModules.sector.tableId, status, function (res) {
      if (res != null && res.status == true) {
        sector.clearValues(true);
      }
    });
  },

  /**
   * calls when user selects one of the options from select action dropdown, i.e. select all and delete
   */
  mutipleaction: function (action) {
    var checkboxValues = [];
    $('input[name="row[]"]:checked').map(function () {
      checkboxValues.push($(this).val());
    });
    if (checkboxValues.length == 0) {
      common.showMessage('Please select at least one checkbox to do this action', true);
      return false;
    }
    var confirmation = 1;
    if (action == 'delete') {
      if (!confirm("Are you sure you want to delete multiple records ?")) {
        confirmation = 0;
      }
    }
    if (confirmation == 1) {
      var data = {};
      data.ids = checkboxValues;
      data.action = action;

      var headers = {
        Authorization: $.cookie(Constants.User.authToken)
      };

      Api.post(Constants.Api.sectorUpdateMultipleRecords, headers, data, function (error, res) {
        $(".loading").addClass('hidden');
        sector.saveDisabled = false;
        if (res != undefined && res.status == true) {
          common.showMessage(res.data.message, false);
          sector.clearValues(true);
        } else if (res != undefined && res.status == false) {
          common.showMessage(res.error.message, true);
          //sector.clearValues(false);
        } else if (error != undefined && error.status == false) {
          common.showMessage(error.error.message, true);
          //sector.clearValues(false);
        }
      });
    }
  }
}
