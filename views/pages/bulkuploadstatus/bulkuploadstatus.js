$(document).ready(function () {
  bulkUploadError.init();
  // $("#customCheckBox0").change(function () {
  //   $('.cb-element').prop('checked', this.checked);
  // });
});
// $("#frmbulkUploadError").validate({
//   onkeyup: false,
//   errorClass: 'has-error-text',
//   validClass: 'valid',
//   errorElement: 'span',
//   rules: {
//     "bulkUploadErrorName": {
//       required: true,
//     },
//     "bulkUploadErrorCode": {
//       required: true,
//     }
//   },
//   messages: {
//     "bulkUploadErrorName": {
//       required: 'Please enter job role name',
//     },
//     "bulkUploadErrorCode": {
//       required: 'Please enter job role code',
//     }
//   },
//   highlight: function (element) {
//     $(element).closest('div').addClass("has-error");
//   },
//   unhighlight: function (element) {
//     $(element).closest('div').removeClass("has-error");
//   },
//   errorPlacement: function (error, element) {
//     $(element).closest('div').append(error);
//   },
//   submitHandler: function (form) {
//     $(".has-error-text").hide();
//     $(".has-error-text").closest('div').removeClass("has-error");

//     $(".loading").removeClass('hidden');
//     var data = {};
//     data = common.getFormValues($("#frmbulkUploadError"));

//     data.bulkUploadErrorid = bulkUploadError.bulkUploadErrorid;

//     var headers = {
//       Authorization: $.cookie(Constants.User.authToken)
//     };

//     Api.post(Constants.Api.addUpdatebulkUploadError, headers, data, function (error, res) {
//       $(".loading").addClass('hidden');
//       bulkUploadError.saveDisabled = false;
//       if (res != undefined && res.status == true) {
//         common.showMessage(res.data.message, false);
//         bulkUploadError.clearValues(true);
//       } else if (res != undefined && res.status == false) {
//         common.showMessage(res.error.message, true);
//       } else if (error != undefined && error.status == false) {
//         common.showMessage(error.error.message, true);
//       }
//     });
//   }
// });
bulkUploadError = {
  bulkUploadErrors: [],
  bulkUploadErrorid: -1,
  bulkUploadErrorTable: {},
  formMode: "",
  saveDisabled: false,
  /**
   * this function will be called on page load
   * @return {[type]} [description]
   */
  init: function () {
    // bulkUploadError.bulkUploadErrors.push({
    //   isActionButton: true,
    //   targets: 0,
    //   orderable: false,
    //   searchable: false,
    //   isVisible: true,
    //   buttons: [{
    //     checkboxObj: 1,
    //     dataRowField: "bulkUploadErrorid"
    //   }]
    // })
    /* bulkUploadError.bulkUploadErrors.push({
      name: "sectorName",
      targets: 0
    }) */
    bulkUploadError.bulkUploadErrors.push({
      name: "filename",
      targets: 0
    })
    bulkUploadError.bulkUploadErrors.push({
      name: "remaining",
      targets: 1
    })
    bulkUploadError.bulkUploadErrors.push({
      name: "module",
      targets: 2
    })
    bulkUploadError.bulkUploadErrors.push({
      name: "error_file_name",
      targets: 3
    })
    bulkUploadError.bulkUploadErrors.push({
      name: "created",
      targets: 4
    })
    // if (canEdit > 0) {
    //   var buttons = [];
    //   if (canDelete > 0) {
    //     buttons.push({
    //       buttonObj: Constants.staticHtml.editButton,
    //       onClickEvent: bulkUploadError.edit
    //     }, {
    //         buttonObj: Constants.staticHtml.deleteButton,
    //         onClickEvent: bulkUploadError.delete
    //       });
    //   } else {
    //     buttons.push({
    //       buttonObj: Constants.staticHtml.editButton,
    //       onClickEvent: bulkUploadError.edit
    //     });
    //   }

      // bulkUploadError.bulkUploadErrors.push({
      //   isActionButton: true,
      //   targets: 3,
      //   orderable: false,
      //   searchable: false,
      //   isVisible: true,
      //   buttons: [{
      //     buttonObj: Constants.staticHtml.approveButton,
      //     onClickEvent: bulkUploadError.onActiveClick,
      //     dataRowField: "status",
      //     compareValue: 1
      //   }, {
      //     buttonObj: Constants.staticHtml.rejectButton,
      //     onClickEvent: bulkUploadError.onActiveClick,
      //     dataRowField: "status",
      //     compareValue: 0
      //   }]
      // });

    //   bulkUploadError.bulkUploadErrors.push({
    //     isActionButton: true,
    //     targets: 4,
    //     orderable: false,
    //     searchable: false,
    //     isVisible: true,
    //     buttons: buttons
    //   })
    // }
    bulkUploadError.getData();
  },
  /**
   * this function get user data and show into grid
   * @return {[type]} [description]
   */
  getData: function () {
    $(".loading").removeClass('hidden');
    var getErrorFileListURL = Constants.Api.getErrorFileList;
    // error-file-list
    // var getbulkUploadErrorMappingUrl = Constants.Api.getbulkUploadError
    var headers = {
      Authorization: $.cookie(Constants.User.authToken)
    };
    Api.post(getErrorFileListURL, headers, '', function (error, res) {
      $(".loading").addClass('hidden');
      if (res != undefined && res.status == true) {
        console.log(res,"RESPONSE FROM SERVER");
        // for(let every_row of res.data){
        //   every_row.filename = ;
        // }
        bulkUploadError.bulkUploadErrorTable = common.bindCommonDatatable(res.data, bulkUploadError.bulkUploadErrors, "gridbulkUploadError", objModules.bulkUploadError);//, bulkUploadError.edit, bulkUploadError.delete);
      } else if (res != undefined && res.status == false) {
        console.log('response is false');
        // common.showMessage(res.error.message, true);
      } else if (error != undefined && error.status == false) {
        console.log('response error');
        // common.showMessage(error.error.message, true);
      }
    });
  },
  // /**
  //  * this function will be called when user click on edit from user grid and show currentRow into edit mode
  //  * @param  {[type]} currentRow [description]
  //  * @return {[type]}            [description]
  //  */
  // edit: function (currentRow) {
  //   bulkUploadError.formMode = "edit";
  //   //event.preventDefault();
  //   var currentRowData = currentRow.data();
  //   $(".addeditCustom").text('Edit');
  //   if (currentRowData != undefined && currentRowData.bulkUploadErrorid > 0) {
  //     bulkUploadError.bulkUploadErrorid = currentRowData.bulkUploadErrorid;
  //     common.showHideDiv(false, objModules.bulkUploadError);
  //     common.fillFormValues($("#frmbulkUploadError"), currentRowData);
  //   }
  // },
  
  // /**
  //  * this function will be called when user click on view from user grid and show currentRows details
  //  * @param  {[type]} currentRow [description]
  //  * @return {[type]}            [description]
  //  */
  // // view: function (currentRow) {
  // //   $(".loading").removeClass('hidden');
  // //   //event.preventDefault();
  // //   var currentRowData = currentRow.data();
  // //   if (currentRowData != undefined && currentRowData.bulkUploadErrorid > 0) {
  // //     var getbulkUploadErrorDetailUrl = Constants.Api.getbulkUploadErrorDetail + currentRowData.bulkUploadErrorid;
  // //     var headers = {
  // //       Authorization: $.cookie(Constants.User.authToken)
  // //     };
  // //     Api.get(getbulkUploadErrorDetailUrl, headers, function (error, res) {
  // //       $(".loading").addClass('hidden');
  // //       if (res != undefined) {
  // //         $("#replaceHTML").html(res);
  // //         $('#training-partner-dialog').modal('show');
  // //       } else if (res != undefined && res.status == false) {
  // //         common.showMessage(res.error.message, true);
  // //       } else if (error != undefined && error.status == false) {
  // //         common.showMessage(error.error.message, true);
  // //       }
  // //     });
  // //   }
  // // },
  // /**
  //  * this function will be called when user click on delete button from grid and delete current row record
  //  * @param  {[type]} currentRow [description]
  //  * @return {[type]}            [description]
  //  */
  // // delete: function (currentRow) {
  // //   common.deleteRecordsApi(currentRow.data().bulkUploadErrorid, objModules.bulkUploadError.tableId, 0, function (res) {
  // //     if (res != undefined && res.status == true) {
  // //       currentRow.remove().draw(false);
  // //       //common.showMessage(res.message, false);
  // //     } else {
  // //       //common.showMessage(res.errorMsg, true);
  // //     }
  // //   });
  // //   /* common.deleteData(objModules.bulkUploadError, Constants.Api.removebulkUploadError, currentRow.data().bulkUploadErrorid, function (res) {
  // //     if (res != undefined && res.status == true) {
  // //       currentRow.remove().draw(false);
  // //       common.showMessage(res.message, false);
  // //     } else {
  // //       common.showMessage(res.errorMsg, true);
  // //     }
  // //   }); */
  // // },
  // /**
  //  * this function clear addRole form values,reset role grid data
  //  * @param  {[type]} isshowGridDiv [description]
  //  * @return {[type]}               [description]
  //  */
  // // clearValues: function (isshowGridDiv) {
  // //   if (isshowGridDiv == true) {
  // //     common.showHideDiv(true, objModules.bulkUploadError);
  // //     bulkUploadError.bulkUploadErrorTable.destroy();
  // //     bulkUploadError.getData();
  // //   }
  // //   bulkUploadError.bulkUploadErrorid = -1;
  // //   common.clearValues($("#frmbulkUploadError"));
  // //   bulkUploadError.saveDisabled = false;
  // //   $(".has-error-text").remove();
  // // },
  // /**
  //  * called when click on add icon to show add role form
  //  */
  // add: function () {
  //   common.showHideDiv(false, objModules.bulkUploadError);
  //   bulkUploadError.clearValues(false);
  //   bulkUploadError.formMode = "add";
  //   $(".addeditCustom").text('Add');
  //   common.getState(function (res) {
  //     if (res != undefined && res.status == true) {
  //       $('#drpState').empty();
  //       bulkUploadError.bindSelect("#drpState", res.data, "stateid", "stateName");
  //       bulkUploadError.changeState();
  //     }
  //   });
  // },
  // /**
  //  * called when user wants to import csv, it renders the view for bulk upload
  //  */
  // import: function () {
  //   $('#divImport' + objModules.bulkUploadError.displayName).show();
  //   $('#import' + objModules.bulkUploadError.displayName).hide();
  //   $('#add' + objModules.bulkUploadError.displayName).hide();
  //   $('#div' + objModules.bulkUploadError.displayName).hide();
  //   $(".customerrorsuccess").addClass('hidden');
  // },
  // /* save: function (event) {
  //   if (bulkUploadError.saveDisabled == false) {
  //     bulkUploadError.saveDisabled = true;
  //   }
  // }, */
  // /**
  //  * called when cancel add or edit role to cancel add/edit
  //  * @return {[type]} [description]
  //  */
  // cancel: function () {
  //   bulkUploadError.clearValues(true);
  // },
  // /**
  //  * this function is used to save bulk upload csv data
  //  * @param  {[type]} event [description]
  //  * @return {[type]}       [description]
  //  */
  // importCSV: function (event) {
  //   $(".loading").removeClass('hidden');
  //   var files = document.getElementById('fileupload').files[0];
  //   common.uploadMedia(files, "spreadSheet", 'bulkUploadErrors', function (res) {
  //     if (res != undefined && res.status == true) {
  //       $(".loading").addClass('hidden');
  //       common.showMessage(res.content.data.message, false);

  //       if (res.content.data.errorrecords.error.length > 0) {
  //         var errorrecords = res.content.data.errorrecords.error;
  //         var p = "";
  //         for (var i = 0; i < errorrecords.length; i++) {
  //           p += '[' + errorrecords[i].errorString + ']';
  //           p += '<br/>';
  //         }
  //         $(".customErrorRecords").html(p);
  //         $(".customerrorsuccess").removeClass('hidden');
  //       }

  //       //window.location.href = '/bulkUploadError';
  //     } else if (res != undefined && res.status == false) {
  //       $(".loading").addClass('hidden');
  //       common.showMessage(res.errorMsg, true);
  //     } else if (error != undefined && error.status == false) {
  //       $(".loading").addClass('hidden');
  //       common.showMessage(error.error.message, true);
  //     }
  //   });
  // },
  // /**
  //  * this function fill values into select(dropdown) component
  //  * @param  {[type]} selectId     [description]
  //  * @param  {[type]} dataSet      [description]
  //  * @param  {[type]} valField     [description]
  //  * @param  {[type]} dispValField [description]
  //  * @return {[type]}              [description]
  //  */
  // bindSelect: function (selectId, dataSet, valField, dispValField) {
  //   try {
  //     var selectOptions = "";
  //     for (var i = 0; i < dataSet.length; i++) {
  //       selectOptions += '<option value="' + dataSet[i][valField] + '">' + dataSet[i][dispValField] + '</option>'
  //     }
  //     $(selectId).append(selectOptions);
  //   } catch (e) {
  //     throw (e);
  //   }
  // },
  // /**
  //  * this function active or deactive user, when click on active/deactive button from grid
  //  * @param  {[type]} rowObj [description]
  //  * @return {[type]}        [description]
  //  */
  // onActiveClick: function (rowObj) {
  //   $(".loading").removeClass('hidden');
  //   var data = rowObj.data();
  //   var status = "0";
  //   if (data.status == "0") {
  //     status = "1";
  //   }
  //   common.updateActiveStatus(data.bulkUploadErrorid, objModules.bulkUploadError.tableId, status, function (res) {
  //     if (res != null && res.status == true) {
  //       bulkUploadError.clearValues(true);
  //     }
  //   });
  // },
  // /**
  //  * calls when user selects one of the options from select action dropdown, i.e. select all and delete
  //  */
  // mutipleaction: function (action) {
  //   var checkboxValues = [];
  //   $('input[name="row[]"]:checked').map(function () {
  //     checkboxValues.push($(this).val());
  //   });
  //   if (checkboxValues.length == 0) {
  //     common.showMessage('Please select at least one checkbox to do this action', true);
  //     return false;
  //   }
  //   var confirmation = 1;
  //   if (action == 'delete') {
  //     if (!confirm("Are you sure you want to delete multiple records ?")) {
  //       confirmation = 0;
  //     }
  //   }
  //   if (confirmation == 1) {
  //     var data = {};
  //     data.ids = checkboxValues;
  //     data.action = action;

  //     var headers = {
  //       Authorization: $.cookie(Constants.User.authToken)
  //     };

  //     Api.post(Constants.Api.bulkUploadErrorUpdateMultipleRecords, headers, data, function (error, res) {
  //       $(".loading").addClass('hidden');
  //       bulkUploadError.saveDisabled = false;
  //       if (res != undefined && res.status == true) {
  //         common.showMessage(res.data.message, false);
  //         bulkUploadError.clearValues(true);
  //       } else if (res != undefined && res.status == false) {
  //         common.showMessage(res.error.message, true);
  //         //bulkUploadError.clearValues(false);
  //       } else if (error != undefined && error.status == false) {
  //         common.showMessage(error.error.message, true);
  //         //bulkUploadError.clearValues(false);
  //       }
  //     });
  //   }
  // }
}
