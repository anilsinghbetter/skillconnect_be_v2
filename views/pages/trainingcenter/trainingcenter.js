$(document).ready(function () {
  trainingCenter.init();
  $("#customCheckBox0").change(function () {
    $('.cb-element').prop('checked', this.checked);
  });
});

$("#frmtrainingCenter").validate({
  onkeyup: false,
  errorClass: 'has-error-text',
  validClass: 'valid',
  errorElement: 'span',
  rules: {
    "trainingcenter_name": {
      required: true,
    },
    "sdms_trainingcenter_id": {
      required: true,
    },
    "address": {
      required: true,
    },
    "contact_person_name": {
      required: true,
    },
    "contact_email_address": {
      required: true,
    },
    "pincode": {
      required: true,
    },
    "drpState": {
      required: true,
    },
    "drpCity": {
      required: true,
    },
  },
  messages: {
    "trainingcenter_name": {
      required: 'Please enter training center name',
    },
    "sdms_trainingcenter_id": {
      required: 'Please enter Training center ID',
    },
    "address": {
      required: 'Please enter adress',
    },
    "contact_person_name": {
      required: 'Please enter contact person name',
    },
    "contact_email_address": {
      required: 'Please enter contact person e-mail address',
    },
    "pincode": {
      required: 'Please enter pincode',
    },
    "drpState": {
      required: 'Please select state',
    },
    "drpCity": {
      required: 'Please select city',
    },
  },
  highlight: function (element) {
    $(element).closest('div').addClass("has-error");
  },
  unhighlight: function (element) {
    $(element).closest('div').removeClass("has-error");
  },
  errorPlacement: function (error, element) {
    $(element).closest('div').append(error);
  },
  submitHandler: function (form) {
    $(".has-error-text").hide();
    $(".has-error-text").closest('div').removeClass("has-error");

    $(".loading").removeClass('hidden');
    var data = {};
    data = common.getFormValues($("#frmtrainingCenter"));
    if(data.cityid != undefined) {
      data.districtname = data.cityid;
    }
    if(data.stateid != undefined) {
      data.statename = data.stateid;
    }
    data.trainingcenterid = trainingCenter.trainingcenterid;

    var headers = {
      Authorization: $.cookie(Constants.User.authToken)
    };

    Api.post(Constants.Api.addUpdatetrainingCenter + 'admin', headers, data, function (error, res) {
      $(".loading").addClass('hidden');
      trainingCenter.saveDisabled = false;
      if (res != undefined && res.status == true) {
        common.showMessage(res.data.message, false);
        trainingCenter.clearValues(true);
      } else if (res != undefined && res.status == false) {
        common.showMessage(res.error.message, true);
      } else if (error != undefined && error.status == false) {
        common.showMessage(error.error.message, true);
      }
    });
  }
});

trainingCenter = {
  trainingCenters: [],
  user_id: -1,
  trainingCenterTable: {},
  formMode: "",
  saveDisabled: false,
  /**
   * this function will be called on page load
   * @return {[type]} [description]
   */
  init: function () {
    trainingCenter.trainingCenters.push({
      isActionButton: true,
      targets: 0,
      orderable: false,
      searchable: false,
      isVisible: true,
      buttons: [{
        checkboxObj: 1,
        dataRowField: "trainingcenterid"
      }]
    })
    trainingCenter.trainingCenters.push({
      name: "sdms_trainingcenter_id",
      targets: 1
    })
    trainingCenter.trainingCenters.push({
      name: "trainingcenter_name",
      targets: 2
    })
    trainingCenter.trainingCenters.push({
      name: "contact_person_name",
      targets: 3
    })
    trainingCenter.trainingCenters.push({
      name: "contact_email_address",
      targets: 4
    })
    trainingCenter.trainingCenters.push({
      name: "contact_mobile",
      targets: 5
    })

    if (canAdd > 0) {
      var buttons = [];
      if (canDelete > 0) {
        buttons.push(
          {
            buttonObj: Constants.staticHtml.editButton,
            onClickEvent: trainingCenter.edit
          },
          {
            buttonObj: Constants.staticHtml.viewButton,
            onClickEvent: trainingCenter.view
          },
          {
            buttonObj: Constants.staticHtml.deleteButton,
            onClickEvent: trainingCenter.delete
          });
      } else {
        buttons.push({
          buttonObj: Constants.staticHtml.viewButton,
          onClickEvent: trainingCenter.view
        });
      }

      trainingCenter.trainingCenters.push({
        isActionButton: true,
        targets: 6,
        orderable: false,
        searchable: false,
        isVisible: true,
        buttons: [{
          buttonObj: Constants.staticHtml.approveButton,
          onClickEvent: trainingCenter.onActiveClick,
          dataRowField: "status",
          compareValue: 1
        }, {
          buttonObj: Constants.staticHtml.rejectButton,
          onClickEvent: trainingCenter.onActiveClick,
          dataRowField: "status",
          compareValue: 0
        }]
      });
    }
    trainingCenter.trainingCenters.push({
      isActionButton: true,
      targets: 7,
      orderable: false,
      searchable: false,
      isVisible: true,
      buttons: buttons
    })

    trainingCenter.getData();
  },
  /**
   * this function get user data and show into grid
   * @return {[type]} [description]
   */
  getData: function () {
    $(".loading").removeClass('hidden');
    var gettrainingCenterUrl = Constants.Api.getTrainingCenter + 'admin';
    var headers = {
      Authorization: $.cookie(Constants.User.authToken)
    };
    Api.post(gettrainingCenterUrl, headers, null, function (error, res) {
      $(".loading").addClass('hidden');
      if (res != undefined && res.status == true) {
        trainingCenter.trainingCenterTable = common.bindCommonDatatable(res.data, trainingCenter.trainingCenters, "gridtrainingCenter", objModules.trainingCenter);//, trainingCenter.edit, trainingCenter.delete);
      } else if (res != undefined && res.status == false) {
        common.showMessage(res.error.message, true);
      } else if (error != undefined && error.status == false) {
        common.showMessage(error.error.message, true);
      }
    });
  },
  /**
   * this function will be called when user click on edit from user grid and show currentRow into edit mode
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  edit: function (currentRow) {
    trainingCenter.formMode = "edit";
    //event.preventDefault();
    $(".addeditCustom").text('Edit');
    var currentRowData = currentRow.data();
    if (currentRowData != undefined && currentRowData.trainingcenterid > 0) {
      trainingCenter.trainingcenterid = currentRowData.trainingcenterid;
      common.showHideDiv(false, objModules.trainingCenter);
      common.fillFormValues($("#frmtrainingCenter"), currentRowData);
      common.getState(function (res) {
        if (res != undefined && res.status == true) {
          $('#drpState').empty();
          trainingCenter.bindSelect("#drpState", res.data, "stateid", "stateName");

          common.getCity(currentRowData.stateid, function (res) {
            if (res != undefined && res.status == true) {
              $('#drpCity').empty();
              trainingCenter.bindSelect("#drpCity", res.data, "cityid", "cityName");

              trainingCenter.trainingCenterid = currentRowData.trainingCenterid;
              common.showHideDiv(false, objModules.trainingCenter);
              common.fillFormValues($("#frmtrainingCenter"), currentRowData);
            }
          });
        } else {
          common.showMessage(res.errorMsg, true);
        }
      });
    }
  },
  /**
   * this function will be called when user click on delete button from grid and delete current row record
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  delete: function (currentRow) {
    common.deleteRecordsApi(currentRow.data().trainingcenterid, objModules.trainingCenter.tableId, 0, function (res) {
      if (res != undefined && res.status == true) {
        currentRow.remove().draw(false);
        //common.showMessage(res.message, false);
      } else {
        //common.showMessage(res.errorMsg, true);
      }
    });
  },
  /**
   * this function will be called when user click on view from user grid and show currentRows details
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  view: function (currentRow) {
    $(".loading").removeClass('hidden');
    //event.preventDefault();
    var currentRowData = currentRow.data();
    if (currentRowData != undefined && currentRowData.trainingcenterid > 0) {
      var gettrainingCenterDetailUrl = Constants.Api.getTrainingCenterDetail + currentRowData.trainingcenterid;
      var headers = {
        Authorization: $.cookie(Constants.User.authToken)
      };
      Api.post(gettrainingCenterDetailUrl, headers, { data: currentRowData }, function (error, res) {
        $(".loading").addClass('hidden');
        if (res != undefined) {
          $("#replaceHTML").html(res);
          $('#training-center-dialog').modal('show');
        } else if (res != undefined && res.status == false) {
          common.showMessage(res.error.message, true);
        } else if (error != undefined && error.status == false) {
          common.showMessage(error.error.message, true);
        }
      });
    }
  },
  /**
   * this function clear addRole form values,reset role grid data
   * @param  {[type]} isshowGridDiv [description]
   * @return {[type]}               [description]
   */
  clearValues: function (isshowGridDiv) {
    if (isshowGridDiv == true) {
      common.showHideDiv(true, objModules.trainingCenter);
      trainingCenter.getData();
      trainingCenter.trainingCenterTable.destroy();
    }
    trainingCenter.user_id = -1;
    common.clearValues($("#frmtrainingCenter"));
    trainingCenter.saveDisabled = false;
  },
  /**
   * called when click on add icon to show add role form
   */
  add: function () {
    common.showHideDiv(false, objModules.trainingCenter);
    trainingCenter.clearValues(false);
    trainingCenter.formMode = "add";
    // $("#txtPassword").attr("validation","required");
  },
  /**
   * called when cancel add or edit role to cancel add/edit
   * @return {[type]} [description]
   */
  cancel: function () {
    trainingCenter.clearValues(true);
  },
  /**
   * this function is used to save or update role data
   * @param  {[type]} event [description]
   * @return {[type]}       [description]
   */
  /* save: function (event) {
    if (trainingCenter.saveDisabled == false) {
      trainingCenter.saveDisabled = true;
      var validation = formValidation('frmtrainingCenter');
      if (validation == true) {
        var data = {};
        data = common.getFormValues($("#frmtrainingCenter"));
        data.user_id = trainingCenter.user_id;
        var headers = {
          Authorization: $.cookie(Constants.User.authToken)
        };
        Api.post(Constants.Api.addUpdatetrainingCenter, headers, data, function (error, res) {
          trainingCenter.saveDisabled = false;
          if (res != undefined && res.status == true) {
            common.showMessage(res.content.message, false);
            trainingCenter.clearValues(true);
          } else if (res != undefined && res.status == false) {
            common.showMessage(res.error.message, true);
            trainingCenter.clearValues(false);
          } else if (error != undefined && error.status == false) {
            common.showMessage(error.error.message, true);
            trainingCenter.clearValues(false);
          }
        });
      } else {
        trainingCenter.saveDisabled = false;
      }
    }

  }, */
  /**
   * this function will be called when user changes the state from dropdown to render the city dropdown
   */
  changeState: function () {
    $(".loading").removeClass('hidden');
    common.getCity($("#drpState").val(), function (res) {
      if (res != undefined && res.status == true) {
        $(".loading").addClass('hidden');
        $('#drpCity').empty();
        trainingCenter.bindSelect("#drpCity", res.data, "cityid", "cityName");
      }
    });
  },
  /**
   * this function fill values into select(dropdown) component
   * @param  {[type]} selectId     [description]
   * @param  {[type]} dataSet      [description]
   * @param  {[type]} valField     [description]
   * @param  {[type]} dispValField [description]
   * @return {[type]}              [description]
   */
  bindSelect: function (selectId, dataSet, valField, dispValField) {
    try {
      var selectOptions = "";
      for (var i = 0; i < dataSet.length; i++) {
        selectOptions += '<option value="' + dataSet[i][valField] + '">' + dataSet[i][dispValField] + '</option>'
      }
      $(selectId).append(selectOptions);
    } catch (e) {
      throw (e);
    }
  },
  /**
   * this function active or deactive user, when click on active/deactive button from grid
   * @param  {[type]} rowObj [description]
   * @return {[type]}        [description]
   */
  onActiveClick: function (rowObj) {
    var data = rowObj.data();
    var status = "0";
    if (data.status == "0") {
      status = "1";
    }
    common.updateActiveStatus(data.trainingcenterid, objModules.trainingCenter.tableId, status, function (res) {
      if (res != null && res.status == true) {
        trainingCenter.clearValues(true);
      }
    });
  },
  /**
   * calls when user selects one of the options from select action dropdown, i.e. select all and delete
   */
  mutipleaction: function (action) {
    var checkboxValues = [];
    $('input[name="row[]"]:checked').map(function () {
      checkboxValues.push($(this).val());
    });
    if (checkboxValues.length == 0) {
      common.showMessage('Please select at least one checkbox to do this action', true);
      return false;
    }
    var confirmation = 1;
    if (action == 'delete') {
      if (!confirm("Are you sure you want to delete multiple records ?")) {
        confirmation = 0;
      }
    }
    if (confirmation == 1) {
      var data = {};
      data.ids = checkboxValues;
      data.action = action;

      var headers = {
        Authorization: $.cookie(Constants.User.authToken)
      };

      Api.post(Constants.Api.trainingCenterUpdateMultipleRecords, headers, data, function (error, res) {
        $(".loading").addClass('hidden');
        trainingCenter.saveDisabled = false;
        if (res != undefined && res.status == true) {
          common.showMessage(res.data.message, false);
          trainingCenter.clearValues(true);
        } else if (res != undefined && res.status == false) {
          common.showMessage(res.error.message, true);

        } else if (error != undefined && error.status == false) {
          common.showMessage(error.error.message, true);

        }
      });
    }
  }
}
