$(document).ready(function () {
  contact.init();
  $("#customCheckBox0").change(function () {
    $('.cb-element').prop('checked', this.checked);
  });
});

$("#frmContact").validate({
  onkeyup: false,
  errorClass: 'has-error-text',
  validClass: 'valid',
  errorElement: 'span',
  rules: {
    "comment": {
      required: true,
    },
  },
  messages: {
    "comment": {
      required: 'Please enter reply',
    },
  },
  highlight: function (element) {
    $(element).closest('div').addClass("has-error");
  },
  unhighlight: function (element) {
    $(element).closest('div').removeClass("has-error");
  },
  errorPlacement: function (error, element) {
    $(element).closest('div').append(error);
  },
  submitHandler: function (form) {
    $(".has-error-text").hide();
    $(".has-error-text").closest('div').removeClass("has-error");

    $(".loading").removeClass('hidden');
    var data = {};
    data = common.getFormValues($("#frmContact"));
    
    data.row_id = contact.row_id;

    var headers = {
      Authorization: $.cookie(Constants.User.authToken)
    };

    Api.post(Constants.Api.updateContactEnquiry , headers, data, function (error, res) {
      $(".loading").addClass('hidden');
      contact.saveDisabled = false;
      if (res != undefined && res.status == true) {
        common.showMessage(res.data.message, false);
        contact.clearValues(true);
      } else if (res != undefined && res.status == false) {
        common.showMessage(res.error.message, true);
      } else if (error != undefined && error.status == false) {
        common.showMessage(error.error.message, true);
      }
    });
  }
});


contact = {
  contacts: [],
  contactid: -1,
  contactTable: {},
  formMode: "",
  saveDisabled: false,
  /**
   * this function will be called on page load
   * @return {[type]} [description]
   */
  init: function () {
    // contact.contacts.push({
    //   isActionButton: true,
    //   targets: 0,
    //   orderable: false,
    //   searchable: false,
    //   isVisible: false,
    //   buttons: [{
    //     checkboxObj: 0,
    //     dataRowField: "request_id"
    //   }]
    // })
  
    contact.contacts.push({
      name: "firstName",
      targets: 0
    })
    contact.contacts.push({
      name: "subject",
      targets: 1
    })
    contact.contacts.push({
      name: "message",
      targets: 2 
    })
    contact.contacts.push({
      name: "eMail",
      targets: 3
    })
    /* contact.contacts.push({
      name: "is_mail_sent",
      targets: 5
    }) */
    var buttons = [];
    buttons.push(
      {
        buttonObj: Constants.staticHtml.viewButton,
        onClickEvent: contact.view
      },{
        buttonObj: Constants.staticHtml.replyButton,
        onClickEvent: contact.reply
      });
    // if (canAdd > 0) {
    //   var buttons = [];
    //   if (canDelete > 0) {
    //     buttons.push(
    //       {
    //         buttonObj: Constants.staticHtml.viewButton,
    //         onClickEvent: contact.view
    //       }
    //       /* {
    //         buttonObj: Constants.staticHtml.editButton,
    //         onClickEvent: contact.edit
    //       }, */
    //       // {
    //       //   buttonObj: Constants.staticHtml.deleteButton,
    //       //   onClickEvent: contact.delete
    //       // }
    //     );
    //   } else {
    //     buttons.push({
    //       buttonObj: Constants.staticHtml.viewButton,
    //       onClickEvent: contact.view
    //     });
    //   }

    //   /* contact.contacts.push({
    //     isActionButton: true,
    //     targets: 5,
    //     orderable: false,
    //     searchable: false,
    //     isVisible: true,
    //     buttons: [{
    //       buttonObj: Constants.staticHtml.approveButton,
    //       onClickEvent: contact.onActiveClick,
    //       dataRowField: "status",
    //       compareValue: 1
    //     }, {
    //       buttonObj: Constants.staticHtml.rejectButton,
    //       onClickEvent: contact.onActiveClick,
    //       dataRowField: "status",
    //       compareValue: 0
    //     }]
    //   }); */
    // }

    contact.contacts.push({
      isActionButton: true,
      targets: 4,
      orderable: false,
      searchable: false,
      isVisible: true,
      buttons: buttons
    })
    
    contact.getData();
  
  },

  /**
   * this function get user data and show into grid
   * @return {[type]} [description]
   */
  getData: function () {
    $(".loading").removeClass('hidden');
    var getcontactUrl = Constants.Api.getcontact;
    //console.log("URL",getcontactUrl);
    var headers = {
      Authorization: $.cookie(Constants.User.authToken)
    };
    Api.post(getcontactUrl, headers, '', function (error, res) {
      $(".loading").addClass('hidden');
      if (res != undefined && res.status == true) {
        // Combining firstname and lastname to display
        for (let row of res.data){
          if (row.lastName !== null){
            row.firstName += ' ' + row.lastName;
          }
        }
        //console.log(objModules.contact,"RESPONSE");
        contact.contactTable = common.bindCommonDatatable(res.data, contact.contacts, "gridContact", objModules.contact);
      } else if (res != undefined && res.status == false) {
        common.showMessage(res.error.message, true);
      } else if (error != undefined && error.status == false) {
        common.showMessage(error.error.message, true);
      }
    });
  },

  reply: function (currentRow) {
    contact.formMode = "edit";
    $(".addeditCustom").text('Reply Of');
    var currentRowData = currentRow.data(); 
    //console.log(currentRowData,"DATA");
    if (currentRowData != undefined && currentRowData.row_id > 0) {
      contact.row_id = currentRowData.row_id;
      common.showHideDiv(false, objModules.contact);
      common.fillFormValues($("#frmContact"), currentRowData);
    }
  },
  /**
   * this function will be called when user click on view from user grid and show currentRows details
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  view: function (currentRow) {
     $(".loading").removeClass('hidden');
    var currentRowData = currentRow.data();
    if (currentRowData != undefined && currentRowData.partner_id > 0) {
      // console.log(currentRowData,"ROW DATA")
      var getcontactDetailUrl = Constants.Api.getcontactDetail + currentRowData.partner_id + '/' + currentRowData.partner_type + '/' + currentRowData.row_id ;
      //console.log("URL",getcontactDetailUrl);
      var headers = {
        Authorization: $.cookie(Constants.User.authToken)
      };
      Api.get(getcontactDetailUrl, headers, function (error, res) {
        $(".loading").addClass('hidden');
        if (res != undefined) {
          $("#replaceHTML").html(res);
          $('#contact-dialog').modal('show');
        } else if (res != undefined && res.status == false) {
          common.showMessage(res.error.message, true);
        } else if (error != undefined && error.status == false) {
          common.showMessage(error.error.message, true);
        }
      });
     }
  },
  /**
   * this function will be called when user click on delete button from grid and delete current row record
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  // delete: function (currentRow) {
  //   common.deleteRecordsApi(currentRow.data().request_id, objModules.contact.tableId, 0, function (res) {
  //     if (res != undefined && res.status == true) {
  //       currentRow.remove().draw(false);
  //       //common.showMessage(res.message, false);
  //     } else {
  //       //common.showMessage(res.errorMsg, true);
  //     }
  //   });
  // },
  /**
   * this function clear addRole form values,reset role grid data
   * @param  {[type]} isshowGridDiv [description]
   * @return {[type]}               [description]
   */
  clearValues: function (isshowGridDiv) {
    if (isshowGridDiv == true) {
      common.showHideDiv(true, objModules.contact);
      contact.contactTable.destroy();
      contact.getData();
    }
    common.clearValues($("#frmContact"));
    contact.saveDisabled = false;
    contact.contactid = -1;
   // common.clearValues($("#frmTrainingPartner"));
    contact.saveDisabled = false;
    $(".has-error-text").remove();
  },
  cancel: function () {
    contact.clearValues(true);
  },
  /**
   * calls when user selects one of the options from select action dropdown, i.e. select all and delete
   */
  // mutipleaction: function (action) {
  //   var checkboxValues = [];
  //   $('input[name="row[]"]:checked').map(function () {
  //     checkboxValues.push($(this).val());
  //   });
  //   if (checkboxValues.length == 0) {
  //     common.showMessage('Please select at least one checkbox to do this action', true);
  //     return false;
  //   }
  //   var confirmation = 1;
  //   if (action == 'delete') {
  //     if (!confirm("Are you sure you want to delete multiple records ?")) {
  //       confirmation = 0;
  //     }
  //   }
  //   if (confirmation == 1) {
  //     var data = {};
  //     data.ids = checkboxValues;
  //     data.action = action;

  //     var headers = {
  //       Authorization: $.cookie(Constants.User.authToken)
  //     };

  //     // Api.post(Constants.Api.contactsUpdateMultipleRecords, headers, data, function (error, res) {
  //     //   $(".loading").addClass('hidden');
  //     //   contact.saveDisabled = false;
  //     //   if (res != undefined && res.status == true) {
  //     //     common.showMessage(res.data.message, false);
  //     //     contact.clearValues(true);
  //     //   } else if (res != undefined && res.status == false) {
  //     //     common.showMessage(res.error.message, true);
  //     //   } else if (error != undefined && error.status == false) {
  //     //     common.showMessage(error.error.message, true);
  //     //   }
  //     // });
  //   }
  // }
}
