$(document).ready(function () {
  sectorJobRole.init();
  $("#customCheckBox0").change(function () {
    $('.cb-element').prop('checked', this.checked);
  });
});

sectorJobRole = {
  sectorJobRoles: [],
  sectorJobRoleid: -1,
  sectorJobRoleTable: {},
  formMode: "",
  saveDisabled: false,
  /**
   * this function will be called on page load
   * @return {[type]} [description]
   */
  init: function () {
    /* sectorJobRole.sectorJobRoles.push({
      isActionButton: true,
      targets: 0,
      orderable: false,
      searchable: false,
      isVisible: true,
      buttons: [{
        checkboxObj: 1,
        dataRowField: "sectorid"
      }]
    }) */
    sectorJobRole.sectorJobRoles.push({
      name: "sectorName",
      targets: 0
    })
    /* sectorJobRole.sectorJobRoles.push({
      name: "sectorSkillCouncil",
      targets: 1
    }) */
    sectorJobRole.sectorJobRoles.push({
      name: "jobroleName",
      targets: 1
    })
    /* sectorJobRole.sectorJobRoles.push({
      name: "jobroleCode",
      targets: 3
    }) */
    
    if (canEdit > 0) {
      /* var buttons = [];
      if (canDelete > 0) {
        buttons.push({
          buttonObj: Constants.staticHtml.editButton,
          onClickEvent: sectorJobRole.edit
        }, {
            buttonObj: Constants.staticHtml.deleteButton,
            onClickEvent: sectorJobRole.delete
          });
      } else {
        buttons.push({
          buttonObj: Constants.staticHtml.editButton,
          onClickEvent: sectorJobRole.edit
        });
      } */

      /* sectorJobRole.sectorJobRoles.push({
        isActionButton: true,
        targets: 5,
        orderable: false,
        searchable: false,
        isVisible: true,
        buttons: [{
          buttonObj: Constants.staticHtml.approveButton,
          onClickEvent: sectorJobRole.onActiveClick,
          dataRowField: "isJobRoleActive",
          compareValue: 1
        }, {
          buttonObj: Constants.staticHtml.rejectButton,
          onClickEvent: sectorJobRole.onActiveClick,
          dataRowField: "isJobRoleActive",
          compareValue: 0
        }]
      });

      sectorJobRole.sectorJobRoles.push({
        isActionButton: true,
        targets: 6,
        orderable: false,
        searchable: false,
        isVisible: true,
        buttons: buttons
      }) */
    }
    sectorJobRole.getData();
  },
  /**
   * this function get user data and show into grid
   * @return {[type]} [description]
   */
  getData: function () {
    $(".loading").removeClass('hidden');
    var getSectorJobRoleMappingUrl = Constants.Api.getSectorJobRoleMapping
    var headers = {
      Authorization: $.cookie(Constants.User.authToken)
    };
    Api.post(getSectorJobRoleMappingUrl, headers, '', function (error, res) {
      $(".loading").addClass('hidden');
      if (res != undefined && res.status == true) {
        sectorJobRole.sectorJobRoleTable = common.bindCommonDatatable(res.data, sectorJobRole.sectorJobRoles, "gridSectorJobRole", objModules.sectorJobRole);//, sectorJobRole.edit, sectorJobRole.delete);
      } else if (res != undefined && res.status == false) {
        common.showMessage(res.error.message, true);
      } else if (error != undefined && error.status == false) {
        common.showMessage(error.error.message, true);
      }
    });
  },
  /**
   * this function will be called when user click on edit from user grid and show currentRow into edit mode
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  edit: function (currentRow) {
    sectorJobRole.formMode = "edit";
    //event.preventDefault();
    var currentRowData = currentRow.data();
    $(".addeditCustom").text('Edit');
    if (currentRowData != undefined && currentRowData.sectorJobRoleid > 0) {
      common.getState(function (res) {
        if (res != undefined && res.status == true) {
          $('#drpState').empty();
          sectorJobRole.bindSelect("#drpState", res.data, "stateid", "stateName");

          common.getCity(currentRowData.stateid, function (res) {
            if (res != undefined && res.status == true) {
              $('#drpCity').empty();
              sectorJobRole.bindSelect("#drpCity", res.data, "cityid", "cityName");

              sectorJobRole.sectorJobRoleid = currentRowData.sectorJobRoleid;
              common.showHideDiv(false, objModules.sectorJobRole);
              common.fillFormValues($("#frmsectorJobRole"), currentRowData);
            }
          });
        } else {
          common.showMessage(res.errorMsg, true);
        }
      });
    }
  },
  /**
   * this function will be called when user changes the state from dropdown to render the city dropdown
   */
  changeState: function () {
    $(".loading").removeClass('hidden');
    common.getCity($("#drpState").val(), function (res) {
      if (res != undefined && res.status == true) {
        $(".loading").addClass('hidden');
        $('#drpCity').empty();
        sectorJobRole.bindSelect("#drpCity", res.data, "cityid", "cityName");
      }
    });
  },
  /**
   * this function will be called when user click on view from user grid and show currentRows details
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  view: function (currentRow) {
    $(".loading").removeClass('hidden');
    //event.preventDefault();
    var currentRowData = currentRow.data();
    if (currentRowData != undefined && currentRowData.sectorJobRoleid > 0) {
      var getsectorJobRoleDetailUrl = Constants.Api.getsectorJobRoleDetail + currentRowData.sectorJobRoleid;
      var headers = {
        Authorization: $.cookie(Constants.User.authToken)
      };
      Api.get(getsectorJobRoleDetailUrl, headers, function (error, res) {
        $(".loading").addClass('hidden');
        if (res != undefined) {
          $("#replaceHTML").html(res);
          $('#training-partner-dialog').modal('show');
        } else if (res != undefined && res.status == false) {
          common.showMessage(res.error.message, true);
        } else if (error != undefined && error.status == false) {
          common.showMessage(error.error.message, true);
        }
      });
    }
  },
  /**
   * this function will be called when user click on delete button from grid and delete current row record
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  delete: function (currentRow) {
    common.deleteData(objModules.sectorJobRole, Constants.Api.removeSectorJobRoleMapping, currentRow.data().id, function (res) {
      if (res != undefined && res.status == true) {
        currentRow.remove().draw(false);
        common.showMessage(res.message, false);
      } else {
        common.showMessage(res.errorMsg, true);
      }
    });
  },
  /**
   * this function clear addRole form values,reset role grid data
   * @param  {[type]} isshowGridDiv [description]
   * @return {[type]}               [description]
   */
  clearValues: function (isshowGridDiv) {
    if (isshowGridDiv == true) {
      common.showHideDiv(true, objModules.sectorJobRole);
      sectorJobRole.sectorJobRoleTable.destroy();
      sectorJobRole.getData();
    }
    sectorJobRole.sectorJobRoleid = -1;
    common.clearValues($("#frmsectorJobRole"));
    sectorJobRole.saveDisabled = false;
    $(".has-error-text").remove();
  },
  /**
   * called when click on add icon to show add role form
   */
  add: function () {
    common.showHideDiv(false, objModules.sectorJobRole);
    sectorJobRole.clearValues(false);
    sectorJobRole.formMode = "add";
    $(".addeditCustom").text('Add');
    common.getState(function (res) {
      if (res != undefined && res.status == true) {
        $('#drpState').empty();
        sectorJobRole.bindSelect("#drpState", res.data, "stateid", "stateName");
        sectorJobRole.changeState();
      }
    });
  },
  /**
   * called when user wants to import csv, it renders the view for bulk upload
   */
  import: function () {
    $('#divImport' + objModules.sectorJobRole.displayName).show();
    $('#import' + objModules.sectorJobRole.displayName).hide();
    $('#add' + objModules.sectorJobRole.displayName).hide();
    $('#div' + objModules.sectorJobRole.displayName).hide();
    $(".customerrorsuccess").addClass('hidden');
    const file = document.querySelector('#fileupload');
    file.value = '';
  },
  /* save: function (event) {
    if (sectorJobRole.saveDisabled == false) {
      sectorJobRole.saveDisabled = true;
    }
  }, */
  /**
   * called when cancel add or edit role to cancel add/edit
   * @return {[type]} [description]
   */
  cancel: function () {
    sectorJobRole.clearValues(true);
  },
  /**
   * this function is used to save bulk upload csv data
   * @param  {[type]} event [description]
   * @return {[type]}       [description]
   */
  importCSV: function (event) {
    $(".loading").removeClass('hidden');
    var files = document.getElementById('fileupload').files[0];
    common.uploadMedia(files, "spreadSheet", 'jobRoles', function (res) {
      if (res != undefined && res.status == true) {
        $(".loading").addClass('hidden');
        
        if (res.content.data.errorrecords != 0 && res.content.data.errorrecords.error.length > 0) {
          if(res.content.data.successrecords > 0){
            common.showMessage('Total '+res.content.data.successrecords+' records are imported successfully', false);
          }
          var errorrecords = res.content.data.errorrecords.error;
          var p = "";
          for (var i = 0; i < errorrecords.length; i++) {
            p += '[' + errorrecords[i].errorString + ']';
            p += '<br/>';
          }
          $(".customErrorRecords").html(p);
          $(".customerrorsuccess").removeClass('hidden');
        } else {
          common.showMessage(res.content.data.message, false);
        }

        //window.location.href = '/sectorJobRole';
      } else if (res != undefined && res.status == false) {
        $(".loading").addClass('hidden');
        common.showMessage(res.errorMsg, true);
      } else if (error != undefined && error.status == false) {
        $(".loading").addClass('hidden');
        common.showMessage(error.error.message, true);
      }
    });
  },
  /**
   * this function fill values into select(dropdown) component
   * @param  {[type]} selectId     [description]
   * @param  {[type]} dataSet      [description]
   * @param  {[type]} valField     [description]
   * @param  {[type]} dispValField [description]
   * @return {[type]}              [description]
   */
  bindSelect: function (selectId, dataSet, valField, dispValField) {
    try {
      var selectOptions = "";
      for (var i = 0; i < dataSet.length; i++) {
        selectOptions += '<option value="' + dataSet[i][valField] + '">' + dataSet[i][dispValField] + '</option>'
      }
      $(selectId).append(selectOptions);
    } catch (e) {
      throw (e);
    }
  },
  /**
   * this function active or deactive user, when click on active/deactive button from grid
   * @param  {[type]} rowObj [description]
   * @return {[type]}        [description]
   */
  onActiveClick: function (rowObj) {
    $(".loading").removeClass('hidden');
    var data = rowObj.data();
    var status = "0";
    if (data.status == "0") {
      status = "1";
    }
    if (status == "1" && data.is_email_sent == 0) {
      var emailSendURL = Constants.Api.emailSend
      var headers = {
        Authorization: $.cookie(Constants.User.authToken)
      };
      data.tableName = 'tbl_sectorJobRoles';
      data.filedToUpdate = 'sectorJobRoleid';
      data.filedUpdateValue = data.sectorJobRoleid;
      data.emailType = 'tp_onboarding';
      data.contact_person_name = data.contact_person_name;
      Api.post(emailSendURL, headers, data, function (error, res) {
        $(".loading").addClass('hidden');
        if (res != undefined && res.status == true) {
          sectorJobRole.updateActiveStatus(data, status);
        }
      });
    } else {
      sectorJobRole.updateActiveStatus(data, status);
    }
  },
  /**
   * called when user clicks on active / inactive, triggers the api call 
   */
  updateActiveStatus: function (data, status) {
    common.updateActiveStatus(data.sectorJobRoleid, objModules.sectorJobRole.tableId, status, function (res) {
      if (res != null && res.status == true) {
        sectorJobRole.clearValues(true);
      }
    });
  },
  /**
   * calls when user selects one of the options from select action dropdown, i.e. select all and delete
   */
  mutipleaction: function (action) {
    var checkboxValues = [];
    $('input[name="row[]"]:checked').map(function () {
      checkboxValues.push($(this).val());
    });
    if (checkboxValues.length == 0) {
      common.showMessage('Please select at least one checkbox to do this action', true);
      return false;
    }
    var data = {};
    data.ids = checkboxValues;
    data.action = action;

    var headers = {
      Authorization: $.cookie(Constants.User.authToken)
    };

    Api.post(Constants.Api.sectorJobRoleUpdateMultipleRecords, headers, data, function (error, res) {
      $(".loading").addClass('hidden');
      sectorJobRole.saveDisabled = false;
      if (res != undefined && res.status == true) {
        common.showMessage(res.data.message, false);
        sectorJobRole.clearValues(true);
      } else if (res != undefined && res.status == false) {
        common.showMessage(res.error.message, true);
        //sectorJobRole.clearValues(false);
      } else if (error != undefined && error.status == false) {
        common.showMessage(error.error.message, true);
        //sectorJobRole.clearValues(false);
      }
    });
  }
}
