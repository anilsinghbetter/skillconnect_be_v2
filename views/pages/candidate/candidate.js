$(document).ready(function () {
  candidate.getData();
  $("#customCheckBox0").change(function () {
    $('.cb-element').prop('checked', this.checked);
  });
  $(".select-tags").select2({
    tags: false,
    theme: "bootstrap",
  });
});

$("#frmcandidate").validate({
  onkeyup: false,
  errorClass: 'has-error-text',
  validClass: 'valid',
  errorElement: 'span',
  rules: {
    "candidate_name": {
      required: true,
    },
    /* "aadhaar_number": {
      required: true,
    }, */
    "age": {
      required: true,
    },
    "phone": {
      required: true,
    },
    /* "dob": {
      required: true,
    }, */
    "gender": {
      required: true,
    },
    /* "willing_to_relocate": {
      required: true,
    }, */
    "address": {
      required: true,
    },
    "pincode": {
      required: true,
    },
    "training_type": {
      required: true,
    },
    "sdmsnsdc_enrollment_number": {
      required: true,
    },
    "training_status": {
      required: true,
    },
    /* "placement_status": {
      required: true,
    }, */
    /* "employment_type": {
      required: true,
    }, */
    "assessment_date": {
      required: true,
    },
    "sectorid": {
      required: true,
    },
    "jobroleid": {
      required: true,
    },
    "batch_id": {
      required: true,
    },
    "batch_start_date": {
      required: true,
    },
    "batch_end_date": {
      required: true,
    },
    "stateid": {
      required: true,
    },
    "cityid": {
      required: true,
    },
    "trainingcenterid": {
      required: true,
    },
    "pk_schemeID": {
      required: true,
    },
    "fk_ssdm": {
      required: true,
    }
  },
  messages: {
    "candidate_name": {
      required: 'Please enter Candidate name',
    },
    /* "aadhaar_number": {
      required: 'Please enter Aadhaar Number ',
    }, */
    "age": {
      required: 'Please enter age ',
    },
    "phone": {
      required: 'Please enter Phone Number',
    },
    /* "dob": {
      required: 'Please enter Date of Birth',
    }, */
    "gender": {
      required: 'Please select Gender',
    },
    /* "willing_to_relocate": {
      required: 'please select is candidate willing to relocate',
    }, */
    "address": {
      required: 'Please Enter Address',
    },
    "pincode": {
      required: 'Please Enter Pincode',
    },
    "training_type": {
      required: 'Please select Training Type',
    },
    "sdmsnsdc_enrollment_number": {
      required: 'Please enter SDMS/NSDC Enrollment Number',
    },
    "training_status": {
      required: 'Please select Training Status',
    },
    /* "placement_status": {
      required: 'Plesae select Placement Status',
    }, */
    /* "employment_type": {
      required: 'Please select Employment Type',
    }, */
    "assessment_date": {
      required: 'Please select Assessment Date',
    },
    "sectorid": {
      required: 'Please select Sector',
    },
    "jobroleid": {
      required: 'Please select JobRole',
    },
    "batch_id": {
      required: 'Please enter Batch-ID',
    },
    "batch_start_date": {
      required: 'Please enter Batch Start Date',
    },
    "batch_end_date": {
      required: 'Please enter Batch End Date',
    },
    "stateid": {
      required: 'Please select State',
    },
    "cityid": {
      required: 'Please select City',
    },
    "trainingcenterid": {
      required: 'Please select Training Center',
    },
    "pk_schemeID": {
      required: 'Please select Scheme Name',
    },
    "fk_ssdm": {
      required: 'Please select State Name for Scheme',
    }
  },
  highlight: function (element) {
    $(element).closest('div').addClass("has-error");
  },
  unhighlight: function (element) {
    $(element).closest('div').removeClass("has-error");
  },
  errorPlacement: function (error, element) {
    $(element).closest('div').append(error);
  },
  submitHandler: function (form) {
    $(".has-error-text").hide();
    $(".has-error-text").closest('div').removeClass("has-error");

    $(".loading").removeClass('hidden');
    var data = {};
    data = common.getFormValues($("#frmcandidate"));
    if (data.fk_trainingpartnerid != undefined) {
      data.trainingpartnerid = data.fk_trainingpartnerid;
      delete data.fk_trainingpartnerid;
    }
    if (data.trainingcenterid != undefined) {
      data.training_center = data.trainingcenterid;
      delete data.trainingcenterid;
    }
    if (data.cityid != undefined) {
      data.districtname = data.cityid;
      delete data.cityid;
    }
    if (data.stateid != undefined) {
      data.statename = data.stateid;
      delete data.stateid;
    }
    if (data.sectorid != undefined) {
      data.sectorname = data.sectorid;
      delete data.sectorid;
    }
    if (data.jobroleid != undefined) {
      data.jobrole = data.jobroleid;
      delete data.jobroleid;
    }
    if (data.pk_schemeID != undefined) {
      data.schemename = data.pk_schemeID;
      delete data.pk_schemeID;
    }
    if (data.fk_ssdm != undefined && data.fk_ssdm != "") {
      data.state_scheme_name = data.fk_ssdm;
      delete data.fk_ssdm;
    }
    if (data.candidate_name != undefined) {
      data.name = data.candidate_name;
      delete data.candidate_name;
    }
    if (data.aadhaar_number != undefined) {
      data.aadhar_card_no = data.aadhaar_number;
      delete data.aadhaar_number;
    }
    if (data.alternateidtype != undefined) {
      data.alternate_id_type = data.alternateidtype;
      delete data.alternateidtype;
    }
    if (data.alternate_id_number != undefined) {
      data.altername_id_no = data.alternate_id_number;
      delete data.alternate_id_number;
    }
    if (data.gender != undefined) {
      data.sex = data.gender;
      delete data.gender;
    }
    if (data.experience_in_years != undefined) {
      data.exp_in_years = data.experience_in_years;
      delete data.experience_in_years;
    }
    if (data.sdmsnsdc_enrollment_number != undefined) {
      data.nsdc_enrollment_no = data.sdmsnsdc_enrollment_number;
      delete data.sdmsnsdc_enrollment_number;
    }
    if (data.attendence_percentage != undefined) {
      data.attendance_percent = data.attendence_percentage;
      delete data.attendence_percentage;
    }
    if (data.willing_to_relocate != undefined) {
      data.is_willing_to_relocate = data.willing_to_relocate;
      delete data.willing_to_relocate;
    }
    data.candidateid = candidate.candidateid;

    var headers = {
      Authorization: $.cookie(Constants.User.authToken)
    };
    Api.post(Constants.Api.addUpdateCandidate + 'admin', headers, data, function (error, res) {
      $(".loading").addClass('hidden');
      candidate.saveDisabled = false;
      if (res != undefined && res.status == true) {
        common.showMessage(res.data.message, false);
        candidate.clearValues(true);
      } else if (res != undefined && res.status == false) {
        common.showMessage(res.error.message, true);
      } else if (error != undefined && error.status == false) {
        common.showMessage(error.error.message, true);
      }
    });
  }
});


candidate = {
  candidates: [],
  user_id: -1,
  candidateTable: {},
  formMode: "",
  saveDisabled: false,
  tableid : "gridcandidate",
  
  /**
   * this function get user data and show into grid
   * @return {[type]} [description]
   */
  getData: function () {
    //$(".loading").removeClass('hidden');
    //var getcandidateUrl = Constants.Api.getCandidates + 'admin'
    var getcandidateUrl = APIBASEURL+""+Constants.Api.getCandidatesForAdmin
    var headers = {
      "api-key": Constants.Api.apiKey,
      "UDID": common.getUDID(),
      "device-type": common.getDeviceType(),
      "Authorization": $.cookie(Constants.User.authToken)
    };
    candidate.candidateTable = candidate.bindCandidateDatatable(headers,getcandidateUrl, candidate.tableid);
  },
  /**
   * this function will be called when user click on edit from user grid and show currentRow into edit mode
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  edit: function (currentRow) {
    candidate.formMode = "edit";
    //event.preventDefault();
    var currentRowData = $('#' + candidate.tableid).DataTable().row(currentRow).data();
    $(".addeditCustom").text('Edit');

    var candidateDetailFullUrl = Constants.Api.getCandidateDetailFull + currentRowData.candidateid + '/web';
    Api.post(candidateDetailFullUrl,undefined ,{} , function(error,res){
      if(res.status === true){
        currentRowData = res.data[0];
      }
      if (currentRowData != undefined && currentRowData.candidateid > 0) {
        if (currentRowData.schemeid != undefined) {
          currentRowData.pk_schemeID = currentRowData.schemeid;
        }
        if (currentRowData.trainingcenter_id != undefined) {
          currentRowData.trainingcenterid = currentRowData.trainingcenter_id;
        }
        if (currentRowData.stateministryid != undefined) {
          currentRowData.fk_ssdm = currentRowData.stateministryid;
        }
        // console.log(currentRowData,"ROW DATA")
        common.getSector(function (res) {
          if (res != undefined && res.status == true) {
            $('#drpSector').empty();
            candidate.bindSelect("#drpSector", res.data, "sectorid", "sectorName");

            common.getJobrole(currentRowData.sectorid, function (res) {
              if (res != undefined && res.status == true) {
                candidate.bindSelect("#drpJobrole", res.data, "jobroleid", "jobroleName");
                common.getState(function (res) {
                  if (res != undefined && res.status == true) {
                    $('#drpState').empty();
                    //console.log("response data in STATE", res.data);
                    candidate.bindSelect("#drpState", res.data, "stateid", "stateName");

                    common.getCity(currentRowData.stateid, function (res) {
                      if (res != undefined && res.status == true) {
                        $(".loading").addClass('hidden');
                        $('#drpCity').empty();
                        candidate.bindSelect("#drpCity", res.data, "cityid", "cityName");
                        common.getScheme(function (res) {
                          if (res != undefined && res.status == true) {
                            $(".loading").addClass('hidden');
                            $('#drpScheme').empty();
                            candidate.bindSelect("#drpScheme", res.data, "pk_schemeID", "schemeType");

                            common.getTrainingCenter(currentRowData.fk_trainingpartnerid, function (res) {
                              if (res != undefined && res.status == true) {
                                $(".loading").addClass('hidden');
                                $('#drpCenter').empty();
                                candidate.bindSelect("#drpCenter", res.data, "trainingcenterid", "trainingcenter_name");
                                //console.log(currentRowData.pk_schemeID);

                                candidate.candidateid = currentRowData.candidateid;
                                common.showHideDiv(false, objModules.candidate);
                                common.fillFormValues($("#frmcandidate"), currentRowData);

                                candidate.changeScheme(currentRowData);
                              }
                            });

                          }
                        });
                      }
                    });
                  } else {
                    common.showMessage(res.errorMsg, true);
                  }
                });
              }
            });
          }
        });
      }

    });
  },
  /**
   * this function will be called when user click on view from user grid and show currentRows details
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  view: function (currentRow) {
    $(".loading").removeClass('hidden');
    //event.preventDefault();
    var currentRowData = $('#' + candidate.tableid).DataTable().row(currentRow).data();
    // console.log(currentRowData,"CURRENT ROW");
      // console.log(error,"ERROR",res,"RESPONSE");
      if (currentRowData != undefined && currentRowData.candidateid > 0) {
        var getCandidateDetailUrl = Constants.Api.getCandidateDetail + currentRowData.candidateid + '/admin';
        var headers = {
          Authorization: $.cookie(Constants.User.authToken)
        };
        Api.post(getCandidateDetailUrl, headers, { data: currentRowData }, function (error, res) {
          $(".loading").addClass('hidden');
          if (res != undefined) {
            $("#replaceHTML").html(res);
            $('#candidate-dialog').modal('show');
          } else if (res != undefined && res.status == false) {
            common.showMessage(res.error.message, true);
          } else if (error != undefined && error.status == false) {
            common.showMessage(error.error.message, true);
          }
        });
      }
  },
  /**
   * this function will be called when user click on delete button from grid and delete current row record
   * @param  {[type]} currentRow [description]
   * @return {[type]}            [description]
   */
  delete: function (currentRow) {
    var data = $('#' + candidate.tableid).DataTable().row(currentRow).data();
    common.deleteRecordsApi(data.candidateid, objModules.candidate.tableId, 0, function (res) {
      if (res != undefined && res.status == true) {
        $('#' + candidate.tableid).DataTable().row(currentRow).remove().draw(false);
        //common.showMessage(res.message, false);
      } else {
        //common.showMessage(res.errorMsg, true);
      }
    });
  },
  /**
   * this function clear addRole form values,reset role grid data
   * @param  {[type]} isshowGridDiv [description]
   * @return {[type]}               [description]
   */
  clearValues: function (isshowGridDiv) {
    if (isshowGridDiv == true) {
      common.showHideDiv(true, objModules.candidate);
      candidate.candidateTable.destroy();
      candidate.getData();
    }
    candidate.user_id = -1;
    common.clearValues($("#frmcandidate"));
    candidate.saveDisabled = false;
  },
  /**
   * called when click on add icon to show add role form
   */
  add: function () {
    common.showHideDiv(false, objModules.candidate);
    candidate.clearValues(false);
    candidate.formMode = "add";
    // $("#txtPassword").attr("validation","required");
  },
  /**
   * called when cancel add or edit role to cancel add/edit
   * @return {[type]} [description]
   */
  cancel: function () {
    window.location.reload(true);
  },
  /**
   * this function is used to save or update role data
   * @param  {[type]} event [description]
   * @return {[type]}       [description]
   */
  save: function (event) {
    if (candidate.saveDisabled == false) {
      candidate.saveDisabled = true;
      var validation = formValidation('frmcandidate');
      if (validation == true) {
        var data = {};
        data = common.getFormValues($("#frmcandidate"));
        data.user_id = candidate.user_id;
        var headers = {
          Authorization: $.cookie(Constants.User.authToken)
        };
        Api.post(Constants.Api.addUpdatecandidate, headers, data, function (error, res) {
          candidate.saveDisabled = false;
          if (res != undefined && res.status == true) {
            common.showMessage(res.content.message, false);
            candidate.clearValues(true);
          } else if (res != undefined && res.status == false) {
            common.showMessage(res.error.message, true);
            candidate.clearValues(false);
          } else if (error != undefined && error.status == false) {
            common.showMessage(error.error.message, true);
            candidate.clearValues(false);
          }
        });
      } else {
        candidate.saveDisabled = false;
      }
    }

  },
  /**
   * this function will be called when user changes the state from dropdown to render the city dropdown
   */
  changeState: function () {
    $(".loading").removeClass('hidden');
    common.getCity($("#drpState").val(), function (res) {
      if (res != undefined && res.status == true) {
        $(".loading").addClass('hidden');
        $('#drpCity').empty();
        candidate.bindSelect("#drpCity", res.data, "cityid", "cityName");
      }
    });
  },
  /**
   * this function will be called when user changes the sector from dropdown to render the jobrole dropdown
   */
  changeSector: function () {
    $(".loading").removeClass('hidden');
    common.getJobrole($("#drpSector").val(), function (res) {
      if (res != undefined && res.status == true) {
        $(".loading").addClass('hidden');
        $('#drpJobrole').empty();
        candidate.bindSelect("#drpJobrole", res.data, "jobroleid", "jobroleName");
      }
    });
  },/**
  * this function will be called when user changes the Scheme from dropdown to render the State/Central Ministry dropdown
  */
  changeScheme: function (currentRowData) {
    $(".loading").removeClass('hidden');
    //console.log('value of drope scheme',$("#drpScheme").val());
    if ($("#drpScheme").val() == 5) {
      $('#ministry').addClass('hidden');
      $(".loading").addClass('hidden');
      $('#drpMinistry').removeClass('hidden');
      common.getState(function (res) {
        if (res != undefined && res.status == true) {
          $('#drpMinistry').empty();
          // res.data.stateid = $('#drpState').val();

          candidate.bindSelect("#drpMinistry", res.data, "stateid", "stateName");
          if (currentRowData != undefined && currentRowData.fk_ssdm != undefined) {
            $("select[name='fk_ssdm']").val(currentRowData.fk_ssdm);
          }
        }
      });
    } else {
      common.getMinistry($("#drpScheme").val(), function (res) {
        if (res != undefined && res.status == true) {
          $(".loading").addClass('hidden');
          $("#drpMinistry").addClass('hidden');
          $('#ministry').removeClass('hidden');
          $('#ministry').empty();
          $('#ministry').text(res.data[0].centralministryname);

        }
      });
    }

  },
  /**
   * this function fill values into select(dropdown) component
   * @param  {[type]} selectId     [description]
   * @param  {[type]} dataSet      [description]
   * @param  {[type]} valField     [description]
   * @param  {[type]} dispValField [description]
   * @return {[type]}              [description]
   */
  bindSelect: function (selectId, dataSet, valField, dispValField) {
    try {
      var selectOptions = "";
      for (var i = 0; i < dataSet.length; i++) {
        selectOptions += '<option value="' + dataSet[i][valField] + '">' + dataSet[i][dispValField] + '</option>'
      }
      $(selectId).append(selectOptions);
    } catch (e) {
      throw (e);
    }
  },
  /**
   * this function active or deactive user, when click on active/deactive button from grid
   * @param  {[type]} rowObj [description]
   * @return {[type]}        [description]
   */
  onActiveClick: function (rowObj) {
    $(".loading").removeClass('hidden');
    var data = $('#'+candidate.tableid).DataTable().row(rowObj).data();
    var status = "0";
    if (data.status == "0") {
      status = "1";
    }
    common.updateActiveStatus(data.candidateid, objModules.candidate.tableId, status, function (res) {
      $(".loading").addClass('hidden');
      if (res != null && res.status == true) {
        candidate.clearValues(true);
      }
      
    });
  },
  /**
   * calls when user selects one of the options from select action dropdown, i.e. select all and delete
   */
  mutipleaction: function (action) {
    var checkboxValues = [];
    $('input[name="row[]"]:checked').map(function () {
      checkboxValues.push($(this).val());
    });
    if (checkboxValues.length == 0) {
      common.showMessage('Please select at least one checkbox to do this action', true);
      return false;
    }
    var confirmation = 1;
    if (action == 'delete') {
      if (!confirm("Are you sure you want to delete multiple records ?")) {
        confirmation = 0;
      }
    }
    if (confirmation == 1) {
      var data = {};
      data.ids = checkboxValues;
      data.action = action;

      var headers = {
        Authorization: $.cookie(Constants.User.authToken)
      };

      Api.post(Constants.Api.candidateUpdateMultipleRecords, headers, data, function (error, res) {
        $(".loading").addClass('hidden');
        candidate.saveDisabled = false;
        if (res != undefined && res.status == true) {
          common.showMessage(res.data.message, false);
          candidate.clearValues(true);
        } else if (res != undefined && res.status == false) {
          common.showMessage(res.error.message, true);

        } else if (error != undefined && error.status == false) {
          common.showMessage(error.error.message, true);
        }
        $("#customCheckBox0").prop("checked",false);
      });
    }
  },
  /**
   * Bind datatable of candidate
   */
  bindCandidateDatatable: function (headers, getCandiadteUrl, datatableID) {
    var gridCandiadteObj = $('#' + datatableID).DataTable({
      serverSide: true,
      processing: true,
      bFilter: true,
      bLengthChange: true,
      stateSave: true,
      order: [0, 'desc'],
      // "searching": true,
      "bSearchable": true,
      "ajax": {
        "url": getCandiadteUrl,
        "type": "POST",
        "headers": headers,
        'beforeSend': function (request) {
         
        },
        "data": function (d) {
          d.status = $('#customCheckBox0').is(":checked");
          d.candidate_name = d.search.value;
          d.sdmsnsdc_enrollment_number = d.search.value;
          d.phone = d.search.value;
          d.email_address = d.search.value;
          d.batch_id = d.search.value;
        }
      },
      responsive: {
        details: {
          type: 'column',
          target: 'tr'
        }
      },
      "columns": [{
        name: "pk_candidateid",
        "data": "candidateid",
        "width": "5%"
      }, {
        name: "candidate_name",
        "data": "candidate_name",
        "width": "10%"
      }, {
        name: "sdmsnsdc_enrollment_number",
        "data": "sdmsnsdc_enrollment_number",
        "width": "20%"
      },
      {
        name: "training_status",
        "data": "training_status",
        "width": "15%"
      },
      {
        name: "batch_id",
        "data": "batch_id",
        "width": "10%"
      },
      {
        name: "status",
        "data": "status",
        "width": "10%"
      },
      {
        "data": "Actions",
        "width": "10%"
      }
      ],
      columnDefs: [{
        orderable: false,
        //  className: 'select-checkbox',
        targets: 0,
        'render': function (data, type, full, meta) {
          return buttons = '<div class="custom-control custom-checkbox pmd-checkbox"><input class="custom-control-input cb-element" type="checkbox" name="row[]" value="' + full.candidateid + '" id="customCheckBox' + full.candidateid + '"><label class="custom-control-label" for="customCheckBox' + full.candidateid + '"></label></div>';
        }
      }, {
        orderable: true,
        targets: 1
      }, {
        orderable: true,
        targets: 2
      },
      {
       orderable: true,
        targets: 3
      },
      {
       orderable: true,
        targets: 4
      },
      {
       orderable: false,
        targets: 5,
        'render': function (data, type, full, meta) {
          if (data == 1) var buttonHTML = '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary btn-success btnApprove" title="Status" type="button" onclick="candidate.onActiveClick(' + meta.row + ');" value="' + data + '"><i class="material-icons pmd-sm">check</i></button>';
          if (data == 0) var buttonHTML = '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary btn-danger btnReject" title="Status" type="button" onclick="candidate.onActiveClick(' + meta.row + ');" value="' + data + '"><i class="material-icons pmd-sm">clear</i></button>';
          return buttonHTML;
        }
      },
      {
       orderable: false,
        targets: 6,
        'render': function (data, type, full, meta) {
          var buttonHTML = '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary btnEdit" onclick="candidate.edit(' + meta.row + ');" type="button" title="Edit"><i class="material-icons pmd-sm">edit</i></button> &nbsp;';
          buttonHTML += '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary btnView" type="button" onclick="candidate.view(' + meta.row + ');" value="' + data + '" title="Detail"><i class="material-icons pmd-sm">remove_red_eye</i></button>';
          buttonHTML += '<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-danger btnDelete"  type="button" onclick="candidate.delete(' + meta.row + ');" value="' + data + '" title="Delete"><i class="material-icons pmd-sm">delete</i></button>';
          return buttonHTML;
        }
      }
      ]
      ,
      //For checkall..
      select: {
        style: 'multi',
        selector: 'td:first-child'
      },
      "language": {
        "info": " _START_ - _END_ of _TOTAL_ ",
        "sLengthMenu": "<span class='custom-select-title hidden'>Rows per page:</span> <span class='pmd-custom-select hidden'> _MENU_ </span>",
        processing: "<span class='loading'></span>",
        "sSearch": "",
        "sSearchPlaceholder": "Search",
        "paginate": {
          "sNext": " ",
          "sPrevious": " "
        },
      },
      dom:
        "<'card-header d-flex flex-row'<'data-table-title-responsive'><'search-paper pmd-textfield ml-auto'f>>" +
        "<'row'<'col-sm-12 datatableCustom'tr>>" +
        "<'card-footer' <'pmd-datatable-pagination' l i p>>",
    });

    // Apply the filter
    $('.custom-select-info').hide();

    //$('#' + datatableID + '_wrapper').find("div.data-table-title-responsive").html();

    $('#' + datatableID + ' tbody').unbind("click");
    $(".custom-select-action").html('<button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary" type="button"><i class="material-icons pmd-sm">delete</i></button><button class="btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect btn-primary" type="button"><i class="material-icons pmd-sm">more_vert</i></button>');

    $(".cb-element").change(function () {
      if ($(".cb-element").length == $(".cb-element:checked").length)
        $("#customCheckBox0").prop('checked', true);
      else
        $("#customCheckBox0").prop('checked', false);
    });

    $('#' + datatableID).on('page.dt', function () {
      //var info = tableObj.page.info();
      $("#customCheckBox0").prop('checked', false);
    });

    var btnClear = $('<a class="btnClearDataTableFilter cursor-pointer hidden" style="position: absolute;right: 20px;z-index: 9999;top: 25px;"><i class="material-icons md-dark pmd-xs">cancel</i></a>');
    btnClear.appendTo($('#' + datatableID).parents('.dataTables_wrapper').find('.dataTables_filter'));

    $('#' + datatableID + '_wrapper .btnClearDataTableFilter').click(function () {
      $('#' + datatableID).dataTable().fnFilter('');
      $('.btnClearDataTableFilter').addClass('hidden');
    });

    if (gridCandiadteObj.data().any()) {
      $('#' + datatableID + "_filter").hide();
      $('#' + datatableID + "_info").hide();
      $('#' + datatableID + "_paginate").hide();
      $('.customSelectAll').hide();
      $('#selectAction').hide();
    } else {
      $('#' + datatableID + "_filter").show();
      $('#' + datatableID + "_info").show();
      $('#' + datatableID + "_paginate").show();
      $('.customSelectAll').show();
      $('#selectAction').show();
    }
    function clickHandler(btnObj) {
      $('#' + datatableID + ' tbody').on('click', btnObj.buttonObj.targetClass, function () {
        btnObj.onClickEvent(tableObj.row($(this).parents('tr')));
      });
    }
    $("div.dataTables_filter input").keyup(function (e) {
      if ($(this).val() != '') {
        $('.btnClearDataTableFilter').removeClass('hidden');
      } else {
        $('.btnClearDataTableFilter').addClass('hidden');
      }
    });

    return gridCandiadteObj;
  },
}
