let otherService = require('./server/api/v1/other/other.service');

//console.log('In Send Email cron file');

let trainingpartner = trainingpartnerCron();
let industrypartner = industrypartnerCron();

async function trainingpartnerCron() {
    //console.log('trainingpartnerCron');
    let res = await otherService.emailCronService('trainingpartner');
    console.log(res);
}

async function industrypartnerCron() {
    //console.log('industrypartnerCron');
    let res = await otherService.emailCronService('industrypartner');
    //console.log(res);
}
